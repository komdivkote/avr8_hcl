/*-
 *	Copyright (c) 2014-2016	Alexander Morozov. www.nixd.org
 *	All rights reserved.
 *
 * LICENSE: This source file is subject to the New BSD license that is
 * available through the world-wide-web at the following URI:
 * http://www.opensource.org/licenses/bsd-license.php or in the root folder
 * of the project in file LICENSE.TXT.
 */

#include <stdint.h>
#include <stdbool.h>
#include <avr/pgmspace.h>
#include "../extlib/e_enum.h"
#include "./mega2350.h"

VERIFY_ENUM_FUNCTION_VAL_P(io_port, FOREACH_IO_ADDRESSES);

//------------------------------------------------------A-----B-----C------D-----E-----F----G-----H-------J------K------L---
const uint16_t port_addresses[MAX_IO_PORTS] PROGMEM = {0X02, 0x05, 0x08, 0x0B, 0x0E, 0x11, 0x14, 0x102, 0x105, 0x108, 0x10B};
const uint16_t ddr_addresses[MAX_IO_PORTS] PROGMEM =  {0X01, 0x04, 0x07, 0x0A, 0x0D, 0x10, 0x13, 0x101, 0x104, 0x107, 0x10A};
const uint16_t pin_addresses[MAX_IO_PORTS] PROGMEM =  {0X00, 0X03, 0x06, 0x09, 0x0C, 0x0F, 0x12, 0x100, 0x103, 0x106, 0x109};

const struct hw_ports spi_pins[MAX_SPI_PORTS] PROGMEM =
{
		{
			PORTS_B, (uint8_t) 0b0001111
		}
};

const struct hw_ports adc_pins[ADC_CHANNELS] PROGMEM =
{		//bank1
		{
			PORTS_F, (uint8_t) 0b00000001
		},
		{
			PORTS_F, (uint8_t) 0b00000010
		},
		{
			PORTS_F, (uint8_t) 0b00000100
		},
		{
			PORTS_F, (uint8_t) 0b00001000
		},
		{
			PORTS_F, (uint8_t) 0b00010000
		},
		{
			PORTS_F, (uint8_t) 0b00100000
		},
		{
			PORTS_F, (uint8_t) 0b01000000
		},
		{
			PORTS_F, (uint8_t) 0b10000000
		},
		//bank2
		{
			PORTS_K, (uint8_t) 0b00000001
		},
		{
			PORTS_K, (uint8_t) 0b00000010
		},
		{
			PORTS_K, (uint8_t) 0b00000100
		},
		{
			PORTS_K, (uint8_t) 0b00001000
		},
		{
			PORTS_K, (uint8_t) 0b00010000
		},
		{
			PORTS_K, (uint8_t) 0b00100000
		},
		{
			PORTS_K, (uint8_t) 0b01000000
		},
		{
			PORTS_K, (uint8_t) 0b10000000
		},
};

const struct hw_ports usart_pins[MAX_USART_PORTS] PROGMEM =
{
	{
		PORTS_E, (uint8_t) 0b00000011
	},
	{
		PORTS_D, (uint8_t) 0b00001100
	},
	{
		PORTS_H, (uint8_t) 0b00000011
	},
	{
		PORTS_J, (uint8_t) 0b00000011
	}
};


const struct usart_hwaddr s_usart_addr[MAX_USART_PORTS] PROGMEM =
{
	{
		0xC0, 0xC1, 0xC2, 0xC4, 0xC6, 7, 5
	},
	{
		0xC8, 0xC9, 0xCA, 0xCC, 0xCE, 7, 5
	},
	{
		0xD0, 0xD1, 0xD2, 0xD4, 0xD6, 7, 5
	},
	{
		0x130, 0x131, 0x132, 0x134, 0x136, 7, 5
	}
};

const struct spi_hwaddr s_spi_addr[MAX_SPI_PORTS] PROGMEM =
{
    {
        0x2C, 0x2D, 0X2E
    }
};

const struct spi_pins_nums s_spi_pins[MAX_SPI_PORTS] PROGMEM =
{
    {
        1, 2, 3, 0
    }
};



//TCCR0A  _SFR_IO8
//TCCR0B  _SFR_IO8
 //TCCR1A  _SFR_MEM8
//TCNT - _SFR_MEM8
//OCR1A   _SFR_MEM16
//TIMSK1  _SFR_MEM8
//TIFR1   _SFR_IO8
//ICR1    _SFR_MEM16
const struct timer_hwaddr s_timer_addr[MAX_TIMERS] PROGMEM =
{
    {
        TIMER_HW_0, TIMER_BIT_WIDTH_8, 0x24, 0x25, 0x00, 0x26, 0x27, 0x28, 0x00, 0x6E, 0x15, 0x00, (uint8_t) 2//, &pwm_pins_0[0]
    },
    {
        TIMER_HW_1, TIMER_BIT_WIDTH_16, 0x80, 0x81, 0x82, 0x84, 0x88, 0x8A, 0x00, 0x6F, 0x16, 0x86, (uint8_t) 3//, pwm_pins_1
    },
    {
        TIMER_HW_2, TIMER_BIT_WIDTH_8, 0xB0, 0xB1, 0x00, 0xB2, 0xB3, 0xB4, 0x00, 0x70, 0x17, 0x00, (uint8_t) 2//, pwm_pins_2
    },
    {
        TIMER_HW_3, TIMER_BIT_WIDTH_16, 0x90, 0x91, 0x91, 0x94, 0x98, 0x9A, 0x9C, 0x71, 0x18, 0x96, (uint8_t) 3//, &pwm_pins_3[0]
    },
    {
        TIMER_HW_4, TIMER_BIT_WIDTH_16, 0xA0, 0xA1, 0xA2, 0xA4, 0xA8, 0xAA, 0xAC, 0x72, 0x19, 0xA6, (uint8_t) 3//, pwm_pins_4
    },
    {
        TIMER_HW_5, TIMER_BIT_WIDTH_16, 0x120, 0x121, 0x122, 0x124, 0x128, 0x12A, 0x12C, 0x73, 0x1A, 0x126, (uint8_t) 3//, pwm_pins_5
    }
};

const struct hw_ports pwm_pins[][3] PROGMEM =
{
    {
        {
			PORTS_B, (uint8_t) 0b10000000
		},
		{
		    PORTS_G, (uint8_t) 0b00100000
		},
		{
		    0, 0
		}
    },
    {
        {
			PORTS_B, (uint8_t) 0b00100000
		},
		{
		    PORTS_B, (uint8_t) 0b01000000
		},
		{
		    PORTS_B, (uint8_t) 0b10000000
		}
    },
    {
        {
			PORTS_B, (uint8_t) 0b00010000
		},
		{
		    PORTS_H, (uint8_t) 0b01000000
		},
		{
		    0, 0
		}
    },
    {
        {
			PORTS_E, (uint8_t) 0b00001000
		},
		{
		    PORTS_E, (uint8_t) 0b00010000
		},
		{
		    PORTS_E, (uint8_t) 0b00100000
		}
    },
    {
        {
			PORTS_H, (uint8_t) 0b00001000
		},
		{
		    PORTS_H, (uint8_t) 0b00010000
		},
		{
		    PORTS_H, (uint8_t) 0b00100000
		}
    },
    {
        {
			PORTS_L, (uint8_t) 0b00001000
		},
		{
		    PORTS_L, (uint8_t) 0b00010000
		},
		{
		    PORTS_L, (uint8_t) 0b00100000
		}
    }

};

