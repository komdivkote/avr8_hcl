#include "../../build_defs.h"
#include <avr/io.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include "../../hal.h"
#include "./hal_net_stack.h"
#include "./hal_net_stack_func.h"

#include "../../../os/kernel/kernel_process.h"
#include "../../../os/kernel/kernel_memory.h"
/*
 * TCP (ver. 3.0)
 * lots of indian bydlocode here
 *
 * History:
 *	1.0 first attempt
 *	2.0 second attempt, first suitable working variant
 *	2.1 added normal seq/ack management
 *	3.0 added rexmit feature
 */

#if WITH_TCP == YES


// send TCP packet
// must be set manually:
//	- tcp.flags
int32_t tcp_xmit(struct knetdevice * knd,
                 struct knetstate * state,
                 eth_frame_t *frame,
                 uint16_t len)
{
	struct tcp_state * tcps = (struct tcp_state *) state->sock_opts;
	ASSERT_NULL_E(tcps, SYSTEM_BAD_ACCESS, error);

	uint16_t status = 1;
	uint16_t temp, plen = len;

	ip_packet_t *ip = (void*)(frame->data);
	tcp_packet_t *tcp = (void*)(ip->data);

	if(tcps->tcp_send_mode == TCP_SENDING_SEND)
	{
		// set packet fields
		ip->to_addr = state->raddr;
		ip->from_addr = state->laddr;
		ip->protocol = IP_PROTOCOL_TCP;
		tcp->to_port = state->rport;
		tcp->from_port = state->lport;
	}

	if(tcps->tcp_send_mode == TCP_SENDING_REPLY)
	{
		// exchange src/dst ports
		temp = tcp->from_port;
		tcp->from_port = tcp->to_port;
		tcp->to_port = temp;
	}

	if(tcps->tcp_send_mode != TCP_SENDING_RESEND)
	{
		// fill packet header ("static" fields)
		tcp->window = htons(TCP_WINDOW_SIZE);
		tcp->urgent_ptr = 0;
	}

	if(tcp->flags & TCP_FLAG_SYN)
	{
		// add MSS option (max. segment size)
		tcp->data_offset = (sizeof(tcp_packet_t) + 4) << 2;
		tcp->data[0] = 2;//option: MSS
		tcp->data[1] = 4;//option len
		tcp->data[2] = TCP_SYN_MSS>>8;
		tcp->data[3] = TCP_SYN_MSS&0xff;
		plen = 4;
	}
	else
	{
		tcp->data_offset = sizeof(tcp_packet_t) << 2;
	}

	// set stream pointers
	tcp->seq_num = htonl(tcps->seq_num);
	tcp->ack_num = htonl(tcps->ack_num);

	// set checksum
	plen += sizeof(tcp_packet_t);
	tcp->cksum = 0;
	tcp->cksum = ip_cksum(plen + IP_PROTOCOL_TCP, (uint8_t*)tcp - 8, plen + 8);

	// send packet
	switch(tcps->tcp_send_mode)
	{
		case TCP_SENDING_SEND:
			status = ip_send(knd, frame, plen);
			tcps->tcp_send_mode = TCP_SENDING_RESEND;
		break;
		case TCP_SENDING_REPLY:
			ip_reply(knd, frame, plen);
			tcps->tcp_send_mode = TCP_SENDING_RESEND;
		break;
		case TCP_SENDING_RESEND:
			ip_resend(knd, frame, plen);
		break;
	}

	// advance sequence number
	tcps->seq_num += len;
	if( (tcp->flags & TCP_FLAG_SYN) || (tcp->flags & TCP_FLAG_FIN) )
	{
		tcps->seq_num++;
	}

	// set "ACK sent" flag
	if( (tcp->flags & TCP_FLAG_ACK) && (status) )
	{
		tcps->tcp_ack_sent = 1;
	}

	return status;

error:
	return 1;
}

// sending SYN to peer
// return: 0xff - error, other value - connection id (not established)
int8_t tcp_open(struct knetdevice * knd, struct knetlisten * knetl, struct knetstate * state)
{
	eth_frame_t *frame = (void*)knd->tx_buf;
	ip_packet_t *ip = (void*)(frame->data);
	tcp_packet_t *tcp = (void*)(ip->data);
	uint8_t id;
	uint32_t seq_num;

	struct tcp_state * tcps = (struct tcp_state *) kernel_malloc(sizeof(struct tcp_state));
	ASSERT_NULL_E(tcps, SYSTEM_MALLOC_FAILED, error);
	state->sock_opts = (void *) tcps;
	// free connection slot created

	// add new connection
	tcps->seq_num = kernel_get_milis() + (kernel_get_milis() << 16);

	tcps->status = TCP_SYN_SENT;
	tcps->event_time = kernel_get_milis();
	tcps->seq_num = seq_num;
	tcps->ack_num = 0;
	/*tcps->remote_addr = (*(uint32_t *)sock->raddr);
	tcps->remote_port = sock->rport;
	tcps->local_port = sock->lport;*/

#if WITH_TCP_REXMIT == YES
	tcps->is_closing = 0;
	tcps->rexmit_count = 0;
	tcps->seq_num_saved = seq_num;
#endif

	// send packet
	tcps->tcp_send_mode = TCP_SENDING_SEND;
	tcp->flags = TCP_FLAG_SYN;
	if(tcp_xmit(knd, state, frame, 0))
	{
		return id;
	}

	//open failed

	tcps->status = TCP_CLOSED;

error:
	return 1;
}

// send TCP data
// dont use somewhere except tcp_write callback!
int32_t
tcp_send(struct knetdevice * knd, struct knetstate * sock,
         eth_frame_t *frame, uint16_t len, uint8_t options)
{
	ip_packet_t *ip = (void*)(frame->data);
	tcp_packet_t *tcp = (void*)(ip->data);
	struct tcp_state *st = (struct tcp_state *) sock->sock_opts;
	uint8_t flags = TCP_FLAG_ACK;

	// check if connection established
	if(st->status != TCP_ESTABLISHED)
	{
		return 0;
	}

	// send PSH/ACK
	if(options & TCP_OPTION_PUSH)
	{
		flags |= TCP_FLAG_PSH;
	}

	// send FIN/ACK
	if(options & TCP_OPTION_CLOSE)
	{
		flags |= TCP_FLAG_FIN;
		st->status = TCP_FIN_WAIT;
	}

	if (options & TCP_OPTION_ABORT)
    {
        flags |= TCP_FLAG_RST;
        st->status = TCP_CLOSED;
    }

	// send packet
	tcp->flags = flags;

	return tcp_xmit(knd, sock, frame, len);
}


// processing tcp packets
void tcp_filter(struct knetdevice * knd, eth_frame_t *frame, uint16_t len)
{
	ip_packet_t *ip = (void*)(frame->data);
	tcp_packet_t *tcp = (void*)(ip->data);
	uint8_t id, tcpflags;

	// tcp data length
	len -= tcp_head_size(tcp);

	// me needs only SYN/FIN/ACK/RST
	tcpflags = tcp->flags & (TCP_FLAG_SYN|TCP_FLAG_ACK|TCP_FLAG_RST|TCP_FLAG_FIN);

	//check if there is any state
	const struct knetstate * st = hal_get_state(ip->from_addr, tcp->from_port, tcp->to_port);
	struct tcp_state * tst = NULL;
	//check if any service is listening on this port and ip
	const struct knetlisten * listen = hal_get_listen(ip->to_addr, tcp->to_port, KNET_TCP);
	// connection not found/new connection
	if(st == NULL)
	{

		if (listen == NULL)
		{
			//got packet on closed port
			setSysError(SYSTEM_NET_DATA_ON_CLOSED_PORT);
			return;
		}

		//try to create new tcp_state record
		tst = (struct tcp_state *) kernel_malloc(sizeof(struct tcp_state));
		if (tst == NULL)
		{
			setSysError(SYSTEM_MALLOC_FAILED);
			goto error;
		}

		struct knetstate * st = hal_request_new_state();
		// slot found and app accepts connection?
		if( st != NULL )
		{
			// add embrionic connection to pool
			st->sock_opts = (void *) tst;
			tst->status = TCP_SYN_RECEIVED;
			tst->event_time = kernel_get_milis();
			tst->seq_num = (uint32_t) kernel_get_milis() + (kernel_get_milis() << 16);
			tst->ack_num = ntohl(tcp->seq_num) + 1;
			st->raddr = ip->from_addr;
			st->rport = tcp->from_port;
			st->laddr = ip->to_addr;
			st->lport = tcp->to_port;

#if WITH_TCP_REXMIT == YES
			tst->is_closing = 0;
			tst->rexmit_count = 0;
			tst->seq_num_saved = tst->seq_num;
#endif

			// send SYN/ACK
			tcp->flags = TCP_FLAG_SYN|TCP_FLAG_ACK;
			tcp_xmit(knd, st, frame, 0);
		}
		else
		{
			kernel_free((void *) tst);
		}
	}
	else
	{
		tst = (struct tcp_state *) st->sock_opts;
	// sending packets back
		tst->tcp_send_mode = TCP_SENDING_REPLY;
		tst->tcp_ack_sent = 0;
		// connection reset by peer?
		if(tcpflags & TCP_FLAG_RST)
		{
			if( (tst->status == TCP_ESTABLISHED) ||
				(tst->status == TCP_FIN_WAIT) )
			{
			    tst->status = TCP_CLOSED;
				listen->hndls.CLOSED(knd, st);
				hal_net_remove_state(st);
			}

			return;
		}

		// me needs only ack packet
		if( (ntohl(tcp->seq_num) != tst->ack_num) ||
			(ntohl(tcp->ack_num) != tst->seq_num) ||
			(!(tcpflags & TCP_FLAG_ACK)) )
		{
			return;
		}

#if WITH_TCP_REXMIT == YES
		// save sequence number
		tst->seq_num_saved = tst->seq_num;

		// reset rexmit counter
		tst->rexmit_count = 0;
#endif

		// update ack pointer
		tst->ack_num += len;
		if( (tcpflags & TCP_FLAG_FIN) || (tcpflags & TCP_FLAG_SYN) )
		{
			tst->ack_num++;
		}

		// reset timeout counter
		tst->event_time = kernel_get_milis();

		switch(tst->status)
		{

			// SYN sent by me (active open, step 1)
			// awaiting SYN/ACK (active open, step 2)
			case TCP_SYN_SENT:

				// received packet must be SYN/ACK
				if(tcpflags != (TCP_FLAG_SYN|TCP_FLAG_ACK))
				{
					tst->status = TCP_CLOSED;
					break;
				}

				// send ACK (active open, step 3)
				tcp->flags = TCP_FLAG_ACK;
				tcp_xmit(knd, st, frame, 0);

				// connection is now established
				tst->status = TCP_ESTABLISHED;

				//allow user code to appeal the open

				if (listen->hndls.ONOPEN(knd, st) != 0)
                {
                    //close connection
                    kernel_mutex_lock(knd->m_tx);
                    memset(knd->tx_buf, 0, 200);
                    tcp_send(knd, st, knd->tx_buf, 0, TCP_OPTION_ABORT);
                    hal_net_remove_state(st);
                    kernel_mutex_unlock(knd->m_tx);
                }
				// app can send some data
				//tcp_read(id, frame, 0);//<--------------

			break;

			// SYN received my me (passive open, step 1)
			// SYN/ACK sent by me (passive open, step 2)
			// awaiting ACK (passive open, step 3)
			case TCP_SYN_RECEIVED:

				// received packet must be ACK
				if(tcpflags != TCP_FLAG_ACK)
				{
					tst->status = TCP_CLOSED;
					hal_net_remove_state(st);
					break;
				}

				// connection is now established
				tst->status = TCP_ESTABLISHED;

				// app can send some data
				//tcp_read(id, frame, 0); //<--------------------

				break;

			// connection established
			// awaiting ACK or FIN/ACK
			case TCP_ESTABLISHED:

				// received FIN/ACK?
				// (passive close, step 1)
				if(tcpflags == (TCP_FLAG_FIN|TCP_FLAG_ACK))
				{
					// feed data to app
					if(len)
					{
						listen->hndls.ONRCV(listen->k_listen, st, frame, len);
					} //tcp_write(id, frame, len);

					// send FIN/ACK (passive close, step 2)
					tcp->flags = TCP_FLAG_FIN|TCP_FLAG_ACK;
					tcp_xmit(knd, st, frame, 0);

					// connection is now closed
					tst->status = TCP_CLOSED;
					listen->hndls.CLOSED(knd, st);
					hal_net_remove_state(st);
				}
				else if(tcpflags == TCP_FLAG_ACK)
				{
				    // received ACK

					// feed data to app
					if(len)
					{
						listen->hndls.ONRCV(listen->k_listen, st, frame, len);
					} //tcp_write(id, frame, len);

					// app can send some data
					//tcp_read(id, frame, 0);

					// send ACK
					if( (len) && (!tst->tcp_ack_sent) )
					{
						tcp->flags = TCP_FLAG_ACK;
						tcp_xmit(knd, st, frame, 0);
					}
				}

				break;

			// FIN/ACK sent by me (active close, step 1)
			// awaiting ACK or FIN/ACK
			case TCP_FIN_WAIT:

				// received FIN/ACK?
				// (active close, step 2)
				if(tcpflags == (TCP_FLAG_FIN|TCP_FLAG_ACK))
				{
					// feed data to app
					if(len)
					{
						listen->hndls.ONRCV(listen->k_listen, st, frame, len);
					} //tcp_write(id, frame, len);

					// send ACK (active close, step 3)
					tcp->flags = TCP_FLAG_ACK;
					tcp_xmit(knd, st, frame, 0);

					// connection is now closed
					tst->status = TCP_CLOSED;
					listen->hndls.CLOSED(knd, st);
					hal_net_remove_state(st);
				}

				// received ACK+data?
				// (buffer flushing by peer)
				else if( (tcpflags == TCP_FLAG_ACK) && (len) )
				{
					// feed data to app

					listen->hndls.ONRCV(listen->k_listen, st, frame, len);
					//tcp_write(id, frame, len);

					// send ACK
					tcp->flags = TCP_FLAG_ACK;
					tcp_xmit(knd, st, frame, 0);

	#if WITH_TCP_REXMIT == YES
					// our data+FIN/ACK acked
					tst->is_closing = 1;
	#endif
				}

				break;

			default:
				break;
		}
	}

error:
	return;
}

// periodic event
/*void tcp_poll()
{
#if WITH_TCP_REXMIT == YES
	eth_frame_t *frame = (void*)net_buf;
	ip_packet_t *ip = (void*)(frame->data);
	tcp_packet_t *tcp = (void*)(ip->data);
#endif

	uint8_t id;
	tcp_state_t *st;

	for(id = 0; id < TCP_MAX_CONNECTIONS; ++id)
	{
		st = tcp_pool + id;

#if WITH_TCP_REXMIT == YES
		// connection timed out?
		if( (st->status != TCP_CLOSED) &&
			(gettc() - st->event_time > TCP_REXMIT_TIMEOUT) )
		{
			// rexmit limit reached?
			if(st->rexmit_count > TCP_REXMIT_LIMIT)
			{
				// close connection
				st->status = TCP_CLOSED;
				tcp_closed(id, 1);
			}

			// should rexmit?
			else
			{
				// reset timeout counter
				st->event_time = gettc();

				// increment rexmit counter
				st->rexmit_count++;

				// load previous state
				st->seq_num = st->seq_num_saved;

				// will send packets
				tcp_send_mode = TCP_SENDING_SEND;
				tcp_ack_sent = 0;

				// rexmit
				switch(st->status)
				{
				// rexmit SYN
				case TCP_SYN_SENT:
					tcp->flags = TCP_FLAG_SYN;
					tcp_xmit(st, frame, 0);
					break;

				// rexmit SYN/ACK
				case TCP_SYN_RECEIVED:
					tcp->flags = TCP_FLAG_SYN|TCP_FLAG_ACK;
					tcp_xmit(st, frame, 0);
					break;

				// rexmit data+FIN/ACK or ACK (in FIN_WAIT state)
				case TCP_FIN_WAIT:

					// data+FIN/ACK acked?
					if(st->is_closing)
					{
						tcp->flags = TCP_FLAG_ACK;
						tcp_xmit(st, frame, 0);
						break;
					}

					// rexmit data+FIN/ACK
					st->status = TCP_ESTABLISHED;

				// rexmit data
				case TCP_ESTABLISHED:
					tcp_read(id, frame, 1);
					if(!tcp_ack_sent)
					{
						tcp->flags = TCP_FLAG_ACK;
						tcp_xmit(st, frame, 0);
					}
					break;

				default:
					break;
				}
			}
		}
#else
		// check if connection timed out
		if( (st->status != TCP_CLOSED) &&
			(gettc() - st->event_time > TCP_CONN_TIMEOUT) )
		{
			// kill connection
			st->status = TCP_CLOSED;
			tcp_closed(id, 1);
		}
#endif
	}
}*/

#endif
