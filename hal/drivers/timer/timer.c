/*-
 *	Copyright (c) 2014-2015	Alexander Morozov www.nixd.org
 *	All rights reserved.
 *
 * LICENSE: This source file is subject to the New BSD license that is
 * available through the world-wide-web at the following URI:
 * http://www.opensource.org/licenses/bsd-license.php or in the root folder
 * of the project in file LICENSE.TXT.
 */

#include "../../build_defs.h"
#include <avr/io.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stddef.h>
#include <avr/interrupt.h>
#include <avr/sfr_defs.h>
#include <util/atomic.h>
#include "../../hal.h"
#include "./timer.h"


#include "../../../os/kernel/kernel_process.h"
#include "../../../os/kernel/kernel_mutex.h"
#include "../../../os/kernel/kernel_isr.h"

/*
ToDo: this driver needs futher development, works only with mega2560
*/
#if BUILD_WITH_PWM == YES
static int8_t timer_attach(struct dev_descr_node * ddn, struct timer_setup * tsp);
static int8_t timer_query(struct dev_descr_node * ddn, struct hal_query_info * data0);
static int8_t timer_detach(struct dev_descr_node * ddn);
static int8_t timer_update(struct dev_descr_node * ddn, struct timer_setup * tsp);
static int8_t timer_dump(struct dev_descr_node * ddn, void * data0, size_t * dsize0);
static int8_t timer_restore(struct dev_descr_node * ddn, void * data0, size_t * dsize0);


static struct timer_rec * volatile timer_list[MAX_TIMERS] = {[ 0 ... MAX_TIMERS-1 ] = NULL};


//table17-2 p.145
const struct waveform_mode_gen waveform_gens[] PROGMEM =
{
    {
        0, 0 //NORMAL
    },
    {
        1, 0
    },
    {
        2, 0
    },
    {
        3, 0
    },
    {
        0, 8
    },
    {
        1, 8
    },
    {
        2, 8
    },
    {
        3, 8 //7
    },
    {
        0, 16
    },
    {
        1, 16
    },
    {
        2, 16
    },
    {
        3, 16
    },
    {
        0, 24
    },
    {
        1, 24
    },
    {
        2, 24
    },
    {
        3, 24
    }
};


int8_t timer_hal_message(struct dev_descr_node * ddn,
                        enum dd_event e_event,
                        void * data0,
                        size_t * dsize0)
{
    int8_t ret = 0;

    switch (e_event)
    {
        case DD_EVENT_ATTACH:
            ret = timer_attach(ddn, (struct timer_setup *) data0);
        break;

        case DD_EVENT_DETACH:
            ret = timer_detach(ddn);
        break;

        case DD_EVENT_UPDATE:
            ret = timer_update(ddn, (struct timer_setup *) data0);
        break;

        case DD_DUMP:
            ret = timer_dump(ddn, data0, dsize0);
        break;

        case DD_RESTORE:
            ret = timer_restore(ddn, data0, dsize0);
        break;

        case DD_QUERY:
            ret = timer_query(ddn, (struct hal_query_info *) data0);
        break;

        case DD_ISRSIGNAL:
            ret = K_MSG_ERROR;
        break;

        case DD_RESET:
            ret = K_MSG_ERROR;
        break;

        case DD_DROP:
            ret = K_MSG_ERROR;
        break;

        case DD_FLUSH:
            ret = K_MSG_ERROR;
        break;
    }

    return ret;
}

int8_t
timer_kstream_init(struct dev_descr_node * ddn, struct kstream * ks)
{
    setSysError(SYSTEM_OPERATION_NOT_SUPPORTED);
    return K_FAIL_STREAM;
}

int8_t
timer_stream_init(struct dev_descr_node * ddn, FILE * file)
{
    setSysError(SYSTEM_OPERATION_NOT_SUPPORTED);
    return K_FAIL_STREAM;
}

static int8_t
timer_attach(struct dev_descr_node * ddn, struct timer_setup * tsp)
{
    clearSysError();
    ASSERT_NULL_ER(tsp, SYSTEM_NULL_ARGUMENT, K_NO_DD);

    if (timer_list[tsp->e_timer] != NULL)
    {
        setSysError(SYSTEM_DEVICE_ALREADY_INITED);
        return K_NO_DD;
    }

    int8_t res = timer_mode_setting(ddn, tsp);
   // printf_P(PSTR("PREINITED\r\n"));
    ASSERT_DATA(res == 1, return K_NO_DD);

     //printf_P(PSTR("INITED\r\n"));

    return 0;
}

static int8_t
timer_query(struct dev_descr_node * ddn, struct hal_query_info * data0)
{
    FILE * strm = (FILE *)data0->data0;
#if IO_BUILD_WITH_TEXT_QUERY == YES
    fprintf_P(strm, PSTR("TIMER REVISION: %u\r\n"), TIMER_DRIVER_VERSION);
    fprintf_P(strm, PSTR("TIMERS IN SYSTEM: %d\r\n"), MAX_TIMERS);
    for (uint8_t i = 0; i < MAX_TIMERS; i++)
    {
        fprintf_P(strm, PSTR("TIMER[%d] --> %p\r\n"), i, timer_list[i]);
        if (timer_list[i] != NULL)
        {
            fprintf_P(strm, PSTR("timer:%d bitwidth:%d\r\n"), timer_list[i]->e_timer, timer_list[i]->counter_bit_width);
            fprintf_P(strm, PSTR("-->waveform: %d clock_source: %d pwma:%d pwmb:%d\r\n"), timer_list[i]->e_wave_mode, timer_list[i]->e_clock, timer_list[i]->e_operA, timer_list[i]->e_operB);
            if (timer_list[i]->counter_bit_width == TIMER_BIT_WIDTH_8)
            {
                fprintf_P(strm, PSTR("-->ocra:%u ocrb:%u ocrc:%u icr:%u\r\n"), timer_list[i]->ocra.ocr8, timer_list[i]->ocrb.ocr8, timer_list[i]->ocrc.ocr8, timer_list[i]->icr.ocr8);
            }
            else
            {
                fprintf_P(strm, PSTR("-->ocra:%u ocrb:%u ocrc:%u icr:%u\r\n"), timer_list[i]->ocra.ocr16, timer_list[i]->ocrb.ocr16, timer_list[i]->ocrc.ocr16, timer_list[i]->icr.ocr16);
            }
            printf_P(PSTR("\r\n"));
        }

    }
#else
    setSysError(SYSTEM_OPERATION_NOT_SUPPORTED);
    fprintf_P(strm, PSTR("TIMER was built without query! \r\n"));
    return K_MSG_ERROR;
#endif // IO_BUILD_WITH_TEXT_QUERY
}

static int8_t
timer_detach(struct dev_descr_node * ddn)
{
    struct timer_rec * rec = (struct timer_rec *) ddn->instance;

    enum ports tm_port = pgm_read_byte(&(pwm_pins[rec->e_timer][1].e_dev_port));
    uint8_t tm_mask = pgm_read_byte(&(pwm_pins[rec->e_timer][1].pins));
    _SFR_IO8( hal_hw_get_addr_ddr(tm_port) ) &= ~tm_mask;

    tm_port = pgm_read_byte(&(pwm_pins[rec->e_timer][0].e_dev_port));
    tm_mask = pgm_read_byte(&(pwm_pins[rec->e_timer][0].pins));
    _SFR_IO8( hal_hw_get_addr_ddr(tm_port) ) &= ~tm_mask;

    hal_hw_unbind_dd_all_pin(ddn->dev_id);

    timer_list[rec->e_timer] = NULL;

    kernel_free((void *) rec);

    ddn->instance = NULL;

    return 0;
}

static int8_t
timer_update(struct dev_descr_node * ddn, struct timer_setup * tsp)
{
    ASSERT_NULL_ER(tsp, SYSTEM_NULL_ARGUMENT, K_NO_DD);
    (void)timer_detach(ddn);

    return timer_attach(ddn, tsp);
}

static int8_t
timer_dump(struct dev_descr_node * ddn, void * data0, size_t * dsize0)
{
    struct timer_envelope * envlp = (struct timer_envelope *) data0;
    struct timer_rec * tsp = (struct timer_rec *) ddn->instance;

    envlp->e_timer = tsp->e_timer;
    envlp->e_operA = tsp->e_operA;
    envlp->e_operB = tsp->e_operB;
    envlp->e_wave_mode = tsp->e_wave_mode;
    envlp->e_clock = tsp->e_clock;

    if (tsp->counter_bit_width == TIMER_BIT_WIDTH_8)
    {
        envlp->icr = tsp->icr.ocr8;
        envlp->ocra = tsp->ocra.ocr8;
        envlp->ocrb = tsp->ocrb.ocr8;
        envlp->ocrc = tsp->ocrc.ocr8;
    }
    else
    {
        envlp->icr = tsp->icr.ocr16;
        envlp->ocra = tsp->ocra.ocr16;
        envlp->ocrb = tsp->ocrb.ocr16;
        envlp->ocrc = tsp->ocrc.ocr16;
    }

    return 0;
}

static int8_t
timer_restore(struct dev_descr_node * ddn, void * data0, size_t * dsize0)
{
    struct timer_envelope * envlp = (struct timer_envelope *) data0;
    //printf_P(PSTR("%u %u %u %u %u %u %u %u %u"), envlp->e_timer, envlp->e_operA, envlp->e_operB, envlp->e_wave_mode,
    //envlp->e_clock, envlp->icr, envlp->ocra, envlp->ocrb, envlp->ocrc);
   /* struct timer_setup tsp;

    tsp.e_timer = envlp->e_timer;
    tsp.e_operA = envlp->e_operA;
    tsp.e_operB = envlp->e_operB;
    tsp.e_wave_mode = envlp->e_wave_mode;
    tsp.e_clock = envlp->e_clock;
    tsp.icr = envlp->icr;
    tsp.ocra = envlp->ocra;
    tsp.ocrb = envlp->ocrb;
    tsp.ocrc = envlp->ocrc;*/

    return timer_attach(ddn, data0);
}

struct timer_rec *
timer_get_by_did(t_dev_id did)
{
    for (uint8_t i = 0; i < MAX_TIMERS; i++)
    {
        if ((timer_list[i] != NULL) && (timer_list[i]->ddn->dev_id == did))
        {
            return timer_list[i];
        }
    }

    setSysError(SYSTEM_ITEM_NOT_FOUND);
    return NULL;
}

int8_t
timer_is_system(enum timers_id e_timer)
{
    if (e_timer == TIMER_USED_BY_SYSTEM)
    {
        return 1;
    }

    return 0;
}

int8_t
timer_mode_setting(struct dev_descr_node * ddn, struct timer_setup * tsp)
{
    const struct timer_hwaddr * tim_hw_ptr = &s_timer_addr[(uint8_t) tsp->e_timer];
    uint8_t cnt_width = pgm_read_byte( &(tim_hw_ptr->timer_bit_width));

    //validate timer parameters befor creating or changing anything
    ASSERT_DATA((cnt_width == TIMER_BIT_WIDTH_8) && (tsp->e_wave_mode > WAVEFORM_MODE_7), goto error_timer_illigal_opts);

    struct timer_rec * trec = timer_list[tsp->e_timer];
    if (trec == NULL)
    {
        trec = (struct timer_rec *) kernel_malloc(sizeof(struct timer_rec));
        ASSERT_NULL_ER(trec, SYSTEM_MALLOC_FAILED, 1);
        timer_list[tsp->e_timer] = trec;
    }

    trec->e_timer = tsp->e_timer;
    trec->e_operA = tsp->e_operA;
    trec->e_operB = tsp->e_operB;
    trec->e_wave_mode = tsp->e_wave_mode;
    trec->e_clock = tsp->e_clock;
    trec->counter_bit_width = cnt_width;
    trec->ddn = ddn;

    const struct waveform_mode_gen * tim_waveform = &waveform_gens[(uint8_t) tsp->e_wave_mode];
    uint8_t wgm01 = pgm_read_byte(&(tim_waveform->tccra));
    uint8_t wgm23 = pgm_read_byte(&(tim_waveform->tccrb));

    //check if any of the oc pin is in use
    //page 127 datasheet
    //ToDo: tccrc check
    uint8_t pwm_port_count = pgm_read_byte(&(tim_hw_ptr->pwm_pins));

    //ToDo: optimize data
    if ((tsp->e_operB >= OCR_pwm_B1) || ((tsp->e_operB == OCR_pwm_B0) && (wgm23 != 0)))
    {

        enum ports tm_port = pgm_read_byte(&(pwm_pins[tsp->e_timer][1].e_dev_port));
        uint8_t tm_mask = pgm_read_byte(&(pwm_pins[tsp->e_timer][1].pins));
        if (hal_hw_bind_pin(ddn->dev_id, tm_port, tm_mask) == 1)
        {
            //failed to bind pin, exit
            goto error_pin_bind_0;
        }
        /*MUTEX_BLOCK(mtx_giant_io, MUTEX_ACTION_MANUAL)
        {*/
        kernel_mutex_lock(mtx_giant_io);
        //set ddr to out state
            _SFR_IO8( hal_hw_get_addr_ddr(tm_port) ) |= tm_mask;
        //}
        kernel_mutex_unlock(mtx_giant_io);
    }

    if ((tsp->e_operA >= OCR_pwm_A1) || ((tsp->e_operA == OCR_pwm_A0) && (wgm23 != 0)))
    {
        enum ports tm_port = pgm_read_byte(&(pwm_pins[tsp->e_timer][0].e_dev_port));
        uint8_t tm_mask = pgm_read_byte(&(pwm_pins[tsp->e_timer][0].pins));
        if (hal_hw_bind_pin(ddn->dev_id, tm_port, tm_mask) == 1)
        {
            //failed to bind pin, exit
            goto error_pin_bind_1;
        }

        /*MUTEX_BLOCK(mtx_giant_io, MUTEX_ACTION_MANUAL)
        {*/
        kernel_mutex_lock(mtx_giant_io);
            //set ddr to out state
            _SFR_IO8( hal_hw_get_addr_ddr(tm_port) ) |= tm_mask;
        //}
        kernel_mutex_unlock(mtx_giant_io);
    }


    //cache common timer registers
    uint16_t a_tccra = pgm_read_word(&(tim_hw_ptr->tccra));
    uint16_t a_tccrb = pgm_read_word(&(tim_hw_ptr->tccrb));
    uint16_t a_tcnt = pgm_read_word(&(tim_hw_ptr->tcnt));
    uint16_t a_ocra = pgm_read_word(&(tim_hw_ptr->ocra));
    uint16_t a_ocrb = pgm_read_word(&(tim_hw_ptr->ocrb));
    uint16_t a_timsk = pgm_read_word(&(tim_hw_ptr->timsk));

    if (trec->counter_bit_width == TIMER_BIT_WIDTH_8)
    {
        trec->ocra.ocr8 = (uint8_t) tsp->ocra;
        trec->ocrb.ocr8 = (uint8_t) tsp->ocrb;
        trec->ocrc.ocr8 = (uint8_t) tsp->ocrc;
        trec->icr.ocr8 = (uint8_t) tsp->icr;

        //reset values in registers
        _HAL_WREG8(a_tccra, 0);
        _HAL_WREG8(a_tccrb, 0);
        _HAL_WREG8(a_tcnt, 0);
        _HAL_WREG8(a_ocra, 0);
        _HAL_WREG8(a_ocrb, 0);
        _HAL_WREG8(a_timsk, 0);

        _HAL_WREG8(a_tccra, tsp->e_operA | tsp->e_operB | wgm01);
        _HAL_WREG8(a_tccrb, wgm23 | tsp->e_clock);
        _HAL_WREG8(a_ocra, trec->ocra.ocr8);
        _HAL_WREG8(a_ocrb, trec->ocrb.ocr8);
    }
    else
    {
        trec->ocra.ocr16 = tsp->ocra;
        trec->ocrb.ocr16 = tsp->ocrb;
        trec->ocrc.ocr16 = tsp->ocrc;
        trec->icr.ocr16 = tsp->icr;

        //cache specific registers
        uint16_t a_tccrc = pgm_read_word(&(tim_hw_ptr->tccrc));
        uint16_t a_ocrc = pgm_read_word(&(tim_hw_ptr->ocrc));
        uint16_t a_icr = pgm_read_word(&(tim_hw_ptr->icr));

         //reset values in registers
        _HAL_WREG8(a_tccra, 0);
        _HAL_WREG8(a_tccrb, 0);
        _HAL_WREG8(a_timsk, 0);
        _HAL_WREG8(a_tccrc, 0);
        _HAL_WREG16(a_icr, 0);
        _HAL_WREG16(a_ocrc, 0);
        _HAL_WREG16(a_ocrb, 0);
        _HAL_WREG16(a_ocra, 0);
        _HAL_WREG16(a_tcnt, 0);

        //ICR3 = icr;//0xFFFF; //FIX
        _HAL_WREG16(a_icr, tsp->icr);
        _HAL_WREG16(a_ocra, tsp->ocra);
        _HAL_WREG16(a_ocrb, tsp->ocrb);
        _HAL_WREG16(a_ocrc, tsp->ocrc);
        _HAL_WREG8(a_tccra, tsp->e_operA | tsp->e_operB | wgm01);
        _HAL_WREG8(a_tccrb, wgm23 | tsp->e_clock);


    }

    ddn->instance = (void *) trec;

    return 0;

error_pin_bind_1:
    //ToDo add in kernel_hw_bind check if same fid is trying to bind pin
error_pin_bind_0:
    if (trec == NULL)
    {
        kernel_free((void *) trec);
    }
    return 1;
error_bad_arg:
    setSysError(SYSTEM_SIZE_MISMATCH);
    return 1;
error_null_arg:
    setSysError(SYSTEM_NULL_ARGUMENT);
    return 1;
error_malloc:
    setSysError(SYSTEM_MALLOC_FAILED);
    return 1;
error_timer_illigal_opts:
    setSysError(SYSTEM_INCOMPAT_MODE);
    return 1;
}

int8_t
timer_update_single_ocr(struct dev_descr_node * dss, enum timer_ocr_registers e_reg, uint16_t ocr_val)
{
    struct timer_rec * rec = (struct timer_rec *) dss->instance;

    const struct timer_hwaddr * tim_hw_ptr = &s_timer_addr[(uint8_t) rec->e_timer];
    uint8_t ocr_count = pgm_read_word(&(tim_hw_ptr->pwm_pins));
    uint16_t a_ocr = 0;

    switch (e_reg)
    {
        case OCR_REG_A:
            a_ocr = pgm_read_word(&(tim_hw_ptr->ocra));
        break;

        case OCR_REG_B:
            ASSERT_DATA(ocr_count < 2, goto error_bad_access);
            a_ocr = pgm_read_word(&(tim_hw_ptr->ocrb));
        break;

        case OCR_REG_C:
            ASSERT_DATA(ocr_count < 3, goto error_bad_access);

            a_ocr = pgm_read_word(&(tim_hw_ptr->ocrc));
        break;
    }

    if (rec->counter_bit_width == TIMER_BIT_WIDTH_8)
    {
        _HAL_WREG8(a_ocr, (uint8_t) ocr_val);

        switch (e_reg)
        {
            case OCR_REG_A:
                rec->ocra.ocr8 = (uint8_t) ocr_val;
            break;

            case OCR_REG_B:
                rec->ocrb.ocr8 = (uint8_t) ocr_val;
            break;

            case OCR_REG_C:
                rec->ocrc.ocr8 = (uint8_t) ocr_val;
            break;
        }
    }
    else
    {
         _HAL_WREG16(a_ocr, ocr_val);

        switch (e_reg)
        {
            case OCR_REG_A:
                rec->ocra.ocr16 = ocr_val;
            break;

            case OCR_REG_B:
                rec->ocrb.ocr16 = ocr_val;
            break;

            case OCR_REG_C:
                rec->ocrc.ocr16 = ocr_val;
            break;
        }
    }

    return 0;

error_bad_access:
    setSysError(SYSTEM_BAD_ACCESS);
    return 1;
}

int8_t timer_update_ocrs(struct timer_rec * tmr, uint16_t ocra, uint16_t ocrb, uint16_t ocrc)
{

    struct timer_rec * rec = timer_list[tmr->e_timer];
    ASSERT_NULL_ER(rec, SYSTEM_NULL_ARGUMENT, 1);

    const struct timer_hwaddr * tim_hw_ptr = &s_timer_addr[(uint8_t) rec->e_timer];
    uint8_t ocr_count = pgm_read_word(&(tim_hw_ptr->pwm_pins));

    if (rec->counter_bit_width == TIMER_BIT_WIDTH_8)
    {
        _HAL_WREG8(pgm_read_word(&(tim_hw_ptr->ocra)), (uint8_t) ocra);
        if (ocr_count >= 2)
        {
            _HAL_WREG8(pgm_read_word(&(tim_hw_ptr->ocrb)), (uint8_t) ocrb);
        }
        if (ocr_count >=3)
        {
            _HAL_WREG8(pgm_read_word(&(tim_hw_ptr->ocrc)), (uint8_t) ocrc);
        }

        rec->ocra.ocr8 = (uint8_t) ocra;
        rec->ocrb.ocr8 = (uint8_t) ocrb;
        rec->ocrc.ocr8 = (uint8_t) ocrc;

    }
    else
    {
        _HAL_WREG16(pgm_read_word(&(tim_hw_ptr->ocra)), ocra);

        if (ocr_count >= 2)
        {
            _HAL_WREG16(pgm_read_word(&(tim_hw_ptr->ocrb)), ocrb);
        }
        if (ocr_count >=3)
        {
            _HAL_WREG16(pgm_read_word(&(tim_hw_ptr->ocrc)), ocrc);
        }

        rec->ocra.ocr16 = ocra;
        rec->ocrb.ocr16 = ocrb;
        rec->ocrc.ocr16 = ocrc;

    }

    return 0;

error:
    setSysError(SYSTEM_ITEM_NOT_FOUND);
    return 1;
}

#endif // BUILD_WITH_PWM
