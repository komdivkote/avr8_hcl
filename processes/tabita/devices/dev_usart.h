#ifndef DEV_USART_H_INCLUDED
#define DEV_USART_H_INCLUDED

#define FOREACH_USART_CMDS(DV)\
    DV(USART_CMD_SENDBYTE, (uint8_t) 0x01)\
    DV(USART_CMD_OPERATION_NOT_SUP, (uint8_t) 0xFF)

enum usart_cmds
{
    FOREACH_USART_CMDS(GENERATE_ENUM_VAL)
};

struct proto_usart_send
{
    uint8_t data;
};


#endif // DEV_USART_H_INCLUDED
