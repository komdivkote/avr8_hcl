#ifndef DEV_ANONPAN_H_INCLUDED
#define DEV_ANONPAN_H_INCLUDED

#define FOREACH_A_PAN_CMD(DV)\
	DV(A_PAN_CMD_TURNON_SINGLE, (uint8_t) 0)\
	DV(A_PAN_CMD_TURNOFF_SINGLE, (uint8_t) 1)\
	DV(A_PAN_CMD_BLINK_SINGLE, (uint8_t) 2)\
	DV(A_PAN_CMD_OPER_NOT_SUP, (uint8_t) 0xFF)

enum a_pan_cmds
{
	FOREACH_A_PAN_CMD(GENERATE_ENUM_VAL)
};


struct a_pan_proto_cell
{
    uint8_t id;
};

#endif // DEV_ANONPAN_H_INCLUDED
