#ifndef HAL_PRIVATE_H_INCLUDED
#define HAL_PRIVATE_H_INCLUDED

#include <stdio.h>
#include "hal.h"

struct hal_stream_list
{
    t_stream_id sid;                    //stream id
    struct dev_descr_node * dest;       //destination
    FILE * node;                        //pointer to the record
	struct kern_stream_list * next;     //pointer ot the next record
};

extern volatile struct dd_list  * volatile dd_list_ptr;               /*fd records*/
extern volatile struct hw_bind_list  * volatile hw_list_ptr;          /*hardware bind (only physical ports)*/
extern volatile t_dev_count dev_amount;                               /*device amount*/
extern volatile struct hal_stream_list * volatile stream_list_ptr;    //pointer to list of created streams

extern struct kproc_mutex * hal_mux;

int8_t hal_init_statically();

t_dev_id hal_dev_node_attach(struct dev_descr_node * node, void * data0, size_t data0_len);
t_dev_id hal_dev_node_detach(struct dev_descr_node * node);
t_dev_id hal_dev_node_dump(struct dev_descr_node * node, void * data_out, size_t data_size);
t_dev_id hal_dev_node_restore(struct dev_descr_node * node, void * data_in, size_t data_size);

int8_t hal_external_config_dump();
int8_t hal_external_config_restore();

#endif // HAL_PRIVATE_H_INCLUDED
