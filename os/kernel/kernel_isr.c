/*-
 *	Copyright (c) 2014-2016	Alexander Morozov. www.nixd.org
 *	All rights reserved.
 *
 * LICENSE: This source file is subject to the New BSD license that is
 * available through the world-wide-web at the following URI:
 * http://www.opensource.org/licenses/bsd-license.php or in the root folder
 * of the project in file LICENSE.TXT.
 */

#include <avr/io.h>

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include "kernel_sectioning.h"
#include "kernel_memory_private.h"
#include "kernel_process_private.h"
#include "portable/kavr_io.h"
#include "portable/kavr_io_titles.h"

#if KERNEL_BUILD_WITH_ISR == YES

volatile uint8_t isr_to_process = 0;                         //delayed ISR left unprocessed
k_pid isr_itr_pid = -1;                             //delayed ISR processing proc ID
k_iid isr_count = 0;                                //counter of the registered ISR
volatile struct kisr * volatile isr_list = NULL;    //system ISR vector list

void __void_isr_handler();                          //NULL is handler
int8_t __check_isr_id(k_iid isr_id);                //performs check for system occupied ISR
void __isr_iterator_proc(void * arg0);              //process function

ATTRIBUTE_KERNEL_SECTION
void
__void_isr_handler()
{
    return;
}


ATTRIBUTE_KERNEL_SECTION
int8_t
__check_isr_id(k_iid isr_id)
{
    if ((isr_id == TIMER0_COMPB_vect_num) ||
        (isr_id == TIMER0_COMPA_vect_num) ||
        (isr_id == TIMER0_OVF_vect_num)   ||
        (isr_id == WDT_vect_num)
        #if (KERNEL_MEASURE_PERF_ISR == YES)
        || (isr_id == TIMER1_CAPT_vect_num)
        || (isr_id == TIMER1_COMPA_vect_num)
        || (isr_id == TIMER1_COMPB_vect_num)
        || (isr_id == TIMER1_COMPC_vect_num)
        || (isr_id == TIMER1_OVF_vect_num)
        #endif
        )
    {
        return RET_FAIL;
    }

    return RET_OK;
}

ATTRIBUTE_KERNEL_SECTION
void
__isr_iterator_proc(void * arg0)
{
    volatile struct kisr * iisr = NULL;
    struct kproc_wait_condition * kwc = kernel_cond_wait_init(&isr_to_process, SC_WAKEUP_LARGER_ZERO);

    KERNEL_SET_STATE(KSTATE_KERN)
    {
        while (1)
        {
            if (isr_to_process == 0)
            {
                #if KERNEL_BUILD_WITH_CONDITION_WAIT == YES
                    if (kwc != NULL)
                    {
                        kernel_cond_wait(kwc);
                    }
                    else
                    {
                        kernel_process_usleep(100000, 0);
                    }
                #else
                    kernel_process_usleep(100000, 0);
                #endif // KERNEL_BUILD_WITH_CONDITION_WAIT
            }
            iisr = isr_list;

            while (iisr != NULL)
            {
                if (iisr->flags.cnt > 0)// & KISRF_ISR)
                {
                    //iisr->isr_reqs &= ~KISRF_ISR;
                    --iisr->flags.cnt;
                    --isr_to_process;
                    iisr->ISR_HANDLER();
                }
                iisr = iisr->next;
            }

        }
    }
    return;
}

ATTRIBUTE_KERNEL_SECTION
int8_t
kernel_isr_init()
{
    if (isr_itr_pid == -1)
    {
        isr_itr_pid = kernel_create_process(PSTR("[ISR_ITERATOR]"), KPROC_TYPE_SYSTEM, KERNEL_PROC_ISR_STATCK_SIZE, __isr_iterator_proc, NULL);
        if (isr_itr_pid == -1)
        {
            return 1;
        }
    }

    return 0;
}

ATTRIBUTE_KERNEL_SECTION
int8_t
kernel_isr_grab(k_iid isr_id, void (*ISR_HANDLER)(), enum isr_type e_itype)
{
    //check if isr_id is not which used by kernel
    if (__check_isr_id(isr_id) == RET_FAIL)
    {
        return RET_FAIL;
    }

    //create KISR record
    struct kisr * visr = (struct kisr *) kernel_malloc(sizeof(struct kisr));
    if (visr == NULL)
    {
        return RET_FAIL;
    }

    //initialize values
    memset((void *) visr, 0, sizeof(struct kisr));
    visr->ISR_HANDLER = (ISR_HANDLER != NULL) ? ISR_HANDLER : __void_isr_handler;
    visr->isr_num = isr_id;
    visr->next = NULL;
    if (e_itype == ISR_DELAYED)
    {
        //setting flag delayd
        //visr->isr_reqs |= KISRF_DELAYED;
        visr->flags.flags |= KISRF_DELAYED;
    }


    //add to the list
    if (isr_list == NULL)
    {
        isr_list = visr;
    }
    else if (isr_list->isr_num > visr->isr_num)
    {
        //insert before
        visr->next = isr_list;
        isr_list = visr;
    }
    else
    {
        struct kisr * it = isr_list;
        while (it != NULL)
        {

            if (it->next == NULL)
            {
                //set at the end and exit
                it->next = visr;
                break;
            }
            else if ((it->isr_num < visr->isr_num) && (it->next->isr_num > visr->isr_num))
            {
                //set there
                visr->next = it->next;
                it->next = visr;
                break;
            }

            it = it->next;
        }
    }

    ++isr_count;

    return RET_OK;
}

ATTRIBUTE_KERNEL_SECTION
int8_t
kernel_isr_release(k_iid isr_id)
{
    //check if isr_id is not used by kernel
    if (__check_isr_id(isr_id) == RET_FAIL)
    {
        return RET_FAIL;
    }

    struct kisr * it = isr_list;
    struct kisr * it_p = NULL;
    while (it != NULL)
    {
        if (it->isr_num == isr_id)
        {
            //found, removing from list
            if (it_p == NULL)
            {
                isr_list = it->next;
            }
            else
            {
                it_p->next = it->next;
            }

            break;
        }

        it_p = it;
        it = it->next;
    }

    if (it == NULL)
    {
        return RET_FAIL;
    }
    else
    {
        kernel_free((void*) it);
    }
    it = NULL;
    --isr_count;

    return RET_OK;
}

ATTRIBUTE_KERNEL_SECTION
int8_t
kernel_isr_suspend(k_iid isr_id)
{
    //check if isr_id is not which used by kernel
    if (__check_isr_id(isr_id) == RET_FAIL)
    {
        return RET_FAIL;
    }

    struct kisr * it = isr_list;
    while (it != NULL)
    {
        if (it->isr_num == isr_id)
        {
            //found
            //it->isr_reqs |= KISRF_SUSPEND;
            it->flags.flags |= KISRF_SUSPEND;
            break;
        }
        it = it->next;
    }

    if (it == NULL)
    {
        return RET_FAIL;
    }

    return RET_OK;

}

ATTRIBUTE_KERNEL_SECTION
int8_t
kernel_isr_restore(k_iid isr_id)
{
    //check if isr_id is not which used by kernel
    if (__check_isr_id(isr_id) == RET_FAIL)
    {
        return RET_FAIL;
    }

    struct kisr * it = isr_list;
    while (it != NULL)
    {
        if (it->isr_num == isr_id)
        {
            //found
            //it->isr_reqs &= ~KISRF_SUSPEND;
            it->flags.flags &= ~KISRF_SUSPEND;
            break;
        }

        it = it->next;
    }

    if (it == NULL)
    {
        return RET_FAIL;
    }

    return RET_OK;

}

ATTRIBUTE_KERNEL_SECTION
PGM_P
kernel_isr_get_caption(k_iid isr_id)
{
    PGM_P p;

    memcpy_P(&p, &isrcaps0[isr_id-1], sizeof(PGM_P));
    return p;
}

ATTRIBUTE_KERNEL_SECTION
int8_t
kernel_isr_get_amount()
{
    return isr_count;
}

ATTRIBUTE_KERNEL_SECTION
int8_t
kernel_isr_get_info(k_iid isr_rec_ord, struct kisr * isrv)
{
    if (isrv == NULL)
    {
        return RET_FAIL;
    }

    if (isr_rec_ord < 0)
    {
        return RET_FAIL;
    }

    struct kisr * it = isr_list;
    while (it != NULL)
    {
        if (isr_rec_ord == 0)
        {
            //found
            isrv->isr_num = it->isr_num;
            //isrv->isr_reqs = it->isr_reqs;
            isrv->flags = it->flags;
            isrv->tcnt_cnt = it->tcnt_cnt;
            isrv->total_cals = it->total_cals;
            isrv->ISR_HANDLER = it->ISR_HANDLER;
            break;
        }

        --isr_rec_ord;
        it = it->next;
    }

    if (it == NULL)
    {
        return RET_FAIL;
    }

    return RET_OK;
}

//PRIVATE
//defined in the kernel_isr_private
ATTRIBUTE_KERNEL_SECTION
struct kisr *
kernel_isr_get(k_iid isr_id)
{
    volatile struct kisr * isr_itr = isr_list;

    while (isr_itr != NULL)
    {
        if (isr_itr->isr_num == isr_id)
        {
            return isr_itr;
        }
        isr_itr = isr_itr->next;
    }

    return NULL;
}

#endif // KERNEL_BUILD_WITH_ISR
