/*-
 *	Copyright (c) 2014-2015	Alexander Morozov www.nixd.org
 *	All rights reserved.
 *
 * LICENSE: This source file is subject to the New BSD license that is
 * available through the world-wide-web at the following URI:
 * http://www.opensource.org/licenses/bsd-license.php or in the root folder
 * of the project in file LICENSE.TXT.
 */

#include "../../build_defs.h"

#if BUILD_WITH_IO == YES
#include <avr/io.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stddef.h>
#include <avr/interrupt.h>
#include <avr/sfr_defs.h>
#include <util/atomic.h>
#include "../../hal.h"
#include "./io.h"

#include "../../../os/kernel/kernel_process.h"
#include "../../../os/kernel/kernel_mutex.h"
//#include "../../../os/kernel/kernel_isr.h"



static int8_t io_attach(struct dev_descr_node * ddn, struct io_setup * ios);
static int8_t io_query(struct dev_descr_node * ddn, struct hal_query_info * data0);
static int8_t io_detach(struct dev_descr_node * ddn);
static int8_t io_update(struct dev_descr_node * ddn, struct io_setup * ios);
static int8_t io_dump(struct dev_descr_node * ddn, void * data0, size_t * dsize0);
static int8_t io_restore(struct dev_descr_node * ddn, void * data0, size_t * dsize0);

VERIFY_ENUM_FUNCTION_VAL_P(io_dir_func, FOREACH_IO_PORT_DIR);
static uint8_t __io_count_bits(uint8_t mask);

static struct io_null_dev * io_ports_list = NULL;
static int8_t io_count = 0;

int8_t
io_hal_message(struct dev_descr_node * ddn,
              enum dd_event e_event,
              void * data0,
              size_t * dsize0)
{
    int8_t ret = 0;

    switch (e_event)
    {
        case DD_EVENT_ATTACH:
            ret = io_attach(ddn, (struct io_setup *) data0);
        break;

        case DD_EVENT_DETACH:
            ret = io_detach(ddn);
        break;

        case DD_EVENT_UPDATE:
            ret = io_update(ddn, (struct io_setup *) data0);
        break;

        case DD_DUMP:
            ret = io_dump(ddn, data0, dsize0);
        break;

        case DD_RESTORE:
            ret = io_restore(ddn, data0, dsize0);
        break;

        case DD_QUERY:
            ret = io_query(ddn, (struct hal_query_info *) data0);
        break;

        case DD_ISRSIGNAL:

        break;

        case DD_RESET:

        break;

        case DD_DROP:
            ret = K_MSG_ERROR;
        break;

        case DD_FLUSH:
            ret = K_MSG_ERROR;
        break;
    }

    return ret;
}

int8_t
io_kstream_init(struct dev_descr_node * ddn, struct kstream * ks)
{
    setSysError(SYSTEM_OPERATION_NOT_SUPPORTED);
    return K_FAIL_STREAM;
}

int8_t
io_stream_init(struct dev_descr_node * ddn, FILE * file)
{
    setSysError(SYSTEM_OPERATION_NOT_SUPPORTED);
    return K_FAIL_STREAM;
}

static int8_t
io_attach(struct dev_descr_node * ddn, struct io_setup * ios)
{
    clearSysError();
    ASSERT_NULL_ER(ios, SYSTEM_NULL_ARGUMENT, K_NO_DD);

    uint8_t ret = 0;

    //declare list item
    //LIST_ITEM(struct io_null_dev, iod);
    struct io_null_dev * iod = NULL;

    //everything finished, now set hardware
	uint16_t p_ddr = hal_hw_get_addr_ddr(ios->e_port);
	uint16_t p_port = hal_hw_get_addr_port(ios->e_port);

    //if node don't have a record then it is new otherwise instance requires update
    if (ddn->instance == NULL)
    {
        ret = hal_hw_bind_pin(ddn->dev_id, ios->e_port, ios->bit_mask);
        ASSERT_DATA(ret == 1, return K_NO_DD);

        //create new record
        iod = (struct io_null_dev *) kernel_malloc(sizeof(struct io_null_dev));
        ASSERT_NULL_E(iod, SYSTEM_MALLOC_FAILED, malloc_failed_0);

        iod->bit_mask   = ios->bit_mask;
        iod->e_port     = ios->e_port;
        iod->ddn        = ddn;
        iod->e_io_dir   = ios->e_io_dir;
        iod->bit_count  = __io_count_bits(iod->bit_mask);
    }
    else
    {
        if (hal_hw_check_available(ios->e_port, ios->bit_mask) == K_HW_BUSY)
        {
            setSysError(SYSTEM_HW_PIN_BUSY);
            return K_NO_DD; //FAIL
        }

        hal_hw_unbind_dd_all_pin(ddn->dev_id);

        iod = (struct io_null_dev *) ddn->instance;

        //reset hardware
        //lock GIANT_IO mutex before working with general IO
       // MUTEX_BLOCK(mtx_giant_io, MUTEX_ACTION_MANUAL)
        //{
        kernel_mutex_lock(mtx_giant_io);
            _HAL_WREG8(p_ddr, _HAL_RREG8(p_ddr) & ~(iod->bit_mask));
            _HAL_WREG8(p_port, _HAL_RREG8(p_port) & ~(iod->bit_mask));
        kernel_mutex_unlock(mtx_giant_io);
        //}

        //update values
        iod->bit_mask   = ios->bit_mask;
        iod->e_port     = ios->e_port;
        iod->ddn        = ddn;
        iod->e_io_dir   = ios->e_io_dir;
        iod->bit_count  = __io_count_bits(iod->bit_mask);

        ret = hal_hw_bind_pin(ddn->dev_id, ios->e_port, ios->bit_mask);
        ASSERT_DATA(ret == 1, return K_NO_DD);
    }


	//lock GIANT_IO mutex before working with general IO
   /* MUTEX_BLOCK(mtx_giant_io, MUTEX_ACTION_MANUAL)
    {*/
    kernel_mutex_lock(mtx_giant_io);
        switch (iod->e_io_dir)
        {
            case IO_PORT_DIR_IN:
                _HAL_WREG8(p_ddr, _HAL_RREG8(p_ddr) & ~(iod->bit_mask));
                _HAL_WREG8(p_port, _HAL_RREG8(p_port) | (iod->bit_mask));
            break;

            case IO_PORT_DIR_OUT:
                _HAL_WREG8(p_ddr, _HAL_RREG8(p_ddr) | iod->bit_mask);
                _HAL_WREG8(p_port, _HAL_RREG8(p_port) & ~(iod->bit_mask));
            break;

            case IO_PORT_DIR_BI:
                //set default to ddr in
                _HAL_WREG8(p_ddr, _HAL_RREG8(p_ddr) & ~(iod->bit_mask));
                _HAL_WREG8(p_port, _HAL_RREG8(p_port) | (iod->bit_mask));
            break;
        }
    kernel_mutex_unlock(mtx_giant_io);
    //}
	ddn->instance = (void *) iod;

	return 0;

malloc_failed_0:
	setSysError(SYSTEM_MALLOC_FAILED);
	hal_hw_unbind_dd_all_pin(ddn->dev_id);
	return K_NO_DD;
}

static int8_t
io_query(struct dev_descr_node * ddn, struct hal_query_info * data0)
{
    FILE * strm = (FILE *)data0->data0;
#if IO_BUILD_WITH_TEXT_QUERY == YES

    fprintf_P(strm, PSTR("IO REVISION: %u\r\n"), IO_REVISION);
    struct io_null_dev * iod = ddn->instance;
    if (iod == NULL)
    {
        fprintf_P(strm, PSTR("Internal Error!io_null_dev record missing!\r\n"));
    }
    else
    {
        fprintf_P(strm,
                  PSTR("fid=%d\r\nport=%d\r\ndirection=%d\r\nmask=%d\r\nbit_count=%d\r\n"),
                  iod->ddn->dev_id,
                  iod->e_port,
                  iod->e_io_dir,
                  iod->bit_mask,
                  iod->bit_count);
        uint8_t pd = 0;
        uint8_t res = io_read8_direct(iod, &pd);
        fprintf_P(strm, PSTR("reading=%d\r\n"), pd);
    }

    return 0;
#else
    setSysError(SYSTEM_OPERATION_NOT_SUPPORTED);
    fprintf_P(strm, PSTR("IO was built without query! \r\n"));
    return K_MSG_ERROR;
#endif // IO_BUILD_WITH_TEXT_QUERY

}

static int8_t
io_detach(struct dev_descr_node * ddn)
{
    ASSERT_NULL_ER(ddn, SYSTEM_NULL_ARGUMENT, 1);

    struct io_null_dev *iod = (struct io_null_dev *)ddn->instance;
    //unbind pins
    hal_hw_unbind_dd_all_pin(ddn->dev_id);

    //everything finished, now set hardware
	uint16_t p_ddr = hal_hw_get_addr_ddr(iod->e_port);
	uint16_t p_port = hal_hw_get_addr_port(iod->e_port);

    //reset hardware
    //lock GIANT_IO mutex before working with general IO
    MUTEX_BLOCK(mtx_giant_io, MUTEX_ACTION_MANUAL)
    {
        _HAL_WREG8(p_ddr, _HAL_RREG8(p_ddr) & ~(iod->bit_mask));
        _HAL_WREG8(p_port, _HAL_RREG8(p_port) & ~(iod->bit_mask));
    }

    //free instance memory
    kernel_free((void *) iod);

    ddn->instance = NULL;

    return 0;
//normally this should not happen
not_found:
    setSysError(SYSTEM_ITEM_NOT_FOUND);
    return K_NO_DD;
}

static int8_t
io_update(struct dev_descr_node * ddn, struct io_setup * ios)
{
    ASSERT_NULL_ER(ios, SYSTEM_NULL_ARGUMENT, K_NO_DD);
    ASSERT_NULL_ER(ddn->instance, SYSTEM_NODE_INSTANCE_NULL, K_NO_DD);

    //clear hardware binds

    if (io_attach(ddn, ios) != 0)
    {
        return K_NO_DD;
    }

    return 0;
}

static int8_t
io_dump(struct dev_descr_node * ddn, void * data0, size_t * dsize0)
{
    ASSERT_NULL_ER(ddn->instance, SYSTEM_NODE_INSTANCE_NULL, K_NO_DD);

    struct io_null_envlp * envlp = (struct io_null_envlp *)data0;
    struct io_null_dev * iod = (struct io_null_dev *) ddn->instance;

    envlp->e_io_dir = iod->e_io_dir;
    envlp->e_port = iod->e_port;
    envlp->bit_mask = iod->bit_mask;

    return 0;
}

static int8_t
io_restore(struct dev_descr_node * ddn, void * data0, size_t * dsize0)
{
    struct io_null_envlp * envlp = (struct io_null_envlp *) data0;
    struct io_setup ioset;
    ioset.e_io_dir = envlp->e_io_dir;
    ioset.e_port = envlp->e_port;
    ioset.bit_mask = envlp->bit_mask;

    if (io_attach(ddn, &ioset) != 0)
    {
        return K_NO_DD;
    }

    return 0;
}

static uint8_t
__io_count_bits(uint8_t mask)
{
	uint8_t count = 0;
	while(mask)
	{
		count += mask & 1;
		mask >>= 1;
	}
	return count;
}

/*struct io_null_dev *
io_get_by_did(t_dev_id did)
{
	ASSERT_NEGATIVE_ER(did, SYSTEM_ID_INCORRECT, NULL);
	SEARCH_IN_LIST_2(struct io_null_dev, io_ports_list, ret, ret->ddn->dev_id == did, io_get_by_id_not_found);

	return ret;

io_get_by_id_not_found:
	return NULL;
}*/

int8_t
io_count_ones(struct io_null_dev * iod)
{
	ASSERT_NULL_E(iod, SYSTEM_NULL_ARGUMENT, io_count_ones_leave);
	uint8_t i = iod->bit_mask;

	return ((i>>7)&1)+((i>>6)&1)+((i>>5)&1)+((i>>4)&1)+((i>>3)&1)+((i>>2)&1)+((i>>1)&1)+(i&1);

io_count_ones_leave:
	return -1;
}

uint8_t
io_find_less_significant_bit(struct io_null_dev * iod)
{
	ASSERT_NULL_E(iod, SYSTEM_NULL_ARGUMENT, io_find_less_significant_bit_leave);
	uint8_t mask = iod->bit_mask;
	mask &= -mask;
	return mask;

io_find_less_significant_bit_leave:
	return 0;
}

uint8_t
io_find_mask_by_order(struct io_null_dev * iod, uint8_t n)
{
	ASSERT_NULL_E(iod, SYSTEM_NULL_ARGUMENT, io_find_mask_by_order_leave);

	uint8_t mask = iod->bit_mask;
	uint8_t mask_res = iod->bit_mask;
	while (n)
	{
		mask &= -mask;
		mask_res &= ~(mask); //turn off bit
		--n;
		mask = mask_res;
	}

	return mask;

io_find_mask_by_order_leave:
	return 0;
}

uint8_t
io_find_mask_by_selector(struct io_null_dev * iod, uint8_t n)
{
	ASSERT_NULL_E(iod, SYSTEM_NULL_ARGUMENT, error);

    //uint8_t m = 0b00010101;
	uint8_t mask = iod->bit_mask;
	uint8_t mask_res = iod->bit_mask;
	uint8_t mask_tmp = 0;
	uint8_t f = 0;

	while (n)
	{
		mask &= -mask;
		mask_res &= ~(mask); //turn off bit
		--n;
		mask = mask_res;

		if (f == 0)
		{
			f = iod->bit_mask ^ mask;
		}
		else
		{
			f = (mask | mask_tmp) ^ iod->bit_mask;
		}
		mask_tmp |= f;
	}

    return f;

error:
    return 0;
}

uint8_t
io_mask_prepare(struct io_null_dev * iod, uint8_t mask)
{
    return iod->bit_mask & mask;
}

//without checks
int8_t
io_write8_direct(struct io_null_dev * iod, uint8_t ready_mask)
{
	ASSERT_NULL_E(iod, SYSTEM_NULL_ARGUMENT, error_null);
	uint16_t p_port = hal_hw_get_addr_port(iod->e_port);

	MUTEX_BLOCK(mtx_giant_io, MUTEX_ACTION_MANUAL)
    {
        uint8_t temp = _HAL_RREG8(p_port);//_KRN_RREG8(p_port); //i.e 0000 0001
        temp = temp & ~(iod->bit_mask); //reset bits by mask (i.e 10100001 & 11111110 = 10100000)
        //_KRN_WREG8(p_port, (temp |ready_mask)); //set new bits to port
        _HAL_WREG8(p_port, temp |ready_mask);
    }
	return 0;

error_null:
	return 1;
}

int8_t
io_read8_direct(struct io_null_dev * iod, uint8_t * data_out)
{
    ASSERT_NULL_E(iod, SYSTEM_NULL_ARGUMENT, error_null);
    ASSERT_NULL_E(data_out, SYSTEM_NULL_ARGUMENT, error_null);
	uint16_t pin_port = hal_hw_get_addr_pin(iod->e_port);
	//uint8_t temp = _SFR_IO8(p_port);
	MUTEX_BLOCK(mtx_giant_io, MUTEX_ACTION_MANUAL)
    {
        *data_out = _HAL_RREG8(pin_port) & iod->bit_mask;
    }

    return 0;

error_null:
    return 1;
}

int8_t
io_write8(struct io_null_dev * iod, uint8_t dt)
{
	uint8_t mask = iod->bit_mask & dt; //i.e 00110000 & 01110000 = 00110000 filter write data according to mask

	return io_write8_direct(iod, mask);

error_null:
    return 1;

}

int8_t
io_read8(struct io_null_dev * iod, uint8_t * data_out)
{
    return io_read8_direct(iod, data_out);
}

int8_t
io_get_mask(struct io_null_dev * iod, uint8_t * mask_out)
{
    ASSERT_NULL_ER(iod, SYSTEM_NULL_ARGUMENT, 1);
    ASSERT_NULL_ER(mask_out, SYSTEM_NULL_ARGUMENT, 1);

    return iod->bit_mask;
}

#endif // BUILD_WITH_IO == YES
