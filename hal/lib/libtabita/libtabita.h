#ifndef LIBTABITA_H_INCLUDED
#define LIBTABITA_H_INCLUDED

#include "../../build_defs.h"

//frame size to both parties ar limited to 256 bytes


struct tabita_proto_head
{
	fd_dev_id fid;
	k_trans_id tid;
	uint8_t frame_size;
    uint16_t crc16;
    uint8_t msg[];
};

struct tabita_proto_body
{
    uint16_t crc16_body;
    uint8_t msg[];
};

struct tabita_session
{
    struct kstream * ks;    //stream to tabita fd
    struct
};

int8_t tabita_run_session(struct kstream * ks);


#endif // LIBTABITA_H_INCLUDED
