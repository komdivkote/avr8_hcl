/*-
 *  ATTENTION!: the initial version of the DHCP logic was taken from:
 *		link:http://easyelectronics.ru/files/WE/redsh/eth_232.zip
 *		link2:http://we.easyelectronics.ru/electro-and-pc/podklyuchenie-mikrokontrollera-k-lokalnoy-seti-udp-server.html
 *		License: WTFPL
 *
 *	--------APPLICIABLE ONLY FOR DSC code, not DHCP logic/routnies------
 *	Copyright (c) 2014-2016	Alexander Morozov. NiXD ORG
 *	All rights reserved.
 *
 * LICENSE: This source file is subject to the New BSD license that is
 * available through the world-wide-web at the following URI:
 * http://www.opensource.org/licenses/bsd-license.php or in the root folder
 * of the project in file LICENSE.TXT.
 */


#include "../../build_defs.h"
#if (BUILD_WITH_U2ETH == YES)

#include <avr/io.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stddef.h>
#include <string.h>
#include <avr/interrupt.h>
#include <avr/sfr_defs.h>
#include <util/atomic.h>
#include <util/delay.h>
#include "../../hal.h"

#include "../serial/usart.h"


#include "../../../os/kernel/kernel_memory.h"
#include "../../../os/kernel/kernel_mutex.h"

#include "u2eth.h"

#include "../../../processes/tabita/tabita.h"
#include "../../../processes/tabita/devices/dev_u2eth.h"

static int8_t u2eth_attach(struct dev_descr_node * ddn);
static int8_t u2eth_query(struct dev_descr_node * ddn, struct hal_query_info * data0);
static int8_t u2eth_detach(struct dev_descr_node * ddn);
static int8_t u2eth_update(struct dev_descr_node * ddn);
static int8_t u2eth_dump(struct dev_descr_node * ddn, void * data0, size_t * dsize0);
static int8_t u2eth_restore(struct dev_descr_node * ddn, void * data0, size_t * dsize0);
static void u2eth_process(void * arg0);

static void
u2eth_process(void * arg0)
{
    uint8_t ind = 0;
    struct u2eth_instance * u2einst = (struct u2eth_instance*) arg0;

    while (1)
    {
        kernel_mutex_lock(u2einst->m_u2e);
        ITERATE_BLOCK_DIRECT_MAN(struct u2eth_route, u2einst->u2eth_list_ptr, _it)
        {
            //reset counter
            ind = 0;

            //check if data has arrived
            struct usart_io * uio = (struct usart_io *)_it->from->instance;

            if (b_has_more(uio->rx_buf) == B_MORE)
            {
                struct knetdevice * kndev = hal_get_knetdev(_it->netdev);
                if (kndev == NULL)
                {
                     ITERATE_NEXT(_it);
                     continue;
                }
                uint8_t * buf = hal_net_grab_tx_buffer(kndev, KNET_UDP);
                struct tabita_request_ans * buf_data = tabita_prepare_header(buf, TAB_PKT_OPER_REQUEST_DD);
                buf_data->did = u2einst->ddn->dev_id;
                buf_data->opcode = U2ETH_CMD_RECEIVE;
                buf_data->error_no = 0;

                struct u2eth_proto_receive * uprcv = buf_data->data;

                int32_t res = -1;
                while ((res = bRBf8(uio->rx_buf)) != -1)
                {
                    uprcv->data[ind++] = (uint8_t) res;
                }
                uprcv->data_len = ind;
                uprcv->did_orig = uio->ddn->dev_id;

                buf = tabita_cound_crc16(sizeof(struct u2eth_proto_receive)+sizeof(struct tabita_request_ans)+ind, buf);

                hal_send_dgram(kndev,
                               _it->ip_to,
                               _it->port_to,
                               TABITA_PORT,
                               KNET_UDP,
                               kndev->tx_buf,
                               ((struct tabita_header *)buf)->payload_len);

                hal_net_relese_tx_buffer(kndev);
            }
            ITERATE_NEXT(_it);

        }

        kernel_mutex_unlock(u2einst->m_u2e);

        kernel_process_usleep(100000, 0);
    }
    return;
}

int8_t u2eth_hal_message(struct dev_descr_node * ddn,
                          enum dd_event e_event,
                          void * data0,
                          size_t * dsize0)
{
    int8_t ret = 0;

    switch (e_event)
    {
        case DD_EVENT_ATTACH:
            ret = u2eth_attach(ddn);
        break;

        case DD_EVENT_DETACH:
            ret = u2eth_detach(ddn);
        break;

        case DD_EVENT_UPDATE:
            ret = u2eth_update(ddn);
        break;

        case DD_DUMP:
            ret = u2eth_dump(ddn, data0, dsize0);
        break;

        case DD_RESTORE:
            ret = u2eth_restore(ddn, data0, dsize0);
        break;

        case DD_QUERY:
            ret = u2eth_query(ddn, (struct hal_query_info *) data0);
        break;

        case DD_ISRSIGNAL:
            ret = K_MSG_ERROR;
        break;

        case DD_RESET:
            ret = K_MSG_ERROR;
        break;

        case DD_DROP:
            ret = K_MSG_ERROR;
        break;

        case DD_FLUSH:
            ret = K_MSG_ERROR;
        break;
    }

    return ret;
}

int8_t
u2eth_kstream_init(struct dev_descr_node * ddn,
                           struct kstream * ks)
{
    setSysError(SYSTEM_OPERATION_NOT_SUPPORTED);
    return K_FAIL_STREAM;
}

int8_t
u2eth_stream_init(struct dev_descr_node * ddn,
                             FILE * file)
{
    setSysError(SYSTEM_OPERATION_NOT_SUPPORTED);
    return K_FAIL_STREAM;
}

static int8_t
u2eth_attach(struct dev_descr_node * ddn)
{
    struct u2eth_instance * u2e = kernel_malloc(sizeof(struct u2eth_instance));
    ASSERT_NULL_ER(u2e, SYSTEM_MALLOC_FAILED, K_NO_DD);
    memset(u2e, 0, sizeof(struct u2eth_instance));

    ddn->instance = (void *) u2e;
    u2e->ddn = ddn;

    u2e->m_u2e = kernel_mutex_init();
    u2e->pid = kernel_create_process(PSTR("[U2ETH]"), KPROC_TYPE_SYSTEM, 200, u2eth_process, (void *)u2e);
    if (u2e->pid == K_NO_PID)
    {
        setSysError(SYSTEM_KERNEL_PROCESS_CREATE_FAILED);
        kernel_free((void*) u2e->m_u2e);
        ddn->instance = NULL;
        kernel_free((void*) u2e);
        return K_NO_DD;
    }


    return 0;
}

static int8_t
u2eth_query(struct dev_descr_node * ddn, struct hal_query_info * data0)
{
    FILE * strm = (FILE *)data0->data0;
#if ANON_PANEL_BUILD_WITH_TEXT_QUERY == YES
    fprintf_P(strm, PSTR("REVISION %"PRIu8"\r\n"), U2ETH_REVISION);

    struct u2eth_instance * u2e = ((struct u2eth_instance *)ddn->instance);

    ITERATE_BLOCK_DIRECT_MAN(struct u2eth_route, u2e->u2eth_list_ptr, _it)
    {
        fprintf_P(strm, PSTR("From DID: %"PRId8" to IP: %u.%u.%u.%u:%"PRIu16"\r\n"),
                  _it->from->dev_id, ((uint8_t*)&_it->ip_to)[0], ((uint8_t*)&_it->ip_to)[1],
                  ((uint8_t*)&_it->ip_to)[2], ((uint8_t*)&_it->ip_to)[3], _it->port_to);

        ITERATE_NEXT(_it);
    }
#else
    setSysError(SYSTEM_OPERATION_NOT_SUPPORTED);
    fprintf_P(strm, PSTR("Anon panel was built without query! \r\n"));
    return K_MSG_ERROR;
#endif

    return 0;
}

static int8_t
u2eth_detach(struct dev_descr_node * ddn)
{
    struct u2eth_instance * u2e = ((struct u2eth_instance *)ddn->instance);

    kernel_process_suspend(u2e->pid);

    ITERATE_BLOCK_DIRECT_MAN(struct u2eth_route, u2e->u2eth_list_ptr, _it)
    {
        struct u2eth_route * prev = _it;
        ITERATE_NEXT(_it);
        kernel_free((void*)prev);
    }

    kernel_free((void *) u2e->m_u2e);
    kernel_process_remove(u2e->pid);

    kernel_free((void*) u2e);

    ddn->instance = NULL;

    return 0;
}

static int8_t
u2eth_update(struct dev_descr_node * ddn)
{
    return 0;
}

static int8_t
u2eth_dump(struct dev_descr_node * ddn, void * data0, size_t * dsize0)
{
    uint8_t * amount = (uint8_t*) data0;
    struct u2eth_envelope * route = (struct u2eth_envelope*) (amount+sizeof(uint8_t));
    uint8_t i = 0;

    struct u2eth_instance * u2e = ((struct u2eth_instance *)ddn->instance);

    ITERATE_BLOCK_DIRECT_MAN(struct u2eth_route, u2e->u2eth_list_ptr, _it)
    {
        route[i].ip_to = _it->ip_to;
        route[i].port_to = _it->port_to;
        route[i].serial_did = _it->from->dev_id;
        route[i].netdev = _it->netdev;
        i++;
        ITERATE_NEXT(_it);
    }

    *amount = i;

    return 0;
}

static int8_t
u2eth_restore(struct dev_descr_node * ddn, void * data0, size_t * dsize0)
{
    uint8_t * amount = (uint8_t*) data0;
    struct u2eth_envelope * route = (struct u2eth_envelope*) (amount+sizeof(uint8_t));

    if (u2eth_attach(ddn) == K_NO_DD)
    {
        return K_NO_DD;
    }

    for (uint8_t i = 0; i < *amount; i++)
    {
        u2eth_add(ddn, route[i].serial_did, route[i].ip_to, route[i].port_to, route[i].netdev);
    }


    return 0;
}

int8_t
u2eth_add(struct dev_descr_node * ddn, t_dev_id did, uint32_t ip, uint16_t port, t_netdev_id tnid)
{
    ASSERT_NULL_ER(ddn, SYSTEM_NULL_ARGUMENT, 1);

    struct dev_descr_node * ddn_serial = hal_node_get(did);
    ASSERT_NULL_R(ddn_serial, 1);

    if ( (ddn_serial->e_dev_descr != DEV_DESCR_USART) ||
         (ddn_serial->e_dh_code != ROOT_DRIVER_DEV) )
    {
        setSysError(SYSTEM_DEVICE_NOT_COMPATIABLE);
        return 1;
    }

    if (usart_is_buffered_rx(ddn_serial) == 0)
    {
        setSysError(SYSTEM_USART_REQUIRED_BUFFERED_RX);
        return 1;
    }


    struct u2eth_route * u2er = (struct u2eth_route *) kernel_malloc(sizeof(struct u2eth_route));
    ASSERT_NULL_ER(u2er, SYSTEM_MALLOC_FAILED, 1);

    u2er->from  = ddn_serial;
    u2er->ip_to = ip;
    u2er->netdev    = tnid;
    u2er->port_to   = port;
    u2er->next      = NULL;

    struct u2eth_instance * u2e = (struct u2eth_instance *) ddn->instance;

    kernel_mutex_lock(u2e->m_u2e);
    PUSH_TO_LIST(u2e->u2eth_list_ptr, u2er);
    kernel_mutex_unlock(u2e->m_u2e);

    return 0;
}


#endif
