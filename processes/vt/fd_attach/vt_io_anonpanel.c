/*-
 *	Copyright (c) 2014-2016	Alexander Morozov. NiXD ORG
 *	All rights reserved.
 *
 * LICENSE: This source file is subject to the New BSD license that is
 * available through the world-wide-web at the following URI:
 * http://www.opensource.org/licenses/bsd-license.php or in the root folder
 * of the project in file LICENSE.TXT
 */

#include "../../../hal/build_defs.h"
#include <avr/io.h>
#include <avr/pgmspace.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include "../../../hal/hal.h"
#include "../../../hal/drivers/annunciator_panel/a_panel.h"
#include "../../../hal/handlers.h"
#include "../virtual_terminal.h"
#include "../../../lib/conio/conio.h"
#include "../../../lib/conio/kbd.h"
#include "../../../lib/conio/ui.h"
#include "../vt_text.h"
#include "vt_attach.h"

FOREACH_A_PAN_ACT(GENERATE_VARS_VAL);
const struct menudialog_entry vt_anon_pan_captions_struct[] PROGMEM = {
	FOREACH_A_PAN_ACT(GENERATE_VAR_STRUCT)
	{
		P_NULL, NULL
	}
};

const struct menudialog_entry vt_anon_pan_options_captions_struct[] PROGMEM = {
	{
	    value_capt, VT_INFO_OPTS_CHANGEVAL
	},
	{
		P_NULL, NULL
	}
};

static int8_t __ui_query(FILE * file, struct anon_panel_setup * aps);
static int8_t __ui_query_info(FILE * file, uint8_t * opt);
static int8_t __ui_query_input(FILE * file, uint8_t * val, enum a_pan_acts * e_act );

static int8_t
__ui_query(FILE * file, struct anon_panel_setup * ap)
{
	int ret = 0;
	int res = 0;
    uint8_t data[5];
    memset(data, 0, 5);
	ret = draw_input_dialog(file, 10, 10, io_sel_id, data, 5);
	if (ret < 1)
    {
        return F_FAIL;
    }

    ap->io_x_id = (t_dev_id) atoi ( (const char *) data );

    memset(data, 0, 5);
	ret = draw_input_dialog(file, 10, 10, io_sel_id, data, 5);
	if (ret < 1)
    {
        return F_FAIL;
    }

    ap->io_y_id = (t_dev_id) atoi ( (const char *) data );

	return F_OK;
}

static int8_t
__ui_query_input(FILE * file, uint8_t * val, enum a_pan_acts * e_act )
{
	int ret = 0;
	int res = 0;
    uint8_t data[5];
    memset(data, 0, 5);
	ret = draw_input_dialog(file, 10, 10, anon_pan_sel, data, 5);
	if (ret < 1)
    {
        return F_FAIL;
    }

    *val = (uint8_t) atoi ( (const char *) data );

    ret = runmenu0_s(file, 10, 10, 0, 10, vt_anon_pan_captions_struct, &res);
	//try to create fd for the device
	if (ret == UI_NOTHING)
	{
		return F_FAIL;
	}
	*e_act = (uint8_t) res;

	return F_OK;
}

static int8_t
__ui_query_info(FILE * file, uint8_t * opt)
{
    int ret = 0;
	int res = 0;
	ret = runmenu0_s(file, 10, 10, 0, 10, vt_anon_pan_options_captions_struct, &res);
	//try to create fd for the device
	if (ret == UI_NOTHING)
	{
		return F_FAIL;
	}
	*opt = (uint8_t) res;

	return F_OK;
}

int8_t vt_attach_anonpan(FILE * file, struct anon_panel_setup * aps)
{
	int8_t res = 0;

	return __ui_query(file, aps);
}

int8_t vt_modify_anonpan(FILE * file, struct dev_descr_node * dd)
{
    return F_FAIL;
}

int8_t vt_info_anonpan(FILE * file, struct dev_descr_node * dd)
{
    uint8_t opt = 0;
    int8_t res = 0;

    res = __ui_query_info(file, &opt);
	ASSERT_DATA(res != F_OK, return F_FAIL);

    switch (opt)
    {

        case VT_INFO_OPTS_CHANGEVAL:
        {
            //we can use dd->instance but we need to make sure that record presents
            //show dialog with input
            uint8_t mask = 0;
            enum a_pan_acts e_act = 0;
            res = __ui_query_input(file, &mask, &e_act);
            ASSERT_DATA(res != F_OK, return F_FAIL);

            switch (e_act)
            {
                case A_PAN_ACT_ON:
                    res = a_panel_turn_on_single(dd, mask);
                break;

                case A_PAN_ACT_OFF:
                    res = a_panel_turn_off_single(dd, mask);
                break;

                case A_PAN_ACT_BLINK:
                    res = a_panel_turn_blink_single(dd, mask);
                break;

                default:
                    setSysError(SYSTEM_OPTION_UNKNOWN);
                return F_FAIL;
            }

            ASSERT_DATA(res == 1, return F_FAIL);

        //ToDo: complete this part

        }
        break;
    }



	return F_OK;
}
