/*-
 *	Copyright (c) 2014-2016	Alexander Morozov. www.nixd.org
 *	All rights reserved.
 *
 * LICENSE: This source file is subject to the New BSD license that is
 * available through the world-wide-web at the following URI:
 * http://www.opensource.org/licenses/bsd-license.php or in the root folder
 * of the project in file LICENSE.TXT.
 */

#if BUILD_WITH_SERIAL == YES

#include "../../build_defs.h"
#include <avr/io.h>
#include <stdio.h>
#include <string.h>
#include <stddef.h>
#include <stdbool.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stdint.h>
#include <limits.h>
#include <util/atomic.h>
#include "../../lib/nongnu/avr-gcc/crc16.h"
#include <avr/pgmspace.h>
#include <avr/interrupt.h>
#include <util/delay_basic.h>
#include <util/delay.h>
#include <avr/wdt.h>
#include "../../device/dev.h"
#include "../../hal.h"
#include "../../error.h"
#include "../../shared.h"
#include "../../lib/stream/stream.h"
#include "./usart.h"

#include "../../../os/kernel/kernel_process.h"
#include "../../../os/kernel/kernel_mutex.h"
#include "../../../os/kernel/kernel_isr.h"

struct usart_io * volatile usart_hw_arr_ptr[MAX_USART_PORTS] = {[ 0 ... MAX_USART_PORTS-1 ] = NULL};

static struct usart_io * __get_usart_io(struct dev_descr_node * ddn);

static int8_t usart_attach(struct usart_setup_struct * uss, struct dev_descr_node * ddn);
static int8_t usart_detach(struct dev_descr_node * ddn);
static int8_t usart_update(struct dev_descr_node * ddn, struct usart_setup_struct * uss);
static int8_t usart_query(struct dev_descr_node * ddn, struct hal_query_info * data0);
static int8_t usart_dump(struct dev_descr_node * ddn, void * data0, size_t * dsize0);
static int8_t usart_restore(struct dev_descr_node * ddn, void * data0, size_t * dsize0);
static int8_t __usart_bind_cts_rts_helper(enum ports port_rts,
                                            int8_t pin_rts_n,
                                            enum ports port_cts,
                                            int8_t pin_cts_n,
                                            t_dev_id dev_id);

static int8_t
__usart_bind_tx_rx_helper(t_dev_id dev_id, enum usart_dev e_usart_hw);

static int usart_static_file_write_sync(char var, FILE *stream);
static int usart_static_file_read_sync(FILE * stream);
static int usart_static_file_read_async(FILE * stream);
static int usart_static_file_write_async(char var, FILE *stream);

/*#if MAX_USART_PORTS >= 2
static int usart1_static_file_write_sync(char var, FILE *stream);
static int usart1_static_file_read_sync(FILE * stream);
static int usart1_static_file_read_async(FILE * stream);
static int usart1_static_file_write_async(char var, FILE *stream);
#endif // MAX_USART_PORTS

#if MAX_USART_PORTS >= 3
static int usart2_static_file_write_sync(char var, FILE *stream);
static int usart2_static_file_read_sync(FILE * stream);
static int usart2_static_file_read_async(FILE * stream);
static int usart2_static_file_write_async(char var, FILE *stream);
#endif // MAX_USART_PORTS

#if MAX_USART_PORTS >= 4
static int usart3_static_file_write_sync(char var, FILE *stream);
static int usart3_static_file_read_sync(FILE * stream);
static int usart3_static_file_read_async(FILE * stream);
static int usart3_static_file_write_async(char var, FILE *stream);
#endif // MAX_USART_PORTS
*/

void
usart_hw_init_helper(struct usart_hw_setup * hw_setup_ptr,
                     enum usart_hw_clock_pol clock_pol,
                     enum usart_hw_char_size char_size,
                     enum usart_hw_stop_bit stop_bit,
                     enum usart_hw_parity parity,
                     enum usart_hw_mode hw_mode)
{
	ASSERT_NULL_E(hw_setup_ptr, SYSTEM_NULL_ARGUMENT, usart_hw_init_helper_null_error);

	switch (clock_pol)
	{
		case CLOCK_POL_RF:
			hw_setup_ptr->ucsrc &= ~(1 << UCPOL);
		break;

		case CLOCK_POL_FR:
			hw_setup_ptr->ucsrc |= (1 << UCPOL);
		break;
	}

	switch (char_size)
	{
		case CHAR_SIZE_5BIT:
			hw_setup_ptr->ucsrc &= ~(1 << UCSZ1) & ~(1 << UCSZ0);
			hw_setup_ptr->ucsrb &= ~(1 << UCSZ2);
		break;

		case CHAR_SIZE_6BIT:
			hw_setup_ptr->ucsrc |= (1 << UCSZ0);
			hw_setup_ptr->ucsrc &= ~(1 << UCSZ1);
			hw_setup_ptr->ucsrb &= ~(1 << UCSZ2);
		break;

		case CHAR_SIZE_7BIT:
			hw_setup_ptr->ucsrc &= ~(1 << UCSZ0);
			hw_setup_ptr->ucsrc |= (1 << UCSZ1);
			hw_setup_ptr->ucsrb &= ~(1 << UCSZ2);
		break;

		case CHAR_SIZE_8BIT:
			hw_setup_ptr->ucsrc |= (1 << UCSZ1) | (1 << UCSZ0);
			hw_setup_ptr->ucsrb &= ~(1 << UCSZ2);
		break;

		case CHAR_SIZE_9BIT:
			hw_setup_ptr->ucsrc |= (1 << UCSZ1) | (1 << UCSZ0);
			hw_setup_ptr->ucsrb |= (1 << UCSZ2);
		break;
	}

	switch (stop_bit)
	{
		case STOP_BIT1:
			hw_setup_ptr->ucsrc &= ~(1 << USBS);
		break;

		case STOP_BIT2:
			hw_setup_ptr->ucsrc |= (1 << USBS);
		break;
	}

	switch (parity)
	{
		case PARITY_NONE:
			hw_setup_ptr->ucsrc &= ~(1 << UPM1) & ~(1 << UPM0);
		break;

		case PARITY_EVEN:
			hw_setup_ptr->ucsrc |= (1 << UPM1);
		break;

		case PARITY_ODD:
			hw_setup_ptr->ucsrc |= (1 << UPM0);
		break;
	}

	switch (hw_mode)
	{
		case MODE_ASYNC:
			hw_setup_ptr->ucsrc &= ~(1 << UMSEL1) & ~(1 << UMSEL0);
		break;

		case MODE_SYNC:
			hw_setup_ptr->ucsrc |= (1 << UMSEL0);
		break;

		case MODE_SPI:
			hw_setup_ptr->ucsrc |= (1 << UMSEL1) | (1 << UMSEL0);
		break;
	}

usart_hw_init_helper_null_error:

	return;
}

void
usart_hw_init_helper2(struct usart_hw_setup * hw_setup_ptr, uint8_t flags1)
{
	ASSERT_NULL_E(hw_setup_ptr, SYSTEM_NULL_ARGUMENT, usart_hw_init_helper2_null_error);

	if (flags1 & USART_DOUBLE_TXE_SPEED)
	{
		hw_setup_ptr->ucsra |= (1 << U2X);
	}
	else
	{
		hw_setup_ptr->ucsra &= ~(1 << U2X);
	}

	if (flags1 & USART_MULTIPROC_MODE)
	{
		hw_setup_ptr->ucsra |= (1 << MPCM);
	}
	else
	{
		hw_setup_ptr->ucsra &= ~(1 << MPCM);
	}

	if (flags1 & USART_ENABLE_TXE)
	{
		hw_setup_ptr->ucsrb |= (1 << TXEN);
	}
	else
	{
		hw_setup_ptr->ucsrb &= ~(1 << TXEN);
	}

	if (flags1 & USART_ENABLE_RXE)
	{
		hw_setup_ptr->ucsrb |= (1 << RXEN);
	}
	else
	{
		hw_setup_ptr->ucsrb &= ~(1 << RXEN);
	}

	if (flags1 & USART_RCV_INT)
	{
		hw_setup_ptr->ucsrb |= (1 << RXCIE);
	}
	else
	{
		hw_setup_ptr->ucsrb &= ~(1 << RXCIE);
	}

	if (flags1 & USART_TXE_INT)
	{
		hw_setup_ptr->ucsrb |= (1 << TXCIE);
	}
	else
	{
		hw_setup_ptr->ucsrb &= ~(1 << TXCIE);
	}

usart_hw_init_helper2_null_error:
	return;
}

void
usart_hw_init_helper3(struct usart_hw_setup * hw_setup_ptr, uint32_t baud)
{
	ASSERT_NULL_E(hw_setup_ptr, SYSTEM_NULL_ARGUMENT, usart_hw_init_helper3_null);

	hw_setup_ptr->ubrr = BAUD_END(baud, F_OSC);

usart_hw_init_helper3_null:
	return;
}

void
usart_hw_init_helperf(struct usart_setup_struct * hw_set_ptr,
                      struct usart_hw_setup * hw_setup_ptr,
                      enum usart_dev e_usart_hw,
                      enum usart_sw_RTopermode e_oper_mode,
                      enum usart_sw_buftype e_buf_type,
                      uint16_t ibs,
                      uint16_t obs)
{
    ASSERT_NULL_ER(hw_set_ptr, SYSTEM_NULL_ARGUMENT, );
    hw_set_ptr->hw_setup = hw_setup_ptr;
    hw_set_ptr->inc_buf_size = ibs;
    hw_set_ptr->out_buf_size = obs;
    hw_set_ptr->e_usart_hw = e_usart_hw;
    hw_set_ptr->tr_oper_mode = e_oper_mode;
    hw_set_ptr->e_sw_byftype = e_buf_type;

    return;
}

void usart_hw_init_helperio(struct usart_setup_struct * hw_set_ptr,
                            enum ports port_rts,
                            uint8_t pin_rts_n,
                            enum ports port_cts,
                            uint8_t pin_cts_n)
{
    ASSERT_NULL_ER(hw_set_ptr, SYSTEM_NULL_ARGUMENT, );
    hw_set_ptr->port_rts = port_rts;
    hw_set_ptr->pin_rts_n = pin_rts_n;
    hw_set_ptr->port_cts = port_cts;
    hw_set_ptr->pin_cts_n = pin_cts_n;
}

//static initialization should not be used
void usart_init()
{
	//statically initializing usart to communicate with PC
	//enable recv and tran
	UCSR0B = (1 << RXEN0) | (1 << TXEN0);
	//SET FRAME FORMAT TO 8DATA 1 STOP
	UCSR0C = (1 << UCSZ00) | (1 << UCSZ01);

	UBRR0L = BAUD_END(1000000UL, F_OSC);
	UBRR0H = (BAUD_END(1000000UL, F_OSC)>>8);

	UDR0 = 'A';

	return;
}

static void
isr_usart0_rx()
{
    struct usart_io * uio = usart_hw_arr_ptr[(uint8_t) DEV_USART0];
    if (uio == NULL)
    {
        return;
    }

    #if SERIAL_BUILD_WITH_BYTES_COUNTER == YES
        uio->rxbytes++;
    #endif // SERIAL_BUILD_WITH_BYTES_COUNTER

    uint8_t error = UCSR0A & 0x00011100;
    if (error == 0)
    {
        uint8_t data = UDR0;
        bTBf8(uio->rx_buf, data);
    }
    else
    {
        uio->rxerrors++;
    }

    return;
}

static void
isr_usart0_tx()
{
    struct usart_io * uio = usart_hw_arr_ptr[(uint8_t) DEV_USART0];
    if (uio == NULL)
    {
        return;
    }

    if (b_has_more(uio->tx_buf) == B_MORE)
    {
        #if SERIAL_BUILD_WITH_BYTES_COUNTER == YES
        uio->txbytes++;
        #endif // SERIAL_BUILD_WITH_BYTES_COUNTER

        uio->flags |= USART_TXE_STARTED;
        UDR0 = bRBf8(uio->tx_buf);
    }
    else
    {
        uio->flags &= ~USART_TXE_STARTED;
    }
}

#if MAX_USART_PORTS >= 2
static void
isr_usart1_rx()
{
    struct usart_io * uio = usart_hw_arr_ptr[(uint8_t) DEV_USART1];
    if (uio == NULL)
    {
        return;
    }

    #if SERIAL_BUILD_WITH_BYTES_COUNTER == YES
        uio->rxbytes++;
    #endif // SERIAL_BUILD_WITH_BYTES_COUNTER

    uint8_t error = UCSR1A & 0x00011100;
    if (error == 0)
    {
        uint8_t data = UDR1;

        bTBf8(uio->rx_buf, data);
    }
    else
    {
        uio->rxerrors++;
    }

    return;
}

static void
isr_usart1_tx()
{
    struct usart_io * uio = usart_hw_arr_ptr[(uint8_t) DEV_USART1];
    if (uio == NULL)
    {
        return;
    }

    if (b_has_more(uio->tx_buf) == B_MORE)
    {
        #if SERIAL_BUILD_WITH_BYTES_COUNTER == YES
        uio->txbytes++;
        #endif // SERIAL_BUILD_WITH_BYTES_COUNTER

        uio->flags |= USART_TXE_STARTED;
        UDR1 = bRBf8(uio->tx_buf);
    }
    else
    {
        uio->flags &= ~USART_TXE_STARTED;
    }
}

#endif // MAX_USART_PORTS

#if MAX_USART_PORTS >= 3
static void
isr_usart2_rx()
{
    struct usart_io * uio = usart_hw_arr_ptr[(uint8_t) DEV_USART2];
    if (uio == NULL)
    {
        return;
    }

    #if SERIAL_BUILD_WITH_BYTES_COUNTER == YES
        uio->rxbytes++;
    #endif // SERIAL_BUILD_WITH_BYTES_COUNTER

    uint8_t error = UCSR2A & 0x00011100;
    if (error == 0)
    {
        uint8_t data = UDR2;

        bTBf8(uio->rx_buf, data);
    }
    else
    {
        uio->rxerrors++;
    }

    return;
}

static void
isr_usart2_tx()
{
    struct usart_io * uio = usart_hw_arr_ptr[(uint8_t) DEV_USART2];
    if (uio == NULL)
    {
        return;
    }

    if (b_has_more(uio->tx_buf) == B_MORE)
    {
        #if SERIAL_BUILD_WITH_BYTES_COUNTER == YES
        uio->txbytes++;
        #endif // SERIAL_BUILD_WITH_BYTES_COUNTER

        uio->flags |= USART_TXE_STARTED;
        UDR2 = bRBf8(uio->tx_buf);
    }
    else
    {
        uio->flags &= ~USART_TXE_STARTED;
    }
}

#endif // MAX_USART_PORTS

#if MAX_USART_PORTS >= 4
static void
isr_usart3_rx()
{
    struct usart_io * uio = usart_hw_arr_ptr[(uint8_t) DEV_USART3];
    if (uio == NULL)
    {
        return;
    }

    #if SERIAL_BUILD_WITH_BYTES_COUNTER == YES
        uio->rxbytes++;
    #endif // SERIAL_BUILD_WITH_BYTES_COUNTER

    uint8_t error = UCSR3A & 0x00011100;
    if (error == 0)
    {
        uint8_t data = UDR3;

        bTBf8(uio->rx_buf, data);
    }
    else
    {
        uio->rxerrors++;
    }

    return;
}

static void
isr_usart3_tx()
{
    struct usart_io * uio = usart_hw_arr_ptr[(uint8_t) DEV_USART3];
    if (uio == NULL)
    {
        return;
    }

    if (b_has_more(uio->tx_buf) == B_MORE)
    {
        #if SERIAL_BUILD_WITH_BYTES_COUNTER == YES
        uio->txbytes++;
        #endif // SERIAL_BUILD_WITH_BYTES_COUNTER

        uio->flags |= USART_TXE_STARTED;
        UDR3 = bRBf8(uio->tx_buf);
    }
    else
    {
        uio->flags &= ~USART_TXE_STARTED;
    }
}

#endif // MAX_USART_PORTS


static struct usart_io *
__get_usart_io(struct dev_descr_node * ddn)
{
	uint8_t i = 0;
	for (; i < MAX_USART_PORTS; i++)
	{
		if (usart_hw_arr_ptr[i] == NULL) continue;

		if (usart_hw_arr_ptr[i]->ddn == ddn)
		{
			return usart_hw_arr_ptr[i];
		}
	}

	return NULL;
}

int8_t usart_hal_message(struct dev_descr_node * ddn,
                          enum dd_event e_event,
                          void * data0,
                          size_t * dsize0)
{
    int8_t ret = 0;

    switch (e_event)
    {
        case DD_EVENT_ATTACH:
            ret = usart_attach((struct usart_setup_struct *) data0, ddn);
        break;

        case DD_EVENT_DETACH:
            ret = usart_detach(ddn);
        break;

        case DD_EVENT_UPDATE:
            ret = usart_update(ddn, (struct usart_setup_struct *) data0);
        break;

        case DD_DUMP:
            ret = usart_dump(ddn, data0, dsize0);
        break;

        case DD_RESTORE:
            ret = usart_restore(ddn, data0, dsize0);
        break;

        case DD_QUERY:
            ret = usart_query(ddn, (struct hal_query_info *) data0);
        break;

        case DD_ISRSIGNAL:
            ret = K_MSG_ERROR;
        break;

        case DD_RESET:
            ret = K_MSG_ERROR;
        break;

        case DD_DROP:
            ret = K_MSG_ERROR;
        break;

        case DD_FLUSH:
        {
           /* struct devent_flush * df = (struct devent_flush *) data0;*/
            struct usart_io * uio = usart_get_descr_by_did(ddn->dev_id);
            ret = usart_flush(uio);
        }
        break;
    }

    return ret;
}

int8_t usart_kstream_init(struct dev_descr_node * ddn, struct kstream * ks)
{
/* ks->fid_locked = K_NO_FD;

    //create stream
    struct usart_io * uio = __get_usart_io(fd);
    ASSERT_NULL_E(uio, SYSTEM_DEVICE_NOT_FOUND, error);

    ks->loc_instance = (void *)usart_hw_arr_ptr[(uint8_t) uio->dev_d];

    if (uio->RT_oper_mode == USART_BUFFERED)
    {   //BUFFERED
        if (ks->flags & SWRITE)
        {
            ks->KSWRITE = usart_static_write_async;
            ks->KSWINIT = usart_static_buffer_init;
            ks->KSWACK  = usart_static_buffer_finish;
            ks->KSWLEN  = usart_static_write_length;
        }

        if (ks->flags & SREAD)
        {
            ks->KSRBUF  = usart_static_get_rbuf;
            ks->KSREAD  = usart_static_read_async;
            ks->KSRLEN  = usart_static_read_length;
        }

        ks->KSDROP  = __drop_frame;
        ks->KSFLUSH = __flush_buffer;


        ks->flags |= SASYNC;

    }
    else
    {   //NOT BUFFERED
        if (ks->flags & SWRITE)
        {
            ks->KSWRITE = usart_static_write;
            ks->KSWINIT = usart_static_pipe_init_dummy;
            ks->KSWACK  = usart_static_pipe_init_dummy;
            ks->KSWLEN  = usart_static_write_length_sync;
        }

        if (ks->flags & SREAD)
        {
            ks->KSRBUF      = usart_static_get_rbuf_dummy;
            ks->KSREAD      = usart_static_read;
            ks->KSRLEN      = usart_static_read_length_sync;
            ks->KSREADARR   = usart_static_read_arr_sync;
        }

        ks->KSDROP  = __drop_frame;
        ks->KSFLUSH = __flush_buffer;

        ks->flags &= SSYNC;

    }


    return 0;

error:*/
    return 1;
}

int8_t
usart_stream_init(struct dev_descr_node * ddn, FILE * file)
{
    struct usart_io * uio = (struct usart_io *)ddn->instance; //usart_get_descr_by_did(ddn->dev_id);
    ASSERT_NULL_ER(uio, SYSTEM_ITEM_NOT_FOUND, 1);

    if (file->get != NULL)
    {
        //file->get = usart0_static_file_read_sync;
        if (uio->rx_buf == NULL)
        {
            file->get = usart_static_file_read_sync;
        }
        else
        {
            file->get = usart_static_file_read_async;
        }
        file->flags |= __SRD;
    }

    if (file->put != NULL)
    {
        if (uio->tx_buf == NULL)
        {
            file->put = usart_static_file_write_sync;
        }
        else
        {
            file->put = usart_static_file_write_async;
        }
        file->flags |= __SWR;
    }

    file->udata = ddn->instance;

    /*switch (uio->dev_d)
    {
        case DEV_USART0:
            if (file->get != NULL)
            {
                //file->get = usart0_static_file_read_sync;
                if (uio->rx_buf == NULL)
                {
                    file->get = usart0_static_file_read_sync;
                }
                else
                {
                    file->get = usart0_static_file_read_async;
                }
                file->flags |= __SRD;
            }

            if (file->put != NULL)
            {
                file->put = usart0_static_file_write_sync;
                file->flags |= __SWR;
            }
        break;

        case DEV_USART1:
            if (file->get != NULL)
            {
                //file->get = usart0_static_file_read_sync;
                if (uio->rx_buf == NULL)
                {
                    file->get = usart1_static_file_read_sync;
                }
                else
                {
                    file->get = usart1_static_file_read_async;
                }
                file->flags |= __SRD;
            }

            if (file->put != NULL)
            {
                file->put = usart1_static_file_write_sync;
                file->flags |= __SWR;
            }
        break;

        case DEV_USART2:
            if (file->get != NULL)
            {
                //file->get = usart0_static_file_read_sync;
                if (uio->rx_buf == NULL)
                {
                    file->get = usart2_static_file_read_sync;
                }
                else
                {
                    file->get = usart2_static_file_read_async;
                }
                file->flags |= __SRD;
            }

            if (file->put != NULL)
            {
                file->put = usart2_static_file_write_sync;
                file->flags |= __SWR;
            }
        break;

        case DEV_USART3:
            if (file->get != NULL)
            {
                //file->get = usart0_static_file_read_sync;
                if (uio->rx_buf == NULL)
                {
                    file->get = usart3_static_file_read_sync;
                }
                else
                {
                    file->get = usart3_static_file_read_async;
                }
                file->flags |= __SRD;
            }

            if (file->put != NULL)
            {
                file->put = usart3_static_file_write_sync;
                file->flags |= __SWR;
            }
        break;
    }*/

    return 0;
}

static int8_t
__usart_bind_tx_rx_helper(t_dev_id dev_id, enum usart_dev e_usart_hw)
{
    return hal_hw_bind_pin(dev_id,
                           pgm_read_byte(&(usart_pins[(uint8_t) e_usart_hw].e_dev_port)),
                           pgm_read_byte(&(usart_pins[(uint8_t) e_usart_hw].pins))
                           );
}
static int8_t
__usart_bind_cts_rts_helper(enum ports port_rts,
                            int8_t pin_rts_n,
                            enum ports port_cts,
                            int8_t pin_cts_n,
                            t_dev_id dev_id)
{
    uint8_t ret = 0;

     //bind RTX and/or CTS
    if (port_cts == port_rts)
    {
        if ((pin_cts_n == -1) && (pin_rts_n == -1))
        {
            return 0;
        }

        uint8_t portfctrl = 0;
        if (pin_cts_n != -1)
        {
            portfctrl |= (1 << pin_cts_n);
        }
        if (pin_rts_n != -1)
        {
            portfctrl |= (1 << pin_rts_n);
        }
        ret = hal_hw_bind_pin(dev_id, port_cts, portfctrl);
        ASSERT_DATA(ret == 1, return K_NO_DD);
    }
    else
    {
        if (pin_cts_n != -1)
        {
            ret = hal_hw_bind_pin(dev_id, port_cts, (1 << pin_cts_n));
            ASSERT_DATA(ret == 1, return K_NO_DD);
        }
        if (pin_rts_n != -1)
        {
            ret = hal_hw_bind_pin(dev_id, port_rts, (1 << pin_rts_n));
            ASSERT_DATA(ret == 1, return K_NO_DD);
        }
    }

    return 0;

}

//tr_oper_mode is used only when hw_setup is was intentionaly set to NULL
static int8_t
usart_attach(struct usart_setup_struct * uss, struct dev_descr_node * ddn)
{
    ASSERT_NULL_ER(uss, SYSTEM_NULL_ARGUMENT, 1);
    ASSERT_DATA(uss->e_usart_hw >= MAX_USART_PORTS, setSysError(SYSTEM_UNKNOWN_ID_OF_IO_HW); goto error_leave);

	//set default if no setup provided
	if (uss->hw_setup == NULL)
	{
		struct usart_hw_setup def_setup;
		def_setup.ucsra = 0;
		if (uss->tr_oper_mode == USART_NON_BUFFERED)
		{
			def_setup.ucsrb = (1 << RXEN0) | (1 << TXEN0);
		}
		else
		{
			def_setup.ucsrb = (1 << RXEN0) | (1 << TXEN0) | (1 << RXCIE0) | (1 << TXCIE0);
		}

		def_setup.ucsrc = (1 << UCSZ00) | (1 << UCSZ01); //SET FRAME FORMAT TO 8DATA 1 STOP
		def_setup.ubrr = BAUD_END(UART_BAUD_RATE, F_OSC);
		uss->hw_setup = &def_setup;
	}

	//check if usart device already in use
	if (usart_hw_arr_ptr[(uint8_t) uss->e_usart_hw] != NULL)
	{
		setSysError(SYSTEM_DEVICE_BUSY);
		return K_NO_DD;
	}

	//bind pins, if already busy (one or many) return fail
	//uint8_t ret = hal_hw_bind_pin(ddn->dev_id, pgm_read_byte(&(usart_pins[(uint8_t) uss->e_usart_hw].e_dev_port)), pgm_read_byte(&(usart_pins[(uint8_t) uss->e_usart_hw].pins)) );
	uint8_t ret = __usart_bind_tx_rx_helper(ddn->dev_id, uss->e_usart_hw);
	ASSERT_DATA(ret == 1, return K_NO_DD);


    //bind RTX and/or CTS

    ret = __usart_bind_cts_rts_helper(uss->port_rts, uss->pin_rts_n, uss->port_cts, uss->pin_cts_n, ddn->dev_id);
    ASSERT_DATA(ret == 1, return K_NO_DD);

	//create record
	struct usart_io * u_dev_desc = (struct usart_io *) kernel_malloc(sizeof(struct usart_io));
	ASSERT_NULL_E(u_dev_desc, SYSTEM_MALLOC_FAILED, malloc_failed);

	//setting instance to ddn
    ddn->instance = (void *) u_dev_desc;

    if (uss->tr_oper_mode != USART_NON_BUFFERED)
	{
	    /*enum buffer_type e_bft;
	    switch (uss->e_sw_byftype)
	    {
            case USART_BUF_LINEAR:
                e_bft = BUF_TYPE_LINEAR;
            break;

            case USART_BUF_CIRCULAR:
                e_bft = USART_BUF_CIRCULAR;
            break;
	    }*/

	    switch (uss->tr_oper_mode)
	    {
            case USART_BUFFERED_RX:
                u_dev_desc->rx_buf = b_create(NULL, uss->e_sw_byftype, NULL, uss->inc_buf_size);
                ASSERT_NULL(u_dev_desc->rx_buf, error);
                uss->hw_setup->ucsrb |= (1 << RXCIE0);
#if DEBUG_MESSAGES == YES
    printf_P(PSTR("attaching isr\r\n"));
#endif // DEBUG_MESSAGES
            break;

            case USART_BUFFERED_TX:
                u_dev_desc->tx_buf = b_create(NULL, uss->e_sw_byftype, NULL, uss->out_buf_size);
                ASSERT_NULL(u_dev_desc->tx_buf, error);
                uss->hw_setup->ucsrb |= (1 << TXCIE0);
            break;

            case USART_BUFFERED_BOTH:
                u_dev_desc->rx_buf = b_create(NULL, uss->e_sw_byftype, NULL, uss->inc_buf_size);
                ASSERT_NULL(u_dev_desc->rx_buf, error);
                u_dev_desc->tx_buf = b_create(NULL, uss->e_sw_byftype, NULL, uss->out_buf_size);
                if (u_dev_desc->tx_buf == NULL)
                {
                    b_free(u_dev_desc->tx_buf);
                    goto error;
                }
                uss->hw_setup->ucsrb |= (1 << RXCIE0) | (1 << TXCIE0);
            break;
	    }

	    switch (uss->e_usart_hw)
        {
            case DEV_USART0:
                if (uss->hw_setup->ucsrb & (1 << RXCIE0))
                {
                    kernel_isr_grab(USART0_RX_vect_num, isr_usart0_rx, ISR_NORMAL);
                }

                if (uss->hw_setup->ucsrb & (1 << TXCIE0))
                {
                    kernel_isr_grab(USART0_TX_vect_num, isr_usart0_tx, ISR_NORMAL);
                }
            break;
#if MAX_USART_PORTS >= 2
            case DEV_USART1:
                if (uss->hw_setup->ucsrb & (1 << RXCIE1))
                {
                    kernel_isr_grab(USART1_RX_vect_num, isr_usart1_rx, ISR_NORMAL);
                }

                if (uss->hw_setup->ucsrb & (1 << TXCIE1))
                {
                    kernel_isr_grab(USART1_TX_vect_num, isr_usart1_tx, ISR_NORMAL);
                }
            break;
#endif // MAX_USART_PORTS
#if MAX_USART_PORTS >= 3
            case DEV_USART2:
                if (uss->hw_setup->ucsrb & (1 << RXCIE2))
                {
                    kernel_isr_grab(USART2_RX_vect_num, isr_usart2_rx, ISR_NORMAL);
                }

                if (uss->hw_setup->ucsrb & (1 << TXCIE2))
                {
                    kernel_isr_grab(USART2_TX_vect_num, isr_usart2_tx, ISR_NORMAL);
                }
            break;
#endif // MAX_USART_PORTS
#if MAX_USART_PORTS >= 4
            case DEV_USART3:
                if (uss->hw_setup->ucsrb & (1 << RXCIE3))
                {
                    kernel_isr_grab(USART3_RX_vect_num, isr_usart3_rx, ISR_NORMAL);
                }

                if (uss->hw_setup->ucsrb & (1 << TXCIE3))
                {
                    kernel_isr_grab(USART3_TX_vect_num, isr_usart3_tx, ISR_NORMAL);
                }
            break;
#endif // MAX_USART_PORTS
        }
	}

    //setting values to the fields of the struct
	u_dev_desc->dev_d = uss->e_usart_hw;
	u_dev_desc->ddn = ddn;
	u_dev_desc->RT_oper_mode = uss->tr_oper_mode;
    u_dev_desc->flags = 0;
    u_dev_desc->pin_cts_n = uss->pin_cts_n;
    u_dev_desc->pin_rts_n = uss->pin_rts_n;
    u_dev_desc->port_cts = uss->port_cts;
    u_dev_desc->port_rts = uss->port_rts;

    //setting instance
	usart_hw_arr_ptr[(uint8_t) uss->e_usart_hw] = u_dev_desc;

	//initializing hardware
    //there is no need to lock any mutexes because uscr* normally is not accessed from outside
    //of this module
    const struct usart_hwaddr * dev_hw_ptr = &s_usart_addr[(uint8_t) uss->e_usart_hw];
    _HAL_WREG8( pgm_read_word( &(dev_hw_ptr->uscra)), uss->hw_setup->ucsra);
    _HAL_WREG8(pgm_read_word( &(dev_hw_ptr->uscrb)), uss->hw_setup->ucsrb);
    _HAL_WREG8(pgm_read_word( &(dev_hw_ptr->uscrc)), uss->hw_setup->ucsrc);
    _HAL_WREG16(pgm_read_word( &(dev_hw_ptr->ubrr)), uss->hw_setup->ubrr);


	return 0;

error:
    kernel_free((void *) u_dev_desc);

malloc_failed:
	//unbind pins
	hal_hw_unbind_dd_all_pin(ddn->dev_id);
	//hal_hw_unbind_pin(uss->ddn->dev_id, pgm_read_byte(&(usart_pins[(uint8_t) uss->e_usart_hw].e_dev_port)), pgm_read_byte(&(usart_pins[(uint8_t) uss->e_usart_hw].pins)) );
	usart_hw_arr_ptr[(uint8_t) uss->e_usart_hw] = NULL;

error_leave:
	return K_NO_DD;
}

static int8_t
usart_detach(struct dev_descr_node * ddn)
{
    ASSERT_NULL_ER(ddn, SYSTEM_NULL_ARGUMENT, 1);

    //check if usart device was not used previously, then return with error
	struct usart_io * uio = usart_get_descr_by_did(ddn->dev_id);

    if (uio == NULL)
	{
		setSysError(SYSTEM_DEVICE_NOT_INITED);
		return K_NO_DD;
	}

	//checkif instances match
	if (ddn->instance != uio)
    {
        //memory corruption
        setSysError(SYSTEM_MEMORY_CORUPTION);
    }

    //reset hardware
    const struct usart_hwaddr * dev_hw_ptr = &s_usart_addr[(uint8_t) uio->dev_d];
    _HAL_WREG8( pgm_read_word( &(dev_hw_ptr->uscra)), 0);
    _HAL_WREG8(pgm_read_word( &(dev_hw_ptr->uscrb)), 0);
    _HAL_WREG8(pgm_read_word( &(dev_hw_ptr->uscrc)), 0);
    _HAL_WREG16(pgm_read_word( &(dev_hw_ptr->ubrr)), 0);

    //unbind pins
    hal_hw_unbind_dd_all_pin(ddn->dev_id);

    //deallocate buffers and record
    b_free(uio->rx_buf);
    b_free(uio->tx_buf);

    usart_hw_arr_ptr[(uint8_t) uio->dev_d] = NULL;

    kernel_free((void*) uio);


    return 0;
}

static int8_t
usart_query(struct dev_descr_node * ddn, struct hal_query_info * data0)
{
    FILE * strm = (FILE *)data0->data0;

    #if SERIAL_BUILD_WITH_TEXT_QUERY == YES

    struct usart_io * uio = usart_get_descr_by_did(ddn->dev_id);
    if (uio == NULL)
    {
        fprintf_P(strm, PSTR("Unable to find usart_io rec for fid=%d \r\n"), ddn->dev_id);
        return 1;
    }
    else
    {
        const struct usart_hwaddr * dev_hw_ptr = &s_usart_addr[(uint8_t) uio->dev_d];
        fprintf_P(strm, PSTR("USART %d: -> %p\r\n"), uio->dev_d, uio);
        fprintf_P(strm, PSTR("	->DID=%i DD: %p\r\n"), uio->ddn->dev_id, uio->ddn);
        fprintf_P(strm, PSTR(" ->UCSRA=%d UCSRB=%d UCSRC=%d UBRR=%u\r\n"), _SFR_MEM8( pgm_read_word( &(dev_hw_ptr->uscra))), _SFR_MEM8( pgm_read_word( &(dev_hw_ptr->uscrb)))
                 , _SFR_MEM8( pgm_read_word( &(dev_hw_ptr->uscrc))), _SFR_MEM8( pgm_read_word( &(dev_hw_ptr->ubrr))));
        fprintf_P(strm, PSTR(" ->buffered=%d\r\n"), uio->RT_oper_mode);

        if (uio->rx_buf != NULL)
        {
            fprintf_P(strm, PSTR("RCV Buffer: %p\r\n"), uio->rx_buf);
            fprintf_P(strm, PSTR("start: %p end: %p read: %p write: %p flags: %d\r\n"), uio->rx_buf->buf_start,
                      uio->rx_buf->buf_end, uio->rx_buf->read_ptr, uio->rx_buf->write_ptr, uio->rx_buf->flags);
            #if SERIAL_BUILD_WITH_BYTES_COUNTER == YES
            fprintf_P(strm, PSTR("received bytes: %d\r\n"), uio->rxbytes);
            fprintf_P(strm, PSTR("received errors: %d\r\n"), uio->rxerrors);
            #endif // SERIAL_BUILD_WITH_BYTES_COUNTER
        }

        if (uio->tx_buf != NULL)
        {
            fprintf_P(strm, PSTR("TXE Buffer: %p\r\n"), uio->tx_buf);
            fprintf_P(strm, PSTR("start: %p end: %p read: %p write: %p flags: %d\r\n"), uio->rx_buf->buf_start,
                      uio->rx_buf->buf_end, uio->rx_buf->read_ptr, uio->rx_buf->write_ptr, uio->rx_buf->flags);
            #if SERIAL_BUILD_WITH_BYTES_COUNTER == YES
            fprintf_P(strm, PSTR("transmited bytes: %d\r\n"), uio->txbytes);
            #endif // SERIAL_BUILD_WITH_BYTES_COUNTER
        }
    }
    #else
        fprintf_P(strm, PSTR("SERIAL was built without query! \r\n"));
    #endif // SERIAL_BUILD_WITH_TEXT_QUERY
    return 0;
}

static int8_t
usart_update(struct dev_descr_node * ddn, struct usart_setup_struct * uss)
{
    ASSERT_NULL_ER(uss, SYSTEM_NULL_ARGUMENT, K_NO_DD);
    ASSERT_DATA(uss->e_usart_hw >= MAX_USART_PORTS, setSysError(SYSTEM_UNKNOWN_ID_OF_IO_HW); goto error_leave);

    //The initial idea was to simply update values, but it will be safer to deallocate everything and
    //create new instance again!

	//check if usart device was not used previously, then return with error
	if (usart_hw_arr_ptr[(uint8_t) uss->e_usart_hw] == NULL)
	{
		setSysError(SYSTEM_DEVICE_NOT_INITED);
		return K_NO_DD;
	}

    int8_t ret = usart_detach(ddn); //the dd in HAL will remain the same, only instance will be updated
    if (ret != 0)
    {
        //something happened during deinitialization!
        //ToDo: add handler
        //for now: anyway, try to attach
    }

    ret = usart_attach(uss, ddn); //now attach to the dd node once again with new params and bind hardware
    if (ret != 0)
    {
        //my congratulations! the initialization failed and now 100% it require hard reboot and bug hunting
        goto error_leave;
    }

    return 0;

error_leave:
	return K_NO_DD;
}

static int8_t
usart_dump(struct dev_descr_node * ddn, void * data0, size_t * dsize0)
{
    struct usart_io * uio = usart_get_descr_by_did(ddn->dev_id);
    if (uio == NULL)
    {
        setSysError(SYSTEM_ITEM_NOT_FOUND);
        return K_NO_DD;
    }

    //prepare config and write to data0
    //make sure that your config is less or equal to dsize0, otherwise you will corrupt memory
    if ((*dsize0) < sizeof(struct usart_io_eeprom_envilope))
    {
        setSysError(SYSTEM_EEPROM_PAGE_EXCEED);
        return K_NO_DD;
    }

    struct usart_io_eeprom_envilope * envlp = (struct usart_io_eeprom_envilope *) data0;
    envlp->pin_cts_n = uio->pin_cts_n;
    envlp->pin_rts_n = uio->pin_rts_n;
    envlp->port_rts = uio->port_rts;
    envlp->port_cts = uio->port_cts;

    const struct usart_hwaddr * dev_hw_ptr = &s_usart_addr[(uint8_t) uio->dev_d];

    envlp->reg_ubrr = _HAL_RREG16(pgm_read_word( &(dev_hw_ptr->ubrr)));
    envlp->reg_ucsra = _HAL_RREG8(pgm_read_word( &(dev_hw_ptr->uscra)));
    envlp->reg_ucsrb = _HAL_RREG8(pgm_read_word( &(dev_hw_ptr->uscrb)));
    envlp->reg_ucsrc = _HAL_RREG8(pgm_read_word( &(dev_hw_ptr->uscrc)));

    envlp->dev_d = uio->dev_d;
    envlp->RT_oper_mode = uio->RT_oper_mode;
    envlp->buf_types = 0;

    if (uio->tx_buf != NULL)
    {
        envlp->out_buf_size = b_get_buf_len(uio->tx_buf);
        envlp->buf_types = uio->tx_buf->flags;
    }
    else
    {
        envlp->out_buf_size = 0;
    }

    if (uio->rx_buf != NULL)
    {
        envlp->inc_buf_size = b_get_buf_len(uio->rx_buf);
        envlp->buf_types = uio->rx_buf->flags;
    }
    else
    {
        envlp->inc_buf_size = 0;
    }


    return 0;
}

static int8_t
usart_restore(struct dev_descr_node * ddn, void * data0, size_t * dsize0)
{
    struct usart_io_eeprom_envilope * envlp = (struct usart_io_eeprom_envilope *) data0;

    //check if instance already restored (normally this should not happen)
    if (usart_hw_arr_ptr[(uint8_t) envlp->dev_d] != NULL)
    {
        setSysError(SYSTEM_DEVICE_ALREADY_INITED);
        return K_NO_DD;
    }

    //restore instance by calling intialization handler
    struct usart_setup_struct uss_setup;
    struct usart_hw_setup def_setup;
    def_setup.ubrr = envlp->reg_ubrr;
    def_setup.ucsra = envlp->reg_ucsra;
    def_setup.ucsrb = envlp->reg_ucsrb;
    def_setup.ucsrc = envlp->reg_ucsrc;
    usart_hw_init_helperf(&uss_setup,
                          &def_setup,
                          envlp->dev_d,
                          envlp->RT_oper_mode,
                          ((envlp->buf_types & 0x03) == BUF_FLAG_CIRCULAR) ? USART_BUF_CIRCULAR : USART_BUF_LINEAR,
                          envlp->inc_buf_size,
                          envlp->out_buf_size);
    usart_hw_init_helperio(&uss_setup, envlp->port_rts, envlp->pin_rts_n, envlp->port_cts, envlp->pin_cts_n);

#if DEBUG_MESSAGES == YES
    printf_P(PSTR("attaching a:%d b:%d c:%d dev_d:%d rt_mode: %d buf_t:%d rx:%d tx:%d\r\n"), envlp->reg_ucsra, envlp->reg_ucsrb, envlp->reg_ucsrc, envlp->dev_d,envlp->RT_oper_mode, (envlp->buf_types & 0x03), envlp->inc_buf_size, envlp->out_buf_size);
#endif // DEBUG_MESSAGES
    def_setup.ucsra &= (0x00000011);
    return usart_attach(&uss_setup, ddn);

}

/*int8_t usart_kernel_data(const struct fd_dev_node * fd, struct kstream * stream, int8_t trans_id, uint8_t len)
{
	if (len == 0)
	{
	    kslock(stream, fd->dev_fd);     //lock
	    ksinitack(stream);              //init crc
		kswrite8(stream, fd->dev_fd);   //FID
		kswrite8(stream, trans_id);     //transaction id
		kswrite16(stream, 4);           //length
		kswrite8(stream, fd->dev_fd);   //[1]
		kswrite8(stream, fd->e_dev);    //[1]
		kswrite16(stream, fd->d_hndl);  //[2]
        ksfinishack(stream);            //finish crc
		ksflush(stream);                //flush data
		ksunlock(stream, fd->dev_fd);   //unlock

		return 0;
	}

	return 0;
}*/

struct usart_io *
usart_get_descr(enum usart_dev u_dev_i)
{
		return (const struct usart_io *) usart_hw_arr_ptr[(uint8_t) u_dev_i];
}

struct usart_io *
usart_get_descr_by_did(t_dev_id did)
{
    for (uint8_t i = 0; i < MAX_USART_PORTS; i++)
    {
        if ( (usart_hw_arr_ptr[i] != NULL) && (usart_hw_arr_ptr[i]->ddn->dev_id == did) )
        {
            return usart_hw_arr_ptr[i];
        }
    }

    return NULL;
}

//async USART IO
int8_t
usart_flush(struct usart_io * s_usart_ptr)
{
    //check record
	ASSERT_NULL_ER(s_usart_ptr, SYSTEM_NULL_ARGUMENT, K_MSG_ERROR)

    if (s_usart_ptr->tx_buf == NULL)
    {
        return K_MSG_ERROR;
    }

    //check if the transmission is still in progress
	if (s_usart_ptr->flags & USART_TXE_STARTED)
    {
        return K_MSG_ERROR;
    }

    //check if there is something to read
    if (b_has_more(s_usart_ptr->tx_buf) == B_MORE)
    {
        b_reset_for_reading(s_usart_ptr->tx_buf);

        //setting flag
        s_usart_ptr->flags |= USART_TXE_STARTED;

        //sending data to UDR
        const struct usart_hwaddr * dev_hw_ptr = &s_usart_addr[(uint8_t) s_usart_ptr->dev_d];

        _SFR_MEM8( pgm_read_word( &(dev_hw_ptr->udr))) = bRBf8(s_usart_ptr->tx_buf);
    }

	return 0;
}

void usart_drop_frame(struct usart_io * s_usart_ptr)
{
	ASSERT_NULL(s_usart_ptr, usart_drop_frame_null);


usart_drop_frame_null:
	return;
}

enum rts_f
{
    RTS_CLEAR = (uint8_t) 0,
    RTS_BUSY = (uint8_t) 1
};

static int8_t
usart_set_rts(struct usart_io * s_usart_ptr, enum rts_f e_rts)
{
    if (s_usart_ptr->pin_rts_n != -1)
    {
        MUTEX_BLOCK(mtx_giant_io, MUTEX_ACTION_MANUAL)
        {
            if ( MUTEX_BLOCK_VAR_RET == MUTEX_LOCK_FAILED )
            {
                //lock failed
                return (-1);
            }
            uint16_t p_port = hal_hw_get_addr_port(s_usart_ptr->port_rts);
            uint8_t temp = _HAL_RREG8(p_port);              //_KRN_RREG8(p_port); //i.e 0000 0001
            if (e_rts == RTS_CLEAR)
            {
                temp = temp & ~(1 << s_usart_ptr->pin_rts_n);   //reset bits by mask (i.e 10100001 & 11111110 = 10100000)
            }
            else
            {
                temp = temp & (1 << s_usart_ptr->pin_rts_n);   //reset bits by mask (i.e 10100000 & 00000001 = 10100001)
            }

            _HAL_WREG8(p_port, temp);
        }
    }

    return 0;
}

int16_t
usart_sync_receive_byte(struct usart_io * s_usart_ptr)
{
	if ( s_usart_ptr == NULL) return 0;

    if (usart_set_rts(s_usart_ptr, RTS_CLEAR) != 0)
    {
        return (-1);
    }

	uint8_t data = 0, status = 0;
	uint8_t retry = 0;
	const struct usart_hwaddr * dev_hw_ptr = &s_usart_addr[(uint8_t) s_usart_ptr->dev_d];

	register uint16_t reg_io = pgm_read_word( &(dev_hw_ptr->uscra));
	register uint8_t bit_rxc = pgm_read_byte( &(dev_hw_ptr->rxc));

	if ( !(_SFR_MEM8( reg_io ) & (1 << bit_rxc)) )
    {
        return EOF;
    }

	status = _SFR_MEM8( reg_io );

	reg_io = pgm_read_word( &(dev_hw_ptr->udr) );
	data = _SFR_MEM8( reg_io );

    usart_set_rts(s_usart_ptr, RTS_BUSY);

	return(data);

}

int8_t usart_sync_check_rcv(struct usart_io * s_usart_ptr)
{
    if ( s_usart_ptr == NULL) return 0;

	uint8_t data = 0, status = 0;
	uint8_t retry = 0;
	const struct usart_hwaddr * dev_hw_ptr = &s_usart_addr[(uint8_t) s_usart_ptr->dev_d];

	register uint16_t reg_io = pgm_read_word( &(dev_hw_ptr->uscra));
	register uint8_t bit_rxc = pgm_read_byte( &(dev_hw_ptr->rxc));

	if ( (_SFR_MEM8( reg_io ) & (1 << bit_rxc)) )
    {
        return 1;
    }

	return 0;
}

static int8_t
usart_check_cts(struct usart_io * s_usart_ptr)
{
    if (s_usart_ptr->pin_cts_n != -1)
    {
        /*MUTEX_BLOCK(mtx_giant_io, MUTEX_ACTION_MANUAL)
        {*/
        //kernel_mutex_lock(mtx_giant_io);
            //get CTS
            uint16_t pin_cts = hal_hw_get_addr_pin(s_usart_ptr->port_cts);

            //wait for CTS
            while(_HAL_RREG8(pin_cts) & (1 << s_usart_ptr->pin_cts_n)); //handshaking
        //}
    }
    return 0;
}

int16_t usart_sync_send_byte(struct usart_io * s_usart_ptr, uint8_t data)
{
	if ( s_usart_ptr == NULL )
	{
        setSysError(SYSTEM_NULL_ARGUMENT);
        return -1;
	}

    usart_check_cts(s_usart_ptr);

	const struct usart_hwaddr * dev_hw_ptr = &s_usart_addr[(uint8_t) s_usart_ptr->dev_d];
	register uint16_t reg_io = pgm_read_word( &(dev_hw_ptr->uscra));
	register uint8_t bit_udre = pgm_read_byte( &(dev_hw_ptr->udre));

	while ( !(_SFR_MEM8( reg_io ) & (1 << bit_udre) ) )
	;

	reg_io = pgm_read_word( &(dev_hw_ptr->udr) );
	_SFR_MEM8( reg_io ) = data; //start transmission

	//while ( !(UCSR0A & (1<<UDRE0)))
	//; 			                /* Wait for empty transmit buffer */
	//UDR0 = data; 			        /* Start transmition */

	return data;
}

void usart_transmitString_F(struct usart_io * s_usart_ptr, const char* string)
{
	if ( (s_usart_ptr == NULL) & (string != NULL) ) return;

	while (pgm_read_byte(&(*string)))
	{
		usart_sync_send_byte(s_usart_ptr, pgm_read_byte(&(*string++)));
	}

}

/*------------------------------------file support------------------------------------*/
static int
usart_static_file_write_sync(char var, FILE *stream)
{
    struct usart_io * uio = (struct usart_io *)stream->udata;
	return usart_sync_send_byte(/*usart_hw_arr_ptr[(uint8_t) DEV_USART0]*/ uio, (uint8_t) var);
}

static int
usart_static_file_read_sync(FILE * stream)
{
    struct usart_io * uio = (struct usart_io *)stream->udata;
	return usart_sync_receive_byte(uio);
}

static int
usart_static_file_read_async(FILE * stream)
{
    struct usart_io * uio = (struct usart_io *)stream->udata;

    int32_t res = bRBf8(uio->rx_buf);
	if (res == -1)
	{
		//stream->flags |= SEOF;
		return EOF;
	}
   // stream->flags &= ~SEOF;
	return res;
}

static int
usart_static_file_write_async(char var, FILE *stream)
{
    struct usart_io * uio = (struct usart_io *)stream->udata;

    if (b_get_free_space(uio->tx_buf) < 2)
    {
         //flush
         usart_flush(uio);
         kernel_process_usleep(5000, 0); //skip 5000 microseconds = 5ms
    }

    bTBf8(uio->tx_buf, var);


   return var;
}

int8_t usart_is_buffered_tx(struct dev_descr_node * ddn)
{
    struct usart_io * uio = (struct usart_io *) ddn->instance;

    if ((uio->RT_oper_mode == USART_BUFFERED_TX) ||
        (uio->RT_oper_mode == USART_BUFFERED_BOTH))
    {
        return 1;
    }

    return 0;
}

int8_t usart_is_buffered_rx(struct dev_descr_node * ddn)
{
    struct usart_io * uio = (struct usart_io *) ddn->instance;

    if ((uio->RT_oper_mode == USART_BUFFERED_RX) ||
        (uio->RT_oper_mode == USART_BUFFERED_BOTH))
    {
        return 1;
    }

    return 0;
}

// MAX_USART_PORTS

/*-------------------------------kstream support functions----------------------*/

int16_t usart_static_read_length(struct kstream * stream)
{
    return 0;//((struct usart_io *) stream->loc_instance)->rcv_buf.to_read_left;
}

int16_t usart_static_write_length(struct kstream * stream)
{
    //return usart_hw_arr_ptr[(uint8_t) DEV_USART0]->txe_buf._space_left;
    return 0;//__get_free_txe_space((struct usart_io *) stream->loc_instance);
}

int8_t usart_static_write_async(struct kstream * stream, uint8_t * buf_ptr, uint16_t bwsize)
{
    struct usart_io * uio = (struct usart_io *) stream->loc_instance;

    int16_t res = bTBfBuf(uio->tx_buf, buf_ptr, bwsize);

	if (res == -1)
	{
		stream->flags |= SERR;
	}

	return res;
}

int32_t
usart_static_read_async(struct kstream * stream, uint8_t * rarr, uint16_t rlen)
{
    struct usart_io * uio = (struct usart_io *) stream->loc_instance;

	int32_t res = bRBfArr(uio->rx_buf, rarr, rlen);

	if (res == -1)
	{
		stream->flags |= SEOF;
	}

	return res;
}

int32_t
usart_static_read_arr_sync(struct kstream * stream, uint8_t * rarr, uint16_t rlen)
{
   //ToDo complete this section

	return 1;
}

int16_t
usart_static_read_length_sync(struct kstream * stream)
{
    return usart_sync_check_rcv((struct usart_io *) stream->loc_instance);
}

int16_t
usart_static_write_length_sync(struct kstream * stream)
{
    return 1; //check transmit ready flag
}

int8_t
usart_static_write(struct kstream * stream, uint8_t var)
{
	return usart_sync_send_byte((struct usart_io *) stream->loc_instance, (uint8_t) var);
}

int16_t
usart_static_read(struct kstream * stream)
{
	return usart_sync_receive_byte((struct usart_io *) stream->loc_instance);
}

int8_t
usart_static_pipe_init_dummy(struct kstream * stream)
{

    return 0;
}

int8_t
usart_static_buffer_init(struct kstream * stream)
{
/*    struct usart_io * uio = (struct usart_io *) stream->loc_instance;
    uint8_t i = 0;

    //check if the transmission is still in progress
    //this is performed by write_finish so it is not nessasary
    //to check it but in case...
	if (s_usart_ptr->flags & USART_TXE_STARTED)
    {
        return 1;
    }

    return kernel_stream_lock(stream, uio->fd->dev_fd);*/
    return 1;
}

int8_t
usart_static_write_finish(struct kstream * stream)
{

   /* struct usart_io * uio = (struct usart_io *) stream->loc_instance;

    //unlock stream
    //check if the transmission is still in progress
	if (s_usart_ptr->flags & USART_TXE_STARTED)
    {
        return 1;
    }

    return kernel_stream_unlock(stream, uio->fd->dev_fd);*/
    return 1;
}

uint8_t *
usart_static_get_rbuf(struct kstream * ks)
{
    return NULL;
}

uint8_t *
usart_static_get_rbuf_dummy(struct kstream * ks)
{
    return NULL;
}

/*----kstream support functions ends----*/

/*----all debug code should be placed below-----*/
void debug(char ch)
{
	/*uint8_t data = 0, status = 0;
	uint8_t retry = 0;
	const struct usart_hwaddr * dev_hw_ptr = &s_usart_addr[(uint8_t) 0];

	register uint16_t reg_io = pgm_read_word( &(dev_hw_ptr->uscra));
	register uint8_t bit_rxc = pgm_read_byte( &(dev_hw_ptr->rxc));

	while ( !(_SFR_MEM8( reg_io ) & (1 << bit_rxc )) )
	;

	//while(!(UCSR0A & (1<<RXC0)));

	status = _SFR_MEM8( reg_io );*/
	const struct usart_hwaddr * dev_hw_ptr = &s_usart_addr[(uint8_t) 0];
	uint16_t reg_io = pgm_read_word( &(dev_hw_ptr->udr) );
	uint8_t data = _SFR_MEM8( reg_io );


	reg_io = pgm_read_word( &(dev_hw_ptr->uscra));
	uint8_t bit_udre = pgm_read_byte( &(dev_hw_ptr->udre));

	while ( !(_SFR_MEM8( reg_io ) & (1 << bit_udre) ) )
	;

	reg_io = pgm_read_word( &(dev_hw_ptr->udr) );
	_SFR_MEM8( reg_io ) = ch; //start transmission
}
#endif // BUILD_WITH_SERIAL
