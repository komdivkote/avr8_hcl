/*-
 *	Copyright (c) 2014-2016	Alexander Morozov. www.nixd.org
 *	All rights reserved.
 *
 * LICENSE: This source file is subject to the New BSD license that is
 * available through the world-wide-web at the following URI:
 * http://www.opensource.org/licenses/bsd-license.php or in the root folder
 * of the project in file LICENSE.TXT.
 */

#include <avr/io.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <avr/pgmspace.h>
#include <avr/interrupt.h>
#include <util/atomic.h>
#include <avr/wdt.h>
#include "kernel_sectioning.h"
#include "kernel_memory_private.h"
#include "kernel_process_private.h"

#define FLAGS_INT_ENABLED		0x80

//state titles (process only)
#if (KERNEL_TRACK_PROC_STATE == YES)
const char KSTATE_SLEEP[]   PROGMEM	= "ksleep";
const char KSTATE_MUX[]     PROGMEM	= "kmutx";
const char KSTATE_MSG[]     PROGMEM	= "kmsg";
const char KSTATE_MSGR[]    PROGMEM	= "krmsg";
const char KSTATE_KERN[]    PROGMEM	= "kernel";
const char KSTATE_WAIT[]    PROGMEM = "kwait";
const char KSTATE_CPU[]     PROGMEM	= "CPU";
#endif /* KERNEL_TRACK_PROC_STATE */

void __kernel_release_control( void ) __attribute__ ( ( naked ) );

//running system timer
void kernel_init_system_timer( void ) __attribute__((optimize("Os")));

void __proc_hook_return_from_process();
k_proc_stack *__proc_init_stack( k_proc_stack *stack_start, void (*PROCESS)(void*), void *arg0 );
struct kprocess * __create_process( const char * pm_title,
									enum kproc_type e_proc_type,
									uint16_t stack_size,
									void (*PROCESS)(void*), void *arg0
								  );
void __kernel_process_switch_context();
void _isr_kern_sleep_clear_stat();
void _isr_kern_sleep_switch_ocrb();
void _isr_kern_sleep_update();

volatile k_pid lpid								= 0;
volatile struct kern_uptime uptime_cnt 			= {0};
volatile k_pid pid_cnt							= 0;
volatile k_proc_stack * volatile mainStackaddr	= NULL;
volatile k_proc_stack ** volatile selectedStack	= NULL;
//process list
volatile struct kprocess_list * volatile kproc_list				= NULL;
//current running process
volatile struct kprocess_list * volatile current_kprocess		= NULL;
//the nearest to wakeup sleeping process
volatile struct kprocess_list * volatile sleeping_kprocess		= NULL;
//the nearest to wakeup sleeping process
volatile struct kprocess_list * volatile sleeping_kprocess_list = NULL;

//memory mutex
volatile struct kproc_mutex * volatile memmux = NULL;


/*
 * ISR(TIMER0_CAMPA_vect) interrupt request from TIMER0 A channel.
 */
ISR(TIMER0_COMPA_vect, ISR_NAKED)
{
	//save context
	STORE_REGISTERS();

	//restore stack for timer near the main()
	RESTORE_TIMER_STACK();

	//reset watchdog was moved below

	#if (KERNEL_MEASURE_PERF==YES)
	//count performance
	_isr_kernel_process_count_time_spent();

	#endif

	if (uptime_cnt.milis_cnt == 999)
	{
		#if (KERNEL_MEASURE_PERF==YES)
		_isr_kernel_cpu_utilization_eval();
		#endif
		++uptime_cnt.sec_cnt;
		uptime_cnt.milis_cnt = 0;
	}
	else
	{
		++uptime_cnt.milis_cnt;
	}

	//update waiting if needed
	if ((sleeping_kprocess_list) || (sleeping_kprocess))
	{
		_isr_kern_sleep_update();
	}

	//if (multitask is NOT cooperative) OR task is rising release then switch
	//so this expression works only when current_kprocess is NOT COOPERATIVE (Normal mode)
	// OR is is COOPERATIVE but would like to release control!
	if ((!(current_kprocess->node->flags & KPROC_FLAG_COOP)) || (current_kprocess->node->flags & KPROC_FLAG_SIG_RELEASE))
	{
		//reset sig_release even if it was not set (faster than involve if)
		current_kprocess->node->flags &= ~KPROC_FLAG_SIG_RELEASE;
		__kernel_process_switch_context();
		//reset watchdog
        wdt_reset();
	}

	#if (KERNEL_MEASURE_PERF==YES)
	//start measuring time
	_isr_kernel_process_start_measure_time();
	#endif

	//restore registers file
	RESTORE_REGISTERS();

	//return from ISR
	asm volatile ( "reti" );
}

/*
 * ISR(TIMER0_COMPB_vect) interrupt occurs only when task is in "sleeping" state
 *	and required an wakeup.
 */
ISR(TIMER0_COMPB_vect, ISR_NAKED)
{
	//dump registers
	STORE_REGISTERS();

	//restore stack for timer near the main()
	RESTORE_TIMER_STACK();

	//clear status
	_isr_kern_sleep_clear_stat();

	//switch next sleeping task for OCR0B
	_isr_kern_sleep_switch_ocrb();

	//restore registers
	RESTORE_REGISTERS();

	//return from ISR
	asm volatile ( "reti" );
}

ISR(WDT_vect, ISR_NAKED)
{
    //the selected process has received control and did not released it yet 2.0seconds passed
    //force this process to switch
    current_kprocess->node->flags |= KPROC_FLAG_SIG_RELEASE;
}
ATTRIBUTE_KERNEL_SECTION
void
kernel_proc_enter_cooperative_mode()
{
    //entering (for process) cooperative mode and wait state
    ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
    {
        //__set_proc_state(KSTATE_SLEEP);
        current_kprocess->node->flags |= KPROC_FLAG_COOP;
    }
}

ATTRIBUTE_KERNEL_SECTION
void
kernel_proc_leave_cooperative_mode()
{
    current_kprocess->node->flags &= ~KPROC_FLAG_COOP;
}

ATTRIBUTE_KERNEL_SECTION
void
kernel_release_control(void)
{
	if (TCNT0 <= SAFE_OCR0A_TCNT_MAN_SWITCH)
	{
		//manual release
		__kernel_release_control();
	}
	else
	{
		//setting flag release and wait for the timer ISR
		current_kprocess->node->flags |= KPROC_FLAG_SIG_RELEASE;
		while (current_kprocess->node->flags & KPROC_FLAG_SIG_RELEASE);
	}
}

ATTRIBUTE_KERNEL_SECTION
void
__kernel_release_control( void )
{
	//clear I bit in SREG
	asm volatile ("cli \n\t");

	//dump registers file
	STORE_REGISTERS();

	//restore stack for timer near the main()
	RESTORE_TIMER_STACK();

	#if (KERNEL_MEASURE_PERF==YES)
	//stop mesuring for current process
	_isr_kernel_process_count_time_spent();
	#endif

	//check mode, if proc is in cooperation mode
	if (current_kprocess->node->flags & KPROC_FLAG_COOP)
	{
		__kernel_process_switch_context();
	}

	#if (KERNEL_MEASURE_PERF==YES)
	//start time measuring
	_isr_kernel_process_start_measure_time();
	#endif

	//restore registers file
	RESTORE_REGISTERS();

	//restore I flag in SREG
	asm volatile (	"sei \n\t"
					"ret \n\t"
				 );
}

//changing the process to execute
ATTRIBUTE_KERNEL_SECTION
void
__kernel_process_switch_context()
{
	lpid = current_kprocess->node->pid;

	do
	{
		if (current_kprocess->next == NULL)
		{
			if (lpid != 0)
			{
				//set last pid to zero
				lpid = 0;
				//skip idle task
				current_kprocess = kproc_list->next;
			}
			else
			{
				//run idle
				current_kprocess = kproc_list;
				//set lpid to -1 to check other tasks again
				lpid = -1;
			}

		}
		else
		{
			current_kprocess = current_kprocess->next;
		}

		//testing if waiting for mutext to be released
		if ((current_kprocess->wait_mutex != NULL) &&
			(current_kprocess->mutex_wait_order == current_kprocess->wait_mutex->c_mutx_counter))
		{
			if (current_kprocess->wait_mutex->owner == NULL)
			{
				//mutex was released, drop the wait flag
				current_kprocess->node->flags &= ~KPROC_FLAG_WAIT;
				if (current_kprocess->wait_mutex->c_mutx_counter == 255)
				{
					current_kprocess->wait_mutex->c_mutx_counter = 0;
				}
				else
				{
					++current_kprocess->wait_mutex->c_mutx_counter;
				}
				break; //select this task
			}
			/*else
			{
				continue;
			}*/
		}
		else if ( (current_kprocess->wait_sem != NULL) && (current_kprocess->wait_sem->sem_lcnt > 0) )
		{
            current_kprocess->node->flags &= ~KPROC_FLAG_WAIT;
            break; //select this task immidiatly
		}
#if KERNEL_BUILD_WITH_CONDITION_WAIT == YES
		else if (current_kprocess->wait_cond != NULL)
        {

            if ( ( (current_kprocess->wait_cond->e_sceq == SC_WAKEUP_LARGER_ZERO) && (*(current_kprocess->wait_cond->cvar) > 0)) ||
                 ( (current_kprocess->wait_cond->e_sceq == SC_WAKEUP_EQUAL_ZERO) && (*(current_kprocess->wait_cond->cvar) == 0)) )
            {
                current_kprocess->wait_cond = NULL;
                current_kprocess->node->flags &= ~KPROC_FLAG_WAIT;
            }

        }
#endif // KERNEL_BUILD_WITH_CONDITION_WAIT

	} while ( ((current_kprocess->node->flags & 0x0F) == 0) ||
              (current_kprocess->node->flags & KPROC_FLAG_SLEEP) ||
              (current_kprocess->node->flags & KPROC_FLAG_WAIT)
	);


	//updating selectedStack to point to the selected instance stack
	selectedStack = &current_kprocess->node->stack_saved;

	return;
}
/*----REGISTER FILE DUMP/RESTORE AND SHEDULING ENDS HERE---------------------------------------*/

/*----SLEEPING PROCESSES STATIC CODE-----------------------------------------------------------*/
ATTRIBUTE_KERNEL_SECTION
void
_isr_kern_sleep_clear_stat()
{
	sleeping_kprocess->node->flags &= ~KPROC_FLAG_SLEEP;

	return;
}

ATTRIBUTE_KERNEL_SECTION
void
_isr_kern_sleep_switch_ocrb()
{

	if (sleeping_kprocess_list == NULL)
	{
		//disable int ocie0b
		TIMSK0 &= ~(1 << OCIE0B);
		OCR0B = 0;
		TIFR0 |= (1 << OCF0B);
		sleeping_kprocess = NULL;
	}
	else
	{
		//look for the 'nearest' task

		//temp iterator
		volatile struct kprocess_list * volatile temp = sleeping_kprocess_list;
		//previos temp iterator
		volatile struct kprocess_list * volatile temp_prev = NULL;
		//previos rec for sleeping
		volatile struct kprocess_list * volatile sleeping_prev = NULL;
		//sleeping process selector
		sleeping_kprocess = NULL;
		while (temp != NULL)
		{
			if (temp->sleep->ocr0g < OCR0A)
			{
				if (sleeping_kprocess == NULL)
				{
					sleeping_prev = temp_prev;
					sleeping_kprocess = temp;
				}
				else
				{
					if (sleeping_kprocess->sleep->ocr0g > temp->sleep->ocr0g)
					{
						sleeping_prev = temp_prev;
						sleeping_kprocess = temp;
					}
				}
			}

			//iterate next
			temp_prev = temp;
			temp = temp->sleep->next_sleeping_kprocess;
		}

		if (sleeping_kprocess)
		{
			//was found
			if (sleeping_prev == NULL)
			{
				sleeping_kprocess_list = sleeping_kprocess->sleep->next_sleeping_kprocess;
			}
			else
			{
				sleeping_prev->sleep->next_sleeping_kprocess = sleeping_kprocess->sleep->next_sleeping_kprocess;
			}

			TIFR0 |= (1 << OCF0B);
			OCR0B = (uint8_t) sleeping_kprocess->sleep->ocr0g;
		}
		else
		{
			//temporary turn off TIMER0B interrupts
			TIFR0 |= (1 << OCF0B);
			TIMSK0 &= ~(1 << OCIE0B);
		}
	}

	return;
}

ATTRIBUTE_KERNEL_SECTION
void
_isr_kern_sleep_update()
{
	//temp iterator of sleeping_kprocess_list
	volatile struct kprocess_list * volatile temp = sleeping_kprocess_list;
	while (temp != NULL)
	{
		if (temp->sleep->ocr0g > OCR0A)
		{
			temp->sleep->ocr0g -= OCR0A;
		}

		temp = temp->sleep->next_sleeping_kprocess;
	}

	if (sleeping_kprocess != NULL)
	{
		//if int of the OCR0B is not set then check if current process are close to wakeup
		if ((TIMSK0 & (1 << OCIE0B)) == 0)
		{
			if (sleeping_kprocess->sleep->ocr0g <= OCR0A)
			{
				TIFR0 |= (1 << OCF0B);
				OCR0B = (uint8_t) sleeping_kprocess->sleep->ocr0g;
				TIMSK0 |= (1 << OCIE0B);
			}
			else
			{
				sleeping_kprocess->sleep->ocr0g -= OCR0A;
			}
		}
	}
}

/***SLEEPING PROCESSES STAITIC CODE ENDS HERE----------------------------------*/
ATTRIBUTE_KERNEL_SECTION
void
__proc_hook_return_from_process()
{
    //stop process that left its main subroutine
    current_kprocess->node->flags &= KPROC_FLAG_STOP_MASK;
    //enter wait forever
    while(1);

    //program should never reach this point
    return;
}

ATTRIBUTE_KERNEL_SECTION
k_proc_stack *
__proc_init_stack( k_proc_stack *stack_start, void (*PROCESS)(void*), void *arg0 )
{
	stack_start -= (sizeof(struct _stack_init)-1);
	struct _stack_init * _stack = stack_start;

	_stack->flag1 = 0x11;
	_stack->flag2 = 0x22;
	_stack->flag3 = 0x55;
	_stack->procleave_hook_l = ( (uint16_t)__proc_hook_return_from_process & ( uint16_t ) 0x00ff);
	_stack->procleave_hook_h = (uint16_t) ( ((uint16_t)__proc_hook_return_from_process >> 8) & (uint16_t) 0x00ff);
	_stack->zero0 = 0;
	_stack->procfl = ( (uint16_t)PROCESS & ( uint16_t ) 0x00ff );
	_stack->procfh =  ( uint16_t ) ( ((uint16_t)PROCESS >> 8) & ( uint16_t ) 0x00ff );
	_stack->zero = 0x00;
	_stack->r31 = 0x31;
	_stack->r0 = 0x00;
	_stack->f_int = FLAGS_INT_ENABLED;
	_stack->r1 = 0;
	_stack->r2 = 0x02;
	_stack->r3 = 0x03;
	_stack->r4 = 0x04;
	_stack->r5 = 0x05;
	_stack->r6 = 0x06;
	_stack->r7 = 0x07;
	_stack->r8 = 0x08;
	_stack->r9 = 0x09;
	_stack->r10 = 0x10;
	_stack->r11 = 0x11;
	_stack->r12 = 0x12;
	_stack->r13 = 0x13;
	_stack->r14 = 0x14;
	_stack->r15 = 0x15;
	_stack->r16 = 0x16;
	_stack->r17 = 0x17;
	_stack->r18 = 0x18;
	_stack->r19 = 0x19;
	_stack->r20 = 0x20;
	_stack->r21 = 0x21;
	_stack->r22 = 0x22;
	_stack->r23 = 0x23;
	_stack->argl = ( (uint16_t)arg0 & 0x00ff );
	_stack->argh = ( ((uint16_t)arg0 >> 8) & 0x00ff );
	_stack->r26 = 0x26;
	_stack->r27 = 0x27;
	_stack->r28 = 0x28;
	_stack->r29 = 0x29;
	_stack->r30 = 0x30;


	return (stack_start-1);
}

#if (KERNEL_TRACK_PROC_STATE == YES)
inline uint8_t
__set_proc_state(const char * state)
{
	if ((current_kprocess != NULL) && (state != NULL))
	{
		current_kprocess->node->state = state;
	}

	return 1;
}

inline const char *
__get_proc_state(void)
{
	if (current_kprocess)
	{
		return current_kprocess->node->state;
	}
	else
	{
		return NULL;
	}
}
#endif /* KERNEL_TRACK_PROC_STATE */

ATTRIBUTE_KERNEL_SECTION
struct kprocess *
__create_process(const char * pm_title, enum kproc_type e_proc_type, uint16_t stack_size, void (*PROCESS)(void*), void *arg0)
{
	struct kprocess * proc = (struct kprocess *) kernel_malloc(sizeof(struct kprocess));//malloc(sizeof(struct kprocess));
	if (proc == NULL)
	{
	    return NULL;
		//exit(1);
	}

	memset(proc, 0, sizeof(struct kprocess));

	proc->pid = pid_cnt++;
	proc->flags |= e_proc_type | KPROC_FLAG_RUN;
#if KERNEL_TRACK_PROC_STATE == YES
	proc->state = KSTATE_CPU;
#endif // KERNEL_TRACK_PROC_STATE
#if KERNEL_USE_PROCESS_TITLES == YES
	proc->title = pm_title;									//title is located in PROGMEM
#endif // KERNEL_USE_PROCESS_TITLES
	proc->tcnt0_start = 0;
	proc->tm_cc = 0;
	proc->stack_end = (k_proc_stack *) kernel_malloc(stack_size+38);
	if (proc->stack_end == NULL)
	{
	    kernel_free((void*) proc);
	    return NULL;
		//exit(1);
	}
	proc->stack_start = proc->stack_end + ((stack_size + 38) - 1); //stack_end points to the last address

	proc->stack_saved =__proc_init_stack(proc->stack_start, PROCESS, arg0);

	return proc;
}

ATTRIBUTE_KERNEL_SECTION
k_pid
kernel_create_process(const char * pm_title, enum kproc_type e_proc_type, uint16_t stack_size, void (*PROCESS)(void*), void *arg0)
{

	struct kprocess * proc = __create_process(pm_title, e_proc_type, stack_size, PROCESS, arg0);

	struct kprocess_list * pl = (struct kprocess_list*) kernel_malloc(sizeof(struct kprocess_list));
	if (pl == NULL)
	{
	    return K_NO_PID;
		//exit(1);
	}

	pl->node = proc;
	pl->next = NULL;
	pl->wait_msg = NULL;
	pl->mutex_wait_order = 0;
	pl->sleep = NULL;
	pl->wait_mutex = NULL;

	//add to list
	volatile struct kprocess_list * volatile temp = kproc_list;
	ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
	{
        if (temp == NULL)
        {
            kproc_list = pl;
        }
        else
        {
            while (temp->next != NULL) temp = temp->next;
            temp->next = pl;
        }
	}
	return pl->node->pid;
}

ATTRIBUTE_KERNEL_SECTION
int8_t
kernel_process_usleep(uint32_t usec, uint8_t flags)
{
	struct kproc_sleep kp_sleep = {0,0,flags,0};
	if (usec < 500)
	{
		return -1; //too low
	}

	KERNEL_SET_STATE(KSTATE_SLEEP)
	{
		//entering (for process) cooperative mode and wait state
		ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
		{
			//__set_proc_state(KSTATE_SLEEP);
			current_kprocess->node->flags |= KPROC_FLAG_COOP;
		}

		current_kprocess->sleep = &kp_sleep;
		kp_sleep.ocr0g = (OCR_B_CALC(usec)) + 1;
		kp_sleep.tcnt_start = TCNT0;

		//peroform calculation of the OCR0B value if it is less than OCR0A
		if (kp_sleep.ocr0g <= OCR0A)
		{
			//setting offset againt current TCNT0
			kp_sleep.ocr0g = kp_sleep.ocr0g + kp_sleep.tcnt_start;
		}

		//add to list

		if (sleeping_kprocess)
		{
			ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
			{
				if (sleeping_kprocess_list)
				{
					current_kprocess->sleep->next_sleeping_kprocess = sleeping_kprocess_list;
					sleeping_kprocess_list = current_kprocess;
				}
				else
				{
					sleeping_kprocess_list = current_kprocess;
				}
			}
		}
		else
		{
			//timer is free, setting the sleeping process
			sleeping_kprocess = current_kprocess;
			if (kp_sleep.ocr0g <= OCR0A)
			{
				TIFR0 |= (1 << OCF0B);
				TIMSK0 |= (1 << OCIE0B);
				OCR0B = (uint8_t) kp_sleep.ocr0g;
			}
		}

		//entering sleep mode
		current_kprocess->node->flags |= KPROC_FLAG_SLEEP;

		//releasing control
		kernel_release_control();

		//clear sleeping data from process
		current_kprocess->sleep = NULL;

		//leaving cooperative mode, now the task can be interrupted
		//__set_proc_state(KSTATE_CPU);
		/*ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
		{*/
			current_kprocess->node->flags &= ~KPROC_FLAG_COOP;
		//}
	}

	return 0;
}

//this function only sets the condition
ATTRIBUTE_KERNEL_SECTION
int8_t
kernel_wait_for_msg_resolution(struct kproc_msg * msg)
{
	//enter cooperative mode and wait state
	//__set_proc_state(KSTATE_MSG);
	KERNEL_SET_STATE(KSTATE_MSG)
	{
		current_kprocess->node->flags |= KPROC_FLAG_COOP | KPROC_FLAG_WAIT;

		current_kprocess->wait_msg = msg;

		//releasing control
		kernel_release_control();

		//leaving cooperative mode, the task can be switched

		current_kprocess->node->flags &= (~KPROC_FLAG_COOP);
		//__set_proc_state(KSTATE_CPU);

	}

	return 0;

}

//this function checks for message and gets it for specified proc id
//returns 0 on success
ATTRIBUTE_KERNEL_SECTION
int8_t
kernel_check_for_msg_pending(k_pid target_pid, void **outdata)
{
	//lookup for task
	//__set_proc_state(KSTATE_MSGR);
	KERNEL_SET_STATE(KSTATE_MSGR)
	{
		volatile struct kprocess_list * volatile temp = kproc_list;
		while (temp != NULL)
		{
			if (temp->node->pid == target_pid)
			{
				if (temp->wait_msg == NULL )
				{
					*outdata = NULL;
					return 1;
				}
				else
				{
					*outdata = temp->wait_msg->out_data;
					return 0;
				}

			}
			temp = temp->next;
		}

		//__set_proc_state(KSTATE_CPU);
	}

	return -1;
}

ATTRIBUTE_KERNEL_SECTION
int8_t
kernel_setanswer_for_msg(k_pid target_pid, void *indata)
{
	//lookup for task
	volatile struct kprocess_list * volatile temp = kproc_list;
	while (temp != NULL)
	{
		if (temp->node->pid == target_pid)
		{
			if (temp->wait_msg == NULL )
			{
				return 1;
			}
			else
			{
				temp->wait_msg->in_data = indata;
				//reset container
				temp->wait_msg = NULL;
				//turn off wait flag
				temp->node->flags &= (~KPROC_FLAG_WAIT);

				return 0;
			}
		}
		temp = temp->next;
	}

	return -1;
}



/*
 * Setup timer 1 compare match A to generate a tick interrupt. 24 78
 */
ATTRIBUTE_KERNEL_SECTION
void
kernel_init_system_timer( void )
{
	//timer 0 ocr0a (scheduler ticks)
	OCR0A = OCR_VAL_CALC;
	TIMSK0 = (1 << OCIE0A);
	TCCR0A |= (1 << WGM01);// | (1 << WGM00);
	TCCR0B |= (1 << CS01) | (1 << CS00);

	/*WATCHDOG
	 * (1 << WDP2) | (1 << WDP1) 1.0s
	 * (1 << wdp2) | (1 << wdp1) | (1<<wdp0) 2.0s
	 * (1<<wdp3) 4.0s
	 * (1<<wdp3) | (1 <<wdp0) 8.0s
	 */
	register uint8_t wdts = (1 << WDCE) | (1 << WDE);
	register uint8_t wdts2 = (1 << WDIE) | (1 << WDP2) | (1 << WDP1) | (1 <<WDP0);
	wdt_reset();
	WDTCSR = wdts;
	WDTCSR = wdts2;

#if KERNEL_MEASURE_PERF == YES
#if KERNEL_MEASURE_PERF_ISR == YES
    _isr_bind_timer1();
#endif // KERNEL_MEASURE_PERF_ISR
#endif // KERNEL_MEASURE_PERF
	return;
}

ATTRIBUTE_KERNEL_SECTION
void
kernel_init_local_stack()
{
	uint8_t sp = 0;
	mainStackaddr = &sp;
}

ATTRIBUTE_KERNEL_SECTION
int8_t
kernel_processes_start( void )
{
	memmux = kernel_mutex_init();
	current_kprocess = kproc_list;
	__kernel_process_switch_context();
	kernel_init_system_timer();
	kernel_init_local_stack();
	RESTORE_REGISTERS();
	asm volatile ( "ret" );
}

ATTRIBUTE_KERNEL_SECTION
int8_t
kernel_process_suspend(k_pid pid)
{
    //lookup for task
	volatile struct kprocess_list * volatile temp = kproc_list;
	ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
    {
        while (temp != NULL)
        {
            if (temp->node->pid == pid)
            {
                temp->node->flags &= KPROC_FLAG_STOP_MASK;
                return 0;
            }
            temp = temp->next;
        }
    }
	return 1;
}

ATTRIBUTE_KERNEL_SECTION
int8_t
kernel_process_issuspended(k_pid pid)
{
    //lookup for task
	volatile struct kprocess_list * volatile temp = kproc_list;
	ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
    {
        while (temp != NULL)
        {
            if (temp->node->pid == pid)
            {
                if (temp->node->flags == KRPOC_FLAG_STOP)
                {
                    return 1;
                }

                return 0;
            }
            temp = temp->next;
        }
    }
	return -1;
}

ATTRIBUTE_KERNEL_SECTION
int8_t
kernel_process_restart(k_pid pid, void (*PROCESS)(void*), void * arg0)
{
    ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
    {
        volatile struct kprocess_list * volatile temp = kproc_list;
        while (temp != NULL)
        {
            if (temp->node->pid == pid)
            {
                temp->node->stack_saved =__proc_init_stack(temp->node->stack_start, PROCESS, arg0);
                return 0;
            }
            temp = temp->next;
        }
    }
	return 1;
}

ATTRIBUTE_KERNEL_SECTION
int8_t
kernel_process_remove(k_pid pid)
{
    volatile struct kprocess_list * volatile found = NULL;

    ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
    {
        volatile struct kprocess_list * volatile temp_prev = NULL;
        volatile struct kprocess_list * volatile temp = kproc_list;

        while (temp != NULL)
        {
            if (temp->node->pid == pid)
            {
                //remove from list
                if (temp_prev == NULL)
                {
                    temp = temp->next;
                }
                else
                {
                    temp_prev->next = temp->next;
                }
                //break;
                found = temp;
                break;
            }
            temp_prev = temp;
            temp = temp->next;
        }
    }

    if (found != NULL)
    {
        kernel_free((void*) found->node->stack_end);
        kernel_free((void*) found->node);
        kernel_free((void*) found);
        return 0;
    }

	return 1;
}

ATTRIBUTE_KERNEL_SECTION
k_pid
kernel_get_current_pid(void)
{
	return current_kprocess->node->pid;
}

ATTRIBUTE_KERNEL_SECTION
uint16_t
kernel_processes_get_amount(void)
{
	return pid_cnt;
}

ATTRIBUTE_KERNEL_SECTION
int8_t
kernel_processes_get_procinfo(uint16_t offset, struct kprocess * envelope)
{
	volatile struct kprocess_list * volatile temp = kproc_list;
	while ((offset > 0) && (temp != NULL))
	{
		temp = temp->next;
		--offset;
	}

	if (temp != NULL)
	{
		memmove(envelope, temp->node, sizeof(struct kprocess));
		return 0;
	}
	else
	{
		return 1;
	}
}

ATTRIBUTE_KERNEL_SECTION
k_proc_stack *
kernel_get_common_stack_address()
{
	return mainStackaddr;
}

//-----PERFORMANCE TOOLS
ATTRIBUTE_KERNEL_SECTION
k_ms
kernel_get_milis()
{
	return uptime_cnt.sec_cnt*1000+uptime_cnt.milis_cnt;
}

ATTRIBUTE_KERNEL_SECTION
k_sec
kernel_get_seconds()
{
	return uptime_cnt.sec_cnt;
}
