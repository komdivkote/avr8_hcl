#ifndef STREAM_H_INCLUDED
#define STREAM_H_INCLUDED


typedef int8_t k_sock_id;

//stream
#define SASYNC 1                          /*asynchronous stream (user-read-only)*/
#define SSYNC ~1                          /*synchronous stream (inversion of the previous def) (user-read-only)*/
#define SREAD 2                           /*can read (user RW) via init function*/
#define SWRITE 4                          /*can write (user RW) via init function*/
#define SRW SREAD | SWRITE                /*both RW*/
#define SCRC 32                           /*RISED (crc calculation in progress) user-read-only*/
#define SEOF 64                           /*end of file*/
#define SERR 128                          /*error (any)*/

struct socket
{
    k_sock_id id;                       //automatic set
    struct dev_descr_node * ddn;        //owner
    uint32_t netip;
    uint16_t netport;
	uint8_t flags;                      //flags
};


struct socket * sock_open_server(enum knet_proto knp, uint32_t listenIP, uint16_t listenPORT);
struct socket * sock_open_client(enum knet_proto knp, uint32_t remoteIP, uint16_t remotePORT);
void sock_close(struct socket * sock);




#endif // STREAM_H_INCLUDED
