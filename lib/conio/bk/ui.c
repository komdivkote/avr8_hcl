/*
    ui.c
    User interface functions.
    Part of MicroVGA CONIO library / demo project
    Copyright (c) 2008-9 SECONS s.r.o., http://www.MicroVGA.com
    Modified by Alexander Morozov NiXD ORG.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include "../../hal/build_defs.h"
#include <avr/io.h>
#include <avr/pgmspace.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>
#include "../../hal/shared.h"
#include "./conio.h"
#include "./ui.h"
#include "./kbd.h"

/*Asyncroneous IO UI logic*/

/*Synchronous IO UI logic*/


int runmenu0(char x, char y, int defaultitem, int page_size, const char * const menu_itm[])
{
  ASSERT_NULL(menu_itm, runmenu0_null_error);
  int key, i,j, itemno, pageno;
  int nitems, nitemsk, width, pages;
  const char * s;

 //runmenu_reload:
  itemno = defaultitem-1;
  pageno = 0;
  width = 20;

  width = 10;
  nitems = 0;
  //char menu[25];// {[ 0 ... 15-1 ] = 1};
  PGM_P p;

  memcpy_P(&p, &menu_itm[0], sizeof(PGM_P));
 // strcpy_P(menu, p);
  while (pgm_read_byte(p) != 0) {
	//s = p;
    for (j=0;pgm_read_byte(&(*p++));j++);
    if (j>width)
     width = j;
    nitems++;
	memcpy_P(&p, &menu_itm[nitems], sizeof(PGM_P));
	//strcpy_P(menu, p);
  }
  width+=2;

  if (itemno < 0 || itemno > nitems || page_size < itemno)
  {
    itemno = 0;
  }

  pages = (nitems / (page_size+1));
  int len_lst = (nitems > page_size) ? page_size : nitems;
  nitemsk = (nitems > page_size) ? (nitems - abs((nitems - page_size*(pageno+1))) ) / (pageno+1) : nitems;
  while (1)
  {

  cursoroff();
  setcolor(CYAN<<4 | BLACK);
  gotoxy(x,y);
  _putch(ACS_ULCORNER);
  for (i=0;i<width+2;i++)
  {
		_putch(ACS_HLINE);
  }
  _putch(ACS_URCORNER);



   for (i = 0; i < len_lst;i++)
   {//i = page_size*pageno;i<page_size*(pageno+1)
	  // if (i > nitems) break;
	   uint8_t ips = page_size*pageno + i;
	   gotoxy(x,y+i+1);
	   _putch(ACS_VLINE);
	   _putch(' ');
	   if (i == itemno)
	   {
		setcolor(YELLOW);
	   }
	   if (i < nitemsk)
	   {
	   memcpy_P(&p, &menu_itm[ips], sizeof(PGM_P));
	   // strcpy_P(menu, p);
	   s = p;
	   for (j=0;j<width;j++)
	   {
		   if (pgm_read_byte(&(*s)))
		   {
			   _putch(pgm_read_byte(&(*s++)));
		   }
		   else
		   {
			   _putch(' ');
		   }
	   }
	   }
	   else
	   {
		   for (j=0;j<width;j++)
		   {
			    _putch(' ');
		   }
	   }

	   setcolor(CYAN<<4 | BLACK);
	   _putch(' ');
	   _putch(ACS_VLINE);
   }

  uint8_t is = i+1;
  gotoxy(x,y+is);
  _putch(ACS_LLCORNER);
  for (i=0;i<width+2;i++)
  {
	_putch(ACS_HLINE);
  }
  _putch(ACS_LRCORNER);
  labelf_P(x+2, y+is, PSTR("%i/%i -> %i"), pageno+1, pages+1, nitemsk);

  while (!_kbhit()) ;

   if (_kbhit()) {
     key = _getch();
     switch(key) {
      case KB_UP:
      		if (itemno>0)
      		{
      			itemno--;
      		}
      	    else
      	    {

      	    	if (pageno>0) pageno--;
      			else pageno = pages;
				nitemsk = (nitems > page_size) ? (nitems - abs((nitems - page_size*(pageno+1))) ) / (pageno+1) : nitems;
				itemno = nitemsk-1;//nitems-1;

      	    }
      break;
      case KB_DOWN:
      		itemno++;
      		if ( (itemno %= nitemsk) == 0)
			{
				pageno++;
				pageno %= (pages+1);
				nitemsk = (nitems > page_size) ? (nitems - abs((nitems - page_size*(pageno+1))) ) / (pageno+1) : nitems;
      			/*pageno++;
      			if (pageno > pages)
				{
					pageno = 1;
				}*/
			}
      break;
      case KB_LEFT:
      		if (pageno>0) pageno--;
      		else pageno = pages;

			nitemsk = (nitems > page_size) ? (nitems - abs((nitems - page_size*(pageno+1))) ) / (pageno+1) : nitems;
			if (nitemsk < itemno)
			{
				itemno = nitemsk-1;
			}

      break;
      case KB_RIGHT:
      		pageno++;
      		pageno %= (pages+1);
			nitemsk = (nitems > page_size) ? (nitems - abs((nitems - page_size*(pageno+1))) ) / (pageno+1) : nitems;
			if (nitemsk < itemno)
			{
				itemno = nitemsk-1;
			}
      break;
      case KB_ESC: return UI_NOTHING;
      case KB_ENTER: cursoron(); return page_size*pageno+itemno+1;
     }
   }
  }

runmenu0_null_error:
  return UI_NOTHING;
}

int runmenu0_s(char x, char y, int defaultitem, int page_size, const struct menudialog_entry st_data[], int * ret)
{
  ASSERT_NULL(st_data, runmenu0_s_null_error);
  int key, i,j, itemno, pageno;
  int nitems, nitemsk, width, pages;
  const char * s;

 //runmenu_reload:
  itemno = defaultitem-1;
  pageno = 0;
  width = 20;

  width = 10;
  nitems = 0;

  PGM_P p;
  memcpy_P(&p, &(st_data[0].caption), sizeof(PGM_P));

  while (pgm_read_byte(&(*p)) != 0)
  {

    for (j=0;pgm_read_byte(&(*p++));j++);

    if (j>width)
     width = j;
    nitems++;

	memcpy_P(&p, &(st_data[nitems].caption), sizeof(PGM_P));
  }

   printf_P(PSTR("items: %i\r\n"), nitems);
  width+=2;

  if (itemno < 0 || itemno > nitems || page_size < itemno)
  {
    itemno = 0;
  }

  pages = (nitems / (page_size+1));
  int len_lst = (nitems > page_size) ? page_size : nitems;
  nitemsk = (nitems > page_size) ? (nitems - abs((nitems - page_size*(pageno+1))) ) / (pageno+1) : nitems;
  while (1)
  {

  cursoroff();
  setcolor(CYAN<<4 | BLACK);
  gotoxy(x,y);
  _putch(ACS_ULCORNER);
  for (i=0;i<width+2;i++)
  {
		_putch(ACS_HLINE);
  }
  _putch(ACS_URCORNER);



   for (i = 0; i < len_lst;i++)
   {//i = page_size*pageno;i<page_size*(pageno+1)
	  // if (i > nitems) break;
	   uint8_t ips = page_size*pageno + i;
	   gotoxy(x,y+i+1);
	   _putch(ACS_VLINE);
	   _putch(' ');
	   if (i == itemno)
	   {
		setcolor(YELLOW);
	   }
	   if (i < nitemsk)
	   {
	   memcpy_P(&p, &(st_data[ips].caption), sizeof(PGM_P));
	   // strcpy_P(menu, p);
	   s = p;
	   for (j=0;j<width;j++)
	   {
		   if (pgm_read_byte(&(*s)))
		   {
			   _putch(pgm_read_byte(&(*s++)));
		   }
		   else
		   {
			   _putch(' ');
		   }
	   }
	   }
	   else
	   {
		   for (j=0;j<width;j++)
		   {
			    _putch(' ');
		   }
	   }

	   setcolor(CYAN<<4 | BLACK);
	   _putch(' ');
	   _putch(ACS_VLINE);
   }

  uint8_t is = i+1;
  gotoxy(x,y+is);
  _putch(ACS_LLCORNER);
  for (i=0;i<width+2;i++)
  {
	_putch(ACS_HLINE);
  }
  _putch(ACS_LRCORNER);
  labelf_P(x+2, y+is, PSTR("%i/%i -> %i"), pageno+1, pages+1, nitemsk);

  while (!_kbhit()) ;

   if (_kbhit()) {
     key = _getch();
     switch(key) {
      case KB_UP:
      		if (itemno>0)
      		{
      			itemno--;
      		}
      	    else
      	    {

      	    	if (pageno>0) pageno--;
      			else pageno = pages;
				nitemsk = (nitems > page_size) ? (nitems - abs((nitems - page_size*(pageno+1))) ) / (pageno+1) : nitems;
				itemno = nitemsk-1;//nitems-1;

      	    }
      break;
      case KB_DOWN:
      		itemno++;
      		if ( (itemno %= nitemsk) == 0)
			{
				pageno++;
				pageno %= (pages+1);
				nitemsk = (nitems > page_size) ? (nitems - abs((nitems - page_size*(pageno+1))) ) / (pageno+1) : nitems;
      			/*pageno++;
      			if (pageno > pages)
				{
					pageno = 1;
				}*/
			}
      break;
      case KB_LEFT:
      		if (pageno>0) pageno--;
      		else pageno = pages;

			nitemsk = (nitems > page_size) ? (nitems - abs((nitems - page_size*(pageno+1))) ) / (pageno+1) : nitems;
			if (nitemsk < itemno)
			{
				itemno = nitemsk-1;
			}

      break;
      case KB_RIGHT:
      		pageno++;
      		pageno %= (pages+1);
			nitemsk = (nitems > page_size) ? (nitems - abs((nitems - page_size*(pageno+1))) ) / (pageno+1) : nitems;
			if (nitemsk < itemno)
			{
				itemno = nitemsk-1;
			}
      break;
      case KB_ESC: cursoron(); return UI_NOTHING;
      case KB_ENTER: cursoron();
		  //printf_P(PSTR("ret: %i id:%i %i %i %i\r\n"), pgm_read_dword(&(st_data[page_size*pageno+itemno].val)), page_size*pageno+itemno, itemno, page_size, pageno);
		*ret = pgm_read_dword(&(st_data[page_size*pageno+itemno].val));
		return page_size*pageno+itemno+1;
     }
   }
  }

runmenu0_s_null_error:
  return UI_NOTHING;
}


int runmenu1(char x, char y, int defaultitem, const char * caption, const char * const menu_itm[])
{
	ASSERT_NULL(caption, runmenu1_null_error);

	int key, i,j, itemno;
	int nitems, width;
	const char * s;

	//runmenu_reload:
	itemno = defaultitem-1;

	size_t len = strlen_P(caption);
	width = len;
	nitems = 0;
	//char menu[25];// {[ 0 ... 15-1 ] = 1};
	PGM_P p;

	memcpy_P(&p, &menu_itm[0], sizeof(PGM_P));
	// strcpy_P(menu, p);
	while (pgm_read_byte(p) != 0) {
		//s = p;
		for (j=0;pgm_read_byte(&(*p++));j++);
		if (j>width)
		width = j;
		nitems++;
		memcpy_P(&p, &menu_itm[nitems], sizeof(PGM_P));
		//strcpy_P(menu, p);
	}
	width += 2;

	if (itemno < 0 || itemno > nitems)
	itemno = 0;

	while (1) {

		cursoroff();
		setcolor(CYAN<<4 | BLACK);
		gotoxy(x,y);
		_putch(ACS_ULCORNER);

        s = caption;
		for (i=0;i<width+2;i++)
		{
		    if (i >= len)
            {
                _putch(ACS_HLINE);
            }
            else
            {
                _putch(pgm_read_byte(&(*s++)));
            }
		}

		_putch(ACS_URCORNER);


		for (i = 0;i<nitems;i++)
		{
			gotoxy(x,y+i+1);
			_putch(ACS_VLINE);
			_putch(' ');
			if (i == itemno)
			setcolor(YELLOW);
			memcpy_P(&p, &menu_itm[i], sizeof(PGM_P));
			// strcpy_P(menu, p);
			s = p;
			for (j=0;j<width;j++)
			{
				if (pgm_read_byte(&(*s)))
				{
					_putch(pgm_read_byte(&(*s++)));
				}
				else
				{
					_putch(' ');
				}
			}
			setcolor(CYAN<<4 | BLACK);
			_putch(' ');
			_putch(ACS_VLINE);
		}


		gotoxy(x,y+nitems+1);
		_putch(ACS_LLCORNER);
		for (i=0;i<width+2;i++)
		{
			_putch(ACS_HLINE);
		}
		_putch(ACS_LRCORNER);


		while (!_kbhit()) ;

		if (_kbhit())
		{
			key = _getch();
			switch(key)
			{
				case KB_UP: if (itemno>0) itemno--;  else itemno = nitems-1; break;
				case KB_DOWN: itemno++; itemno %= nitems; break;
				case KB_ESC: cursoron(); return UI_NOTHING;
				case KB_ENTER: cursoron(); return itemno+1;
			}
		}
	}

	return 1;

runmenu1_null_error:
	return UI_NOTHING;
}



void runerrord0(char x, char y, const char * const text, const char * const msg)
{
    ASSERT_NULL(text, runerrord0_null_error);
	ASSERT_NULL(msg, runerrord0_null_error);

    size_t t1 = strlen_P(text);
    size_t t2 = strlen_P(msg);
    if (t1 < t2)
    {
        t1 = t2;
    }
    drawframe(x, y, (x+t1+2), 4, WHITE  | RED <<4);
    putlabel(x+1, y+1, WHITE  | RED <<4, text);
    putlabel(x+2, y+2, WHITE | RED << 4, msg);

    while (!_kbhit());
	(void) _getch();

runerrord0_null_error:
    return;
}

void runerrord1(uint8_t x, uint8_t y, uint8_t ln, const char * const text, const char *fmt, ...)
{
	ASSERT_NULL(text, runerrord0_null_error);
	size_t t1 = strlen_P(text);

	drawframe(x, y, ln, 3, WHITE  | RED <<4);
	putlabel(x+1, y+1, WHITE  | RED <<4, text);
	gotoxy(x+1, y+2);
	//putlabel(x+2, y+2, WHITE | RED << 4, msg);
	setcolor(WHITE | RED << 4);
	va_list arg;
	va_start(arg, fmt);
	vfprintf_P(stdout, fmt, arg);
	va_end(arg);

	while (!_kbhit());
	(void) _getch();
	runerrord0_null_error:
	return;
}

void runinfod0(char x, char y, bool waitkeypr, const char * const text, const char * const msg)
{
	ASSERT_NULL(text, runerrord0_null_error);
    size_t t1 = strlen_P(text);
    size_t t2 = strlen_P(msg);
    if (t1 < t2)
    {
        t1 = t2;
    }
    drawframe(x, y, (x+t1+2), 4, WHITE  | GREEN <<4);
    putlabel(x+1, y+1, WHITE  | GREEN << 4, text);
    putlabel(x+2, y+2, WHITE | GREEN << 4, msg);

	if (waitkeypr)
	{
		while (!_kbhit());
		(void) _getch();
	}
runerrord0_null_error:
    return;
}

int16_t draw_input_dialog(char x, char y, const char * text, char * data, uint8_t max_data_len)
{
    ASSERT_NULL(text, error);
    ASSERT_NULL(data, error);

    uint8_t i = 0;

    size_t x1 = strlen_P(text);

    if (x1 < max_data_len)
    {
        x1 = max_data_len;
    }

    //draw input dialog (frame)
    drawframe(x, y, x+(uint8_t)x1, 5, WHITE | RED << 4);
    labelf_P(x+2, y+1, PSTR("%S"), text);

    gotoxy(x+2,y+3);
    textattr(WHITE | BLACK <<4);

    for (i=0;i<max_data_len;i++)
    {
        _putch(' ');
    }

    //read data
    gotoxy(x+2,y+3);

    cursoron();

    int16_t res = _cgets(data, max_data_len);

    cursoroff();

    return res;

error:
    return -1;
}

void drawfkeys(const char *fkeys[])
{
 const char *s;
 int i, j;

 gotoxy(1,25);
 for (i=0;i<10;i++) {
   textcolor(WHITE);
   textbackground(BLACK);
   if (i!= 0)
    _putch(' ');
   if (i== 9) {
    _putch('1');
    _putch('0');
   } else
   _putch((i%10)+'1');
   textcolor(BLACK);
   textbackground(CYAN);

  s = fkeys[i] ? fkeys[i] : 0;
  for (j=0;j<6;j++) {
   if (s && *s)
    _putch(*s++);
   else _putch(' ');
  }
 }
}


//from ram, not PROGMEM
void labelf(uint8_t x, uint8_t y, const char *fmt, ...)
{
	gotoxy(x,y);
	va_list arg;
	va_start(arg, fmt);
	vfprintf(stdout, fmt, arg);
	va_end(arg);
}

void labelf_P(uint8_t x, uint8_t y, const char *fmt, ...)
{
	gotoxy(x,y);
	va_list arg;
	va_start(arg, fmt);
	vfprintf_P(stdout, fmt, arg);
	va_end(arg);
}

void putlabel(uint8_t x, uint8_t y, int32_t color, const char * itm)
{
	setcolor(color);
	gotoxy(x, y);

	char ch = 0;
	uint8_t i = 0;
	while ((ch = pgm_read_byte(&itm[i])) != 0)
	{
		_putch(ch);
		++i;
	}

	return;
}

void putlabelint(uint8_t x, uint8_t y, int32_t color, uint8_t w_len, const char * itm, int val)
{
	setcolor(color);
	gotoxy(x, y);

	char ch = 0;
	uint8_t i = 0;
	while ((ch = pgm_read_byte(&itm[i])) != 0)
	{
		_putch(ch);
		++i;
	}

	gotoxy(x+w_len, y);

	fprintf(stdout, "%i", val);
	return;
}

void drawgauge2(uint8_t x, uint8_t y, uint8_t x1, uint8_t x_s1, uint8_t x_s2, char xs, uint8_t x_e1, uint8_t x_e2, char xe)
{
	//ASSERT_NULL(panel, drawgauge2_error);
	if (x1 < x) goto drawgauge2_error;

	gotoxy(x,y);
	textbackground(LIGHTGRAY);

	uint8_t i = 0;
	for (;i<x1-x;i++)
	{
		_putch(' ');
	}

	textcolor(WHITE);
	textbackground(LIGHTCYAN);
	gotoxy(x_s1,y);
	i = x_s1;
	while (x_s2 > i)
	{
		_putch(xs);
		++i;
	}

	textbackground(CYAN);
	gotoxy(x_e1, y);
	i = x_e1;
	while (x_e2 > i)
	{
		_putch(xe);
		++i;
	}

drawgauge2_error:
	return;
}

// drawframe(10, 10, 40, 9, WHITE  | RED <<4);
void drawframe(uint8_t x, uint8_t y, uint8_t width, uint8_t height, uint32_t color)
{
  int i,j;

  setcolor(color);
  gotoxy(x,y);

  _putch(ACS_ULCORNER);
  for (i=0;i<width+2;i++)
  {
	_putch(ACS_HLINE);
  }
  _putch(ACS_URCORNER);

  for (i = 0;i<height;i++)
  {
	  gotoxy(x,y+i+1);
	  _putch(ACS_VLINE);
	  _putch(' ');

	   for (j=0;j<width;j++)
	   {
			_putch(' ');
	   }
		_putch(' ');
		_putch(ACS_VLINE);
  }

  gotoxy(x,y+height+1);
  _putch(ACS_LLCORNER);
  for (i=0;i<width+2;i++){
	_putch(ACS_HLINE);
  }
  _putch(ACS_LRCORNER);

  return;
}
