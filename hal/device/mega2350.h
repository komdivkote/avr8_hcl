/*-
 *	Copyright (c) 2014-2015	Alexander Morozov. NiXD ORG
 *	All rights reserved.
 *
 * LICENSE: This source file is subject to the New BSD license that is
 * available through the world-wide-web at the following URI:
 * http://www.opensource.org/licenses/bsd-license.php or in the root folder
 * of the project in file LICENSE.TXT
 */


#ifndef MEGA2350_H_
#define MEGA2350_H_

#include "../extlib/e_enum.h"

#ifndef __AVR_ATmega2560__
	error "Wrong hardware definition file was included. Included for atmega2560."
#endif

#define DEVICE_MODEL_ID 0x10

#define SFR_IO_FINISH 0x5F

VERIFY_ENUM_FUNCTION_DEC_P(io_port);

//HARDWARE COUNT SET
#define HW_RESET_PIN P
#define EEPROM_SIZE 4000
#define MAX_IO_PORTS 11
#define MAX_USART_PORTS 4   //devs
#define MAX_SPI_PORTS 1     //devs
#define MAX_TIMERS 6

#define ADC_MAX_CHAN_RECS 10
#define ADC_CHANNELS 16
#define ADC_CHANNELS_BANK 8


//---TUNING
#define USART_RCV_BUFFER 60//60 //bytes in RAM
#define USART_TXE_BUFFER 60 //bytes in RAM
#define USART_PIPE_SLOTS 5

//---STRUCTS, UNIONS, ENUMS ETC
#define FOREACH_IO_ADDRESSES(DV)\
	DV(PORTS_A, (uint8_t) 0)\
	DV(PORTS_B, (uint8_t) 1)\
	DV(PORTS_C, (uint8_t) 2)\
	DV(PORTS_D, (uint8_t) 3)\
	DV(PORTS_E, (uint8_t) 4)\
	DV(PORTS_F, (uint8_t) 5)\
	DV(PORTS_G, (uint8_t) 6)\
	DV(PORTS_H, (uint8_t) 7)\
	DV(PORTS_J, (uint8_t) 8)\
	DV(PORTS_K, (uint8_t) 9)\
	DV(PORTS_L, (uint8_t) 10)

enum ports
{
	FOREACH_IO_ADDRESSES(GENERATE_ENUM_VAL)
};

struct hw_ports
{
	enum ports e_dev_port;
	uint8_t pins;
};

struct usart_hwaddr
{
	uint16_t uscra;
	uint16_t uscrb;
	uint16_t uscrc;
	uint16_t ubrr;
	uint16_t udr;
	uint8_t rxc;
	uint8_t udre;
};

struct spi_hwaddr
{
    uint16_t spcr;
    uint16_t spsr;
    uint16_t spdr;
};

struct spi_pins_nums
{
    uint8_t sck;
    uint8_t mosi;
    uint8_t miso;
    uint8_t ss;
};

//usart
/*Hardware port selector.*/

#define FOREACH_USART_DEV(DV) \
	DV(DEV_USART0, (uint8_t) 0) \
	DV(DEV_USART1, (uint8_t) 1) \
	DV(DEV_USART2, (uint8_t) 2) \
	DV(DEV_USART3, (uint8_t) 3)

//SPI
#define FOREACH_SPI_DEV(DV) \
	DV(DEV_SPI0, (uint8_t) 0)

//TIMER
#define FOREACH_TIMER_HW(DV)\
    DV(TIMER_HW_0, (uint8_t) 0)\
    DV(TIMER_HW_1, (uint8_t) 1)\
    DV(TIMER_HW_2, (uint8_t) 2)\
    DV(TIMER_HW_3, (uint8_t) 3)\
    DV(TIMER_HW_4, (uint8_t) 4)\
    DV(TIMER_HW_5, (uint8_t) 5)

enum timers_id
{
    FOREACH_TIMER_HW(GENERATE_ENUM_VAL)
};

enum timer_bit_width
{
    TIMER_BIT_WIDTH_8 = 8,
    TIMER_BIT_WIDTH_16 = 16
};

struct timer_hwaddr
{
    uint8_t timer_hw_id;
    uint8_t timer_bit_width;
    uint16_t tccra;
    uint16_t tccrb;
    uint16_t tccrc;
    uint16_t tcnt;
    uint16_t ocra;
    uint16_t ocrb;
    uint16_t ocrc;
    uint16_t timsk;
    uint16_t tifr;
    uint16_t icr;

    uint8_t pwm_pins;
   // struct hw_ports * pwm_ports;
};

//--------------VARIABLES INITIALIZATION----------------

extern const uint16_t port_addresses[MAX_IO_PORTS] PROGMEM;
extern const uint16_t ddr_addresses[MAX_IO_PORTS] PROGMEM;
extern const uint16_t pin_addresses[MAX_IO_PORTS] PROGMEM;

extern const struct hw_ports spi_pins[MAX_SPI_PORTS] PROGMEM;
extern const struct spi_hwaddr s_spi_addr[MAX_SPI_PORTS] PROGMEM;
extern const struct spi_pins_nums s_spi_pins[MAX_SPI_PORTS] PROGMEM;

extern const struct hw_ports usart_pins[MAX_USART_PORTS] PROGMEM;
extern const struct usart_hwaddr s_usart_addr[MAX_USART_PORTS] PROGMEM;
extern const struct hw_ports adc_pins[ADC_CHANNELS] PROGMEM;

//timer
extern const struct timer_hwaddr s_timer_addr[MAX_TIMERS] PROGMEM;
extern const struct hw_ports pwm_pins[][3] PROGMEM;

//---------------HARDWARE SPECIFIC VARIABLES/STRUCTURES---------------

#define U2X 1 //A
#define MPCM 0 //A

#define TXEN 3
#define RXEN 4
#define UDRIE 5
#define TXCIE 6
#define RXCIE 7

#define UCPOL 0 //C

#define UCSZ2 2 //B
#define UCSZ1 2 //C
#define UCSZ0 1 //C

#define USBS 3 //C

#define UPM1 5 //C
#define UPM0 4 //C

#define UMSEL1 7 //C
#define UMSEL0 6 //C

#endif /* MEGA2350_H_ */
