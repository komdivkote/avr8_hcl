/*-
 *	Copyright (c) 2014-2015	Alexander Morozov. NiXD ORG
 *	All rights reserved.
 *
 * LICENSE: This source file is subject to the New BSD license that is
 * available through the world-wide-web at the following URI:
 * http://www.opensource.org/licenses/bsd-license.php or in the root folder
 * of the project in file LICENSE.TXT.
 */


#ifndef DEV_H_
#define DEV_H_
#include <avr/pgmspace.h>
#include <stdint.h>
#include <stdio.h>

#if defined (__AVR_ATmega2560__)
	#include "./mega2350.h"
#else

	#if !defined(__COMPILING_AVR_LIBC__)

		#warning "device type not defined"

	#endif

#endif



#endif /* DEV_H_ */
