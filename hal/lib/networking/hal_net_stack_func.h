#ifndef HAL_NET_STACK_FUNC_H_INCLUDED
#define HAL_NET_STACK_FUNC_H_INCLUDED

int32_t eth_send(struct knetdevice * knd, eth_frame_t *frame, uint16_t len);
int32_t ip_send(struct knetdevice * knd, eth_frame_t *frame, uint16_t len);
int32_t ip_resend(struct knetdevice * knd, eth_frame_t *frame, uint16_t len);
int32_t ip_reply(struct knetdevice * knd, eth_frame_t *frame, uint16_t len);

#if (WITH_TCP == YES)
int32_t tcp_xmit(struct knetdevice * knd, struct knetstate * state, eth_frame_t *frame, uint16_t len);
int32_t tcp_send(struct knetdevice * knd, struct knetstate * sock,
         eth_frame_t *frame, uint16_t len, uint8_t options);
#endif


int32_t eth_resend(struct knetdevice * knd, eth_frame_t *frame, uint16_t len);
void eth_filter(struct knetdevice * knd);

uint8_t *arp_resolve(struct knetdevice * knd, uint32_t node_ip_addr);
void arp_filter(struct knetdevice * knd, uint16_t len);

int32_t eth_reply(struct knetdevice * knd, eth_frame_t *frame, uint16_t len);

#if (WITH_UDP == YES)
int32_t udp_send(struct knetdevice * knd, uint32_t rIP, uint16_t rPORT,
                 uint16_t lPORT, eth_frame_t *frame, uint16_t len);
int32_t udp_send_internal(struct knetdevice * knd, eth_frame_t *frame,
                          uint16_t len);
int32_t udp_reply(struct knetdevice * knd, eth_frame_t *frame,
                  uint16_t len);
#endif

void icmp_filter(struct knetdevice * knd, eth_frame_t *frame, uint16_t len);
void ip_filter(struct knetdevice * knd, uint16_t len);

uint16_t ip_cksum(uint32_t sum, uint8_t *buf, size_t len);

#endif // HAL_NET_STACK_FUNC_H_INCLUDED
