/*-
 *	Copyright (c) 2014-2016	Alexander Morozov. www.nixd.org
 *	All rights reserved.
 *
 * LICENSE: This source file is subject to the New BSD license that is
 * available through the world-wide-web at the following URI:
 * http://www.opensource.org/licenses/bsd-license.php or in the root folder
 * of the project in file LICENSE.TXT.
 */

 /*
  * Not yet tested!!!
  */

#include <avr/io.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <avr/pgmspace.h>
#include <avr/interrupt.h>
#include <util/atomic.h>
#include <avr/wdt.h>
#include "kernel_sectioning.h"
#include "kernel_memory_private.h"
#include "kernel_process_private.h"

#if (KERNEL_TRACK_PROC_STATE == YES)
const char KSTATE_SEM[] PROGMEM	= "KSEM\0";
#endif /* KERNEL_TRACK_PROC_STATE */

ATTRIBUTE_KERNEL_SECTION
struct kproc_sem *
kernel_sem_init(uint8_t samcnt)
{
    struct kproc_sem * sem = (struct kproc_sem *) kernel_malloc(sizeof(struct kproc_sem));
    if (sem == NULL)
    {
        return NULL;
    }

    sem->sem_amnt = samcnt;
    sem->sem_lcnt = samcnt;
    sem->sem_wcnt = 0;

    return sem;
}

ATTRIBUTE_KERNEL_SECTION
int8_t
kernel_sem_enter(struct kproc_sem * s_kproc_sem)
{
    if (s_kproc_sem == NULL)
    {
        return -1;
    }

    KERNEL_SET_STATE(KSTATE_SEM)
    {
        //switching to cooperative mode to prevent other processes form losing sync
		ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
		{
			current_kprocess->node->flags |= KPROC_FLAG_COOP;
		}

        if ( (s_kproc_sem->sem_lcnt > 0) && (s_kproc_sem->sem_wcnt == 0) )
        {
            //use it
            --s_kproc_sem->sem_lcnt;
            current_kprocess->wait_sem = NULL;
            goto leave_locked;
        }

        //setting the process to wait
		current_kprocess->node->flags |= KPROC_FLAG_WAIT;

        current_kprocess->wait_sem = s_kproc_sem;
        ++s_kproc_sem->sem_wcnt;

        //releasing control
		kernel_release_control();

		//we got control back, it mean that sem slot is free!
        current_kprocess->wait_sem = NULL;
        --s_kproc_sem->sem_lcnt;
        --s_kproc_sem->sem_wcnt;

leave_locked:
		ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
		{
			current_kprocess->node->flags &= ~KPROC_FLAG_COOP;
		}
    }

    return 0;
}

ATTRIBUTE_KERNEL_SECTION
int8_t
kernel_sem_leave(struct kproc_sem * s_kproc_sem)
{
    if (s_kproc_sem == NULL)
    {
        return -1;
    }

    KERNEL_SET_STATE(KSTATE_SEM)
    {
        //switching to cooperative mode to prevent other processes form losing sync
		ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
		{
			current_kprocess->node->flags |= KPROC_FLAG_COOP;
		}

        //inc counter of free blocks
        if (s_kproc_sem->sem_lcnt < s_kproc_sem->sem_amnt)
        {
            ++s_kproc_sem->sem_lcnt;
        }

        //switch back
        ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
		{
			current_kprocess->node->flags &= ~KPROC_FLAG_COOP;
		}
    }

    return 0;
}

//information
ATTRIBUTE_KERNEL_SECTION
uint8_t
kernel_sem_get_total(struct kproc_sem * s_kproc_sem)
{
	return s_kproc_sem->sem_amnt;
}

ATTRIBUTE_KERNEL_SECTION
uint8_t
kernel_sem_get_free(struct kproc_sem * s_kproc_sem)
{
	return s_kproc_sem->sem_lcnt;
}

ATTRIBUTE_KERNEL_SECTION
uint8_t
kernel_sem_get_waiting(struct kproc_sem * s_kproc_sem)
{
	return s_kproc_sem->sem_wcnt;
}
