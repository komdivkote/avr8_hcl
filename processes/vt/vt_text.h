/*-
 *	Copyright (c) 2014-2015	Alexander Morozov. NiXD ORG
 *	All rights reserved.
 *
 * LICENSE: This source file is subject to the New BSD license that is
 * available through the world-wide-web at the following URI:
 * http://www.opensource.org/licenses/bsd-license.php or in the root folder
 * of the project in file LICENSE.TXT.
 */


#ifndef VT_TEXT_H_
#define VT_TEXT_H_

#define VT_MAIN_PROCESS 1
#define VT_MAIN_MANAGE 2
#define VT_MAIN_STREAMS 3
#define VT_MAIN_IO_PINS 4
#define VT_MAIN_SAVECFG 5
#define VT_MAIN_CLEAR_EE 6
#define VT_MAIN_ERROR_LIST 7
#define VT_MAIN_REBOOT 8
#define VT_MAIN_SHUTDOWN 9
#define VT_SHOW_EEPORM 10
#define VT_SRAM_VIEWER 11
#define VT_NETSTAT_VIEWER 12

#define VT_EEPR_CLEAN 1
#define VT_EEPR_RESET 2
#define VT_EEPR_BACK 3

#define VT_YES 1
#define VT_NO 2


extern const char * const main_menu[];
extern const char * const eeprom_menu[];
extern const char * const yesno_menu[];

extern const char label_ram[];
extern const char label_exit_conf[];
extern const char label_eepr_d2[];
extern const char label_eepr_d[];
extern const char label_finished_e[];
extern const char label_devices[];
extern const char label_pages[];
extern const char label_dev_add[];
extern const char label_dev_add_h[];
extern const char label_dev_edit[];
extern const char label_dev_rem[];
extern const char label_dev_sel[];
extern const char label_dev_exit[];
extern const char label_dev_left[];
extern const char label_dev_right[];
extern const char label_dev_opts[];
extern const char label_devs[];
extern const char label_tb0_no[];
extern const char label_tb0_did[];
extern const char label_tb0_drv[];
extern const char label_tb0_hnd[];
extern const char label_tb0_mem[];

extern const char label_tb1_title[];
extern const char label_tb1_num[];
extern const char label_tb1_addr[];
extern const char label_tb1_dest[];
extern const char label_tb1_ext[];
extern const char label_tb1_flags1[];
extern const char label_tb1_flags2[];

extern const char label_tb2_title[];
extern const char label_tb2_recno[];
extern const char label_tb2_owner[];
extern const char label_tb2_port[];
extern const char label_tb2_pins[];

extern const char label_tb3_title[];

extern const char label_press_any_key[];

extern const char label_query_fail[];
extern const char label_query_devf[];
extern const char label_query_ddf[];

extern const char title_areyoushure[];
extern const char label_yes[];
extern const char label_no[];

extern const char press_any_key_to_cont[];
extern const char save_changes_to_eeprom[];
extern const char do_not_power_down[];
extern const char do_work_in_prog[];
extern const char no_fd_created[];
extern const char error_capt[];
extern const char unable_to_remove[];
extern const char query_failed[];
extern const char dd_ret_null[];
extern const char dd_dev_memcpy_failed[];

extern const char info_capt[];
extern const char measure_sensors[];
extern const char measure_all[];
extern const char measurments_capt[];
extern const char value_capt[];
extern const char send_data_capt[];
extern const char receive_monitor_capt[];
extern const char add_slave[];
extern const char remove_slave[];
extern const char rescan_bus[];

extern const char not_supported[];

extern const char measuring_capt[];
extern const char measuring_sync_failed[];

extern const char ocra8bit[];
extern const char ocrb8bit[];
extern const char ocrc8bit[];
extern const char ocra16bit[];
extern const char ocrb16bit[];
extern const char ocrc16bit[];
extern const char icr8bit[];
extern const char icr16bit[];

extern const char system_timer_busy_capt[];
extern const char io_mask_input_capt[];

extern const char input_mac_address[];

extern const char usart_buf_rx_size_capt[];
extern const char usart_buf_tx_size_capt[];
extern const char usart_buf_cts_rts_pin[];

extern const char usart_router_scr_capt[];
extern const char usart_router_rcv_capt[];
extern const char usart_router_send_capt[];

extern const char spi_sel_id[];
extern const char io_sel_id[];
extern const char io_sel_id_mon[];

extern const char anon_pan_sel[];

extern const char label_netstat_title[];
extern const char label_netstat_if[];
extern const char label_netstat_if_id[];
extern const char label_netstat_if_did[];
extern const char label_netstat_if_mac[];
extern const char label_netstat_if_ip[];
extern const char label_netstat_if_mask[];
extern const char label_netstat_if_gateway[];
extern const char label_netstat_listen[];
extern const char label_netstat_lis_lid[];
extern const char label_netstat_lis_proto[];
extern const char label_netstat_lis_lport[];
extern const char label_netstat_states[];
extern const char label_netstat_sta_id[];
extern const char label_netstat_if_rip[];
extern const char label_netstat_bottom[];

extern const char proc_sel[];
extern const char proc_mux_nf[];

extern const char inp_ip_addr[];
extern const char inp_mask_addr[];
extern const char inp_gateway_addr[];

extern const char netdev_pan_netid[];
extern const char netdev_pan_notfound[];
extern const char length_assert[];
extern const char dhclient_failed[];
extern const char dhclient_not_sup[];

extern const char label_netstat_arp[];
extern const char label_netstat_arp_bottom[];

extern const char label_u2eth_did[];

#endif /* VT_TEXT_H_ */

/*http://www.atmel.com/webdoc/AVRLibcReferenceManual/FAQ_1faq_rom_array.html
char buf[32];
PGM_P p;
int i;

memcpy_P(&p, &array[i], sizeof(PGM_P));
strcpy_P(buf, p);
*/
