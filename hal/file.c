/*-
 *	Copyright (c) 2014-2016	Alexander Morozov. www.nixd.org
 *	All rights reserved.
 *
 * LICENSE: This source file is subject to the New BSD license that is
 * available through the world-wide-web at the following URI:
 * http://www.opensource.org/licenses/bsd-license.php or in the root folder
 * of the project in file LICENSE.TXT.
 */

#include "build_defs.h"
#include <stdio.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include "hal_sectioning.h"
#include "hal_private.h"
//#include "./stream.h"

ATTRIBUTE_HAL_SECTION
struct dev_descr_node *
filedest(FILE * file)
{
    SEARCH_IN_LIST_2(struct hal_stream_list, stream_list_ptr, item, item->node == file, not_found);

    return item->dest;

not_found:
    setSysError(SYSTEM_ITEM_NOT_FOUND);
    return NULL;
}

ATTRIBUTE_HAL_SECTION
void
fileflush(FILE * file)
{

    struct dev_descr_node * ddn = filedest(file);
    ASSERT_NULL_ER(ddn, SYSTEM_ITEM_NOT_FOUND, );

    (void)hal_dev_node_flush(ddn);

    return;
}
