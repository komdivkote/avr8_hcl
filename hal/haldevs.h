#ifndef HALDEVS_H_INCLUDED
#define HALDEVS_H_INCLUDED

#include "./htypes.h"
#include "./extlib/e_enum.h"
#include "./shared.h"

/* Available device descriptors */
#define FOREACH_DD_CODE(DV)\
	DV(DEV_DESCR_NULL, (uint8_t) 0)\
	DV(DEV_DESCR_VIRTUAL, (uint8_t) 1)\
	DV(DEV_DESCR_USB, (uint8_t) 2)\
	DV(DEV_DESCR_IO, (uint8_t) 3)\
	DV(DEV_DESCR_EXTINT, (uint8_t) 4)\
	DV(DEV_DESCR_ADC, (uint8_t) 5)\
	DV(DEV_DESCR_COMPAR, (uint8_t) 6)\
	DV(DEV_DESCR_TIMER, (uint8_t) 7)\
	DV(DEV_DESCR_USART, (uint8_t) 8)\
	DV(DEV_DESCR_TWI, (uint8_t) 9)\
	DV(DEV_DESCR_OWI, (uint8_t) 10)\
	DV(DEV_DESCR_SPI, (uint8_t) 11)

enum dev_descr_code {
	FOREACH_DD_CODE(GENERATE_ENUM_VAL)
};

FOREACH_DD_CODE(GENERATE_VAR_H_VAL)
extern const char * const dd_dev_captions[]; //captions
extern const uint8_t dd_dev_ids[]; //id list
extern const uint8_t dd_dev_cnt;


#define FOREACH_D_HANDLERS(DV)\
    DV(ROOT_DRIVER_DEV, (uint8_t) 0)\
    DV(USART_ROUTE,  (uint8_t) 1)\
    DV(SOFTWARE_OWI_DS18X20,  (uint8_t) 2)\
    DV(SPI_MAX6675_SINGLETON,  (uint8_t) 3)\
    DV(IO_ANNUNCIATOR_PANEL,  (uint8_t) 4)\
    DV(IO_DIG_INP_MON, (uint8_t) 5)\
    DV(SPI_ENC28J60, (uint8_t) 6)\
    DV(U2ETH_ROUTER, (uint8_t) 7)

enum dev_desc_handlers
{
    FOREACH_D_HANDLERS(GENERATE_ENUM_VAL)
};


#define FOREACH_SPI_HNDLRS(DV)\
    DV(ENC28J60, (uint8_t) SPI_ENC28J60)

enum dev_desc_spi_hndlrs
{
    FOREACH_SPI_HNDLRS(GENERATE_ENUM_VAL)
};

#define FOREACH_OWI_HNDLRS(DV)\
    DV(DS18_b20_s20_22, (uint8_t) SOFTWARE_OWI_DS18X20)

enum dev_desc_owi_hndlrs
{
    FOREACH_OWI_HNDLRS(GENERATE_ENUM_VAL)
};

#define FOREACH_IO_HNDLRS(DV)\
    DV(ANNUNCIATOR_PANEL, (uint8_t) IO_ANNUNCIATOR_PANEL)

enum dev_desc_io_hndlrs
{
    FOREACH_IO_HNDLRS(GENERATE_ENUM_VAL)
};

#define FOREACH_VIRTUAL_HNDLRS(DV)\
    DV(USART2ETHERNET, (uint8_t) U2ETH_ROUTER)\
    DV(INPUTMONITOR, (uint8_t) IO_DIG_INP_MON)

enum dev_desc_virtual_hndlrs
{
    FOREACH_VIRTUAL_HNDLRS(GENERATE_ENUM_VAL)
};

FOREACH_D_HANDLERS(GENERATE_VAR_H_VAL)
FOREACH_SPI_HNDLRS(GENERATE_VAR_H_VAL)
FOREACH_OWI_HNDLRS(GENERATE_VAR_H_VAL)
FOREACH_IO_HNDLRS(GENERATE_VAR_H_VAL)
FOREACH_VIRTUAL_HNDLRS(GENERATE_VAR_H_VAL)

extern const char * const d_handlers_captions[];
extern const char * const spi_handlers_captions[];
extern const char * const owi_handlers_captions[];
extern const char * const io_handlers_captions[];
extern const char * const virtual_handlers_captions[];

extern const uint8_t d_handlers_ids[];
extern const uint8_t spi_handlers_ids[];
extern const uint8_t owi_handlers_ids[];
extern const uint8_t io_handlers_ids[];
extern const uint8_t virtual_handlers_ids[];

extern const uint8_t d_handlers_cnt;
extern const uint8_t spi_handlers_cnt;
extern const uint8_t owi_handlers_cnt;
extern const uint8_t io_handlers_cnt;
extern const uint8_t virtual_handlers_cnt;

#endif // HALDEVS_H_INCLUDED
