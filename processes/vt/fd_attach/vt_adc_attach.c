/*-
 *	Copyright (c) 2014-2015	Alexander Morozov. NiXD ORG
 *	All rights reserved.
 *
 * LICENSE: This source file is subject to the New BSD license that is
 * available through the world-wide-web at the following URI:
 * http://www.opensource.org/licenses/bsd-license.php or in the root folder
 * of the project in file LICENSE.TXT.
 */

/*-
 *	Copyright (c) 2014-2015	Alexander Morozov. NiXD ORG
 *	All rights reserved.
 *
 * LICENSE: This source file is subject to the New BSD license that is
 * available through the world-wide-web at the following URI:
 * http://www.opensource.org/licenses/bsd-license.php or in the root folder
 * of the project in file LICENSE.TXT
 */

#include "../../../hal/build_defs.h"
#include <avr/io.h>
#include <avr/pgmspace.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include "../../../hal/hal.h"
#include "../../../hal/drivers/adc/adc.h"
#include "../../../hal/handlers.h"
#include "../virtual_terminal.h"
#include "../../../lib/conio/conio.h"
#include "../../../lib/conio/kbd.h"
#include "../../../lib/conio/ui.h"
#include "../vt_text.h"
#include "vt_attach.h"

FOREACH_ADC_EVENT_SOURCE(GENERATE_VARS_VAL);
const struct menudialog_entry vt_adc_event_source_captions_struct[] PROGMEM = {
	FOREACH_ADC_EVENT_SOURCE(GENERATE_VAR_STRUCT)
	{
		P_NULL, NULL
	}
};

FOREACH_ADC_REF(GENERATE_VARS_VAL);
const struct menudialog_entry vt_adc_ref_captions_struct[] PROGMEM = {
	FOREACH_ADC_REF(GENERATE_VAR_STRUCT)
	{
		P_NULL, NULL
	}
};

FOREACH_ADC_PRESC(GENERATE_VARS_VAL);
const struct menudialog_entry vt_adc_presc_captions_struct[] PROGMEM = {
	FOREACH_ADC_PRESC(GENERATE_VAR_STRUCT)
	{
		P_NULL, NULL
	}
};

const char ch0[] PROGMEM = "CHANNEL 0\0";
const char ch1[] PROGMEM = "CHANNEL 1\0";
const char ch2[] PROGMEM = "CHANNEL 2\0";
const char ch3[] PROGMEM = "CHANNEL 3\0";
const char ch4[] PROGMEM = "CHANNEL 4\0";
const char ch5[] PROGMEM = "CHANNEL 5\0";
const char ch6[] PROGMEM = "CHANNEL 6\0";
const char ch7[] PROGMEM = "CHANNEL 7\0";
#if ADC_CHANNELS > ADC_CHANNELS_BANK
const char ch8[] PROGMEM = "CHANNEL 8\0";
const char ch9[] PROGMEM = "CHANNEL 9\0";
const char ch10[] PROGMEM = "CHANNEL 10\0";
const char ch11[] PROGMEM = "CHANNEL 11\0";
const char ch12[] PROGMEM = "CHANNEL 12\0";
const char ch13[] PROGMEM = "CHANNEL 13\0";
const char ch14[] PROGMEM = "CHANNEL 14\0";
const char ch15[] PROGMEM = "CHANNEL 15\0";
#endif // ADC_CHANNELS

const struct menudialog_entry vt_adc_channel_captions_struct[] PROGMEM = {
	{
	    ch0, ADC_SEL_CHAN_0
	},
	{
	    ch1, ADC_SEL_CHAN_1
	},
	{
	    ch2, ADC_SEL_CHAN_2
	},
	{
	    ch3, ADC_SEL_CHAN_3
	},
	{
	    ch4, ADC_SEL_CHAN_4
	},
	{
	    ch5, ADC_SEL_CHAN_5
	},
	{
	    ch6, ADC_SEL_CHAN_6
	},
	{
	    ch7, ADC_SEL_CHAN_7
	},
#if ADC_CHANNELS > ADC_CHANNELS_BANK
    {
	    ch8, ADC_SEL_CHAN_8
	},
	{
	    ch9, ADC_SEL_CHAN_9
	},
	{
	    ch10, ADC_SEL_CHAN_10
	},
	{
	    ch11, ADC_SEL_CHAN_11
	},
	{
	    ch12, ADC_SEL_CHAN_12
	},
	{
	    ch13, ADC_SEL_CHAN_13
	},
	{
	    ch14, ADC_SEL_CHAN_14
	},
	{
	    ch15, ADC_SEL_CHAN_15
	},
#endif // ADC_CHANNELS
	{
		P_NULL, NULL
	}
};

const char dig0[] PROGMEM = "DISABLE DIGITAL\0";
const char dig1[] PROGMEM = "ENABLE DIGITAL\0";
const struct menudialog_entry vt_adc_disable_digital_captions_struct[] PROGMEM = {
	{
	    dig0, ADC_FLAG_DISABLE_DIGITAL
	},
	{
	    dig1, 0
	},
	{
		P_NULL, NULL
	}
};

const char adj0[] PROGMEM = "LEFT ADJUST\0";
const char adj1[] PROGMEM = "RIGHT ADJUST\0";
const struct menudialog_entry vt_adc_left_adj_captions_struct[] PROGMEM = {
	{
	    adj0, ADC_FLAG_LEFT_ADJ
	},
	{
	    adj1, 0
	},
	{
		P_NULL, NULL
	}
};

const struct menudialog_entry vt_timer_options_captions_struct[] PROGMEM = {
	{
	    add_slave, VT_INFO_OPTS_ADD
	},
	{
	    measurments_capt, VT_INFO_OPTS_MEAS
	},
	{
        measure_all, VT_INFO_OPTS_MEAS_ASYNC
	},
	{
		P_NULL, NULL
	}
};

static int8_t __ui_query_attach(FILE * file, struct adc_setup * adcset);
static int8_t __ui_query_modify(FILE * file, enum adc_channel_selector * _e_adc_ch_sel, enum adc_ref * e_ref_src, enum adc_presc * _e_adc_clock, uint8_t * flags);
static int8_t __ui_query_info(FILE * file, uint8_t * opt);
static int8_t __ui_measure(FILE * file, struct dev_descr_node * dd, uint8_t meas_mode);

static int8_t __ui_query_attach(FILE * file, struct adc_setup * adcset)
{
	int ret = 0;
	int res = 0;
	ret = runmenu0_s(file, 10, 10, 0, 10, vt_adc_event_source_captions_struct, &res);
	//try to create fd for the device
	if (ret == UI_NOTHING)
	{
		return F_FAIL;
	}
	adcset->event_source = (uint8_t) res;

	return F_OK;
}

static int8_t __ui_query_modify(FILE * file, enum adc_channel_selector * _e_adc_ch_sel, enum adc_ref * e_ref_src, enum adc_presc * _e_adc_clock, uint8_t * flags)
{
	int ret = 0;
	int res = 0;
	ret = runmenu0_s(file, 10, 10, 0, 10, vt_adc_channel_captions_struct, &res);
	//try to create fd for the device
	if (ret == UI_NOTHING)
	{
		return F_FAIL;
	}
	*_e_adc_ch_sel = (uint8_t) res;

	ret = runmenu0_s(file, 12, 10, 0, 10, vt_adc_ref_captions_struct, &res);
	//try to create fd for the device
	if (ret == UI_NOTHING)
	{
		return F_FAIL;
	}
	*e_ref_src = (uint8_t) res;

	ret = runmenu0_s(file, 16, 10, 0, 10, vt_adc_presc_captions_struct, &res);
	//try to create fd for the device
	if (ret == UI_NOTHING)
	{
		return F_FAIL;
	}
	*_e_adc_clock = (uint8_t) res;

    *flags = 0;

	ret = runmenu0_s(file, 21, 10, 0, 10, vt_adc_disable_digital_captions_struct, &res);
	//try to create fd for the device
	if (ret == UI_NOTHING)
	{
		return F_FAIL;
	}
	if (res != 0)
    {
        *flags |= (uint8_t) res;
    }

    ret = runmenu0_s(file, 21, 10, 0, 10, vt_adc_left_adj_captions_struct, &res);
	//try to create fd for the device
	if (ret == UI_NOTHING)
	{
		return F_FAIL;
	}
	if (res != 0)
    {
        *flags |= (uint8_t) res;
    }

	return F_OK;
}

static int8_t __ui_query_info(FILE * file, uint8_t * opt)
{
    int ret = 0;
	int res = 0;
	ret = runmenu0_s(file, 10, 10, 0, 10, vt_timer_options_captions_struct, &res);
	//try to create fd for the device
	if (ret == UI_NOTHING)
	{
		return F_FAIL;
	}
	*opt = (uint8_t) res;

	return F_OK;
}

static int8_t __ui_measure(FILE * file, struct dev_descr_node * dd, uint8_t meas_mode)
{
    struct adc_channel chmem;
    int8_t i = 0;
    int8_t ret = 0;

    while (1)
	{
        drawframe(file, 10, 10, 50, 10, WHITE | BLUE <<4);
        putlabel(file, 12, 12, YELLOW | BLUE <<4, measuring_capt);

        putlabel(file, 12, 13, RED | BLUE <<4, PSTR("ID\0"));
        putlabel(file, 16, 13, RED | BLUE <<4, PSTR("VALUE\0"));
        textattr(file, YELLOW | BLUE <<4);


        i = 0;
        if (meas_mode == VT_INFO_OPTS_MEAS_ASYNC)
        {
            ret = adc_run_reading_ASYNC();
        }
        else
        {
            ret = adc_run_reading_SYNC();
        }

        if (ret == 1)
        {
            runerrord0(file, 15, 10, error_capt, measuring_sync_failed);
        }
        else
        {

            while (get_adc_mem(&chmem, i) != 1)
            {
                labelf_P(file, 12, 14+i, PSTR("%d     %u"), chmem.chans_ids, chmem.adc_reading);
                i++;
            }


        }
        putlabel(file, 12, 13+i+1, YELLOW | BLUE <<4, PSTR("F5-reload, any-exit"));


        while (!_kbhit(file));
        switch(_getch(file))
        {
            case KB_F5:
               continue;
            default:
               return F_OK;
        }
	}

    return F_OK;
}

int8_t vt_attach_adc(FILE * file, struct adc_setup * adcset)
{
	int8_t res = 0;

	res = __ui_query_attach(file, adcset);
	ASSERT_DATA(res != F_OK, return F_FAIL);

	return F_OK;
}

int8_t vt_modify_adc(FILE * file, struct dev_descr_node * dd)
{

	int8_t res = 0;
	enum adc_channel_selector _e_adc_ch_sel;
	enum adc_ref e_ref_src;
	enum adc_presc _e_adc_clock;
	uint8_t flags;

	res = __ui_query_modify(file, &_e_adc_ch_sel, &e_ref_src, &_e_adc_clock, &flags);
	ASSERT_DATA(res != F_OK, return F_FAIL);

	res = adc_enable_channel(dd, _e_adc_ch_sel, e_ref_src, _e_adc_clock, flags, NULL);

	ASSERT_DATA(res == -1, return F_FAIL);

	return F_OK;
}

int8_t vt_info_adc(FILE * file, struct dev_descr_node * dd)
{
    uint8_t opt = 0;
    int8_t res = 0;

    res = __ui_query_info(file, &opt);
	ASSERT_DATA(res != F_OK, return F_FAIL);

    switch (opt)
    {
        case VT_INFO_OPTS_ADD:
            //print information summery
            vt_modify_adc(file, dd);
        break;

        case VT_INFO_OPTS_MEAS:
            __ui_measure(file, dd, VT_INFO_OPTS_MEAS);
        break;

        case VT_INFO_OPTS_MEAS_ASYNC:
            __ui_measure(file, dd, VT_INFO_OPTS_MEAS_ASYNC);
        break;
    }

	return F_OK;
}
