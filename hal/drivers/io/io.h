/*-
 *	Copyright (c) 2014-2015	Alexander Morozov www.nixd.org
 *	All rights reserved.
 *
 * LICENSE: This source file is subject to the New BSD license that is
 * available through the world-wide-web at the following URI:
 * http://www.opensource.org/licenses/bsd-license.php or in the root folder
 * of the project in file LICENSE.TXT
 */


#ifndef IO_H_
#define IO_H_

#define IO_REVISION 1

/*
 * enum io_direction
 * direction of the pin
 */
#define FOREACH_IO_PORT_DIR(DV)\
		DV(IO_PORT_DIR_IN, (uint8_t) 0)\
		DV(IO_PORT_DIR_OUT, (uint8_t) 1)\
		DV(IO_PORT_DIR_BI, (uint8_t) 2)

enum io_direction
{
	FOREACH_IO_PORT_DIR(GENERATE_ENUM_VAL)
};

typedef int8_t io_id;               //io identification nummber (unused, will be removed later)

struct io_setup
{
     enum io_direction e_io_dir;
     enum ports e_port;
     uint8_t bit_mask;
};

struct io_null_envlp
{
    enum io_direction e_io_dir;     //direction of the allocated pins
	enum ports e_port;              //ports
	uint8_t bit_mask;               //mask
};

struct io_null_dev
{
	//io_id id;
	struct dev_descr_node * ddn;        //fd instance (owner)
	enum io_direction e_io_dir;     //direction of the allocated pins
	enum ports e_port;              //ports
	uint8_t bit_mask;               //mask
	uint8_t bit_count;              //amount of pins
};

VERIFY_ENUM_FUNCTION_DEC_P(io_dir_func);

/*
 *   Standart module functions
 */
int8_t io_hal_message(struct dev_descr_node * ddn,
                          enum dd_event e_event,
                          void * data0,
                          size_t * dsize0);

int8_t io_kstream_init(struct dev_descr_node * ddn,
                           struct kstream * ks);

int8_t io_stream_init(struct dev_descr_node * ddn,
                             FILE * file);

/*
 * io_get_by_fid
 * looks up in the io table for record by fid
 */
//struct io_null_dev * io_get_by_did(t_dev_id did);

/*
 * io_count_ones
 * this function counts amount of bits set to 1
 * i.e 00000100 will return 1
 */
int8_t io_count_ones(struct io_null_dev * iod);

/*
 * io_find_less_significant_bit
 * looks fot less significant bit in mask
 */
uint8_t io_find_less_significant_bit(struct io_null_dev * iod);

/*
 * io_find_mask_by_order
 * looks up for mask by order (this function will be removed)
 */
uint8_t io_find_mask_by_order(struct io_null_dev * iod, uint8_t n);

/*
 * io_mask_prepare
 * this function is designed to prepare you input (2nd argument) to
 *  to work with selected instance of io_null_dev
 * i.e iod->mask = 00001100
 *          mask = 01001010
 *          res  = 00001000
 */
uint8_t io_mask_prepare(struct io_null_dev * iod, uint8_t mask);

/*
 * io_find_mask_by_selector
 * this function retuens the mask of the value by order
 *  i.e iod->mask = 00001100
 *              n = 1
 *          result= 00000100
 */
uint8_t io_find_mask_by_selector(struct io_null_dev * iod, uint8_t n);

/*
 * io_write8_direct
 * writes 2nd argument ready_mask to the hardware defined by instance of io_null_dev
 *  you must prepare mask by calling io_mask_repare
 *  i.e io_write8_direct(iod, io_mask_repare(iod, my_value);
 */
int8_t io_write8_direct(struct io_null_dev * iod, uint8_t ready_mask);

/*
 * io_read8_direct
 * reads data according to the io_null_dev port and mask
 */
int8_t io_read8_direct(struct io_null_dev * iod, uint8_t * data_out);

/*
 * unused
 */
int8_t io_write8(struct io_null_dev * iod, uint8_t dt);
int8_t io_read8(struct io_null_dev * iod, uint8_t * data_out);

int8_t io_get_mask(struct io_null_dev * iod, uint8_t * mask_out);

#endif /* IO_H_ */
