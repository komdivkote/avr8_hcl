/*-
 *	Copyright (c) 2014-2016	Alexander Morozov. NiXD ORG
 *	All rights reserved.
 *
 * LICENSE: This source file is subject to the New BSD license that is
 * available through the world-wide-web at the following URI:
 * http://www.opensource.org/licenses/bsd-license.php or in the root folder
 * of the project in file LICENSE.TXT
 */

#include "../../../hal/build_defs.h"
#include <avr/io.h>
#include <avr/pgmspace.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include "../../../hal/hal.h"
#include "../../../hal/drivers/enc28j60/enc28.h"
#include "../../../hal/handlers.h"
#include "../virtual_terminal.h"
#include "../../../lib/conio/conio.h"
#include "../../../lib/conio/kbd.h"
#include "../../../lib/conio/ui.h"
#include "../vt_text.h"
#include "vt_attach.h"

static int8_t __ui_query(FILE * file,
                         struct enc28_config * enc28);
static uint8_t a_to_uint8(uint8_t high, uint8_t low);

//http://www.avrfreaks.net/forum/converting-hex-ascii-int
static uint8_t
a_to_uint8(uint8_t high, uint8_t low)
{
    uint8_t val = 0;
    char c;

    if (isdigit(high))         // C library function
    {
        val *= 10UL;
        val += (uint8_t)(high - '0');
    }

    if (isdigit(low))         // C library function
    {
        val *= 10UL;
        val += (uint8_t)(low - '0');
    }

    return val;
}

static int8_t __ui_query(FILE * file,
                         struct enc28_config * enc28)

{
	int ret = 0;
	int res = 0;
    uint8_t data[13];

    //-----
	memset(data, 0, 5);
	ret = draw_input_dialog(file, 10, 10, spi_sel_id, data, 5);
	if (ret < 1)
    {
        return F_FAIL;
    }
    enc28->spi_dev_id = (uint8_t) atoi ( (const char *) data );

	//------
    ret = runmenu0_s(file, 10, 10, 0, 10, vt_io_ports_captions_struct, &res);
	//try to create fd for the device
	if (ret == UI_NOTHING)
	{
		return F_FAIL;
	}
	enc28->e_port = (uint8_t) res;

    //------------

    memset(data, 0, 5);
	ret = draw_input_dialog(file, 10, 10, io_mask_input_capt, data, 5);
	if (ret < 1)
    {
        return F_FAIL;
    }

    enc28->p_mask = (uint8_t) atoi ( (const char *) data );


    //-----
    memset(data, 0, 13);
	ret = draw_input_dialog(file, 10, 10, input_mac_address, data, 13);
	/*if (ret < 11)
    {
        fprintf_P(file, PSTR("ret: %d\r\n"), ret);
        setSysError(SYSTEM_LENGTH_ASSERT);
        return F_FAIL;
    }*/
   /* for (uint8_t i = 0; i < 6; i++)
    {
        enc28->mac_addr[i] = 33;//a_to_uint8(data[i*2], data[i*2+1]);
    }*/
    enc28->mac_addr[0] = 0xaa;
    enc28->mac_addr[1] = 0x00;
    enc28->mac_addr[2] = 0x00;
    enc28->mac_addr[3] = 0x00;
    enc28->mac_addr[4] = 0x00;
    enc28->mac_addr[5] = 0x01;

    return F_OK;
}

int8_t vt_attach_spi_enc28j60(FILE * file, struct enc28_config * data)
{
    return __ui_query(file, data);
}

int8_t vt_modify_spi_enc28j60(FILE * file, struct dev_descr_node * dd)
{
    return F_FAIL;
}

int8_t vt_info_spi_enc28j60(FILE * file, struct dev_descr_node * dd)
{
    return F_FAIL;
}
