/*-
 *	Copyright (c) 2014-2016	Alexander Morozov www.nixd.org
 *	All rights reserved.
 *
 * LICENSE: This source file is subject to the New BSD license that is
 * available through the world-wide-web at the following URI:
 * http://www.opensource.org/licenses/bsd-license.php or in the root folder
 * of the project in file LICENSE.TXT.
 */

#include "../../build_defs.h"

#if BUILD_WITH_ADC == YES

#include <avr/io.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stddef.h>
#include <string.h>
#include <avr/sfr_defs.h>
#include "../../hal.h"
#include "./adc.h"

#include "../../../os/kernel/kernel_memory.h"
#include "../../../os/kernel/kernel_process.h"
#include "../../../os/kernel/kernel_mutex.h"
#include "../../../os/kernel/kernel_isr.h"



static int8_t adc_attach(struct dev_descr_node * ddn, struct adc_setup * adt);
static int8_t adc_query(struct dev_descr_node * ddn, struct hal_query_info * data0);
static int8_t adc_detach(struct dev_descr_node * ddn);
static int8_t adc_update(struct dev_descr_node * ddn, struct adc_setup * tsp);
static int8_t adc_dump(struct dev_descr_node * ddn, void * data0, size_t * dsize0);
static int8_t adc_restore(struct dev_descr_node * ddn, void * data0, size_t * dsize0);
static void adc_isr();

static void adc_init(enum adc_event_source event_source);
static uint8_t _get_channel_by_selector(enum adc_channel_selector adc_ch_sel);
static void _dummy_func(const struct adc_channel * chanl);


volatile struct adc_config * volatile loc_inst = NULL;
volatile struct kproc_mutex * mtx_giant_adc = NULL;


static void
adc_isr()
{
	ASSERT_NULL(loc_inst->sel_chanl, ISR_adc_vect_null_val); //ops, this must not happen

	loc_inst->sel_chanl->adc_reading = ADC;
	loc_inst->sel_chanl->READING_FINISHED(loc_inst->sel_chanl);

	if (loc_inst->adc_async == 1)
	{
		loc_inst->sel_chanl = loc_inst->sel_chanl->next;
		if (loc_inst->sel_chanl != NULL)
		{
			ADMUX = loc_inst->sel_chanl->admux;
			ADCSRA = loc_inst->sel_chanl->adcsra;
			ADCSRB = loc_inst->sel_chanl->adcsrb;

			//run
			ADCSRA |= (1 << ADSC);
		}
		else
		{
			loc_inst->adc_async = 0;
			loc_inst->sel_chanl = NULL;
		}
	}
	else
	{
		loc_inst->sel_chanl = NULL;
	}

    return;
ISR_adc_vect_null_val:
	setSysError(SYSTEM_NULL_ARGUMENT);
}


static void _dummy_func(const struct adc_channel * chanl)
{

	return;
}

//return (v+1)<<4 | b
static uint8_t _get_channel_by_selector(enum adc_channel_selector adc_ch_sel)
{
	switch (adc_ch_sel)
	{
		case ADC_SEL_CHAN_0:
            return ADC_CHAN_0;

		case ADC_SEL_CHAN_1:
			return ADC_CHAN_1;

		case ADC_SEL_CHAN_2:
			return ADC_CHAN_2;

		case ADC_SEL_CHAN_3:
			return ADC_CHAN_3;

		case ADC_SEL_CHAN_4:
			return ADC_CHAN_4;

		case ADC_SEL_CHAN_5:
			return ADC_CHAN_5;

		case ADC_SEL_CHAN_6:
			return ADC_CHAN_6;

		case ADC_SEL_CHAN_7:
			return ADC_CHAN_7;

		case ADC_SEL_GAIN10x_CHAN_0:
			return ADC_CHAN_0;

		case ADC_SEL_GAIN10x_CHAN_1_0:
			return ((ADC_CHAN_1+1)<<4) | ADC_CHAN_0;

		case ADC_SEL_GAIN200x_CHAN_0:
			return ADC_CHAN_0;

		case ADC_SEL_GAIN200x_CHAN_1_0:
			return ((ADC_CHAN_1+1)<<4) | ADC_CHAN_0;

		case ADC_SEL_GAIN10x_CHAN_2:
			return ADC_CHAN_2;

		case ADC_SEL_GAIN10x_CHAN_3_2:
			return ((ADC_CHAN_3+1)<<4) | ADC_CHAN_2;

		case ADC_SEL_GAIN200x_CHAN_2:
			return ADC_CHAN_2;

		case ADC_SEL_GAIN200x_CHAN_3_2:
			return ((ADC_CHAN_3+1)<<4) | ADC_CHAN_2;

		case ADC_SEL_DIF_CHAN_0_1:
			return ((ADC_CHAN_0+1)<<4) | ADC_CHAN_1;

		case ADC_SEL_DIF_CHAN_1:
			return ADC_CHAN_1;

		case ADC_SEL_DIF_CHAN_2_1:
			return ((ADC_CHAN_2+1)<<4) | ADC_CHAN_1;

		case ADC_SEL_DIF_CHAN_3_1:
			return ((ADC_CHAN_3+1)<<4) | ADC_CHAN_1;

		case ADC_SEL_DIF_CHAN_4_1:
			return ((ADC_CHAN_4+1)<<4) | ADC_CHAN_1;

		case ADC_SEL_DIF_CHAN_5_1:
			return ((ADC_CHAN_5+1)<<4) | ADC_CHAN_1;

		case ADC_SEL_DIF_CHAN_6_1:
			return ((ADC_CHAN_6+1)<<4) | ADC_CHAN_1;

		case ADC_SEL_DIF_CHAN_7_1:
			return ((ADC_CHAN_7+1)<<4) | ADC_CHAN_1;

		case ADC_SEL_DIF_CHAN_0_2:
			return ((ADC_CHAN_0+1)<<4) | ADC_CHAN_2;

		case ADC_SEL_DIF_CHAN_1_2:
			return ((ADC_CHAN_1+1)<<4) | ADC_CHAN_2;

		case ADC_SEL_DIF_CHAN_2_2:
			return ((ADC_CHAN_2+1)<<4) | ADC_CHAN_2;

		case ADC_SEL_DIF_CHAN_3_2:
			return ((ADC_CHAN_3+1)<<4) | ADC_CHAN_2;

		case ADC_SEL_DIF_CHAN_4_2:
			return ((ADC_CHAN_4+1)<<4) | ADC_CHAN_2;

		case ADC_SEL_DIF_CHAN_5_2:
			return ((ADC_CHAN_5+1)<<4) | ADC_CHAN_2;

#if ADC_CHANNELS > ADC_CHANNELS_BANK

		case ADC_SEL_CHAN_8:
			return ADC_CHAN_8 - ADC_CHANNELS_BANK;

		case ADC_SEL_CHAN_9:
			return ADC_CHAN_9 - ADC_CHANNELS_BANK;

		case ADC_SEL_CHAN_10:
			return ADC_CHAN_10 - ADC_CHANNELS_BANK;

		case ADC_SEL_CHAN_11:
			return ADC_CHAN_11 - ADC_CHANNELS_BANK;

		case ADC_SEL_CHAN_12:
			return ADC_CHAN_12 - ADC_CHANNELS_BANK;

		case ADC_SEL_CHAN_13:
			return ADC_CHAN_13 - ADC_CHANNELS_BANK;

		case ADC_SEL_CHAN_14:
			return ADC_CHAN_14 - ADC_CHANNELS_BANK;

		case ADC_SEL_CHAN_15:
			return ADC_CHAN_15 - ADC_CHANNELS_BANK;
#endif
		default:
			return 0xFF;
	}

	return 0xFF;
}

int8_t adc_hal_message(struct dev_descr_node * ddn,
                          enum dd_event e_event,
                          void * data0,
                          size_t * dsize0)
{
    int8_t ret = 0;

    switch (e_event)
    {
        case DD_EVENT_ATTACH:
            ret = adc_attach(ddn, (struct adc_setup *) data0);
        break;

        case DD_EVENT_DETACH:
            ret = adc_detach(ddn);
        break;

        case DD_EVENT_UPDATE:
            ret = adc_update(ddn, (struct adc_setup *) data0);
        break;

        case DD_DUMP:
            ret = adc_dump(ddn, data0, dsize0);
        break;

        case DD_RESTORE:
            ret = adc_restore(ddn, data0, dsize0);
        break;

        case DD_QUERY:
            ret = adc_query(ddn, (struct hal_query_info *) data0);
        break;

        case DD_ISRSIGNAL:
            ret = K_MSG_ERROR;
        break;

        case DD_RESET:
            ret = K_MSG_ERROR;
        break;

        case DD_DROP:
            ret = K_MSG_ERROR;
        break;

        case DD_FLUSH:
            ret = K_MSG_ERROR;
        break;
    }

    return ret;
}

int8_t adc_kstream_init(struct dev_descr_node * ddn,
                           struct kstream * ks)
{
    setSysError(SYSTEM_OPERATION_NOT_SUPPORTED);
    return K_FAIL_STREAM;
}

int8_t adc_stream_init(struct dev_descr_node * ddn,
                             FILE * file)
{
    setSysError(SYSTEM_OPERATION_NOT_SUPPORTED);
    return K_FAIL_STREAM;
}

static int8_t
adc_attach(struct dev_descr_node * ddn, struct adc_setup * adt)
{
    clearSysError();
    ASSERT_NULL_ER(adt, SYSTEM_NULL_ARGUMENT, K_NO_DD);
    if (loc_inst != NULL)
    {
        setSysError(SYSTEM_DEVICE_ALREADY_INITED);
        return K_NO_DD;
    }

    mtx_giant_adc = kernel_mutex_init();
    if (mtx_giant_adc == NULL)
    {
        setSysError(SYSTEM_INTERLOCKING_NOT_CREATED);
    }

    int8_t ret = kernel_isr_grab(ADC_vect_num, adc_isr, ISR_DELAYED);
    if (ret == RET_FAIL)
    {
        ret = kernel_isr_grab(ADC_vect_num, adc_isr, ISR_NORMAL);
        if (ret == RET_FAIL)
        {
            setSysError(SYSTEM_ISR_BUSY);
        }
    }

    struct adc_config * adc = (struct adc_config *) kernel_malloc(sizeof(struct adc_config));
    ASSERT_NULL_ER(adc, SYSTEM_NULL_ARGUMENT, K_NO_DD);

    ddn->instance = (void*) adc;
    loc_inst = adc;
    adc_init(adt->event_source);

    return 0;
}

static int8_t
adc_query(struct dev_descr_node * ddn, struct hal_query_info * data0)
{
    FILE * strm = (FILE *)data0->data0;
#if ADC_BUILD_WITH_TEXT_QUERY == YES
    fprintf_P(strm, PSTR("ADC REVISION: %u\r\n"), ADC_REVISION);
    fprintf_P(strm, PSTR("EVENT SOURCE ID: %u RECS IN SYSTEM: %d\r\n"), loc_inst->event_source, loc_inst->chans_list_len);

    fprintf_P(strm, PSTR("AID\tFLAGS\tCHANNELS H/L\tADCSRA\tADCSRB\tADMUX\treading\r\n"));
    ITERATE_BLOCK_DIRECT_MAN(struct adc_channel, loc_inst->chans_ptr_list, __it_d)
    {
        fprintf_P(strm, PSTR("%i\t%i\t%i %i\t\t%i\t%i\t%i\t%i\r\n"), __it_d->aid, __it_d->flags, ((__it_d->chans_ids & 0xF0) >> 4), (__it_d->chans_ids & 0x0F), __it_d->adcsra, __it_d->adcsrb, __it_d->admux, __it_d->adc_reading);
        ITERATE_NEXT(__it_d);
    }
    fprintf_P(strm, PSTR("\r\nCURRENT AID: %i\n"), (loc_inst->sel_chanl) ? loc_inst->sel_chanl->aid : -1);

#else
    setSysError(SYSTEM_OPERATION_NOT_SUPPORTED);
    fprintf_P(strm, PSTR("ADC was built without query! \r\n"));
    return K_MSG_ERROR;
#endif // IO_BUILD_WITH_TEXT_QUERY
}

static int8_t
adc_detach(struct dev_descr_node * ddn)
{
    ASSERT_NULL_ER(loc_inst, SYSTEM_DEVICE_NOT_INITED, K_NO_DD);

    ADCSRA = 0;
    ADCSRB = 0;

    if (loc_inst->chans_ptr_list != NULL)
    {
        kernel_mutex_lock(mtx_giant_adc);
        ITERATE_BLOCK_DIRECT(struct adc_channel, loc_inst->chans_ptr_list, __it_d)
        {
            struct adc_channel * chan = __it_d;
            ITERATE_NEXT(__it_d);
            kernel_free((void*)chan);
        }
        kernel_mutex_unlock(mtx_giant_adc);
    }

    kernel_isr_release(ADC_vect_num);

    kernel_free((void*) mtx_giant_adc);
    mtx_giant_adc = NULL;

    kernel_free((void*) loc_inst);
    loc_inst = NULL;

    return 0;
}

static int8_t
adc_update(struct dev_descr_node * ddn, struct adc_setup * tsp)
{
    ASSERT_NULL_ER(tsp, SYSTEM_NULL_ARGUMENT, K_NO_DD);

    adc_init(tsp->event_source);

    return 0;
}

static int8_t
adc_dump(struct dev_descr_node * ddn, void * data0, size_t * dsize0)
{
    struct adc_setup * tsp = (struct adc_setup *) data0;

    tsp->event_source = loc_inst->event_source;

    return 0;
}

static int8_t
adc_restore(struct dev_descr_node * ddn, void * data0, size_t * dsize0)
{
    struct adc_setup * tsp = (struct adc_setup *) data0;

    return adc_attach(ddn, tsp);
}

/* Init null device
*/
static void
adc_init(enum adc_event_source event_source)
{
	loc_inst->adc_async = 0;
	loc_inst->admux_temp = 0;
	loc_inst->adcsra_temp = 0;
	//setting ADC
    loc_inst->event_source = event_source;
	loc_inst->adcsra_temp = (1 << ADEN) | (1 << ADIE);
	loc_inst->adcsrb_temp = (uint8_t) event_source;
	//ADMUX = admux_temp;
	ADCSRA = loc_inst->adcsra_temp; //enable interrupt and ADC
	ADCSRB = loc_inst->adcsrb_temp;

	//run
    ADCSRA |= (1 << ADSC);

	return;
}

const struct adc_channel *
get_adc(adc_id aid)
{
    kernel_mutex_lock(mtx_giant_adc);
    SEARCH_IN_LIST_2(struct adc_channel,
                     loc_inst->chans_ptr_list,
                     item,
                     item->aid == aid,
                     get_adc_not_found);

    kernel_mutex_unlock(mtx_giant_adc);

    return item;

get_adc_not_found:
        kernel_mutex_unlock(mtx_giant_adc);
        setSysError(SYSTEM_ITEM_NOT_FOUND);
        return NULL;
}

uint8_t
get_adc_chans_amount()
{
	return loc_inst->chans_list_len;
}

//flags left_adjust - bit 0, disabe_digital_inp - bit 1
adc_id
adc_enable_channel(struct dev_descr_node * ddn,
                   enum adc_channel_selector adc_ch_sel,
                   enum adc_ref ref_src,
                   enum adc_presc adc_clock,
                   uint8_t flags,
                   void (*READING_FINISHED_HND)(const struct adc_channel *))
{
    ASSERT_NULL_ER(loc_inst, SYSTEM_DEVICE_NOT_INITED, -1);
	if (loc_inst->chans_list_len >= ADC_MAX_CHAN_RECS)
	{
		setSysError(SYSTEM_REACHED_MAX_RECS);
		return -1;
	}

	//check if already exist
	MUTEX_BLOCK(mtx_giant_adc, MUTEX_ACTION_MANUAL)
    {
        ITERATE_BLOCK_DIRECT_MAN(struct adc_channel, loc_inst->chans_ptr_list, __it_d)
        {

            uint8_t temp = (__it_d->adcsrb & 0b00100000) | (__it_d->admux & 0x0F);
            if ( temp == adc_ch_sel )
            {
                setSysError(SYSTEM_ALREADY_EXIST);
                return -1;
            }

            ITERATE_NEXT(__it_d);
        }
    }

	NEW_LIST_ITEM(struct adc_channel, ach, adc_enable_channel_bad_alloc);

	ach->adcsra = loc_inst->adcsra_temp | (uint8_t) adc_clock;
	ach->adcsrb = loc_inst->adcsrb_temp | (adc_ch_sel & (1 << MUX5)) ? (1 << MUX5) : 0;
	ach->admux = loc_inst->admux_temp | (uint8_t) ref_src | (adc_ch_sel & 0x0F);
	ach->flags = flags;
	ach->ddn = ddn;

	//bind pins, if already busy (one or many) return fail
	ach->chans_ids =  _get_channel_by_selector(adc_ch_sel);
	uint8_t chan_c = ach->chans_ids & 0x0F;
	uint8_t chan_h = ((ach->chans_ids & 0xF0) >> 4);
	uint8_t ret = 0;


	if (chan_h != 0)
	{
		chan_h -= 1;
		ret = hal_hw_bind_pin(loc_inst->ddn->dev_id, pgm_read_byte(&(adc_pins[(uint8_t) chan_h].e_dev_port)), pgm_read_byte(&(adc_pins[(uint8_t) chan_h].pins)) );

		if (ret != 0) goto adc_enable_channel_pin_alloc_fail;

		adc_control_digital_input(chan_h, (flags & ADC_FLAG_DISABLE_DIGITAL));
	}
	ret = hal_hw_bind_pin(loc_inst->ddn->dev_id, pgm_read_byte(&(adc_pins[(uint8_t) chan_c].e_dev_port)), pgm_read_byte(&(adc_pins[(uint8_t) chan_c].pins)) );

	if (ret != 0) goto adc_enable_channel_pin_alloc_fail2;

	adc_control_digital_input(chan_c, (flags & ADC_FLAG_DISABLE_DIGITAL));

	if (flags & ADC_FLAG_LEFT_ADJ)
	{
		ach->admux |= (1 << ADLAR);
	}
	else
	{
		ach->admux &= ~(1 << ADLAR);
	}

	//setting handler
	if (READING_FINISHED_HND != NULL)
	{
		ach->READING_FINISHED = READING_FINISHED_HND;
	}
	else
	{
		ach->READING_FINISHED = _dummy_func;
	}
	//add to list

    kernel_mutex_lock(mtx_giant_adc);
//	ADD_TO_LIST(struct adc_channel, chans_ptr_list, ach);
	uint8_t sel_adc_id = 0;
	if (loc_inst->chans_ptr_list == NULL)
	{
		ach->next = NULL;
		loc_inst->chans_ptr_list = ach;
	}
	else
	{
		ADD_TO_LIST_MANUAL(struct adc_channel, loc_inst->chans_ptr_list)
		{

			IF_LAST_ELEMENT
			{
				sel_adc_id = PREV_IT->aid + 1;
				INJECT_ELEMENT_AFTER_PREV_IT(ach);
				FINISH_SORTING;
			}

			COMPARE_DIFF_DIR(aid, 2)
			{
				sel_adc_id = CURT_IT->aid - (CURT_IT->aid - PREV_IT->aid);
				INJECT_ELEMENT_BEFORE_IT(ach, loc_inst->chans_ptr_list);
				FINISH_SORTING;
			}

			NEXT_ELEMENT;

		}
	}
	ach->aid = sel_adc_id;
	//ach->fid_owner = fid;

	++loc_inst->chans_list_len;
	kernel_mutex_unlock(mtx_giant_adc);

	return ach->aid;

adc_enable_channel_pin_alloc_fail2:
	hal_hw_unbind_pin(loc_inst->ddn->dev_id,
                   pgm_read_byte(&(adc_pins[(uint8_t) chan_h].e_dev_port)),
                   pgm_read_byte(&(adc_pins[(uint8_t) chan_h].pins)));
adc_enable_channel_pin_alloc_fail:
	kernel_free((void *) ach);
	return -1;

adc_enable_channel_bad_alloc:
	setSysError(SYSTEM_MALLOC_FAILED);
	return -1;

adc_enable_channel_not_running:
	setSysError(SYSTEM_MODULE_NOT_RUNNING);
	return -1;
}

adc_id
adc_reenable_channel(adc_id aid,
                     enum adc_channel_selector adc_ch_sel,
                     enum adc_ref ref_src,
                     enum adc_presc adc_clock,
                     uint8_t flags,
                     void (*READING_FINISHED_HND)(const struct adc_channel *))
{
    ASSERT_NULL_ER(loc_inst, SYSTEM_DEVICE_NOT_INITED, -1);
    ASSERT_NEGATIVE_ER(aid, SYSTEM_ID_INCORRECT, -1);

	struct adc_channel * ach = NULL;

	//check if already exist
	MUTEX_BLOCK(mtx_giant_adc, MUTEX_ACTION_MANUAL)
    {
        ITERATE_BLOCK_DIRECT(struct adc_channel, loc_inst->chans_ptr_list, __it_d)
        {

            uint8_t temp = (__it_d->adcsrb & 0b00100000) | (__it_d->admux & 0x0F);
            if ( (temp == adc_ch_sel) && (aid != __it_d->aid) )
            {
                setSysError(SYSTEM_ALREADY_EXIST);
                return -1;
            }
            else
            {
                ach = __it_d;
            }
        }
    }

	uint8_t chans_ids =  _get_channel_by_selector(adc_ch_sel);
	uint8_t chan_c = chans_ids & 0x0F;
	uint8_t chan_h = ((chans_ids & 0xF0) >> 4);
	uint8_t ret = 0;

	uint8_t chan_c_p = ach->chans_ids & 0x0F;
	uint8_t chan_h_p = ((ach->chans_ids & 0xF0) >> 4);


	if (chan_h != 0)
	{
		chan_h -= 1;
		uint8_t av_h = hal_hw_check_available(pgm_read_byte(&(adc_pins[(uint8_t) chan_h].e_dev_port)), pgm_read_byte(&(adc_pins[(uint8_t) chan_h].pins)));
		uint8_t av_c = hal_hw_check_available(pgm_read_byte(&(adc_pins[(uint8_t) chan_c].e_dev_port)), pgm_read_byte(&(adc_pins[(uint8_t) chan_c].pins)));
		if ( (av_h != 0) || (av_c != 0) )
		{
			goto adc_reenable_channel_pin_busy;
		}
		chan_h += 1;
	}
	else
	{
		uint8_t av_c = hal_hw_check_available(pgm_read_byte(&(adc_pins[(uint8_t) chan_c].e_dev_port)), pgm_read_byte(&(adc_pins[(uint8_t) chan_c].pins)));
		if ( av_c != 0 )
		{
			goto adc_reenable_channel_pin_busy;
		}
	}

	if (chan_h_p != 0)
	{
		//unbnd pins
		chan_h_p -= 1;
		hal_hw_unbind_pin(loc_inst->ddn->dev_id, pgm_read_byte(&(adc_pins[(uint8_t) chan_h_p].e_dev_port)), pgm_read_byte(&(adc_pins[(uint8_t) chan_h_p].pins)));
	}

	hal_hw_unbind_pin(loc_inst->ddn->dev_id, pgm_read_byte(&(adc_pins[(uint8_t) chan_c_p].e_dev_port)), pgm_read_byte(&(adc_pins[(uint8_t) chan_c_p].pins)));

	if (chan_h != 0)
	{
		--chan_h;
		ret = hal_hw_bind_pin(loc_inst->ddn->dev_id, pgm_read_byte(&(adc_pins[(uint8_t) chan_h].e_dev_port)), pgm_read_byte(&(adc_pins[(uint8_t) chan_h].pins)) );
		if (ret != 0) goto adc_reenable_channel_pin_alloc_fail;

		adc_control_digital_input(chan_h, (flags & ADC_FLAG_DISABLE_DIGITAL));
	}

	ret = hal_hw_bind_pin(loc_inst->ddn->dev_id, pgm_read_byte(&(adc_pins[(uint8_t) chan_c].e_dev_port)), pgm_read_byte(&(adc_pins[(uint8_t) chan_c].pins)) );

	if (ret != 0) goto adc_reenable_channel_pin_alloc_fail2;

	adc_control_digital_input(chan_c, (flags & ADC_FLAG_DISABLE_DIGITAL));

	ach->adcsra = loc_inst->adcsra_temp | (uint8_t) adc_clock;
	ach->adcsrb = loc_inst->adcsrb_temp | (adc_ch_sel & (1 << MUX5)) ? (1 << MUX5) : ~(1 << MUX5);
	ach->admux = loc_inst->admux_temp | (uint8_t) ref_src | (adc_ch_sel & 0x0F);

	if (flags & ADC_FLAG_LEFT_ADJ)
	{
		ach->admux |= (1 << ADLAR);
	}
	else
	{
		ach->admux &= ~(1 << ADLAR);
	}

	//setting handler
	if (READING_FINISHED_HND != NULL)
	{
		ach->READING_FINISHED = READING_FINISHED_HND;
	}
	else
	{
		ach->READING_FINISHED = _dummy_func;
	}

adc_reenable_channel_pin_alloc_fail2:
	hal_hw_unbind_pin(loc_inst->ddn->dev_id, pgm_read_byte(&(adc_pins[(uint8_t) chan_h].e_dev_port)), pgm_read_byte(&(adc_pins[(uint8_t) chan_h].pins)));
adc_reenable_channel_pin_alloc_fail:
adc_reenable_channel_pin_busy:
	return -1;
}

int8_t
adc_disable_channel(adc_id adi)
{
	ASSERT_NULL_ER(loc_inst, SYSTEM_DEVICE_NOT_INITED, -1);
    ASSERT_NEGATIVE_ER(adi, SYSTEM_ID_INCORRECT, -1);

    kernel_mutex_lock(mtx_giant_adc);
		REMOVE_FROM_LIST_2(struct adc_channel, loc_inst->chans_ptr_list, adc_ptr, adc_ptr->aid == adi, adc_disable_channel_not_found);

		uint8_t ch = adc_ptr->chans_ids & 0xF0;
		uint8_t ret = 0;
		clearSysError();
		if (ch != 0)
		{
			ch -= 1;
			ret = hal_hw_unbind_pin(loc_inst->ddn->dev_id, pgm_read_byte(&(adc_pins[(uint8_t) ch].e_dev_port)), pgm_read_byte(&(adc_pins[(uint8_t) ch].pins)));
		}
		if (ret != 0)
		{
			if (getSysErrror() == SYSTEM_ITEM_NOT_FOUND)
			{
				// ??? for now I don't know how to handle this error
			}
		}
		ch = adc_ptr->chans_ids & 0x0F;
		ret = hal_hw_unbind_pin(loc_inst->ddn->dev_id, pgm_read_byte(&(adc_pins[(uint8_t) ch].e_dev_port)), pgm_read_byte(&(adc_pins[(uint8_t) ch].pins)));

		kernel_free((void *) adc_ptr);
		--loc_inst->chans_list_len;

    kernel_mutex_unlock(mtx_giant_adc);

	return 0;

adc_disable_channel_not_found:
	setSysError(SYSTEM_ITEM_NOT_FOUND);
	kernel_mutex_unlock(mtx_giant_adc);
	return 1;
}

void
adc_control_digital_input(enum chan_id chan, uint8_t digital_input_status)
{

#if ADC_CHANNELS > ADC_CHANNELS_BANK
	if ((uint8_t)chan < ADC_CHANNELS_BANK)
	{
		DIDR0 = ((digital_input_status == 1) ? DIDR0 | (1 << (uint8_t)chan) : DIDR0 & ~(1 << (uint8_t)chan));
	}
	else
	{
		//bank 1
		DIDR2 = ((digital_input_status == 1) ? DIDR2 | (1 << ( (uint8_t)chan - ADC_CHANNELS_BANK) ) : DIDR2 & ~(1 << ( (uint8_t)chan - ADC_CHANNELS_BANK) ));
	}
#else
	DIDR0 = ((digital_input_status == 1) ? DIDR0 | (1 << (uint8_t)chan) : DIDR0 & ~(1 << (uint8_t)chan));
#endif
}

int8_t
get_adc_mem(struct adc_channel * adc, adc_id offset)
{
    kernel_mutex_lock(mtx_giant_adc);

    struct adc_channel * temp = loc_inst->chans_ptr_list;
    while ( (temp != NULL) && (offset > 0) )
    {
        temp = temp->next;
        --offset;
    }

    kernel_mutex_unlock(mtx_giant_adc);

    if (temp == NULL)
    {
        return 1;
    }
    adc->ddn = temp->ddn;
	adc->aid = temp->aid;
	adc->flags = temp->flags;
	adc->chans_ids = temp->chans_ids;
	adc->adcsra = temp->adcsra;
	adc->adcsrb = temp->adcsrb;
	adc->admux = temp->admux;
	adc->adc_reading = temp->adc_reading;

	return 0;

}

int8_t adc_run_reading_SYNC()
{
	//check if running not in free_running mode
	if ((ADCSRB & ADC_MODE_COUNTER1_CE) != 0)
	{
        setSysError(SYSTEM_TIMER_NOT_FREE_RUNNING);
		return 1;
	}

kernel_mutex_lock(mtx_giant_adc);
	ITERATE_BLOCK_DIRECT_MAN(struct adc_channel, loc_inst->chans_ptr_list, __it_d)
	{
		while (ADC_CONVERSION_RUNNING);
		while (loc_inst->sel_chanl != NULL);
		loc_inst->sel_chanl = __it_d;
		ADMUX = __it_d->admux;
		ADCSRA = __it_d->adcsra;
		ADCSRB = __it_d->adcsrb;

		//run
		ADCSRA |= (1 << ADSC);

		ITERATE_NEXT(__it_d);
	}
kernel_mutex_unlock(mtx_giant_adc);

	return 0;
}

int8_t adc_run_reading_ASYNC()
{
	//check if any conversion is in progress
	if ((ADC_CONVERSION_RUNNING) || (loc_inst->sel_chanl != NULL))
	{
		setSysError(SYSTEM_DEVICE_BUSY);
		return 1;
	}

	//check if running not in free_running mode
	if ((ADCSRB & ADC_MODE_COUNTER1_CE) != 0)
	{
		return 1;
	}

    kernel_mutex_lock(mtx_giant_adc);
	struct adc_channel * ptr_copy = loc_inst->chans_ptr_list;
	kernel_mutex_unlock(mtx_giant_adc);
	loc_inst->sel_chanl = ptr_copy;
	loc_inst->adc_async = 1;

	ADMUX = ptr_copy->admux;
	ADCSRA = ptr_copy->adcsra;
	ADCSRB = ptr_copy->adcsrb;

	//run
	ADCSRA |= (1 << ADSC);
	kernel_mutex_unlock(mtx_giant_adc);

	return 0;
}

int8_t adc_run_reading(adc_id adi, uint16_t * reading)
{
	//check if any conversion is in progress
	if (ADC_CONVERSION_RUNNING)
	{
		setSysError(SYSTEM_DEVICE_BUSY);
		return 1;
	}

	ASSERT_NEGATIVE_ER(adi, SYSTEM_ID_INCORRECT, 1);

	//check if running not in free_running mode
	if ((ADCSRB & ADC_MODE_COUNTER1_CE) != 0)
	{
		goto adc_run_reading_not_freerunning;
	}

    kernel_mutex_lock(mtx_giant_adc);

	SEARCH_IN_LIST_2(struct adc_channel, loc_inst->chans_ptr_list, ach, ach->aid == adi, adc_run_reading_not_found);

	loc_inst->sel_chanl = ach;
	ADMUX = ach->admux;
	ADCSRA = ach->adcsra;
	ADCSRB = ach->adcsrb;

	kernel_mutex_unlock(mtx_giant_adc);
	//run
	ADCSRA |= (1 << ADSC);

    if (reading != NULL)
    {
        //wait for conversion
        while (ADC_CONVERSION_RUNNING);
        while (loc_inst->sel_chanl != NULL);

        *reading = ach->adc_reading;
    }

	return 0;

adc_run_reading_not_found:
	setSysError(SYSTEM_ITEM_NOT_FOUND);
	kernel_mutex_unlock(mtx_giant_adc);
	return 1;

adc_run_reading_not_freerunning:
	setSysError(SYSTEM_INCOMPAT_MODE);
	return 1;
}

int8_t
adc_get_reading(adc_id adi, uint16_t * reading)
{
    ASSERT_NULL_ER(reading, SYSTEM_NULL_ARGUMENT, 1);

    kernel_mutex_lock(mtx_giant_adc);

	SEARCH_IN_LIST_2(struct adc_channel, loc_inst->chans_ptr_list, ach, ach->aid == adi, not_found);

    *reading = ach->adc_reading;

	kernel_mutex_unlock(mtx_giant_adc);

	return 0;

not_found:
    setSysError(SYSTEM_ITEM_NOT_FOUND);
    return -1;
}

int8_t
adc_get_reading_offset(uint16_t * read, adc_id * chid, adc_id offset)
{
    kernel_mutex_lock(mtx_giant_adc);

    struct adc_channel * temp = loc_inst->chans_ptr_list;
    while ( (temp != NULL) && (offset > 0) )
    {
        temp = temp->next;
        --offset;
    }

    kernel_mutex_unlock(mtx_giant_adc);

    if (temp == NULL)
    {
        return 1;
    }

	*chid = temp->aid;
	*read = temp->adc_reading;

	return 0;

}

#endif
