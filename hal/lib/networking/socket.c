/*-
 *	Copyright (c) 2014-2016	Alexander Morozov. www.nixd.org
 *	All rights reserved.
 *
 * LICENSE: This source file is subject to the New BSD license that is
 * available through the world-wide-web at the following URI:
 * http://www.opensource.org/licenses/bsd-license.php or in the root folder
 * of the project in file LICENSE.TXT.
 */

#include "../../build_defs.h"
#include <stdio.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <avr/pgmspace.h>
#include <util/atomic.h>
#include "../../hal.h"
#include "./socket.h"

struct socket * sock_open_server(enum knet_proto knp, uint32_t listenIP, uint16_t listenPORT)
{

}

struct socket * sock_open_client(enum knet_proto knp, uint32_t remoteIP, uint16_t remotePORT)
{

}

void sock_close(struct socket * sock)
{

}
