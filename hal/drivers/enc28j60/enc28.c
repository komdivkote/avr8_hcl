/*-
 *	Copyright (c) 2014-2016	Alexander Morozov. NiXD.ORG
 *	All rights reserved.
 *
 * LICENSE: This source file is subject to the New BSD license that is
 * available through the world-wide-web at the following URI:
 * http://www.opensource.org/licenses/bsd-license.php or in the root folder
 * of the project in file LICENSE.TXT.
 */

#include "../../build_defs.h"
#include <avr/io.h>
#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stdint.h>
#include <string.h>
#include <avr/pgmspace.h>
#include <avr/interrupt.h>
#include <util/atomic.h>
#include <util/delay_basic.h>
#include <util/delay.h>
#include "../../hal.h"
#include "../spi/spi.h"
#include "../../lib/stream/stream.h"
#include "enc28.h"

#include "../../../os/kernel/kernel_process.h"
#include "../../../os/kernel/kernel_mutex.h"
#include "../../../os/kernel/kernel_isr.h"

#if BUILD_WITH_ENC28 == YES

/*//prototype of the list
LIST_STRUCT(enc28_list, enc28_record);

//pointer to the enc28_list
struct enc28_list * enc28_list_ptr = NULL;
*/

static int8_t enc28_attach(struct dev_descr_node * ddn, struct enc28_config * setup);
static int8_t enc28_detach(struct dev_descr_node * ddn);
static int8_t enc28_query(struct dev_descr_node * ddn, struct hal_query_info * data0);
static int8_t enc28_update(struct dev_descr_node * ddn, struct enc28_config * setup);
static int8_t enc28_dump(struct dev_descr_node * ddn, void * data0, size_t * dsize0);
static int8_t enc28_restore(struct dev_descr_node * ddn, void * data0, size_t * dsize0);

static int8_t enc28_mod_socket(enum knet_newconn_opts kconnopts,
                 struct knetdevice * k_net_dev,
                 struct knetstate * k_net_state
                );
static int8_t enc28_poll(struct knetdevice * k_net_dev);
static int32_t enc28_transmit(struct knetdevice * k_net_dev,
                            void * tx_buf_ptr,
                            uint16_t len);
static int8_t enc28_static_file_drop(struct dev_descr_node * ddn);

/*uint16_t __enc28_data_left_len(struct enc28_record * enc28);
int16_t enc28_read_buf_len(struct kstream * stream);
int8_t enc28_write_init(struct kstream * stream);
int8_t enc28_write_finish(struct kstream * stream);
int16_t enc28_write_buf_len(struct kstream * stream);
int16_t enc28_read_buf_len(struct kstream * stream);
int8_t enc28_write_async(struct kstream * stream, uint8_t var);
int16_t enc28_read_async(struct kstream * stream);
int32_t enc28_read_arr_async(struct kstream * stream, uint8_t * rarr, uint16_t rlen);
int8_t __drop_frame(struct kstream * stream, enum drop_data_act e_drop);
int8_t __flush_buffer(struct kstream * stream);
*/

/*struct enc28_record * __get_enc_instance(struct dev_descr_node * ddn)
{
    ASSERT_NULL_E(fd, SYSTEM_NULL_ARGUMENT, error);

    SEARCH_IN_LIST_2(struct enc28_list, enc28_list_ptr, item, item->node->ddn->dev_id == ddn->dev_id, not_found);

    return item->node;

not_found:
    setSysError(SYSTEM_ITEM_NOT_FOUND);
error:
    return NULL;
};
*/

int8_t enc28_hal_message(struct dev_descr_node * ddn,
                        enum dd_event e_event,
                        void * data0,
                        size_t * dsize0)
{
    int8_t ret = 0;

    switch (e_event)
    {
        case DD_EVENT_ATTACH:
            ret = enc28_attach(ddn, (struct enc28_config *) data0);
        break;

        case DD_EVENT_DETACH:
            ret = enc28_detach(ddn);
        break;

        case DD_EVENT_UPDATE:
            ret = enc28_update(ddn, (struct enc28_config *) data0);
        break;

        case DD_DUMP:
            ret = enc28_dump(ddn, data0, dsize0);
        break;

        case DD_RESTORE:
            ret = enc28_restore(ddn, data0, dsize0);
        break;

        case DD_QUERY:
            ret = enc28_query(ddn, (struct hal_query_info *) data0);
        break;

        case DD_ISRSIGNAL:
            ret = K_MSG_ERROR;
        break;

        case DD_RESET:
            ret = K_MSG_ERROR;
        break;

        case DD_DROP:
            ret = enc28_static_file_drop(ddn);
        break;

        case DD_FLUSH:
            ret = K_MSG_ERROR;
        break;
    }

    return ret;
}

int8_t enc28_kstream_init(struct dev_descr_node * ddn, struct kstream * ks)
{
    ASSERT_NULL_ER(ddn, SYSTEM_NULL_ARGUMENT, K_FAIL_STREAM);
    ASSERT_NULL_ER(ks, SYSTEM_NULL_ARGUMENT, K_FAIL_STREAM);

    struct enc28_record * enc28 = (struct enc28_record *)ddn->instance;

    ks->loc_instance = (void*)enc28->k_net_dev;
    if (ks->flags & SREAD)
    {
        /*ks->KSRBUF =;
        ks->KSREAD =;
        ks->KSRLEN = ;*/
    }

    if (ks->flags & SWRITE)
    {
        /*ks->KSWLEN = ;
        ks->KSWRITE = ;*/
    }




/*    ks->KSWACK =;
    ks->KSWINIT = ;*/
    return 0;
}

static int8_t
enc28_static_file_drop(struct dev_descr_node * ddn)
{
    struct enc28_record * enc28 = (struct enc28_record*) ddn->instance;

    if (enc28->file_rxed != NULL)
    {
        hal_net_release_rx(enc28->k_net_dev);
        enc28->file_rxed = NULL;
    }
    return 0;
}

static int
enc28_static_file_read_async(FILE * stream)
{
    struct dev_descr_node * ddn = filedest(stream);
    struct enc28_record * enc28 = (struct enc28_record*) ddn->instance;

    if (enc28->file_rxed == NULL)
    {
       /* uint8_t * rx_ptr = 0;
        uint16_t rx_size = 0;*/
        uint8_t ret = hal_net_receive(enc28->k_net_dev,
                                      hal_get_listen_by_state((struct knetstate *) stream->udata),
                                        NULL,
                                        NULL,
                                        HAL_NRCV_BLOCK);

        if (ret == 0)
        {
            enc28->file_rxed = stream;
        }
    }

    int32_t res = bRBf8(enc28->k_net_dev->b_rx_buf);
	if (res == -1)
	{
	    enc28->file_rxed = NULL;
	    hal_net_release_rx(enc28->k_net_dev);
		return EOF;
	}

	return res;
}

static int
enc28_static_file_write_async(char var, FILE *stream)
{
    struct dev_descr_node * ddn = (struct dev_descr_node *)filedest(stream);
    struct enc28_record * enc28 = (struct enc28_record*) ddn->instance;

    //lock mutex
    //if mutex is alredy locked by oir process then this operation will be skipped
    kernel_mutex_lock(enc28->k_net_dev->m_tx);

    hal_net_send(enc28->k_net_dev, (struct knetstate *)stream->udata,
                      enc28->k_net_dev->tx_buf, sizeof(char));


    kernel_mutex_unlock(enc28->k_net_dev->m_tx);



   return var;
}

int8_t
enc28_stream_init(struct dev_descr_node * ddn, FILE * file)
{
    ASSERT_NULL_ER(ddn, SYSTEM_NULL_ARGUMENT, 1);
    ASSERT_NULL_ER(file, SYSTEM_NULL_ARGUMENT, 1);


    if (file->get != NULL)
    {
        file->get = enc28_static_file_read_async;

        file->flags |= __SRD;
    }

    if (file->put != NULL)
    {

        file->put = enc28_static_file_write_async;

        file->flags |= __SWR;
    }

    return 0;
}

static int8_t
enc28_attach(struct dev_descr_node * ddn, struct enc28_config * setup)
{
    ASSERT_NULL_ER(setup, SYSTEM_NULL_ARGUMENT, K_NO_DD);

    //try to access spi to bind cs
    struct dev_descr_node * spi_ddn = hal_node_get(setup->spi_dev_id);
    ASSERT_NULL_ER(spi_ddn, SYSTEM_DEVICE_NOT_FOUND, K_NO_DD);

    //check if spi_ddn is SPI/ROOT
    if ( (spi_ddn->e_dev_descr != DEV_DESCR_SPI) || (spi_ddn->e_dh_code != ROOT_DRIVER_DEV) )
    {
        //this type of communication bus is not supported
        setSysError(SYSTEM_DEVICE_NOT_COMPATIABLE);
        return K_NO_DD;
    }

    cs_id csid = spi_add_slave((struct spi_dev*)spi_ddn->instance, setup->e_port, setup->p_mask, 0);
    ASSERT_NEGATIVE_ER(csid, getSysErrror(), K_NO_DD);

    struct enc28_record * enc28 = (struct enc28_record *) kernel_malloc(sizeof(struct enc28_record));
    ASSERT_NULL_E(enc28, SYSTEM_MALLOC_FAILED, spi_net_malloc_fail);

    enc28->ddn = ddn;
    enc28->file_rxed = NULL;
    memcpy(enc28->hw_state.mac, setup->mac_addr, 6);
    enc28->hw_state.spi_cs_sel = csid;
    enc28->hw_state.p_mask = setup->p_mask;
    enc28->hw_state.e_port = setup->e_port;
    enc28->hw_state.spi = spi_ddn->instance;
    enc28->hw_state.enc28j60_current_bank = 0;
    enc28->hw_state.enc28j60_rxrdpt = 0;

    ddn->instance = (void*) enc28;

    //register network interface to the system
    struct knetdev_handlers knh;
    knh.MOD_CONNECTION = enc28_mod_socket;
    knh.POLL_DEVICE = enc28_poll;
    knh.TRANSMIT = enc28_transmit;

    int8_t status = enc28j60_init(&enc28->hw_state);

    enc28->k_net_dev = hal_net_add_dd(ddn, (void*) enc28, enc28->hw_state.mac, &knh);
    ASSERT_NULL(enc28->k_net_dev, spi_net_add_dd_fail);

    return 0;

spi_net_add_dd_fail:
    kernel_free((void*)enc28);
    ddn->instance = NULL;

spi_net_malloc_fail:
    spi_remove_slave((struct spi_dev*)spi_ddn->instance, csid);
    return K_NO_DD;
}

static int8_t
enc28_detach(struct dev_descr_node * ddn)
{
    ASSERT_NULL_ER(ddn, SYSTEM_NULL_ARGUMENT, K_NO_DD);

    struct enc28_record * enc28 = (struct enc28_record *)ddn->instance;
    hal_net_remove_dd(enc28->k_net_dev->net_dev_id);
    spi_remove_slave((struct spi_dev*)enc28->hw_state.spi, enc28->hw_state.spi_cs_sel);
    kernel_free((void *)enc28);

    return 0;
}

static int8_t
enc28_query(struct dev_descr_node * ddn, struct hal_query_info * data0)
{
    FILE * strm = (FILE *)data0->data0;

#if ENC28_BUILD_WITH_TEXT_QUERY == YES
    struct enc28_record * enc28 = (struct enc28_record *) ddn->instance;
    fprintf_P(strm, PSTR("ENC28 REVISION: %u\r\n"), ENC28_REVISION);
    fprintf_P(strm, PSTR("MAC: %x:%x:%x:%x:%x:%x\r\n"),
              enc28->hw_state.mac[0], enc28->hw_state.mac[1], enc28->hw_state.mac[2],
              enc28->hw_state.mac[3], enc28->hw_state.mac[4], enc28->hw_state.mac[5]);
    fprintf_P(strm, PSTR("LOCATION: ROOT\\HAL[%d]\\SPI[%d]\\CS[%x->%x:%x]\r\n"),
              enc28->ddn->dev_id, enc28->hw_state.spi->ddn->dev_id,
              enc28->hw_state.spi_cs_sel, enc28->hw_state.e_port,
              enc28->hw_state.p_mask);
    fprintf_P(strm, PSTR("IP: %u.%u.%u.%u\r\n"),
              ((uint8_t*)&enc28->k_net_dev->nic_set.ipv4)[0],
              ((uint8_t*)&enc28->k_net_dev->nic_set.ipv4)[1],
              ((uint8_t*)&enc28->k_net_dev->nic_set.ipv4)[2],
              ((uint8_t*)&enc28->k_net_dev->nic_set.ipv4)[3]
              );
#else
    fprintf_P(strm, PSTR("ENC28 was built without query! \r\n"));
#endif // SPI_BUILD_WITH_TEXT_QUERY

    return 0;
}

static int8_t
enc28_update(struct dev_descr_node * ddn, struct enc28_config * setup)
{
    return K_MSG_ERROR;
}

static int8_t
enc28_dump(struct dev_descr_node * ddn, void * data0, size_t * dsize0)
{
    struct enc28_record * enc28 = ddn->instance;
    struct enc28_envilope * enc28_envlp = (struct enc28_envilope *) data0;

    enc28_envlp->spi_dev_id = enc28->hw_state.spi->ddn->dev_id;
    enc28_envlp->e_port = enc28->hw_state.e_port;
    enc28_envlp->p_mask = enc28->hw_state.p_mask;
    memcpy(enc28_envlp->mac, enc28->hw_state.mac, 6);

    return 0;
}

static int8_t
enc28_restore(struct dev_descr_node * ddn, void * data0, size_t * dsize0)
{
    struct enc28_envilope * enc28_envlp = (struct enc28_envilope *) data0;

    return enc28_attach(ddn, (struct enc28_config *) enc28_envlp);
}

//------

/*
* k_net_state are already created by the
*/
static int8_t
enc28_mod_socket(enum knet_newconn_opts kconnopts,
                 struct knetdevice * k_net_dev,
                 struct knetstate * k_net_state
                )
{
    ASSERT_NULL_E(k_net_dev, SYSTEM_NULL_ARGUMENT, error);
    ASSERT_NULL_E(k_net_state, SYSTEM_NULL_ARGUMENT, error);

    if (k_net_state->e_knet_proto == KNET_TCP)
    {
        //create new socket
    }

    return 0;

error:
    return 1;
}

static int8_t
enc28_poll(struct knetdevice * k_net_dev)
{
    struct enc28_record * enc28 = (struct enc28_record *) k_net_dev->driver_nic_stuct;

    ASSERT_NULL_ER(enc28, SYSTEM_MEMORY_CORUPTION, -1);

    uint16_t len;
	if ((len = enc28j60_recv_packet(&enc28->hw_state, k_net_dev->rx_buf, sizeof(k_net_dev->rx_buf))))
    {
        b_set_new_data(k_net_dev->b_rx_buf, len);
        return 1;
    }

    return 0;

error:
    return -1;
}

static int32_t
enc28_transmit(struct knetdevice * k_net_dev,
               void * tx_buf_ptr,
               uint16_t len)
{
    return enc28j60_send_packet(&((struct enc28_record *) k_net_dev->driver_nic_stuct)->hw_state,
                                tx_buf_ptr,
                                len + sizeof(eth_frame_t));
}

//---------STREAM------------------

/*
uint16_t __enc28_data_left_len(struct enc28_record * enc28)
{
    return enc28->buf_in_data_len - enc28->buf_in_pos;
}

uint16_t __enc28_outcoming_free_space(struct enc28_record * enc28)
{
    return ENC28J60_MAXFRAME - enc28->buf_out_inx;
}

int8_t enc28_write_init(struct kstream * stream)
{
    ASSERT_NULL(stream->state, error);

    //check if buffer is used
    //TODO

    struct enc28_record * enc28 = (struct enc28_record *) stream->loc_instance;

    //prepare buffer offset
    enc28->buf_out_payload = enc28_get_output_buffer((void *)enc28, stream->state->e_knet_proto);
    enc28->buf_out_inx = 0;

    return 0;

error:
    printf_P(PSTR("!!!!!!!!!!!!!!!!!!enc28_write_init: ERROR!\r\n"));
    return 1;
}

int8_t enc28_write_finish(struct kstream * stream)
{
    ASSERT_NULL(stream->state, error);
    //send data

    struct enc28_record * enc28 = (struct enc28_record *) stream->loc_instance;
    int8_t status = 0;

    if (enc28->buf_out_inx != 0)
    {
       // status = kernel_net_send(enc28->k_net_dev, stream->state, enc28->net_buf_out,
        //                         enc28->buf_out_inx);
        status = enc28_send(enc28->k_net_dev, stream->state, enc28->net_buf_out,
                                 enc28->buf_out_inx);

    }

    enc28->buf_out_payload = NULL;
    enc28->buf_out_inx = 0;

    return status;

error:
    printf_P(PSTR("enc28_write_finish: ERROR!\r\n"));
    return 1;
}
int16_t enc28_write_buf_len(struct kstream * stream)
{
    struct enc28_record * enc28 = (struct enc28_record *) stream->loc_instance;
    return ENC28J60_MAXFRAME - enc28->buf_out_inx;
}

//todo update
int16_t enc28_read_buf_len(struct kstream * stream)
{
    struct enc28_record * enc28 = (struct enc28_record *) stream->loc_instance;
    return ENC28J60_MAXFRAME - enc28->buf_in_pos;
}

int8_t enc28_write_async(struct kstream * stream, uint8_t var)
{
    struct enc28_record * enc28 = (struct enc28_record *) stream->loc_instance;
    uint16_t space_left = __enc28_outcoming_free_space(enc28);

    if (space_left > 0)
    {
        ATOMIC_BLOCK(ATOMIC_RESTORESTATE)
        {
            enc28->buf_out_payload[enc28->buf_out_inx] = var;
            enc28->buf_out_inx++;
        }
        return 0;
    }
    else
    {
        //flush buffer and try to write
        if (__flush_buffer(stream) == 0)
        {
            return enc28_write_async(stream, var);
        }
    }

    return 1;
}

int16_t enc28_read_async(struct kstream * stream)
{
    struct enc28_record * enc28 = (struct enc28_record *) stream->loc_instance;
    uint16_t left = __enc28_data_left_len(enc28);

    if (left > 0)
    {
        return (enc28->net_buf_payload[enc28->buf_in_pos++]);
    }

    return -1;
}

int32_t enc28_read_arr_async(struct kstream * stream, uint8_t * rarr, uint16_t rlen)
{
    ASSERT_NULL_E(rarr, SYSTEM_NULL_ARGUMENT, error);

    struct enc28_record * enc28 = (struct enc28_record *) stream->loc_instance;
    //checking range
    //ASSERT_LEN((enc28->buf_in_data_len - enc28->buf_in_pos), rlen, error_range);

    uint16_t wrote = 0;

    if ((enc28->buf_in_pos + rlen) > enc28->buf_in_data_len)
    {
        wrote = (enc28->buf_in_pos + rlen) - enc28->buf_in_data_len;
        memcpy(rarr, (enc28->net_buf_payload + enc28->buf_in_pos), wrote);
    }
    else
    {
        memcpy(rarr, (enc28->net_buf_payload + enc28->buf_in_pos), rlen);
        wrote = rlen;
    }


    enc28->buf_in_pos += wrote;

    return wrote;

error_range:
    setSysError(SYSTEM_LENGTH_ASSERT);
error:
    return -1;
}

int8_t __drop_frame(struct kstream * stream, enum drop_data_act e_drop)
{
    struct enc28_record * enc28 = (struct enc28_record *) stream->loc_instance;
    enc28->buf_in_pos = 0;
    enc28->net_buf_payload = 0;
    enc28->buf_in_data_len = 0;

    return 0;
}

int8_t __flush_buffer(struct kstream * stream)
{
    ASSERT_NULL(stream->state, error);
    //send data

    struct enc28_record * enc28 = (struct enc28_record *) stream->loc_instance;
    int8_t status = 0;

    if (enc28->buf_out_inx != 0)
    {
        status = kernel_net_send(enc28->k_net_dev, stream->state, enc28->net_buf_out,
                                 enc28->buf_out_inx);

    }

    enc28->buf_out_inx = 0;

    return status;

error:
    return -1;
}
*/
#endif // BUILD_WITH_ENC28
