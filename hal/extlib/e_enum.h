/*-
 *	Copyright (c) 2014-2015	Alexander Morozov. NiXD ORG
 *	All rights reserved.
 *
 * LICENSE: This source file is subject to the New BSD license that is
 * available through the world-wide-web at the following URI:
 * http://www.opensource.org/licenses/bsd-license.php or in the root folder
 * of the project in file LICENSE.TXT.
 */


#ifndef E_ENUM_H_
#define E_ENUM_H_

#define STR(x) #x

/*
    generates enum body from defined list of elements
    #define FOREACH_FOO(DV)\
        DV(FOO_BAR1)\
        DV(FOO_BAR2)

    enum example
    {
        FOREACH_FOO(GENERATE_ENUM)
    }
 */
#define GENERATE_ENUM(ENUM) ENUM,
#define GENERATE_ENUM_VAL(ENUM, VAL) ENUM = VAL,
#define GENERATE_ENUM_VAL_EX(ENUM, VAL, UNUSED) GENERATE_ENUM_VAL(ENUM, VAL)

/*
    generates header declarations for the enum names
 */
#define GENERATE_VAR_H(STRING) extern const char STRING##_S[];
#define GENERATE_VAR_H_VAL(STRING, VAL) extern const char STRING##_S[];

/*
    generates enum names in PROGMEM
 */
#define GENERATE_VARS(STRING) const char STRING##_S[] PROGMEM = #STRING "\0"; //$
#define GENERATE_VARS_VAL(STRING, VAL) const char STRING##_S[] PROGMEM = #STRING "\0"; //$

/*
    generates vars names
 */
#define GENERATE_VAR_NAMES(STRING) STRING##_S,
#define GENERATE_VAR_NAMES_VAL(STRING, VAL) STRING##_S,

/*
    generates struct body
    struct menuitems
    {
        FOREACH_FOO(GENERATE_VAR_STRUCT_1)
        NULL
    }
 */
#define GENERATE_VAR_STRUCT(STRING, VAL) {STRING##_S, VAL},
#define GENERATE_VAR_STRUCT_1(STRING) {STRING##_S, STRING},

/*
    generates numirical only of the enum
 */
#define GENERATE_VARS_NUMERICAL(UNUSED, VAL) VAL,
#define GENERATE_VARS_NUMERICAL_1(VAL) VAL,

#define GENERATE_ARRAY_5ARGS(VAL1, VAL2, VAL3, VAL4, VAL5)\
{ \
  VAL1, VAL2, VAL3, VAL4, VAL5  \
},

/*
    defines for case generation
 */
#define SWITCH(INP) switch (INP)
#define GENERATE_CASES(STRING) case STRING:
#define GENERATE_CASES_VAL(STRING, UNUSED) case STRING:
#define GENERATE_CASE_RET(EN1, UNUSED, EN2) case EN1: return EN2;

/*
    count total amount of elements in enum during compile
 */
#define COUNT_TOTAL_ELEM(UNUSED, VAL) 1 +
#define COUNT_TOTAL_ELEM_1(VAL) 1 +
#define COUNT_TOTAL_EMEM_5(UNUSED, UNUSED2, UNUSED3, UNUSED4, UNUSED5) 1+

/*
unused
*/
#define CALL_MATCH_CONVERT(FUNCN) __convert_##FUNCN
#define ENUM_MATCH_CONVERT_FUNCTION_DEC(FUNCN, FOREACH_TYPE_RET, FOREACH_TYPE_INP)\
    static FOREACH_TYPE_RET __convert_##FUNCN(FOREACH_TYPE_INP e_input)
#define ENUM_MATCH_CONVERT_FUNCTION(FUNCN, FOREACH_TYPE_RET, FOREACH_TYPE_INP, FOREACH_MACRO)\
    static FOREACH_TYPE_RET __convert_##FUNCN(FOREACH_TYPE_INP e_input)\
    {\
        SWITCH(e_input)\
        {\
            FOREACH_MACRO(GENERATE_CASE_RET)\
            default:\
                setSysError(SYSTEM_UNKNOWN_CMD);\
                return -1;\
        }\
    }


/*
    verification if enum has specified value
 */
#define CALL_VRFY(FUNC_NAME) __veryfy_##FUNC_NAME
#define VERIFY_ENUM_FUNCTION_DEC(FUNC_NAME) static uint8_t __veryfy_##FUNC_NAME(uint8_t evs)
#define VERIFY_ENUM_FUNCTION_VAL(FUNC_NAME, FOREACH_MACRO) static uint8_t __veryfy_##FUNC_NAME(uint8_t evs)\
											{\
												SWITCH(evs)\
												{\
													FOREACH_MACRO(GENERATE_CASES_VAL)\
													return 1;\
													default:\
													return 0;\
												}\
												return 0;\
											}

#define VERIFY_ENUM_FUNCTION_DEC_P(FUNC_NAME) uint8_t __veryfy_##FUNC_NAME(uint8_t evs)
#define VERIFY_ENUM_FUNCTION_VAL_P(FUNC_NAME, FOREACH_MACRO) uint8_t __veryfy_##FUNC_NAME(uint8_t evs)\
											{\
												SWITCH(evs)\
												{\
													FOREACH_MACRO(GENERATE_CASES_VAL)\
													return 1;\
													default:\
													return 0;\
												}\
												return 0;\
											}

#endif /* ENUM_H_ */
