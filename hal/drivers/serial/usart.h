/*-
 *	Copyright (c) 2014-2015	Alexander Morozov www.nixd.org
 *	All rights reserved.
 *
 * LICENSE: This source file is subject to the New BSD license that is
 * available through the world-wide-web at the following URI:
 * http://www.opensource.org/licenses/bsd-license.php or in the root folder
 * of the project in file LICENSE.TXT.
 */
/*
 *  usart.h
 *  A USART specific driver.
 *  Details: this file contains the control code for the hardware module
 *  USART in synchonious and asyncronious modes.
 *  This module is consuming some amount of RAM.
 */

/*TODO:
    THis module is not finished!
    Add crc sum check
*/

#ifndef USART_H_
#define USART_H_


#include "../../lib/buffer/buffer.h"

//09.05.2016
#define USART_REVISION 100

#define UART_BAUD_RATE 9600UL

#define BAUD_END(UART_BAUD_RATE0, F_OSC0) (((F_OSC0 / (UART_BAUD_RATE0 * 16UL)))-1)

//flags
#define USART_TXE_STARTED 0x01

//---------------------
//hardware
/* additional flags of the usart setup
 * used in junction with usart_hw_init_helper2
 */
#define USART_DOUBLE_TXE_SPEED (1 << 0)
#define USART_MULTIPROC_MODE (1 << 1)
#define USART_ENABLE_TXE (1 << 2)
#define USART_ENABLE_RXE (1 << 3)
#define USART_RCV_INT (1 << 4)
#define USART_TXE_INT (1 << 5)


/*Clock polarity for the hardware.*/
#define FOREACH_USART_HW_CLOCK_POL(DV)\
	DV(CLOCK_POL_RF, (uint8_t) 0) \
	DV(CLOCK_POL_FR, (uint8_t) 1)

enum usart_hw_clock_pol
{
	FOREACH_USART_HW_CLOCK_POL(GENERATE_ENUM_VAL)
};

/*Length of frame selector.*/

#define FOREACH_USART_HW_CHAR_SIZE(DV)\
	DV(CHAR_SIZE_5BIT, (uint8_t) 0)\
	DV(CHAR_SIZE_6BIT, (uint8_t) 1)\
	DV(CHAR_SIZE_7BIT, (uint8_t) 2)\
	DV(CHAR_SIZE_8BIT, (uint8_t) 3)\
	DV(CHAR_SIZE_9BIT, (uint8_t) 7)

enum usart_hw_char_size
{
	FOREACH_USART_HW_CHAR_SIZE(GENERATE_ENUM_VAL)
};

/*Stop bits amount selector.*/

#define FOREACH_USART_HW_STOP_BIT(DV)\
	DV(STOP_BIT1, (uint8_t) 0)\
	DV(STOP_BIT2, (uint8_t) 1)

enum usart_hw_stop_bit
{
	FOREACH_USART_HW_STOP_BIT(GENERATE_ENUM_VAL)
};

/*Parity selector.*/

#define FOREACH_USART_HW_PARITY(DV)\
	DV(PARITY_NONE, (uint8_t) 0)\
	DV(PARITY_EVEN, (uint8_t) 2)\
	DV(PARITY_ODD, (uint8_t) 3)

enum usart_hw_parity
{
	FOREACH_USART_HW_PARITY(GENERATE_ENUM_VAL)
};

/*Hardware mode of operation selector.*/

#define FOREACH_USART_HW_MODE(DV)\
	DV(MODE_ASYNC, (uint8_t) 0)\
	DV(MODE_SYNC, (uint8_t) 1)\
	DV(MODE_SPI, (uint8_t) 3)

enum usart_hw_mode
{
	FOREACH_USART_HW_MODE(GENERATE_ENUM_VAL)
};



enum usart_dev
{
	FOREACH_USART_DEV(GENERATE_ENUM_VAL)
};

/*Software full-frame(packet) format.

#define FOREACH_USART_FRAME_FORMAT(DV)\
	DV(USART_FRAME_RAW, (uint8_t) 0)\
	DV(USART_FRAME_FORMATTED, (uint8_t) 1)

enum usart_frame_format
{
	FOREACH_USART_FRAME_FORMAT(GENERATE_ENUM_VAL)
};*/

/*buildin fifo or buffer with interupts.*/

#define FOREACH_USART_SW_RTOPERMODE(DV)\
	DV(USART_NON_BUFFERED, (uint8_t) 0)\
	DV(USART_BUFFERED_RX, (uint8_t) 1)\
	DV(USART_BUFFERED_TX, (uint8_t) 2)\
	DV(USART_BUFFERED_BOTH, (uint8_t) 3)

enum usart_sw_RTopermode
{
	FOREACH_USART_SW_RTOPERMODE(GENERATE_ENUM_VAL)
};

#define FOREACH_USART_BUFFER_TYPE(DV)\
	DV(USART_BUF_LINEAR, (uint8_t) BUF_TYPE_LINEAR)\
	DV(USART_BUF_CIRCULAR, (uint8_t) BUF_TYPE_CIRCULAR)

enum usart_sw_buftype
{
	FOREACH_USART_BUFFER_TYPE(GENERATE_ENUM_VAL)
};

/* Baudrate of the port selector.*/

#define FOREACH_USART_BAUD_RATE(DV)\
	DV(USART_BAUD_9600, (uint32_t) 9600UL)\
	DV(USART_BAUD_19200, (uint32_t) 19200UL)\
	DV(USART_BAUD_38400, (uint32_t) 38400UL)\
	DV(USART_BAUD_57600, (uint32_t) 57600UL)\
	DV(USART_BAUD_115200, (uint32_t) 115200UL)\
	DV(USART_BAUD_230400, (uint32_t) 230400UL)\
	DV(USART_BAUD_460800, (uint32_t) 460800UL)\
	DV(USART_BAUD_921600, (uint32_t) 921600UL)

enum usart_hw_baudrates
{
	FOREACH_USART_BAUD_RATE(GENERATE_ENUM_VAL)
};


struct usart_io
{
	struct dev_descr_node * ddn;             //fd inst of the usart
	enum usart_dev dev_d;                   //usart device
	enum usart_sw_RTopermode RT_oper_mode;  //software mode (buf/nonbuf)
    uint8_t flags;                          //flags (state)
    struct buffer * rx_buf;                 //incoming buffer (can be null!)
    struct buffer * tx_buf;                 //outgoing buffer (can be null!)
    enum ports port_rts;                    //RTS PORT
	int8_t pin_rts_n;                      //RTS pin
	enum ports port_cts;                    //CTS PORT
	int8_t pin_cts_n;                      //CTS pin

#if SERIAL_BUILD_WITH_BYTES_COUNTER == YES
	uint32_t txbytes;
	uint32_t rxbytes;
	uint16_t rxerrors;
#endif
};

struct usart_io_eeprom_envilope
{
	enum usart_dev dev_d;                    //usart device
	enum usart_sw_RTopermode RT_oper_mode;   //software mode (buf/nonbuf)
    uint8_t reg_ucsra;
    uint8_t reg_ucsrb;
    uint8_t reg_ucsrc;
    uint16_t reg_ubrr;
    enum ports port_rts;                     //RTS PORT
	int8_t pin_rts_n;                        //RTS pin
	enum ports port_cts;                     //CTS PORT
	int8_t pin_cts_n;                        //CTS pin
	uint16_t inc_buf_size;
	uint16_t out_buf_size;
	uint8_t buf_types;  //00xx - rx 00xx - tx
};

/*
 * struct usart_hw_setup
 *  structure used for setup purposes
 */
struct usart_hw_setup
{
	uint8_t ucsra;
	uint8_t ucsrb;
	uint8_t ucsrc;
	uint16_t ubrr;
};

/*
 * struct usart_setup_struct
 *  envelope for the usart init/update
 */
struct usart_setup_struct
{
	struct usart_hw_setup * hw_setup;
	enum usart_dev e_usart_hw;
	enum usart_sw_RTopermode tr_oper_mode;
	enum usart_sw_buftype e_sw_byftype;
	uint16_t inc_buf_size;
	uint16_t out_buf_size;

	//RTS/CTS
	enum ports port_rts;
	int8_t pin_rts_n;
	enum ports port_cts;
	int8_t pin_cts_n;
};

//debug
void usart_init();

/*
* Verification functions.
*/


/*
*   Initialization helper functions.
*/
void usart_hw_init_helper(struct usart_hw_setup * hw_setup_ptr, enum usart_hw_clock_pol clock_pol, enum usart_hw_char_size char_size,
							enum usart_hw_stop_bit stop_bit, enum usart_hw_parity parity, enum usart_hw_mode hw_mode);
void usart_hw_init_helper2(struct usart_hw_setup * hw_setup_ptr, uint8_t flags1);
void usart_hw_init_helper3(struct usart_hw_setup * hw_setup_ptr, uint32_t baud);
void usart_hw_init_helperf(struct usart_setup_struct * hw_set_ptr, struct usart_hw_setup * hw_setup_ptr,
                           enum usart_dev e_usart_hw, enum usart_sw_RTopermode e_oper_mode,
                           enum usart_sw_buftype e_buf_type, uint16_t ibs, uint16_t obs);
void usart_hw_init_helperio(struct usart_setup_struct * hw_set_ptr, enum ports port_rts, uint8_t pin_rts_n, enum ports port_cts,
                            uint8_t pin_cts_n);
/*
 *   Standart module functions
 */
int8_t usart_hal_message(struct dev_descr_node * ddn,
                          enum dd_event e_event,
                          void * data0,
                          size_t * dsize0);

int8_t usart_kstream_init(struct dev_descr_node * ddn,
                           struct kstream * ks);

int8_t usart_stream_init(struct dev_descr_node * ddn,
                             FILE * file);

/*
*   Functions used to assess struct usart_io records.
*/
struct usart_io * usart_get_descr(enum usart_dev u_dev_i);
struct usart_io * usart_get_descr_by_did(t_dev_id did);
int8_t usart_is_buffered_tx(struct dev_descr_node * ddn);
int8_t usart_is_buffered_rx(struct dev_descr_node * ddn);

//async USART IO
/*
* Asynchronous operation functions.
*/

/*
* usart_flush is used to trigger a transmission from the
* outgoing buffer.
*/
int8_t usart_flush(struct usart_io * s_usart_ptr);

/*
* usart_drop_frame is used to tell driver to drop current unreaded bytes
* of current packet
*/
void usart_drop_frame(struct usart_io * s_usart_ptr);


/* Synchronious operation */

/*
 * usart_sync_receive_byte is blocking function for reading from the usart hardware fifo.
 */
int16_t usart_sync_receive_byte(struct usart_io * s_usart_ptr);

int8_t usart_sync_check_rcv(struct usart_io * s_usart_ptr);

/*
 * usart_sync_send_byte is blocking function for writing to the usart hardware fifo.
 */
int16_t usart_sync_send_byte(struct usart_io * s_usart_ptr, uint8_t data);

/*
 * usart_transmitString_F is used to transmit string from PROGMEM memory using blocking
 * type write.
 */
void usart_transmitString_F(struct usart_io * s_usart_ptr, const char* string);


/* Stream specific functions used via kstream.*/
int16_t usart_static_read_length(struct kstream * stream);
int16_t usart_static_write_length(struct kstream * stream);
int8_t usart_static_write_async(struct kstream * stream, uint8_t * buf_ptr, uint16_t bwsize);
int32_t usart_static_read_async(struct kstream * stream, uint8_t * rarr, uint16_t rlen);
int32_t usart_static_read_arr_async(struct kstream * stream, uint8_t * rarr, uint16_t rlen);
int32_t usart_static_read_arr_sync(struct kstream * stream, uint8_t * rarr, uint16_t rlen);
int16_t usart_static_read_length_sync(struct kstream * stream);
int16_t usart_static_write_length_sync(struct kstream * stream);
int8_t usart_static_write(struct kstream * stream, uint8_t var);
int16_t usart_static_read(struct kstream * stream);
int8_t usart_static_pipe_init_dummy(struct kstream * stream);
int8_t usart_static_pipe_init(struct kstream * stream);
int8_t usart_static_pipe_finish(struct kstream * stream);


void debug(char ch);

#endif
