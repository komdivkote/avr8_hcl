/*-
 *	Copyright (c) 2014-2015	Alexander Morozov. NiXD ORG
 *	All rights reserved.
 *
 * LICENSE: This source file is subject to the New BSD license that is
 * available through the world-wide-web at the following URI:
 * http://www.opensource.org/licenses/bsd-license.php or in the root folder
 * of the project in file LICENSE.TXT.
 */


#ifndef VT_ATTACH_H_
#define VT_ATTACH_H_

#define VT_INFO_OPTS_INFO 1
#define VT_INFO_OPTS_MEAS 2
#define VT_INFO_OPTS_CHANGEVAL 3
#define VT_INFO_OPTS_SEND 4
#define VT_INFO_OPTS_RECEIVE 5
#define VT_INFO_OPTS_ADD 6
#define VT_INFO_OPTS_REMOVE 7
#define VT_INFO_OPTS_RESCAN 8
#define VT_INFO_OPTS_MEAS_ASYNC 9

extern const struct menudialog_entry vt_io_ports_captions_struct[];

void __convert_ip(char * str, char * ip);

int8_t vt_attach_driver(FILE * file, struct dev_descr_node * dd, enum dev_descr_code ddc);
int8_t vt_attach_handler(FILE * file, struct dev_descr_node * dd, enum dev_descr_code ddc, enum dev_desc_handlers ddh);
int8_t vt_modify_node(FILE * file, struct dev_descr_node * dd);
int8_t vt_extra_options(FILE * file, struct dev_descr_node * dd);

//SPI
int8_t vt_attach_spi(FILE * file, struct spi_setup * spiset);
int8_t vt_modify_spi(FILE * file, struct dev_descr_node * dd);
int8_t vt_info_spi(FILE * file, struct dev_descr_node * dd);

//io
int8_t vt_attach_io(FILE * file, struct io_setup * ios);
int8_t vt_info_io(FILE * file, struct dev_descr_node * dd);

//USART
int8_t vt_attach_usart(FILE * file, void * data);
int8_t vt_modify_usart(FILE * file, struct dev_descr_node * dd);
int8_t vt_info_usart(FILE * file, struct dev_descr_node * dd);

//ADC
int8_t vt_attach_adc(FILE * file, struct adc_setup * adcset);
int8_t vt_modify_adc(FILE * file, struct dev_descr_node * dd);
int8_t vt_info_adc(FILE * file, struct dev_descr_node * dd);

//TIMER
int8_t vt_attach_timer(FILE * file, struct timer_setup * timerset);
int8_t vt_modify_timer(FILE * file, struct timer_setup * timerset);
int8_t vt_info_timer(FILE * file, struct dev_descr_node * dd);

//HANDLERS
//MAX6675
int8_t vt_attach_max6675_s(struct fd_dev_node * fd);
int8_t vt_modify_max6675_s(struct fd_dev_node * fd);
int8_t vt_info_max6675_s(struct fd_dev_node * fd);

//DS18B20
int8_t vt_attach_ds18b20(FILE * file, struct ds18_setup * dss);
int8_t vt_modify_ds18b20(FILE * file, struct ds18_setup * dss);
int8_t vt_info_ds18b20(FILE * file, struct dev_descr_node * dd);

//usart route
int8_t vt_attach_usart_router(struct fd_dev_node * fd);
int8_t vt_modify_usart_router(struct fd_dev_node * fd);
int8_t vt_info_usart_router(struct fd_dev_node * fd);

//spi enc28j60
int8_t vt_attach_spi_enc28j60(FILE * file, struct enc28_config * data);
int8_t vt_modify_spi_enc28j60(FILE * file, struct dev_descr_node * dd);
int8_t vt_info_spi_enc28j60(FILE * file, struct dev_descr_node * dd);

int8_t vt_attach_anonpan(FILE * file, struct anon_panel_setup * aps);
int8_t vt_modify_anonpan(FILE * file, struct dev_descr_node * dd);
int8_t vt_info_anonpan(FILE * file, struct dev_descr_node * dd);

int8_t vt_attach_u2eth(FILE * file, void * nulla);
int8_t vt_modify_u2eth(FILE * file, struct dev_descr_node * dd);
int8_t vt_info_u2eth(FILE * file, struct dev_descr_node * dd);

int8_t vt_attach_inpscan(FILE * file, struct dim_instance_config * dis);
int8_t vt_modify_inpscan(FILE * file, struct dev_descr_node * dd);
int8_t vt_info_inpscan(FILE * file, struct dev_descr_node * dd);

#endif /* VT_ATTACH_H_ */
