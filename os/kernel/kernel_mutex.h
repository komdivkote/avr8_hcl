/*-
 *	Copyright (c) 2014-2016	Alexander Morozov. www.nixd.org
 *	All rights reserved.
 *
 * LICENSE: This source file is subject to the New BSD license that is
 * available through the world-wide-web at the following URI:
 * http://www.opensource.org/licenses/bsd-license.php or in the root folder
 * of the project in file LICENSE.TXT.
 */


#ifndef KERNEL_MUTEX_H_
#define KERNEL_MUTEX_H_

#include "kernel_types.h"

//mutex
struct kproc_mutex;

/*
 * kernel_init_mutex initializes mutex in heap!
 *	Must be deallocated via free();
 *	returns pointer to the kproc_mutex record or NULL
 */
struct kproc_mutex * kernel_mutex_init(void);

/*
 * kernel_lock_mutex locks mutex if instance is free or switches the process to the coop mode,
 *	process is placed  to the waiting queue and is set to wait state.
 *	mutx (struct kproc_mutex *) - pointer to the valid instance
 * returns: usually returns 0 but can return -1 on error
 */
int8_t kernel_mutex_lock(struct kproc_mutex * mutx);

/*
 * kernel_unlock_mutex unlocks the mutex instance
 *	mutx (struct kproc_mutex *) - pointer to the valid instance
 * returns: usually returns 0 but can return -1 on error
 */
int8_t kernel_mutex_unlock(struct kproc_mutex * mutx);

/*
 * kernel_try_lock_mutex is trying to lock the mutex without sending to wait
 *	the process. If lock is successfull the mutex will be locked and control
 *	released. If not, it will return immidiatly.
 *	mutx (struct kproc_mutex *) - pointer to the valid instance
 *returns: 0 - on success, 1 - cant lock immidiatly, -1 - error
 */
int8_t kernel_mutex_try_lock(struct kproc_mutex * mutx);

/*
 * kernel_mutex_get_cnt
 * mux (struct kproc_mutex *) - valid instance of kproc_mutex
 * returns: mutex counter number
 */
k_pid kernel_mutex_get_cnt(struct kproc_mutex * mux);

/*
 * kernel_mutex_get_cwating
 * mux (struct kproc_mutex *) - valid instance of kproc_mutex
 * returns: mutex counter wating
 */
uint8_t kernel_mutex_get_cwating(struct kproc_mutex * mux);

/*
 * kernel_mutex_get_corder
 * mux (struct kproc_mutex *) - valid instance of kproc_mutex
 * returns: mutex counter order
 */
uint8_t kernel_mutex_get_corder(struct kproc_mutex * mux);

/*
 * kernel_mutex_get_ccounter
 * mux (struct kproc_mutex *) - valid instance of kproc_mutex
 * returns: mutex internal counter
 */
uint8_t kernel_mutex_get_ccounter(struct kproc_mutex * mux);

struct kproc_mutex * kernel_mutex_get_proc_waiting(k_pid pid);

/*--------------------MUTEX BLOCK---------------------*/

struct _mux_ToDo
{
	struct kproc_mutex * mux;
	int8_t ret;
};

static inline void
__unlock_mutex(const struct _mux_ToDo * mtd)
{
	kernel_mutex_unlock(mtd->mux);

	asm volatile ("" ::: "memory");
}

//exit mutex block immediately
#define MUTEX_ACTION_LEAVE 0
//manual problem resolution (see MUTEX_BLOCK_VAR_RET)
#define MUTEX_ACTION_MANUAL 2

//retrives the returnd result
#define MUTEX_BLOCK_VAR_RET st.ret
#define MUTEX_LOCK_FAILED MUTEX_ACTION_MANUAL

#define MUTEX_BLOCK(_MUX_, _ACTION_) \
	for (struct _mux_ToDo st __attribute__((__cleanup__(__unlock_mutex))) = {_MUX_, (kernel_mutex_lock(_MUX_) == 0) ? 1 : _ACTION_};\
		st.ret;\
		st.ret = 0 )

/*
    void bar()
    {
        MUTEX_BLOCK(mtex_var, MUTEX_ACTION_LEAVE)
        {
            ...
            if (condition)
            {
                return 1;
            }
        }
    }

    void bar()
    {
        MUTEX_BLOCK(mtex_var, MUTEX_ACTION_MANUAL)
        {
            if (MUTEX_BLOCK_VAR_RET == MUTEX_LOCK_FAILED)
            {
                //mutex lock failed
            }
            ...
            if (condition)
            {
                return 1;
            }
        }
    }
 */

#endif /* KERNEL_MUTEX_H_ */
