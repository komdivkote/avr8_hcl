#ifndef CRC16_H_INCLUDED
#define CRC16_H_INCLUDED

#define CRC16_INIT 0xFFFF

uint16_t crc16_update(uint16_t crc, uint8_t a);

#endif // CRC16_H_INCLUDED
