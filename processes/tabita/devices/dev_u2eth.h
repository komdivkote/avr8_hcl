#ifndef DEV_U2ETH_H_INCLUDED
#define DEV_U2ETH_H_INCLUDED

#define FOREACH_U2ETH_CMD(DV)\
	DV(U2ETH_CMD_RECEIVE, (uint8_t) 1)\
	DV(U2ETH_SEND, (uint8_t) 2)\
	DV(U2ETH_CMD_OPER_NOT_SUP, (uint8_t) 0xFF)

enum u2eth_commands
{
	FOREACH_U2ETH_CMD(GENERATE_ENUM_VAL)
};



struct u2eth_proto_receive
{
    t_dev_id did_orig;
    uint8_t data_len;
    uint8_t data[];
};

struct u2eth_proto_send
{
    t_dev_id did_dest;
    uint8_t data_len;
    uint8_t data[];
};

#endif // DEV_U2ETH_H_INCLUDED
