/*-
 *	Copyright (c) 2014-2015	Alexander Morozov. NiXD ORG
 *	All rights reserved.
 *
 * LICENSE: This source file is subject to the New BSD license that is
 * available through the world-wide-web at the following URI:
 * http://www.opensource.org/licenses/bsd-license.php or in the root folder
 * of the project in file LICENSE.TXT.
 */
/*
 *  shared.h
 *  shared macroses and functions (specially for Harvard arch).
 *  Details: this file contains common macroses that can be used to work with
 *  lists, data check and data manipulation.
 */
#ifndef SHARED_H_
#define SHARED_H_

#include <stddef.h>
#include <avr/pgmspace.h>
#include "../os/kernel/kernel_memory.h"

extern const char P_NULL[];

/*returns a leangth of array according to its size.*/
#define ARR_LEN(x) (sizeof (x) / sizeof (*(x)))

/*! \def ASSERT_NULL_E(var, error_id, goto_label)
*   \brief checks if var == NULL, sets error and goto to label.
*   \param var Variable to test.
*   \param error_id Error number to set on true condition (see error.h).
*   \param goto_label A label to go to on true condition.
*
*   Details: i.e use it to test arguments of the function on illigal data
*/
#define ASSERT_NULL_E(var, error_id, goto_label)\
if (var == NULL) {\
    setSysError(error_id);\
    goto goto_label;\
}

#define ASSERT_NULL_ER(var, error_id, __RET__)\
if (var == NULL) {\
    setSysError(error_id);\
    return __RET__;\
}

#define ASSERT_NULL_R(var, __RET__)\
if (var == NULL) {\
    return __RET__;\
}

/*! \def ASSERT_NULL(var, goto_label)
*   \brief checks if var == NULL and goto to label.
*   \param var Variable to test.
*   \param goto_label A label to go to on true condition.
*
*   Details: i.e use it to test arguments of the function on illigal data
*/
#define ASSERT_NULL(var, goto_label)\
if (var == NULL) {\
    goto goto_label;\
}

/*! \def ASSERT_LEN(var, len, goto_label)
*   \brief checks if var < len and goto to label.
*   \param var Variable to test.
*   \param len To test val arg againt of length.
*   \param goto_label A label to go to on true condition.
*
*   Details: i.e use it to test arguments of the function on illigal data
*/
#define ASSERT_LEN(var, len, goto_label)\
if (var < len) {\
    goto goto_label;\
}

/*! \def ASSERT_DATA(expr, true_exec)
*   \brief checks if expr is true, then true_exec will be executed.
*   \param expr An expression to test i.e 2 >= 1 or (i > 1) && (y < 4).
*   \param true_exec A set of commands to execute is tested expression is true.
*
*   Details: i.e use it to test arguments of the function on illigal data
*/
#define ASSERT_DATA(expr, true_exec)\
if (expr) {\
    true_exec;\
}

#define ASSERT_DATA_ER(expr, error_id, __RET__)\
if (expr) {\
    setSysError(error_id);\
    return __RET__;\
}

/*! \def ASSERT_NEGATIVE(var, goto_label)
*   \brief checks if var < 0 and goto to label.
*   \param var Variable to test.
*   \param goto_label A label to go to on true condition.
*
*   Details: i.e use it to test arguments of the function on illigal data
*/
#define ASSERT_NEGATIVE(var, goto_label)\
if (var < 0) {\
    goto goto_label;\
}

#define ASSERT_NEGATIVE_ER(var, error_id, __RET__)\
if (var < 0) {\
    setSysError(error_id);\
    return __RET__;\
}

#define LIST struct

/*! \def LIST_STRUCT(_LIST_CAPT_, _T_)\
 *  \brief Generates list structure for _T_
 *  \param _LIST_CAPT_ a caption of the list struct
 *  \param _T_ the node struct
 *
 *  Details: Use or not to use.
 */
#define LIST_STRUCT(_LIST_CAPT_, _T_)\
LIST _LIST_CAPT_\
{\
    struct _T_ * node;\
    LIST _LIST_CAPT_ * next;\
}

#define LIST_ITEM(_data_type_list_, _list_ptr_item_)\
    _data_type_list_ * _list_ptr_item_ = NULL;

#define INIT_LIST_ITEM(data_type_list, list_ptr_item, goto_error)\
    list_ptr_item = (data_type_list*) kernel_malloc(sizeof(data_type_list)); \
    ASSERT_NULL_E(list_ptr_item, SYSTEM_MALLOC_FAILED, goto_error); \
    list_ptr_item->next = NULL;

/*! \def NEW_LIST_ITEM(data_type_list, list_ptr_item, goto_error)
*   \brief Creates new list item.
*   \param data_type_list A structure of the list that contains field next (pointer to next item).
*   \param list_ptr_item A returned pointer of instance of item in heap.
*   \param goto_label A label to go to in case of error.
*
*   Details: Creates a member of linked list based on the provided data type.
*   In case of error, will jump to provided label.
*/
#define NEW_LIST_ITEM(data_type_list, list_ptr_item, goto_error)\
    data_type_list * list_ptr_item = (data_type_list*) kernel_malloc(sizeof(data_type_list)); \
    ASSERT_NULL_E(list_ptr_item, SYSTEM_MALLOC_FAILED, goto_error); \
    list_ptr_item->next = NULL;

/*! \def NEW_LIST_ITEM_S(data_type_list, list_ptr_item)
*   \brief Creates new list item without error handling.
*   \param data_type_list A structure of the list that contains field next (pointer to next item).
*   \param list_ptr_item A returned pointer of instance of item in heap.
*   \param goto_label A label to go to in case of error.
*
*   Details: Creates a member of linked list based on the provided data type. Without error handling.
*/
#define NEW_LIST_ITEM_S(data_type_list, list_ptr_item)\
data_type_list * list_ptr_item = (data_type_list*) kernel_malloc(sizeof(data_type_list));

/*! \def ADD_TO_LIST(data_type_list, list_ptr, item_list)
*   \brief Adds item to the end of the list.
*   \param data_type_list A structure of the list.
*   \param list_ptr A pointer to the list.
*   \param item_list An item to add (should be same type as the list ptr).
*
*   Details: This macro will add item to the end of the list.
*/
#define ADD_TO_LIST(data_type_list, list_ptr, item_list)\
if (list_ptr == NULL)\
{\
    list_ptr = item_list;\
    item_list->next = NULL;\
}\
else\
{\
    data_type_list * temp_list = list_ptr;\
    while (temp_list->next != NULL) temp_list = temp_list->next; \
    temp_list->next = item_list; \
    item_list->next = NULL;\
}

/*! \def PUSH_TO_LIST(data_type_list, list_ptr, item_list)
*   \brief Adds item to the start of the list.
*   \param data_type_list A structure of the list.
*   \param list_ptr A pointer to the list.
*   \param item_list An item to add (should be same type as the list ptr).
*
*   Details: This macro will add item to the start of the list.
*/
#define PUSH_TO_LIST(list_ptr, item_list)\
if (list_ptr == NULL)\
{\
    list_ptr = item_list;\
    item_list->next = NULL;\
}\
else\
{\
    item_list->next = list_ptr; \
    list_ptr = item_list;\
}

/* Manual list manipulation*/
/*! \def ADD_TO_LIST_MANUAL(data_type_list, list_ptr, item_list)
*   \brief Macro-helper which can be used to design an advanced item add code.
*   \param data_type_list A structure of the list.
*   \param list_ptr A pointer to the list.
*   \param item_list An item to add (should be same type as the list ptr).
*
*   Details: This macro is a prolog of the list item insertion i.e
*   ADD_TO_LIST_MANUAL(struct bar, list, item){}
*/
#define ADD_TO_LIST_MANUAL(data_type_list, list_ptr)\
    data_type_list * __it = list_ptr;\
    data_type_list * __it_prev = NULL;\
    while (1)
/*! \def PREV_IT
*   \brief A macro to previous element of the list at current state.
*/
#define PREV_IT __it_prev

/*! \def CURT_IT
*   \brief A macro to current element of the list at current state.
*/
#define CURT_IT __it

/*! \def NEXT_ELEMENT
*   \brief Iterate to the next element.
*/
#define NEXT_ELEMENT __it_prev = __it; \
					__it = __it->next;

/*! \def IF_LAST_ELEMENT
*   \brief Tests if it is last element.
*    i.e IF_LAST_ELEMENT{do_something();}
*/
#define IF_LAST_ELEMENT if ( __it == NULL )

/*! \def INJECT_ELEMENT_BEFORE_IT(elem, orig_list)
*   \brief Injects element before current element.
*   \param elem A item which is required to add.
*   \param orig_list A original pointer to the list.
*    i.e INJECT_ELEMENT_BEFORE_IT(item, list_ptr)
*/
#define INJECT_ELEMENT_BEFORE_IT(elem, orig_list)\
if (__it_prev == NULL)\
{\
    elem->next = __it;\
    orig_list = elem;\
}\
else\
{\
    elem->next = __it_prev->next;\
    __it_prev->next = elem;\
}

/*! \def INJECT_ELEMENT_AFTER_PREV_IT(elem, orig_list)
*   \brief Injects element after previous element.
*   \param elem A item which is required to add.
*   \param orig_list A original pointer to the list.
*    i.e INJECT_ELEMENT_AFTER_PREV_IT(item)
*/
#define INJECT_ELEMENT_AFTER_PREV_IT(elem)\
elem->next = NULL; \
__it_prev->next = elem;

/*! \def COMPARE_ORDER(it_node_var, sign)
*   \brief Checks order of the curent and prev record in the list.
*   \param it_node_var A field of the list struct to compare.
*   \param sign A logical operator i.e >= >...
*    Description: i.e struct bar {uint32_t id ...}
*     COMPARE_ORDER(id, >){...}
*/
#define COMPARE_ORDER(it_node_var, sign) if (__it->node->it_node_var sign __it_prev->node->it_node_var)

/*! \def COMPARE_DIFF(it_node_var, len)
*   \brief Checks the distance between pev and current.
*   \param it_node_var A field of the list struct to compare.
*   \param len A distance value to compare with.
*    Description: i.e struct bar {uint32_t id ...}
*     COMPARE_DIFF(id, 1){...}
*/
#define COMPARE_DIFF(it_node_var, len) if (( __it_prev != NULL ) && ( (__it->node->it_node_var - __it_prev->node->it_node_var) >= len ))

/*! \def COMPARE_ORDER_DIR(it_node_var, len)
*   \brief Checks order of the curent and prev record in the list.
*   \param it_node_var A field of the list struct to compare.
*   \param sign A logical operator i.e >= >...
*    Description: i.e struct bar {uint32_t id ...}.
*     COMPARE_ORDER_DIR(id, >){...}
*/
#define COMPARE_ORDER_DIR(it_node_var, sign) if (__it->it_node_var sign __it_prev->it_node_var)

/*! \def COMPARE_DIFF_DIR(it_node_var, len)
*   \brief Checks the distance between pev and current.
*   \param it_node_var A field of the list struct to compare.
*   \param len A distance value to compare with.
*    Description: i.e struct bar {uint32_t id ...}
*     COMPARE_DIFF_DIR(id, 1){...}
*/
#define COMPARE_DIFF_DIR(it_node_var, len) if (( __it_prev != NULL ) && ( (__it->it_node_var - __it_prev->it_node_var) >= len ))

/*! \def COMPARE_ELEM_DIR_NEXT(it_node_var, item_list, sign)
*   \brief Checks the distance between selected item and current.
*   \param it_node_var A field of the list struct to compare.
*   \param len A distance value to compare with.
*    Description: i.e struct bar {uint32_t id ...}
*     COMPARE_ELEM_DIR_NEXT(id, item, >){...}
*/
#define COMPARE_ELEM_DIR_NEXT(it_node_var, item_list, sign) if (( __it != NULL ) && (item_list->it_node_var sign __it_prev->it_node_var))

/*! \def COMPARE_ELEM_DIR_PREV(it_node_var, item_list, sign)
*   \brief Checks the distance between selected item and previous.
*   \param it_node_var A field of the list struct to compare.
*   \param len A distance value to compare with.
*    Description: i.e struct bar {uint32_t id ...}
*     COMPARE_ELEM_DIR_PREV(id, item, >){...}
*/
#define COMPARE_ELEM_DIR_PREV(it_node_var, item_list, sign) if (( __it_prev != NULL ) && (item_list->it_node_var sign __it_prev->it_node_var))

#define PUT_ELEM_WITH_KNOWN_ORDER_NUM(orig_list, it_node_var, elem, value) \
    if (__it_prev == NULL)\
    {\
        if (value < __it->node->it_node_var)\
        {\
            INJECT_ELEMENT_BEFORE_IT(elem, orig_list)\
            FINISH_SORTING;\
        }\
    }\
    else if ( (__it_prev->node->it_node_var < value) && ((value) < __it->node->it_node_var) )\
    {\
        INJECT_ELEMENT_AFTER_PREV_IT(elem)\
        FINISH_SORTING;\
    }

/*! \def ELSE
*   \brief else.
*    Description: else
*/
#define ELSE else

/*! \def FINISH_SORTING
*   \brief leave sorting immediatly
*/
#define FINISH_SORTING break

/*--------------------------SEARCHING, COUNTING, MODIFYING-----------------------------*/

/*! \def SEARCH_IN_LIST(data_type_list, list_ptr, expression, item_ret, goto_nothing_found)
*   \brief Lookup for item in list.
*   \param data_type_list A list structure.
*   \param list_ptr A pointer to the list.
*   \param expression A creteria which is used to lookup for the item.
*   \param item_ret A returned item if any found (NULL).
*   \param goto_nothing_found A label to jump to if nothing found.
*
*   SEARCH_IN_LIST(struct bar, list, item->id == 5, item, nothing_found_0);
*/
#define SEARCH_IN_LIST(data_type_list, list_ptr, expression, item_ret, goto_nothing_found)\
data_type_list * item_ret = list_ptr;\
while (item_ret != NULL)\
{\
    if (item_ret->expression)\
    { \
        break; \
    } \
    item_ret = item_ret->next; \
} \
if (item_ret == NULL) goto goto_nothing_found;

/*! \def SEARCH_IN_LIST_2(data_type_list, list_ptr, expression, item_ret, goto_nothing_found)
*   \brief Lookup for item in list.
*   \param data_type_list A list structure.
*   \param list_ptr A pointer to the list.
*   \param expression A creteria which is used to lookup for the item.
*   \param item_ret A returned item if any found (NULL).
*   \param goto_nothing_found A label to jump to if nothing found.
*
*   SEARCH_IN_LIST_2(struct bar, list, item, item->id == 5,nothing_found_0);
*/
#define SEARCH_IN_LIST_2(data_type_list, list_ptr, item_ret, expression, goto_nothing_found)\
data_type_list * item_ret = list_ptr; \
while (item_ret != NULL) \
{ \
    if (expression)\
    { \
        break; \
    } \
    item_ret = item_ret->next; \
}\
if (item_ret == NULL) goto goto_nothing_found;

/*! \def SEARCH_IN_LIST_2F(data_type_list, list_ptr, expression, item_ret, goto_found)
*   \brief Lookup for item in list.
*   \param data_type_list A list structure.
*   \param list_ptr A pointer to the list.
*   \param expression A creteria which is used to lookup for the item.
*   \param item_ret A returned item if any found (NULL).
*   \param goto_found A label to jump to if found.
*
*   SEARCH_IN_LIST_2F(struct bar, list, item, item->id == 5, found_0);
*/
#define SEARCH_IN_LIST_2F(data_type_list, list_ptr, item_ret, expression, goto_found)\
data_type_list * item_ret = list_ptr; \
while (item_ret != NULL) \
{ \
    if (expression)\
    { \
        goto goto_found; \
    } \
    item_ret = item_ret->next; \
}

/*! \def SEARCH_IN_LIST_3(data_type_list, list_ptr, expression, item_ret)
*   \brief Lookup for item in list (silent).
*   \param data_type_list A list structure.
*   \param list_ptr A pointer to the list.
*   \param expression A creteria which is used to lookup for the item.
*   \param item_ret A returned item if any found (NULL).
*
*   SEARCH_IN_LIST_3(struct bar, list, item, item->id == 5);
*/
#define SEARCH_IN_LIST_3(data_type_list, list_ptr, item_ret, expression)\
data_type_list * item_ret = list_ptr; \
while (item_ret != NULL) \
{ \
    if (expression)\
    { \
        break; \
    } \
    item_ret = item_ret->next; \
}

/*! \def REMOVE_FROM_LIST(data_type_list, list_ptr, expression, item_ret, goto_nothing_found)
*   \brief Remove specific item from list.
*   \param data_type_list A list structure.
*   \param list_ptr A pointer to the list.
*   \param expression A creteria which is used to lookup for the item.
*   \param item_ret A returned item if any found (NULL).
*   \param goto_nothing_found A label to jump to if found.
*
*   REMOVE_FROM_LIST(struct bar, list, item->id == 5, item, not_found);
*/
#define REMOVE_FROM_LIST(data_type_list, list_ptr, expression, item_ret, goto_nothing_found)\
data_type_list * item_ret = list_ptr; \
data_type_list * item_ret_prev = NULL; \
while (item_ret != NULL) \
{ \
    if (item_ret->expression) { \
        break; \
    } \
    item_ret_prev = item_ret; \
    item_ret = item_ret->next; \
} \
if (item_ret == NULL) goto goto_nothing_found; \
if (item_ret_prev == NULL) \
{ \
    list_ptr = item_ret->next; \
} \
else \
{ \
    item_ret_prev->next = item_ret->next; \
}

/*! \def REMOVE_FROM_LIST_2(data_type_list, list_ptr, item_ret, expression, goto_nothing_found)
*   \brief Remove specific item from list.
*   \param data_type_list A list structure.
*   \param list_ptr A pointer to the list.
*   \param expression A creteria which is used to lookup for the item.
*   \param item_ret A returned item if any found (NULL).
*   \param goto_nothing_found A label to jump to if found.
*
*   REMOVE_FROM_LIST_2(struct bar, list, item, item->id == 5, not_found);
*/
#define REMOVE_FROM_LIST_2(data_type_list, list_ptr, item_ret, expression, goto_nothing_found)\
data_type_list * item_ret = list_ptr; \
data_type_list * item_ret##_prev = NULL; \
while (item_ret != NULL) \
{ \
    if (expression) { \
        break; \
    } \
    item_ret##_prev = item_ret; \
    item_ret = item_ret->next; \
} \
if (item_ret == NULL) goto goto_nothing_found; \
if (item_ret##_prev == NULL) \
{ \
    list_ptr = item_ret->next; \
} \
else \
{ \
    item_ret##_prev->next = item_ret->next; \
}

/*! \def REMOVE_FROM_LIST_S(data_type_list, list_ptr, item_ret, expression)
*   \brief Remove specific item from list (silent).
*   \param data_type_list A list structure.
*   \param list_ptr A pointer to the list.
*   \param expression A creteria which is used to lookup for the item.
*   \param item_ret A returned item if any found (NULL).
*
*   REMOVE_FROM_LIST_S(struct bar, list, item, item->id == 5);
*/
#define REMOVE_FROM_LIST_S(data_type_list, list_ptr, item_ret, expression)\
data_type_list * item_ret = list_ptr; \
data_type_list * item_ret##_prev = NULL; \
while (item_ret != NULL) \
{ \
    if (expression) { \
        break; \
    } \
    item_ret##_prev = item_ret; \
    item_ret = item_ret->next; \
} \
if (item_ret != NULL) \
{ \
    if (item_ret##_prev == NULL) \
    { \
        list_ptr = item_ret->next; \
    } \
    else \
    { \
        item_ret##_prev->next = item_ret->next; \
    } \
}

/*! \def REMOVE_FROM_LIST_ALL_MATCH(data_type_list, list_ptr, item_ret, expression, extra_exp)
*   \brief Remove specific item from list (silent).
*   \param data_type_list A list structure.
*   \param list_ptr A pointer to the list.
*   \param expression A creteria which is used to lookup for the item.
*   \param extra_exp A extra commands which should be used for memory releasing purposes i.e free.
*
*   REMOVE_FROM_LIST_ALL_MATCH(struct bar, list, item, item->id == 5, free(item););
*/
#define REMOVE_FROM_LIST_ALL_MATCH(data_type_list, list_ptr, item_ret, expression, extra_exp)\
data_type_list * item_ret = list_ptr;\
data_type_list * item_ret##_prev = NULL;\
while (item_ret != NULL)\
{\
    if (expression) {\
        if (item_ret##_prev == NULL)\
        {\
            list_ptr = item_ret->next;\
        }\
        else\
        {\
            item_ret##_prev->next = item_ret->next;\
        }\
        data_type_list * temp = item_ret->next;\
        extra_exp\
        item_ret = temp;\
    }\
    else\
    {\
        item_ret##_prev = item_ret;\
        item_ret = item_ret->next;\
    }\
}


/*! \def COUNT_ELEMENTS_IN_LIST(data_type_list, list_ptr, amount)
*   \brief Count elements in list (silent).
*   \param data_type_list A list structure.
*   \param list_ptr A pointer to the list.
*   \param amount A amount of items in list, amount variable should be initialized.
*
*   COUNT_ELEMENTS_IN_LIST(struct bar, list, cnt);
*/
#define COUNT_ELEMENTS_IN_LIST(data_type_list, list_ptr, amount)\
if (list_ptr != NULL)\
{\
    data_type_list * __temp_list_ptr = list_ptr;\
    while (__temp_list_ptr != NULL)\
    {\
        amount++;\
        __temp_list_ptr = __temp_list_ptr->next;\
    }\
}

/*! \def COUNT_ELEMENTS_IN_LIST2(data_type_list, list_ptr, amount, goto_error)
*   \brief Count elements in list.
*   \param data_type_list A list structure.
*   \param list_ptr A pointer to the list.
*   \param amount A amount of items in list, amount variable should be initialized.
*   \param goto_error A labelto where program will jump if list is NULL
*
*   COUNT_ELEMENTS_IN_LIST2(struct bar, list, cnt);
*/
#define COUNT_ELEMENTS_IN_LIST2(data_type_list, list_ptr, amount, goto_error)\
if (list_ptr == NULL) \
{ \
    goto goto_error; \
} \
data_type_list * temp_list_ptr = list_ptr; \
while (temp_list_ptr != NULL) \
{ \
    amount++; \
    temp_list_ptr = temp_list_ptr->next; \
}

//inside brackets use __it as an iterator pointer, require additional sizeof(type) in stack
/*! \def ITERATE_BLOCK(type, list_ptr, __it)
*   \brief Iterate list block from start.
*   \param type A list structure.
*   \param list_ptr A pointer to the list.
*   \param __it An instance of the item.
*
*   ITERATE_BLOCK(type, list_ptr, __it){}
*/
#define ITERATE_BLOCK(type, list_ptr, __it)\
type __temp; \
type * __it = &__temp; \
__temp.next = list_ptr; \
for ( ; __it ; __it = __it->next )

/*! \def ITERATE_BLOCK_DIRECT(type, list_ptr, __it)
*   \brief Iterate list block.
*   \param type A list structure.
*   \param list_ptr A pointer to the list.
*   \param __it An instance of the item.
*
*   ITERATE_BLOCK_DIRECT(type, list_ptr, __it){}
*/
#define ITERATE_BLOCK_DIRECT(type, list_ptr, __it) type * __it = list_ptr; \
											for ( ; __it ; __it = __it->next )

/* MANUAL ITERATION */
/*! \def ITERATE_BLOCK_DIRECT_MAN(type, list_ptr, __it)
*   \brief Iterate list block manually.
*   \param type A list structure.
*   \param list_ptr A pointer to the list.
*   \param __it An instance of the item.
*
*   ITERATE_BLOCK_DIRECT(type, list_ptr, __it){}
*/
#define ITERATE_BLOCK_DIRECT_MAN(type, list_ptr, __it)\
type * __it = list_ptr; \
while ( __it != NULL )

/*! \def ITERATE_NEXT(type, list_ptr, __it)
*   \brief Move to the next item.
*   \param type A list structure.
*   \param list_ptr A pointer to the list.
*   \param __it An instance of the item.
*
*   ITERATE_NEXT(__it);
*/
#define ITERATE_NEXT(__it) __it = __it->next;

/*-MANUAL ITERATION-END*/

//----------function_return
#define F_OK 0
#define F_FAIL 1

//----------PROTOCOL STD
#define P_RES_OK 0
#define	P_RES_FAILED 1

#define htons(a)            ((((a)>>8)&0xff)|(((a)<<8)&0xff00))
#define ntohs(a)			htons(a)

#define htonl(a)			( (((a)>>24)&0xff) | (((a)>>8)&0xff00) |\
								(((a)<<8)&0xff0000) | (((a)<<24)&0xff000000) )
#define ntohl(a)			htonl(a)

#define inet_addr(a,b,c,d)	( ((uint32_t)a) | ((uint32_t)b << 8) |\
							  ((uint32_t)c << 16) | ((uint32_t)d << 24) )


#endif
