/*-
 *  ATTENTION!: the initial version of the DHCP logic was taken from:
 *		link:http://easyelectronics.ru/files/WE/redsh/eth_232.zip
 *		link2:http://we.easyelectronics.ru/electro-and-pc/podklyuchenie-mikrokontrollera-k-lokalnoy-seti-udp-server.html
 *		License: WTFPL
 *
 *	--------APPLICABLE ONLY FOR DSC code, not ENC28J60 logic/routnies------
 *	Copyright (c) 2014-2016	Alexander Morozov. NiXD ORG
 *	All rights reserved.
 *
 * LICENSE: This source file is subject to the New BSD license that is
 * available through the world-wide-web at the following URI:
 * http://www.opensource.org/licenses/bsd-license.php or in the root folder
 * of the project in file LICENSE.TXT.
 */

#include "../../build_defs.h"
#include <avr/io.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include "../../hal.h"
#include "./hal_net_stack.h"
#include "./hal_net_stack_func.h"

#if (WITH_UDP == YES)

uint16_t udp_cksum(struct udp_checksum_pseudo * checks, uint8_t *buf, uint16_t len);


uint16_t udp_cksum(struct udp_checksum_pseudo * checks, uint8_t *buf, uint16_t len)
{
	uint32_t sum = 0;
	struct temp
	{
		uint16_t ipf1;
		uint16_t ipf2;
		uint16_t ipt1;
		uint16_t ipt2;
		uint16_t proto;
		uint16_t len;
	};
	struct temp * tmp = (struct temp *) checks;

	sum += tmp->ipf1;
	sum += tmp->ipf2; //source
	sum += tmp->ipt1;
	sum += tmp->ipt2; //dest
	sum += tmp->proto;//tmp->proto); //reserver+proto
	sum += tmp->len; //length
    // Рассчитываем сумму word'ов блока
    while(len >= 2)
    {
        sum += *(uint16_t *)buf;//ntohs(*(uint16_t *)buf);
        buf += 2;
        len -= 2;
    }

    if(len)
	{
        sum += htons(((uint16_t) ((*buf) << 8) & 0xFF00));
	}
    // Складываем старший и младший word суммы
    // пока не получим число, влезающее в word
    while(sum >> 16)
        sum = (sum & 0xffff) + (sum >> 16);

    //Берём дополнение до единицы
    return ~((uint16_t)sum);
}

// send UDP packet
// fields must be set:
//	- ip.dst
//	- udp.src_port
//	- udp.dst_port
// uint16_t len is UDP data payload length
int32_t
udp_send(struct knetdevice * knd,
		 uint32_t rIP,
		 uint16_t rPORT,
		 uint16_t lPORT,
		 eth_frame_t *frame,
		 uint16_t len
		)
{
	ip_packet_t *ip = (void*)(frame->data);
	udp_packet_t *udp = (void*)(ip->data);

	len += sizeof(udp_packet_t);

	ip->protocol = IP_PROTOCOL_UDP;
	ip->from_addr = knd->nic_set.ipv4;
	ip->to_addr = rIP;

	udp->to_port = htons(rPORT);
	udp->from_port = htons(lPORT);
	udp->len = htons(len);
	udp->cksum = 0;

	struct udp_checksum_pseudo checks;
	checks.from_addr = ip->from_addr;
	checks.to_addr = ip->to_addr;
	checks.reserved = 0;
	checks.len = htons(len);
	checks.protocol = ip->protocol;

	udp->cksum = udp_cksum(&checks, (uint8_t*)udp, len);

	return ip_send(knd, frame, len);
}

int32_t udp_send_internal(struct knetdevice * knd, eth_frame_t *frame, uint16_t len)
{
	ip_packet_t *ip = (void*)(frame->data);
	udp_packet_t *udp = (void*)(ip->data);

	len += sizeof(udp_packet_t);

	ip->protocol = IP_PROTOCOL_UDP;
	ip->from_addr = knd->nic_set.ipv4;

	udp->len = htons(len);
	udp->cksum = 0;
	udp->cksum = ip_cksum(len + IP_PROTOCOL_UDP, (uint8_t*)udp-8, len+8);

	return ip_send(knd, frame, len);
}

// reply to UDP packet
// len is UDP data payload length
int32_t udp_reply(struct knetdevice * knd, eth_frame_t *frame, uint16_t len)
{
	ip_packet_t *ip = (void*)(frame->data);
	udp_packet_t *udp = (void*)(ip->data);
	uint16_t temp;

	len += sizeof(udp_packet_t);

	ip->to_addr = knd->nic_set.ipv4;

	temp = udp->from_port;
	udp->from_port = udp->to_port;
	udp->to_port = temp;

	udp->len = htons(len);

	udp->cksum = 0;
	udp->cksum = ip_cksum(len + IP_PROTOCOL_UDP,
		(uint8_t*)udp-8, len+8);

	return ip_reply(knd, frame, len);
}

// process UDP packet
/*void udp_filter(eth_frame_t *frame, uint16_t len)
{
	ip_packet_t *ip = (void*)(frame->data);
	udp_packet_t *udp = (void*)(ip->data);

	if(len >= sizeof(udp_packet_t))
	{
		len = ntohs(udp->len) - sizeof(udp_packet_t);

		switch(udp->to_port)
		{
#if WITH_DHCP == YES
		case DHCP_CLIENT_PORT:
			dhcp_filter(frame, len);
			break;
#endif
		default:
			//udp_packet(frame, len);
			break;
		}
	}
}*/

#endif
