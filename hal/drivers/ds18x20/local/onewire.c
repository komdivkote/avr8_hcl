/* -----ORIGINAL HEADERS----------
Access Dallas 1-Wire Devices with ATMEL AVRs
Author of the initial code: Peter Dannegger (danni(at)specs.de)
modified by Martin Thomas (mthomas(at)rhrk.uni-kl.de)
modified by Alexander Morozov (alex@nixd.org)
 9/2004 - use of delay.h, optional bus configuration at runtime
10/2009 - additional delay in ow_bit_io for recovery
 5/2010 - timing modifcations, additonal config-values and comments,
          use of atomic.h macros, internal pull-up support
 7/2010 - added method to skip recovery time after last bit transfered
          via ow_command_skip_last_recovery
*/

#include "../../../build_defs.h"
#include <avr/io.h>
#include <stdint.h>
#include <stdbool.h>
#include <util/delay.h>
#include "../../../hal.h"
#include "onewire.h"

#include "../../../../os/kernel/kernel_process.h"
#include "../../../../os/kernel/kernel_cs.h"

static struct ow_bus_rec s_ow_bus;

/*#ifdef OW_ONE_BUS

#define OW_GET_IN()   ( OW_IN & (1<<OW_PIN))
#define OW_OUT_LOW()  ( OW_OUT &= (~(1 << OW_PIN)) )
#define OW_OUT_HIGH() ( OW_OUT |= (1 << OW_PIN) )
#define OW_DIR_IN()   ( OW_DDR &= (~(1 << OW_PIN )) )
#define OW_DIR_OUT()  ( OW_DDR |= (1 << OW_PIN) )

#else*/

/* set bus-config with ow_set_bus() */
/*uint8_t OW_PIN_MASK;
volatile uint8_t* OW_IN;
volatile uint8_t* OW_OUT;
volatile uint8_t* OW_DDR;*/

#define OW_GET_IN(_BUS_)   ( _SFR_IO8(_BUS_->ow_in) & _BUS_->ow_pin )
#define OW_OUT_LOW(_BUS_)  ( _SFR_IO8(_BUS_->ow_port) &= ~_BUS_->ow_pin )
#define OW_OUT_HIGH(_BUS_) ( _SFR_IO8(_BUS_->ow_port) |=  _BUS_->ow_pin )
#define OW_DIR_IN(_BUS_)   ( _SFR_IO8(_BUS_->ow_ddr) &= ~_BUS_->ow_pin )
#define OW_DIR_OUT(_BUS_)  ( _SFR_IO8(_BUS_->ow_ddr) |=  _BUS_->ow_pin )

/*void ow_set_bus(uint16_t pin, uint16_t port, uint16_t ddr, uint8_t pin_mask)
{
	s_ow_bus.ow_ddr = ddr;
	s_ow_bus.ow_port = port;
	s_ow_bus.ow_in = pin;
	s_ow_bus.ow_pin = pin_mask;
	ow_reset();
}*/

//#endif

uint8_t ow_input_pin_state(struct ow_bus_rec * owbr)
{
	return OW_GET_IN(owbr);
}

void ow_parasite_enable(struct ow_bus_rec * owbr)
{
	OW_OUT_HIGH(owbr);
	OW_DIR_OUT(owbr);
}

void ow_parasite_disable(struct ow_bus_rec * owbr)
{
	OW_DIR_IN(owbr);
#if (!OW_USE_INTERNAL_PULLUP)
	OW_OUT_LOW(owbr);
#endif
}

uint8_t ow_reset(struct ow_bus_rec * owbr)
{
    //prevent swith to othe processes
 //   kernel_proc_enter_cooperative_mode();

	uint8_t err;

    CRITICAL_SECTION()
	{
        OW_OUT_LOW(owbr);
        OW_DIR_OUT(owbr);            // pull OW-Pin low for 480us
        _delay_us(480);


		// set Pin as input - wait for clients to pull low
		OW_DIR_IN(owbr); // input
#if OW_USE_INTERNAL_PULLUP
		OW_OUT_HIGH();
#endif

		_delay_us(64);       // was 66 64
		err = OW_GET_IN(owbr);   // no presence detect
		                     // if err!=0: nobody pulled to low, still high


        // after a delay the clients should release the line
        // and input-pin gets back to high by pull-up-resistor
        _delay_us(480 - 64);       // was 480-66
        if( OW_GET_IN(owbr) == 0 ) {
            err = 1;             // short circuit, expected low but got high
        }

	}

   // kernel_proc_leave_cooperative_mode();

	return err;
}


/* Timing issue when using runtime-bus-selection (!OW_ONE_BUS):
   The master should sample at the end of the 15-slot after initiating
   the read-time-slot. The variable bus-settings need more
   cycles than the constant ones so the delays had to be shortened
   to achive a 15uS overall delay
   Setting/clearing a bit in I/O Register needs 1 cyle in OW_ONE_BUS
   but around 14 cyles in configureable bus (us-Delay is 4 cyles per uS) */
static uint8_t ow_bit_io_intern(struct ow_bus_rec * owbr, uint8_t b, uint8_t with_parasite_enable )
{
    //prevent swith to othe processes
    //kernel_proc_enter_cooperative_mode();

    //enter CRITICAL_SECTION to avoid other ISR
	CRITICAL_SECTION()
	{
#if OW_USE_INTERNAL_PULLUP
		OW_OUT_LOW();
#endif
		OW_DIR_OUT(owbr);    // drive bus low
		_delay_us(2);    // T_INT > 1usec accoding to timing-diagramm
		if ( b )
        {
			OW_DIR_IN(owbr); // to write "1" release bus, resistor pulls high
#if OW_USE_INTERNAL_PULLUP
			OW_OUT_HIGH();
#endif
		}

		// "Output data from the DS18B20 is valid for 15usec after the falling
		// edge that initiated the read time slot. Therefore, the master must
		// release the bus and then sample the bus state within 15ussec from
		// the start of the slot."
		_delay_us(15-2-OW_CONF_DELAYOFFSET);

		if( OW_GET_IN(owbr) == 0 ) {
			b = 0;  // sample at end of read-timeslot
		}

		_delay_us(60-15-2+OW_CONF_DELAYOFFSET);
#if OW_USE_INTERNAL_PULLUP
		OW_OUT_HIGH();
#endif
		OW_DIR_IN(owbr);

		if ( with_parasite_enable ) {
			ow_parasite_enable(owbr);
		}

	} /* CRITICAL_SECTION */

   // kernel_proc_leave_cooperative_mode();

	_delay_us(OW_RECOVERY_TIME); // may be increased for longer wires


	return b;
}

uint8_t ow_bit_io(struct ow_bus_rec * owbr, uint8_t b )
{
	return ow_bit_io_intern(owbr, b & 1, 0 );
}

uint8_t ow_byte_wr(struct ow_bus_rec * owbr, uint8_t b )
{
	uint8_t i = 8, j;

	do {
		j = ow_bit_io(owbr, b & 1 );
		b >>= 1;
		if( j ) {
			b |= 0x80;
		}
	} while( --i );

	return b;
}

uint8_t ow_byte_wr_with_parasite_enable(struct ow_bus_rec * owbr, uint8_t b )
{
	uint8_t i = 8, j;

	do {
		if ( i != 1 ) {
			j = ow_bit_io_intern(owbr, b & 1, 0 );
		} else {
			j = ow_bit_io_intern(owbr, b & 1, 1 );
		}
		b >>= 1;
		if( j ) {
			b |= 0x80;
		}
	} while( --i );

	return b;
}


uint8_t ow_byte_rd(struct ow_bus_rec * owbr )
{
	// read by sending only "1"s, so bus gets released
	// after the init low-pulse in every slot
	return ow_byte_wr(owbr, 0xFF );
}


uint8_t ow_rom_search(struct ow_bus_rec * owbr, uint8_t diff, uint8_t *id )
{
	uint8_t i, j, next_diff;
	uint8_t b;

	if( ow_reset(owbr) ) {
		return OW_PRESENCE_ERR;         // error, no device found <--- early exit!
	}

	ow_byte_wr( owbr, OW_SEARCH_ROM );        // ROM search command
	next_diff = OW_LAST_DEVICE;         // unchanged on last device

	i = OW_ROMCODE_SIZE * 8;            // 8 bytes

	do {
		j = 8;                          // 8 bits
		do {
			b = ow_bit_io(owbr, 1 );         // read bit
			if( ow_bit_io(owbr, 1 ) ) {      // read complement bit
				if( b ) {               // 0b11
					return OW_DATA_ERR; // data error <--- early exit!
				}
			}
			else {
				if( !b ) {              // 0b00 = 2 devices
					if( diff > i || ((*id & 1) && diff != i) ) {
						b = 1;          // now 1
						next_diff = i;  // next pass 0
					}
				}
			}
			ow_bit_io(owbr, b );             // write bit
			*id >>= 1;
			if( b ) {
				*id |= 0x80;            // store bit
			}

			i--;

		} while( --j );

		id++;                           // next byte

	} while( i );

	return next_diff;                   // to continue search
}


static void ow_command_intern(struct ow_bus_rec * owbr, uint8_t command, uint8_t *id, uint8_t with_parasite_enable )
{
	uint8_t i;

	ow_reset(owbr);

	if( id ) {
		ow_byte_wr(owbr, OW_MATCH_ROM );     // to a single device
		i = OW_ROMCODE_SIZE;
		do {
			ow_byte_wr(owbr, *id );
			id++;
		} while( --i );
	}
	else {
		ow_byte_wr(owbr, OW_SKIP_ROM );      // to all devices
	}

	if ( with_parasite_enable  ) {
		ow_byte_wr_with_parasite_enable(owbr, command );
	} else {
		ow_byte_wr(owbr, command );
	}
}

void ow_command(struct ow_bus_rec * owbr, uint8_t command, uint8_t *id )
{
	ow_command_intern(owbr, command, id, 0);
}

void ow_command_with_parasite_enable(struct ow_bus_rec * owbr, uint8_t command, uint8_t *id )
{
	ow_command_intern(owbr, command, id, 1 );
}

