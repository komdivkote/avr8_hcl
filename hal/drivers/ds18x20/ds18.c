/*-
 *	Copyright (c) 2014-2016	Alexander Morozov. www.nixd.org
 *	All rights reserved.
 *
 * LICENSE: This source file is subject to the New BSD license that is
 * available through the world-wide-web at the following URI:
 * http://www.opensource.org/licenses/bsd-license.php or in the root folder
 * of the project in file LICENSE.TXT.
 */

#include "../../build_defs.h"

#if BUILD_WITH_DS18X20 == YES
#include <avr/io.h>
#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stdint.h>
#include <string.h>
#include <avr/pgmspace.h>
#include <avr/interrupt.h>
#include <util/delay_basic.h>
#include <util/delay.h>
#include "../../hal.h"
//#include "local/onewire.h"
#include "ds18.h"
#include "local/ds18x20.h"

#include "../../../os/kernel/kernel_process.h"
#include "../../../os/kernel/kernel_mutex.h"
#include "../../../os/kernel/kernel_memory.h"

static int8_t ds18x20_attach(struct dev_descr_node * ddn, struct ds18_setup * dsup);
static int8_t ds18x20_query(struct dev_descr_node * ddn, struct hal_query_info * data0);
static int8_t ds18x20_detach(struct dev_descr_node * ddn);
static int8_t ds18x20_update(struct dev_descr_node * ddn, struct ds18_setup * ios);
static int8_t ds18x20_dump(struct dev_descr_node * ddn, void * data0, size_t * dsize0);
static int8_t ds18x20_restore(struct dev_descr_node * ddn, void * data0, size_t * dsize0);
static struct ds18_bus_dev * ds18_bus_find_dev(struct ds18_bus * bus, ds18_sens_id sid);

volatile ds18_id buses_count = 0;

int8_t
ds18x20_hal_message(struct dev_descr_node * ddn,
                  enum dd_event e_event,
                  void * data0,
                  size_t * dsize0)
{
    int8_t ret = 0;

    switch (e_event)
    {
        case DD_EVENT_ATTACH:
            ret = ds18x20_attach(ddn, (struct ds18_setup *) data0);
        break;

        case DD_EVENT_DETACH:
            ret = ds18x20_detach(ddn);
        break;

        case DD_EVENT_UPDATE:
            ret = ds18x20_update(ddn, (struct ds18_setup *) data0);
        break;

        case DD_DUMP:
            ret = ds18x20_dump(ddn, data0, dsize0);
        break;

        case DD_RESTORE:
            ret = ds18x20_restore(ddn, data0, dsize0);
        break;

        case DD_QUERY:
            ret = ds18x20_query(ddn, (struct hal_query_info *) data0);
        break;

        case DD_ISRSIGNAL:
            ret = K_MSG_ERROR;
        break;

        case DD_RESET:
            ret = K_MSG_ERROR;
        break;

        case DD_DROP:
            ret = K_MSG_ERROR;
        break;

        case DD_FLUSH:
            ret = K_MSG_ERROR;
        break;
    }

    return ret;
}

int8_t
ds18x20_kstream_init(struct dev_descr_node * ddn, struct kstream * ks)
{
    setSysError(SYSTEM_OPERATION_NOT_SUPPORTED);
    return K_FAIL_STREAM;
}

int8_t
ds18x20_stream_init(struct dev_descr_node * ddn, FILE * file)
{
    setSysError(SYSTEM_OPERATION_NOT_SUPPORTED);
    return K_FAIL_STREAM;
}

static int8_t
ds18x20_attach(struct dev_descr_node * ddn, struct ds18_setup * dsup)
{
    ASSERT_NULL_ER(dsup, SYSTEM_NULL_ARGUMENT, K_NO_DD);

    uint8_t ret = hal_hw_bind_pin(ddn->dev_id, dsup->e_port, dsup->pin_mask);
    if (ret != 0)
    {
        return K_NO_DD;
    }

    struct ds18_bus * bus = (struct ds18_bus *) kernel_malloc(sizeof(struct ds18_bus));
    ASSERT_NULL_ER(bus, SYSTEM_MALLOC_FAILED, K_NO_DD);

    bus->mux = kernel_mutex_init();
    bus->e_port         = dsup->e_port;
    /*bus->pin_mask       = dsup->pin_mask;
    bus->ddr            = hal_hw_get_addr_ddr(bus->e_port);
    bus->port           = hal_hw_get_addr_port(bus->e_port);
    bus->pin            = hal_hw_get_addr_pin(bus->e_port);*/
    bus->ow_rec.ow_ddr  = hal_hw_get_addr_ddr(bus->e_port);
    bus->ow_rec.ow_port = hal_hw_get_addr_pin(bus->e_port);
    bus->ow_rec.ow_in   = hal_hw_get_addr_pin(bus->e_port);
    bus->ow_rec.ow_pin  = dsup->pin_mask;
    bus->ow_bus         = NULL;
    bus->ow_bus_count   = 0;
    bus->id             = ddn->dev_id;
    ddn->instance = (void*) bus;
    ++buses_count;

    ds18_search_sensors(bus);

    return 0;
}

static int8_t
ds18x20_query(struct dev_descr_node * ddn, struct hal_query_info * data0)
{
    FILE * strm = (FILE *)data0->data0;
#if DS18X20_BUILD_WITH_TEXT_QUERY == YES

    fprintf_P(strm, PSTR("DS18X20 REVISION: %u\r\n"), DS18_REVISION);
    struct ds18_bus * ds = (struct ds18_bus *) ddn->instance;
    if (ds == NULL)
    {
        fprintf_P(strm, PSTR("Internal Error!ds18_bus record missing!\r\n"));
    }
    else
    {
        fprintf_P(strm, PSTR("BUS ID: %d\r\n"), ds->id);
        fprintf_P(strm, PSTR("Sensors on bus: %d\r\n"), ds->ow_bus_count);
        fprintf_P(strm, PSTR("Port ID: %d mask: %d\r\n"), ds->e_port, ds->ow_rec.ow_pin);
        if (ds->ow_bus == NULL)
        {
            fprintf_P(strm, PSTR("NO SENSORS ON BUS REGISTRED\r\n"));
        }
        else
        {
            ITERATE_BLOCK_DIRECT_MAN(struct ds18_bus_dev, ds->ow_bus, __it_ow)
            {
                char temp[10];
                fprintf_P(strm, PSTR("-> ID: %u ROM:%x:%x:%x:%x:%x:%x:%x:%x"), __it_ow->sid, __it_ow->rom_code[0], __it_ow->rom_code[1],
                        __it_ow->rom_code[2], __it_ow->rom_code[3], __it_ow->rom_code[4], __it_ow->rom_code[5],
                        __it_ow->rom_code[6], __it_ow->rom_code[7]);
                fprintf_P(strm, PSTR(" flags:%d decels:%lu "), __it_ow->flags, __it_ow->result);

                DS18X20_format_from_maxres(__it_ow->result, temp, 10);
                fprintf_P(strm, PSTR("%s\r\n"), temp);

                ITERATE_NEXT(__it_ow);
            }
        }

        fprintf_P(strm, PSTR("\r\nMAX SENSORS ALLOWED:%d CODE SIZE: %d\r\n"), DS18_MAXSENSORS_ON_BUS, OW_ROMCODE_SIZE);
    }

    return 0;
#else
    setSysError(SYSTEM_OPERATION_NOT_SUPPORTED);
    fprintf_P(strm, PSTR("DS18X20 was built without query! \r\n"));
    return K_MSG_ERROR;
#endif // IO_BUILD_WITH_TEXT_QUERY

}

static int8_t
ds18x20_detach(struct dev_descr_node * ddn)
{
    struct ds18_bus * bus = (struct ds18_bus *) ddn->instance;

    ITERATE_BLOCK_DIRECT_MAN(struct ds18_bus_dev, bus->ow_bus, _it)
    {
        struct ds18_bus_dev * sens = _it;
        ITERATE_NEXT(_it);
        kernel_free((void*) sens);
    }

    kernel_free((void*) bus->mux);
    kernel_free((void*) bus);
    ddn->instance = NULL;

    hal_hw_unbind_dd_all_pin(ddn->dev_id);
    --buses_count;

    return 0;
}

static int8_t
ds18x20_update(struct dev_descr_node * ddn, struct ds18_setup * ios)
{
    //rescan bus
    int8_t ret = ds18_rescan_bus((struct ds18_bus *)ddn->instance);
    return (ret != -1) ? 0 : K_NO_DD;
}

static int8_t
ds18x20_dump(struct dev_descr_node * ddn, void * data0, size_t * dsize0)
{
    //dump only location of the bus, don't store sensors
    struct ds18_setup * dsset = (struct ds18_setup *) data0;
    struct ds18_bus * bus = (struct ds18_bus *) ddn->instance;

    dsset->e_port = bus->e_port;
    dsset->pin_mask = bus->ow_rec.ow_pin;

    return 0;
}

static int8_t
ds18x20_restore(struct dev_descr_node * ddn, void * data0, size_t * dsize0)
{
    return ds18x20_attach(ddn, (struct ds18_setup *) data0);
}

static struct ds18_bus_dev *
ds18_bus_find_dev(struct ds18_bus * bus, ds18_sens_id sid)
{
    ITERATE_BLOCK_DIRECT_MAN(struct ds18_bus_dev, bus->ow_bus, _it)
    {
        if (_it->sid == sid)
        {
            return _it;
        }
    }
    setSysError(SYSTEM_ITEM_NOT_FOUND);
    return NULL;
}


//check for the supply type DS18X20_POWER_EXTERN or DS18X20_POWER_PARASITE or -1 if argument is null
int8_t
ds18_probe_dev_supply(struct ds18_bus * ds_bus, ds18_sens_id sens_id)
{
    ASSERT_NEGATIVE_ER(sens_id, SYSTEM_ID_INCORRECT, -1);
    ASSERT_NULL_ER(ds_bus, SYSTEM_NULL_ARGUMENT, -1);

    kernel_mutex_lock(ds_bus->mux);

    struct ds18_bus_dev * dev = ds18_bus_find_dev(ds_bus, sens_id);
    if (dev == NULL)
    {
        kernel_mutex_unlock(ds_bus->mux);
        return -1;
    }

	uint8_t res = DS18X20_ERROR;

	res = DS18X20_OK;
	//setting bus
	//ow_set_bus(ds_bus->pin, ds_bus->port, ds_bus->ddr, ds_bus->pin_mask);
    ow_reset(&ds_bus->ow_rec);

	res = DS18X20_get_power_status(&ds_bus->ow_rec, dev->rom_code);

	kernel_mutex_unlock(ds_bus->mux);

	return res;
}

uint8_t
ds18_read_all_blocking(struct ds18_bus * ds_bus)
{
	uint8_t res = DS18X20_ERROR;

	ASSERT_NULL_ER(ds_bus, SYSTEM_NULL_ARGUMENT, 1);
	ASSERT_NULL_ER(ds_bus->ow_bus, SYSTEM_LIST_EMPTY, 1);

    kernel_mutex_lock(ds_bus->mux);

	res = DS18X20_OK;
	//setting bus
	//ow_set_bus(ds_bus->pin, ds_bus->port, ds_bus->ddr, ds_bus->pin_mask);
    ow_reset(&ds_bus->ow_rec);

	//initiating reading

	uint8_t st_res = DS18X20_start_meas(&ds_bus->ow_rec,
                    (ds_bus->ow_bus->flags & DS18_FLAG_PARASITE) ? DS18X20_POWER_PARASITE : DS18X20_POWER_EXTERN, NULL);

	if (st_res != DS18X20_START_FAIL)
	{

		while (DS18X20_conversion_in_progress(&ds_bus->ow_rec) == DS18X20_CONVERTING)
		{
			//conversion still in progress TODO: maybe add ui callback??
			//kernel_process_usleep(2000, 0); //5ms sleep
		}

		ITERATE_BLOCK_DIRECT_MAN(struct ds18_bus_dev, ds_bus->ow_bus, __it)
		{
			if (__it->flags & DS18_FLAG_ONLINE)
			{
				res = DS18X20_read_maxres(&ds_bus->ow_rec, __it->rom_code, &__it->result);
				ASSERT_DATA(res == DS18X20_ERROR, __it->flags |= DS18_FLAG_ERROR);
			}

			ITERATE_NEXT(__it);
		}

	}

    kernel_mutex_unlock(ds_bus->mux);

	return DS18X20_OK;
}

//synchronous function
uint8_t
ds18_read_blocking(struct ds18_bus * ds_bus, ds18_sens_id sens_id, uint32_t * decel)
{
	uint8_t res = DS18X20_ERROR;

	ASSERT_NULL_ER(ds_bus, SYSTEM_NULL_ARGUMENT, res);

    struct ds18_bus_dev * dev = ds18_bus_find_dev(ds_bus, sens_id);
    if (dev == NULL)
    {
        return res;
    }

	if (!(dev->flags & DS18_FLAG_ONLINE))
    {
        setSysError(SYSTEM_DEVICE_OFFLINE);
        return res;
    }

    MUTEX_BLOCK(ds_bus->mux, MUTEX_ACTION_MANUAL)
    {

        res = DS18X20_OK;
        //setting bus
        //ow_set_bus(ds_bus->pin, ds_bus->port, ds_bus->ddr, ds_bus->pin_mask);
        ow_reset(&ds_bus->ow_rec);

        //initiating reading

        uint8_t st_res = DS18X20_start_meas(&ds_bus->ow_rec, (dev->flags & DS18_FLAG_PARASITE) ? DS18X20_POWER_PARASITE : DS18X20_POWER_EXTERN, dev->rom_code);

        if (st_res != DS18X20_START_FAIL)
        {

            while (DS18X20_conversion_in_progress(&ds_bus->ow_rec) == DS18X20_CONVERTING)
            {
                //conversion still in progress TODO: maybe add ui callback??
               // kernel_process_usleep(2000, 0); //5ms sleep
            }

            //DS18X20_read_decicelsius(ds_bus->rom_code, &ds_bus->result);
            res = DS18X20_read_maxres(&ds_bus->ow_rec, dev->rom_code, &dev->result);
            if (decel != NULL)
            {
                *decel = dev->result;
            }
            ASSERT_DATA(res == DS18X20_ERROR, dev->flags |= DS18_FLAG_ERROR); //set error flag

            //DS18X20_format_from_decicelsius(gSensorStr[i].temp, gSensorStr[i].ctemp, 7);
            //ASSERT_DATA(s_temp_size <= 9, s_temp_size = 10);
            //res = DS18X20_format_from_maxres(dev->result, s_temp, s_temp_size);

        }
        else
        {
            setSysError(SYSTEM_DEVICE_EXTERNAL_ERROR);
        }

    }

	return res;

}

int8_t
ds18_search_sensors(struct ds18_bus * ds_bus)
{
	ASSERT_NULL_ER(ds_bus, SYSTEM_NULL_ARGUMENT, -1);

	uint8_t i;
	uint8_t id[OW_ROMCODE_SIZE];
	uint8_t diff, nSensors;
    struct ds18_bus_dev * dss = NULL;
	//printf_P(PSTR("Scanning Bus for DS18X20:\r\n") );
	kernel_mutex_lock(ds_bus->mux);
        //setting bus
        //ow_set_bus(ds_bus->pin, ds_bus->port, ds_bus->ddr, ds_bus->pin_mask);
        ow_reset(&ds_bus->ow_rec);

        //ow_reset();

        nSensors = 0;

        diff = OW_SEARCH_FIRST;
        while ( diff != OW_LAST_DEVICE && nSensors < DS18_MAXSENSORS_ON_BUS ) {
            DS18X20_find_sensor(&ds_bus->ow_rec, &diff, &id[0] );

            if( diff == OW_PRESENCE_ERR ) {
                //printf_P( PSTR("No Sensor found\r\n") );
                break;
            }

            if( diff == OW_DATA_ERR ) {
                //printf_P( PSTR("Bus Error\r\n") );
                break;
            }
            //printf_P( PSTR("Found: %i 0x"), nSensors);
            //successfully found
            dss = (struct ds18_bus_dev *) kernel_malloc(sizeof(struct ds18_bus_dev));
            //ASSERT_NULL_ER(dss, SYSTEM_MALLOC_FAILED, -1);
            if (dss == NULL)
            {
                setSysError(SYSTEM_MALLOC_FAILED);
                continue;
            }


            dss->sid = nSensors;
            dss->result = 0;
            dss->flags = DS18_FLAG_ONLINE;
            dss->next = NULL;

            for ( i=0; i < OW_ROMCODE_SIZE; i++ )
            {
                dss->rom_code[i] = id[i];
                //printf("%X", id[i]);
            }

            if ( DS18X20_get_power_status( &ds_bus->ow_rec, id ) == DS18X20_POWER_PARASITE )
            {
                dss->flags |= DS18_FLAG_PARASITE;
            }
            else
            {
                dss->flags &= ~(DS18_FLAG_PARASITE);
            }

            switch (dss->rom_code[0])
            {
                case DS18S20_FAMILY_CODE:
                    dss->flags |= DS18_FLAG_FAM_DS18S20;
                break;

                case DS1822_FAMILY_CODE:
                    dss->flags |= DS18_FLAG_FAM_DS1822;
                break;

                case DS18B20_FAMILY_CODE:
                    dss->flags |= DS18_FLAG_FAM_DS18B20;
                break;

                default:
                    setSysError(SYSTEM_DEVICE_NOT_COMPATIABLE);
                    dss->flags &= ~DS18_FLAG_FAM_DS18S20 & ~DS18_FLAG_FAM_DS1822 & ~DS18_FLAG_FAM_DS18B20;
                break;
            }

            ADD_TO_LIST(struct ds18_bus_dev, ds_bus->ow_bus, dss);

            nSensors++;
            dss = NULL;
            //printf_P(PSTR("\r\n"));
        }


        ds_bus->ow_bus_count = nSensors;
        kernel_mutex_unlock(ds_bus->mux);
        return nSensors;
}

int8_t
ds18_rescan_bus(struct ds18_bus * ds_bus)
{
	ASSERT_NULL_ER(ds_bus, SYSTEM_NULL_ARGUMENT, -1);

	uint8_t i;
	uint8_t id[OW_ROMCODE_SIZE];
	uint8_t diff, nSensors;

	if (ds_bus->ow_bus == NULL)
	{
		//call function which will fill the list
		return ds18_search_sensors(ds_bus);
	}

    kernel_mutex_lock(ds_bus->mux);

        ITERATE_BLOCK_DIRECT_MAN(struct ds18_bus_dev, ds_bus->ow_bus, it_d)
        {
            it_d->sid = -1;
            ITERATE_NEXT(it_d);
        }

        //printf_P(PSTR("Scanning Bus for DS18X20:\r\n") );
        //setting bus
        //ow_set_bus(ds_bus->pin, ds_bus->port, ds_bus->ddr, ds_bus->pin_mask);
        ow_reset(&ds_bus->ow_rec);
        //ow_reset();

        nSensors = 0;

        diff = OW_SEARCH_FIRST;
        while ( diff != OW_LAST_DEVICE && nSensors < DS18_MAXSENSORS_ON_BUS ) {
            DS18X20_find_sensor( &ds_bus->ow_rec, &diff, &id[0] );

            if( diff == OW_PRESENCE_ERR ) {
                //printf_P( PSTR("No Sensor found\r\n") );
                break;
            }

            if( diff == OW_DATA_ERR ) {
                //printf_P( PSTR("Bus Error\r\n") );
                break;
            }
           // printf_P( PSTR("Found: %i 0x"), nSensors);
            //successfully found
            //check if already present
            SEARCH_IN_LIST_3(struct ds18_bus_dev, ds_bus->ow_bus, ds_sens, strcmp(ds_sens->rom_code, id) == 0);
            if (ds_sens != NULL)
            {
                ds_sens->sid = nSensors;
                ++nSensors;
                continue;
            }

            //new sensor
            struct ds18_bus_dev * dss = (struct ds18_bus_dev *) kernel_malloc(sizeof(struct ds18_bus_dev));
            if (dss == NULL)
            {
                setSysError(SYSTEM_MALLOC_FAILED);
                continue;
            }
            dss->sid = nSensors;
            dss->result = 0;
            dss->flags = DS18_FLAG_ONLINE;
            dss->next = NULL;
            for ( i=0; i < OW_ROMCODE_SIZE; i++ )
            {
                dss->rom_code[i] = id[i];
                //printf("%X", id[i]);
            }

            if ( DS18X20_get_power_status( &ds_bus->ow_rec, id ) == DS18X20_POWER_PARASITE )
            {
                dss->flags |= DS18_FLAG_PARASITE;
            } else {
                dss->flags &= ~(DS18_FLAG_PARASITE);
            }

            switch (dss->rom_code[0])
            {
                case DS18S20_FAMILY_CODE:
                    dss->flags |= DS18_FLAG_FAM_DS18S20;
                break;

                case DS1822_FAMILY_CODE:
                    dss->flags |= DS18_FLAG_FAM_DS1822;
                break;

                case DS18B20_FAMILY_CODE:
                    dss->flags |= DS18_FLAG_FAM_DS18B20;
                break;

                default:
                    setSysError(SYSTEM_DEVICE_NOT_COMPATIABLE);
                    dss->flags &= ~DS18_FLAG_FAM_DS18S20 & ~DS18_FLAG_FAM_DS1822 & ~DS18_FLAG_FAM_DS18B20;
                break;
            }

            ADD_TO_LIST(struct ds18_bus_dev, ds_bus->ow_bus, dss);

            nSensors++;
            //printf_P(PSTR("\r\n"));

        }

        //remove sensors that left with id -1 (this way will
        REMOVE_FROM_LIST_ALL_MATCH(struct ds18_bus_dev, ds_bus->ow_bus, dev_r, dev_r->sid == -1, kernel_free((void *) dev_r););
//printf_P( PSTR("ret: %i"), nSensors);
        ds_bus->ow_bus_count = nSensors;
        kernel_mutex_unlock(ds_bus->mux);
        return nSensors;

}

int8_t ds18_get_decel(struct ds18_bus * ds_bus, ds18_sens_id sens_id, uint32_t * decel)
{
    uint8_t res = DS18X20_ERROR;

	ASSERT_NULL_ER(ds_bus, SYSTEM_NULL_ARGUMENT, res);
    ASSERT_NULL_ER(decel, SYSTEM_NULL_ARGUMENT, res);

    struct ds18_bus_dev * dev = ds18_bus_find_dev(ds_bus, sens_id);
    if (dev == NULL)
    {
        return res;
    }

    kernel_mutex_lock(ds_bus->mux);
    *decel = dev->result;
    kernel_mutex_unlock(ds_bus->mux);
    res = DS18X20_OK;

    return res;
}
#endif
