/*-
 *	Copyright (c) 2014-2016	Alexander Morozov. www.nixd.org
 *	All rights reserved.
 *
 * LICENSE: This source file is subject to the New BSD license that is
 * available through the world-wide-web at the following URI:
 * http://www.opensource.org/licenses/bsd-license.php or in the root folder
 * of the project in file LICENSE.TXT.
 */

#include <avr/io.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stdbool.h>
#include <string.h>
#include "error.h"
#include "hal_sectioning.h"
#include "hal_private.h"

#include "../os/kernel/kernel_memory.h"
#include "../os/kernel/kernel_process.h"
#include "../os/kernel/kernel_mutex.h"

static t_stream_id sid_ord = 0;
volatile struct hal_stream_list * volatile stream_list_ptr = NULL;

ATTRIBUTE_HAL_SECTION
static int8_t
__hal_register_file(FILE * file, struct dev_descr_node * dest)
{
    NEW_LIST_ITEM_S(struct hal_stream_list, litem);
    ASSERT_NULL_ER(litem, SYSTEM_MALLOC_FAILED, 1);

    litem->sid = sid_ord;
    litem->node = file;
    litem->dest = dest;
    litem->next = NULL;

    kernel_mutex_lock(hal_mux);
        PUSH_TO_LIST(stream_list_ptr, litem);
    kernel_mutex_unlock(hal_mux);

    ++sid_ord;

    return 0;
}

ATTRIBUTE_HAL_SECTION
static int8_t
__hal_unregister_file(FILE * file)
{
    kernel_mutex_lock(hal_mux);
        REMOVE_FROM_LIST_2(struct hal_stream_list, stream_list_ptr, item, item->node == file, not_found);
    kernel_mutex_unlock(hal_mux);

    kernel_free((void *) item);

    return 0;

not_found:
    setSysError(SYSTEM_ITEM_NOT_FOUND);
    return 1;
}

ATTRIBUTE_HAL_SECTION
FILE *
hal_dopen(const struct dev_descr_node * ddest, enum hal_file_perm e_perm, struct knetstate * state)
{
    ASSERT_NULL_ER(ddest, SYSTEM_NULL_ARGUMENT, NULL);

    //check if node support working with std stream
    t_hal_stdstream sfunc = (t_hal_stdstream) pgm_read_word(&ddest->prgm_handlers->STDSTREAM_INIT); //<---- FIX
    ASSERT_NULL_ER(sfunc, SYSTEM_OPERATION_NOT_SUPPORTED, NULL);

    FILE * fl = (FILE*) kernel_malloc(sizeof(FILE));
    ASSERT_NULL_ER(fl, SYSTEM_MALLOC_FAILED, NULL);

    memset(fl, 0, sizeof(FILE));
    fl->flags = __SMALLOC;
    if (state != NULL)
    {
        fl->udata = (void *) state;
    }

    switch (e_perm)
    {
        case FOPEN_R:
            fl->get = 1;
        break;

        case FOPEN_W:
            fl->put = 1;
        break;

        case FOPEN_RW:
            fl->get = 1;
            fl->put = 1;
        break;

        default:
            fl->get = 1;
            fl->put = 1;
        break;
    }

    int8_t ret = sfunc(ddest, fl);

    if (ret != 0)
    {
        //failed to open stream, destroy file
        goto label_fail;
    }

    //file opened, register it
    if (__hal_register_file(fl, ddest) != 0)
    {
        //registration failed
        goto label_fail;
    }

    return fl;

label_fail:
    kernel_free((void*)fl);

    return NULL;
}

ATTRIBUTE_HAL_SECTION
int8_t
hal_dclose(FILE * file)
{
    ASSERT_NULL_ER(file, SYSTEM_NULL_ARGUMENT, 1);

    //unregister file
    __hal_unregister_file(file);

    //close file
    kernel_free((void *) file);

    return 0;
}

ATTRIBUTE_HAL_SECTION
int8_t
hal_dupdate(FILE * file, struct dev_descr_node * ddest)
{

}

ATTRIBUTE_HAL_SECTION
int8_t
hal_ssetext(FILE * file, void * data)
{
    file->udata = data;
    return 0;
}

ATTRIBUTE_HAL_SECTION
int8_t
hal_cpy_stream_mem(t_stream_id offset, struct hal_stream_envelope * env)
{
    ASSERT_NULL_ER(env, SYSTEM_NULL_ARGUMENT, K_NO_DD);
    ASSERT_NEGATIVE_ER(offset, SYSTEM_FD_INVALID, K_NO_DD);

    MUTEX_BLOCK(hal_mux, MUTEX_ACTION_MANUAL)
    {
        volatile struct hal_stream_list * temp_list = stream_list_ptr;
        while ( (offset > 0) && (temp_list != NULL) )
        {
            temp_list = temp_list->next;
            --offset;
        }

        if (temp_list == NULL)
        {
            return 1;
        }
        else
        {
            env->sid = temp_list->sid;
            env->node = temp_list->node;
            env->dest = temp_list->dest;
            return 0;
        }
    }
    return 0;
}

int8_t hal_attach_kstream(FILE * file, struct dev_descr_node * node)
{
    ASSERT_NULL_ER(file, SYSTEM_NULL_ARGUMENT, 1);
    ASSERT_NULL_ER(node, SYSTEM_NULL_ARGUMENT, 1);



    return 0;
}
