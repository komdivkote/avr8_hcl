#ifndef DEV_DS18X20_H_INCLUDED
#define DEV_DS18X20_H_INCLUDED

#define OW_ROMCODE_SIZE 8

#define FOREACH_DS18_CMD(DV)\
	DV(DS18_CMD_SEARCH_ROM, (uint8_t) 1)\
	DV(DS18_CMD_LIST_BUS_ROMS, (uint8_t) 2)\
	DV(DS18_CMD_READ_BUS_SINGLE_ROM, (uint8_t) 3)\
	DV(DS18_CMD_READ_BUS_MULTIPLE, (uint8_t) 4)\
	DV(DS18_CMD_OPER_NOT_SUP, (uint8_t) 0xFF)

enum ds18_commands
{
	FOREACH_DS18_CMD(GENERATE_ENUM_VAL)
};


struct ds18_proto_bus_roms
{
    int8_t id;
    uint8_t flags;
    uint8_t romcode[OW_ROMCODE_SIZE];

    uint8_t next[];
};

struct ds18_proto_meas_single
{
    int8_t id;
};

struct ds18_proto_meas_single_ans
{
    int8_t seid;
    uint32_t decel;
    uint8_t next[];
};

#endif // DEV_DS18X20_H_INCLUDED
