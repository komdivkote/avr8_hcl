/*-
 *	Copyright (c) 2014-2016	Alexander Morozov. www.nixd.org
 *	All rights reserved.
 *
 * LICENSE: This source file is subject to the New BSD license that is
 * available through the world-wide-web at the following URI:
 * http://www.opensource.org/licenses/bsd-license.php or in the root folder
 * of the project in file LICENSE.TXT.
 */

#include "../../../hal/build_defs.h"
#include <avr/io.h>
#include <avr/pgmspace.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include "../../../hal/hal.h"
#include "../../../hal/handlers.h"
#include "../virtual_terminal.h"
#include "../../../lib/conio/conio.h"
#include "../../../lib/conio/kbd.h"
#include "../../../lib/conio/ui.h"
#include "../vt_text.h"
#include "vt_attach.h"
#include "../../../hal/drivers/serial/usart.h"
#include "../../../hal/drivers/io/io.h"
#include "../../../hal/drivers/spi/spi.h"
#include "../../../hal/drivers/adc/adc.h"
#include "../../../hal/drivers/timer/timer.h"

#include "../../../hal/drivers/enc28j60/enc28.h"
#include "../../../hal/drivers/ds18x20/ds18.h"
#include "../../../hal/drivers/annunciator_panel/a_panel.h"
#include "../../../hal/drivers/usart2eth/u2eth.h"
#include "../../../hal/drivers/input_scanner/inpscan.h"

typedef void* (*t_attach)(FILE *, void *);
typedef void* (*t_modify)(FILE *, void *);
typedef void* (*t_info)(FILE *, struct dev_descr_node *);

struct dev_ui_hndlrs
{
    enum dev_descr_code e_dd_code;
    enum dev_desc_handlers e_dh_code;
    t_attach ATTACH;
    t_modify MODIFY;
    t_info INFO;
    uint8_t datasize;
};


static const struct dev_ui_hndlrs *
dev_ui_search(enum dev_descr_code e_dev_code, enum dev_desc_handlers e_dev_hndl);

const struct dev_ui_hndlrs dev_uih[/*DD_HNDLR_COUNT*/] PROGMEM =
{
    {
        DEV_DESCR_USART, ROOT_DRIVER_DEV, vt_attach_usart, vt_attach_usart, vt_info_usart, (sizeof(struct usart_setup_struct) + sizeof(struct usart_hw_setup))
    },
    {
        DEV_DESCR_IO, ROOT_DRIVER_DEV, vt_attach_io, vt_attach_io, vt_info_io, sizeof(struct io_setup)
    },
    {
        DEV_DESCR_SPI, ROOT_DRIVER_DEV, vt_attach_spi, vt_attach_spi, vt_info_spi, sizeof(struct spi_setup)
    },
    {
        DEV_DESCR_SPI, SPI_ENC28J60, vt_attach_spi_enc28j60, vt_attach_spi_enc28j60, vt_info_spi_enc28j60, sizeof(struct enc28_config)
    },
    {
        DEV_DESCR_TIMER, ROOT_DRIVER_DEV, vt_attach_timer, vt_attach_timer, vt_info_timer, sizeof(struct timer_setup)
    },
    {
        DEV_DESCR_ADC, ROOT_DRIVER_DEV, vt_attach_adc, vt_attach_adc, vt_info_adc, sizeof(struct adc_setup)
    },
    {
        DEV_DESCR_OWI, SOFTWARE_OWI_DS18X20, vt_attach_ds18b20, vt_modify_ds18b20, vt_info_ds18b20, sizeof(struct ds18_setup)
    },
    {
        DEV_DESCR_IO, IO_ANNUNCIATOR_PANEL, vt_attach_anonpan, vt_modify_anonpan, vt_info_anonpan, sizeof(struct anon_panel_setup)
    },
    {
        DEV_DESCR_VIRTUAL, U2ETH_ROUTER,  vt_attach_u2eth, vt_modify_u2eth, vt_info_u2eth, 0
    },
    {
        DEV_DESCR_VIRTUAL, IO_DIG_INP_MON, vt_attach_inpscan, vt_modify_inpscan, vt_info_inpscan, sizeof(struct dim_instance_config)
    },
    {
        DEV_DESCR_NULL, ROOT_DRIVER_DEV, NULL, NULL, NULL, 0
    }
};

static const struct dev_ui_hndlrs *
dev_ui_search(enum dev_descr_code e_dev_code, enum dev_desc_handlers e_dev_hndl)
{
    HNDL_CNT i = 0;
    //for (HNDL_CNT i = 0; i < DD_HNDLR_COUNT; i++)
    while(1)
    {
        uint8_t code = pgm_read_byte(&(dev_uih[i].e_dd_code));
        #if DD_HNDLR_COUNT > 255
            uint16_t code2 = pgm_read_word(&(dev_uih[i].e_dh_code));
        #else
            uint8_t code2 = pgm_read_byte(&(dd_hndl_list[i].e_dh_code));
        #endif // DD_HNDLR_COUNT

        if ((code == DEV_DESCR_NULL) && (code2 == ROOT_DRIVER_DEV) )
        {
            fprintf_P(stdout, PSTR("-->NULL\r\n"));
            return NULL;
        }
        else if ( (e_dev_code == code) && (e_dev_hndl == code2) )
        {
            fprintf_P(stdout, PSTR("-->[%p]\r\n"), &dev_uih[i]);
            return &dev_uih[i];
        }
        fprintf_P(stdout, PSTR("-->i=%u %u %u\r\n"), i, code, code2);
        i++;
    }

    return NULL;
}

int8_t
vt_extra_options(FILE * file,
                 struct dev_descr_node * dd)
{
    ASSERT_NULL_ER(dd, SYSTEM_NULL_ARGUMENT, F_FAIL);

    const struct dev_ui_hndlrs * duh = dev_ui_search(dd->e_dev_descr, dd->e_dh_code);
    if (duh == NULL)
    {
        setSysError(SYSTEM_ITEM_NOT_FOUND);
        return F_FAIL;
    }

    t_attach info = (t_hal_message) pgm_read_word(&duh->INFO); //<---- FIX
    return info(file, dd);
};

int8_t
vt_modify_node(FILE * file,
                 struct dev_descr_node * dd)
{
    ASSERT_NULL_ER(dd, SYSTEM_NULL_ARGUMENT, F_FAIL);

    const struct dev_ui_hndlrs * duh = dev_ui_search(dd->e_dev_descr, dd->e_dh_code);
    if (duh == NULL)
    {
        setSysError(SYSTEM_ITEM_NOT_FOUND);
        return F_FAIL;
    }

    t_attach modify = (t_hal_message) pgm_read_word(&duh->MODIFY); //<---- FIX
    size_t ssize = pgm_read_byte(&duh->datasize);
    uint8_t buf[ssize];

    if (modify(file, buf) == F_OK)
    {

        if (hal_dev_node_update(dd, (void *)buf, &ssize) != K_NO_DD)
        {
            return F_OK;
        }
    }
    return F_FAIL;
}

int8_t
vt_attach_driver(FILE * file,
                 struct dev_descr_node * dd,
                 enum dev_descr_code ddc)
{
    const struct dev_ui_hndlrs * duh = dev_ui_search(ddc, ROOT_DRIVER_DEV);
    if (duh == NULL)
    {
        setSysError(SYSTEM_ITEM_NOT_FOUND);
        return F_FAIL;
    }

    t_attach attach = (t_hal_message) pgm_read_word(&duh->ATTACH); //<---- FIX
    uint8_t buf[pgm_read_byte(&duh->datasize)];

   /* int8_t r = attach(file, buf);
    fprintf_P(file, PSTR("RET : %d\r\n"), r);*/
    if (((int8_t)attach(file, buf)) == F_OK)
    {
        struct dev_descr_node * hnode = hal_dev_node_create(K_NO_DD,
                                                            ddc,
                                                            ROOT_DRIVER_DEV,
                                                            (void*)buf,
                                                            pgm_read_byte(&duh->datasize));
        if (hnode != NULL)
        {
            return F_OK;
        }
    }

    return F_FAIL;

}


int8_t
vt_attach_handler(FILE * file,
                  struct dev_descr_node * dd,
                  enum dev_descr_code ddc,
                  enum dev_desc_handlers ddh)
{
	const struct dev_ui_hndlrs * duh = dev_ui_search(ddc, ddh);
    if (duh == NULL)
    {
        setSysError(SYSTEM_ITEM_NOT_FOUND);
        return F_FAIL;
    }


    t_attach attach = (t_hal_message) pgm_read_word(&duh->ATTACH); //<---- FIX
    uint8_t buf[pgm_read_byte(&duh->datasize)];

    if (((int8_t)attach(file, buf)) == F_OK)
    {

        struct dev_descr_node * hnode = hal_dev_node_create(K_NO_DD,
                                                            ddc,
                                                            ddh,
                                                            (void*)buf,
                                                            pgm_read_byte(&duh->datasize));
        //printf_P(PSTR("return from hal_attach\r\n"));
        if (hnode != NULL)
        {
            return F_OK;
        }
    }

    return F_FAIL;

}
