/*-
 *	Copyright (c) 2014-2016	Alexander Morozov. nixd.org
 *	All rights reserved.
 *
 * LICENSE: This source file is subject to the New BSD license that is
 * available through the world-wide-web at the following URI:
 * http://www.opensource.org/licenses/bsd-license.php or in the root folder
 * of the project in file LICENSE.TXT.
 */
/*
 *  error.h
 *  error storage.
 *  Details: this file contains error codes for expected failts which helps
 *  to find out what happening in the firmware.
 */

#ifndef ERROR_H_
#define ERROR_H_

/* A macro which defines the error journal length. */
#define MAX_ERROR_SIZE 10

#define SYSTEM_OK                               0   //no error
#define SYSTEM_LENGTH_ASSERT                    1   //length assertion failed
#define SYSTEM_MALLOC_FAILED                    2   //malloc failed, possibly out of ram
#define SYSTEM_FD_INVALID                       3   //invalid FD or FID
#define SYSTEM_NULL_ARGUMENT                    4   //argument is null
#define SYSTEM_NODE_INSTANCE_NULL               5   //failed to boot with serial com meth
#define SYSTEM_ALREADY_EXIST                    6   //item already exists
#define SYSTEM_ITEM_NOT_FOUND                   7   //item was not found
#define SYSTEM_MODULE_NOT_RUNNING               8   //the singleton driver/handler not inited
#define SYSTEM_ID_INCORRECT                     9   //ID number in not correct
#define SYSTEM_HW_PIN_BUSY                      10  //Hardware pin is busy
#define SYSTEM_NULL_DEV_ALREADY_INITED          11  //driver inst already running
#define SYSTEM_NULL_DEV_NOT_INITED              12  //driver was not initialized prev
#define SYSTEM_MEMORY_CORUPTION                 13  //memory corruption detected
#define SYSTEM_INCOMPAT_MODE                    14  //mode is not compatable
#define SYSTEM_SIZE_MISMATCH                    15  //size differ
#define SYSTEM_OPERATION_NOT_SUPPORTED          16  //the operation can not be performed, unknown
#define SYSTEM_OUT_OF_RANGE                     17
#define SYSTEM_ISR_BUSY                         18
#define SYSTEM_ISR_OVERRANGE                    19
#define SYSTEM_LIST_EMPTY                       20
#define SYSTEM_MASK_ZERO                        21
#define SYSTEM_DEV_HW_NOT_LOCKED                22
#define SYSTEM_INTERLOCKING_NOT_CREATED         23
#define SYSTEM_OPTION_UNKNOWN                   24

#define SYSTEM_DEVICE_NOT_FOUND                 30  //device not found
#define SYSTEM_DEVICE_NOT_COMPATIABLE              31  //can not run with requested instance
#define SYSTEM_DEV_ALREADY_LISTEN               32  //already monitored
#define SYSTEM_DEV_MAX_AMOUNT                   33  //maximum was reached
#define SYSTEM_DEV_DIF_AMOUNT                   34
#define SYSTEM_DEVICE_ALREADY_INITED            35
#define SYSTEM_DEVICE_NOT_INITED                36
#define SYSTEM_DEVICE_BUSY                      37
#define SYSTEM_DEVICE_EXTERNAL_ERROR            38
#define SYSTEM_DEVICE_OFFLINE                   39
#define SYSTEM_FD_LIST_NOT_EMPTY                40
#define SYSTEM_NO_ENTRIES                       41
#define SYSTEM_EEPROM_NO_ENTRIES                42
#define SYSTEM_REACHED_MAX_RECS                 43

#define SYSTEM_KERNEL_PROCESS_CREATE_FAILED     50
#define SYSTEM_KERNEL_PROCESS_SINGLETON         51

#define SYSTEM_TABITA_CRC16_FAILED              60
#define SYSTEM_TABITA_INTERCOM_NOT_SUPP         61
#define SYSTEM_TABITA_UNKNOWN_DEV               62
#define SYSTEM_DHCP_GENERAL_FAIL                65
#define SYSTEM_DHCP_NOT_FOR_US                  66
#define SYSTEM_DHCP_UNKNOWN_PKT_TYPE            67

#define SYSTEM_IO_WRONG_DIRECTION               70
#define SYSTEM_TIMER_NOT_FREE_RUNNING           75

#define SYSTEM_UNKNOWN_CMD                      80
#define SYSTEM_UNKNOWN_OPERATION                81

#define SYSTEM_UNKNOWN_ID_OF_IO_HW              99
#define SYSTEM_PIN_MASK_MORE_ONE_BIT            100

#define SYSTEM_NET_DATA_DROPED                  110
#define SYSTEM_NET_DATA_ON_CLOSED_PORT          111
#define SYSTEM_NET_PORT_IS_BUSY                 112
#define SYSTEM_NET_PROTO_NOT_SUP                113
#define SYSTEM_NET_INTERFACE_INTERNAL_ERR       114
#define SYSTEM_NET_SEND_UDP_FAILED              115

#define SYSTEM_SPI_UNLOCK_FAILED                116
#define SYSTEM_SPI_SESSION_NOT_INITED           117
#define SYSTEM_SPI_SESSION_BUSY                 118
#define SYSTEM_SPI_RCV_TIMEOUT                  119 //<---

#define SYSTEM_BUFFER_DROP_SEGMENT_NOT_SUPP     176
#define SYSTEM_BUFFER_MANUAL_FLUSH_NOT_SUPP     177
#define SYSTEM_ASYNC_BUFFER_FLUSH_GIVEUP        178
#define SYSTEM_STREAM_INVALID                   179
#define SYSTEM_STREAM_CREATE_FAIL               180
#define SYSTEM_STREAM_LOCKED                    181
#define SYSTEM_STREAM_ILLEGAL_UNLOCK_ATTEMPT    182

#define SYSTEM_NOT_ENOUGH_MEM                   190
#define SYSTEM_NOT_ENOUGH_EEPROM                191
#define SYSTEM_EEPROM_PAGE_EXCEED               192

#define SYSTEM_BAD_ACCESS                       200
#define SYSTEM_UNEXPECTED_EOF                   201
#define SYSTEM_DATA_HANDLER_FAILED              202

#define SYSTEM_DEBUG                            251
#define SYSTEM_USART_REQUIRED_BUFFERED_RX       252
#define SYSTEM_USART_RCV_FULL                   253
#define SYSTEM_USART_BUFFER_FULL                254
#define SYSTEM_USART_TIMEOUT                    255

/*Resets the error list parameters and list.*/
void sys_errors_reset();

/*Returns the current write position in the journal.*/
uint8_t getSysEPos();

/*at The position in journal to read at.*/
uint8_t getSysErrorAt(uint8_t at);

/*Returns the previous error (before current).*/
uint8_t getLastSysErrror();

/*Returns the current error.*/
uint8_t getSysErrror();

/*Registering the error.*/
void setSysError(uint8_t err);

/*
 * Cleans the current error cache.
 * It does not remove it from the journal!
 */
void clearSysError();

#endif
