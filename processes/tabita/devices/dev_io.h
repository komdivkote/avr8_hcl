#ifndef DEV_IO_H_INCLUDED
#define DEV_IO_H_INCLUDED

#define FOREACH_IO_CMDS(DV)\
    DV(IO_CMD_WRITE8, (uint8_t) 0)\
    DV(IO_CMD_READ8, (uint8_t) 1)\
    DV(IO_CMD_OPER_NOT_SUP, (uint8_t) 0xFF)


enum io_cmds
{
    FOREACH_IO_CMDS(GENERATE_ENUM_VAL)
};


struct proto_io_rw_ra
{
    uint8_t val;
};

#endif // DEV_IO_H_INCLUDED
