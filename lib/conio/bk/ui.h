/*
    ui.h
    User interface functions.
    Part of MicroVGA CONIO library / demo project
    Copyright (c) 2008-9 SECONS s.r.o., http://www.MicroVGA.com
    Modified by Alexander Morozov NiXD ORG.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#define UI_NOTHING 0

struct menudialog_entry
{
	const char * caption;
	int val;
};

struct ui_panel
{
	uint16_t x;
	uint16_t y;
	uint16_t width;
	uint16_t height;
	uint32_t color;
};


//Display Northon Commander like function key menu on bottom of the screen.
void drawfkeys(const char *fkeys[]);

//Display text-based selection menu on the screen.
//int runmenu(char x, char y, const char *menu[], int defaultitem);
int runmenu0(char x, char y, int defaultitem, int page_size, const char * const menu_itm[]);
int runmenu0_s(char x, char y, int defaultitem, int page_size, const struct menudialog_entry st_data[], int * ret);
int runmenu1(char x, char y, int defaultitem, const char * caption, const char * const menu_itm[]);

void runerrord0(char x, char y, const char * const text, const char * const msg);
void runerrord1(uint8_t x, uint8_t y, uint8_t ln, const char * const text, const char *fmt, ...);

void runinfod0(char x, char y, bool waitkeypr, const char * const text, const char * const msg);

int16_t draw_input_dialog(char x, char y, const char * text, char * data, uint8_t max_data_len);

void labelf(uint8_t x, uint8_t y, const char *fmt, ...);
void labelf_P(uint8_t x, uint8_t y, const char *fmt, ...);
void putlabel(uint8_t x, uint8_t y, int32_t color, const char * itm);
void putlabelint(uint8_t x, uint8_t y, int32_t color, uint8_t w_len, const char * const itm, int val);

void drawgauge2(uint8_t x, uint8_t y, uint8_t x1, uint8_t x_s1, uint8_t x_s2, char xs, uint8_t x_e1, uint8_t x_e2, char xe);

//Display empty frame on the screen.
void drawframe(uint8_t x, uint8_t y, uint8_t width, uint8_t height, uint32_t color);

/* */
