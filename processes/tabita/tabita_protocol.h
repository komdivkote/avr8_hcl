#ifndef TABITA_PROTOCOL_H_INCLUDED
#define TABITA_PROTOCOL_H_INCLUDED
#pragma pack(n)
//typedef int16_t t_trans_id;
typedef uint16_t t_crc16;


enum tabita_pkt_type
{
    TAB_PKT_TYPE_MASTER = (uint8_t) 1,
    TAB_PKT_TYPE_SLAVE = (uint8_t) 2,
};

enum tabita_pkt_oper
{
    TAB_PKT_OPER_CREATE_DD      = (uint8_t) 0,
    TAB_PKT_OPER_MODIFY_DD      = (uint8_t) 1,
    TAB_PKT_OPER_DELETE_DD      = (uint8_t) 2,
    TAB_PKT_OPER_REQUEST_DD     = (uint8_t) 3,
    TAB_PKT_OPER_LIST_DD        = (uint8_t) 4,
    TAB_PKT_OPER_LIST_PR        = (uint8_t) 5,
    TAB_PKT_OPER_QUERY_PR       = (uint8_t) 6,
    TAB_PKT_OPER_AUTH           = (uint8_t) 7
};

enum tabita_pkt_proc_oper
{
    TAB_PKT_PROC_RESUME     = (uint8_t) 0,
    TAB_PKT_PROC_SUSPEND    = (uint8_t) 1,
    TAB_PKT_PROC_REMOVE     = (uint8_t) 2
};


struct tabita_header
{
    uint8_t e_type; //enum tabita_pkt_type
    t_trans_id seq_num_master;
    t_trans_id seq_num_slave;
    uint16_t payload_len;
    t_crc16 crc16;
    uint8_t e_oper; //enum tabita_pkt_oper
    uint8_t data[];
};

struct tabita_create
{
    uint8_t e_dev_descr; //enum dev_descr_code
    uint8_t e_dh_code; //enum dev_desc_handlers
    uint8_t data[];
};

struct tabita_create_answer
{
    t_dev_id did;
    uint8_t error_id;
};

struct tabita_delete
{
    t_dev_id did;
};

struct tabita_delete_answer
{
    uint8_t error_id;
};

struct tabita_list_dd_answer
{
    t_dev_id did;
    uint8_t e_dev_descr; //enum dev_descr_code
    uint8_t e_dh_code; //enum dev_desc_handlers
    uint8_t next[];
};

struct tabita_list_pr_answer
{
    k_pid pid;                  //1
    uint16_t stack_start;       //2
    uint16_t stack_end;         //2
    uint16_t stack_ptr;         //2
    uint8_t flags;              //1
    uint8_t cpup;               //1
};

struct tabita_query_pr
{
    k_pid pid;
    enum tabita_pkt_proc_oper e_proc_op;
};

struct tabita_query_pt_ans
{
    uint8_t error_no;
};

struct tabita_request
{
    t_dev_id did;
    uint8_t opcode;
    uint8_t data[];
};

struct tabita_request_ans
{
    t_dev_id did;
    uint8_t opcode;
    uint8_t error_no;
    uint8_t data[];
};

struct tabita_auth
{
    uint8_t xorkey;
    uint8_t offset;
};

#endif // TABITA_H_INCLUDED
