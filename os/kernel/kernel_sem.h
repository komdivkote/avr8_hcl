/*-
 *	Copyright (c) 2014-2016	Alexander Morozov. www.nixd.org
 *	All rights reserved.
 *
 * LICENSE: This source file is subject to the New BSD license that is
 * available through the world-wide-web at the following URI:
 * http://www.opensource.org/licenses/bsd-license.php or in the root folder
 * of the project in file LICENSE.TXT.
 */

#ifndef KERNEL_SEM_H_INCLUDED
#define KERNEL_SEM_H_INCLUDED

/*
 * struct kproc_sem
 * see kernel_process_private.h
 */
struct kproc_sem;

/*
 * kernel_sem_init
 * This function initializes semaphore.
 * arg0 (uint8_t) - argument takes amount of the free blocks
 * returns: pointer to the semaphore instance or NULL in case of error
 */
struct kproc_sem * kernel_sem_init(uint8_t);

/*
 * kernel_sem_enter
 * This function captures block of semaphore.
 *  If no free blocks available then the process will set to WAIT state.
 * arg0 (struct kproc_sem *) - valid instance of kproc_sem
 * returns: 0 on success and 1 on error
 */
int8_t kernel_sem_enter(struct kproc_sem *);

/*
 * kernel_sem_leave
 * This function releases block of semaphore.
 * arg0 (struct kproc_sem *) - valid instance of kproc_sem
 * returns: 0 on success and 1 o error
 */
int8_t kernel_sem_leave(struct kproc_sem *);

//Information

/*
 * kernel_sem_get_total
 * This function returns the total available slots for the selected semaphore.
 *	This function does not validate input.
 * arg0 (struct kproc_sem *) pointer to the valid instance of semaphore
 * returns: value (uint8_t)
 */
uint8_t kernel_sem_get_total(struct kproc_sem *);

/*
 * kernel_sem_get_free
 * This function returns the free slots available for the selected semaphore.
 *	This function does not validate input.
 * arg0 (struct kproc_sem *) pointer to the valid instance of semaphore
 * returns: value (uint8_t)
 */
uint8_t kernel_sem_get_free(struct kproc_sem *);

/*
 * kernel_sem_get_free
 * This function returns the amount of waiting processes for the slot in the selected semaphore.
 *	This function does not validate input.
 * arg0 (struct kproc_sem *) pointer to the valid instance of semaphore
 * returns: value (uint8_t)
 */
uint8_t kernel_sem_get_waiting(struct kproc_sem *);

/*--------------------SEM BLOCK---------------------*/

/*
 * struct _sem_ToDo overhead structure
 */
struct _sem_ToDo
{
	struct kproc_sem * sem;
	int8_t ret;
};

static inline void
__leave_sem(const struct _sem_ToDo * mtd)
{
	kernel_sem_leave(mtd->sem);

	asm volatile ("" ::: "memory");
}

//exit mutex block immediately
#define SEM_ACTION_LEAVE 0
//manual problem resolution (see MUTEX_BLOCK_VAR_RET)
#define SEM_ACTION_MANUAL 2

//retrives the returnd result
#define SEM_BLOCK_VAR_RET st.ret
#define SEM_LOCK_FAILED SEM_ACTION_MANUAL

#define SEM_BLOCK(_SEM_, _ACTION_) \
	for (struct _sem_ToDo st __attribute__((__cleanup__(__leave_sem))) = {_SEM_, (kernel_sem_enter(_SEM_) == 0) ? 1 : _ACTION_};\
		st.ret;\
		st.ret = ((st.ret == SEM_LOCK_FAILED) ? 2 : 0))

#endif // KERNEL_SEM_H_INCLUDED
