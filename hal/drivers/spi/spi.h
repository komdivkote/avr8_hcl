/*-
 *	Copyright (c) 2014-2015	Alexander Morozov. www.nixd.org
 *	All rights reserved.
 *
 * LICENSE: This source file is subject to the New BSD license that is
 * available through the world-wide-web at the following URI:
 * http://www.opensource.org/licenses/bsd-license.php or in the root folder
 * of the project in file LICENSE.TXT.
 */

/* Todo: Implement async mode, collision handler
*/

#ifndef SPI_H_
#define SPI_H_

#define SPI_REVISION 1

enum spi_port
{
    FOREACH_SPI_DEV(GENERATE_ENUM_VAL)
};

#define FOREACH_SPI_BIT_ORDER(DV)\
	DV(LSBFIRST, (uint8_t) 0)\
	DV(MSBFIRST, (uint8_t) 1)

enum spi_bit_order
{
	FOREACH_SPI_BIT_ORDER(GENERATE_ENUM_VAL)
};

#define FOREACH_SPI_MODE(DV)\
	DV(SPI_MASTER, (uint8_t) 0x01)\
	DV(SPI_SLAVE, (uint8_t) 0x00)

enum spi_mode
{
	FOREACH_SPI_MODE(GENERATE_ENUM_VAL)
};

#define FOREACH_SPI_CLOCK(DV)\
	DV(SPI_CLOCK_DIV4, (uint8_t) 0x00)\
	DV(SPI_CLOCK_DIV16, (uint8_t) 0x01)\
	DV(SPI_CLOCK_DIV64, (uint8_t) 0x02)\
	DV(SPI_CLOCK_DIV128, (uint8_t) 0x03)\
	DV(SPI_CLOCK_DIV2, (uint8_t) 0x04)\
	DV(SPI_CLOCK_DIV8, (uint8_t) 0x05)\
	DV(SPI_CLOCK_DIV32, (uint8_t) 0x06)
//#define SPI_CLOCK_DIV64 0x07

enum spi_clock
{
	FOREACH_SPI_CLOCK(GENERATE_ENUM_VAL)
};

/*
Sample (Rising) Setup (Falling) mode 0
Setup (Rising) Sample (Falling) mode 1
Sample (Falling) Setup (Rising) mode 2
Setup (Falling) Sample (Rising) mode 3
*/

#define FOREACH_SPI_DATA_MODE(DV)\
	DV(SPI_MODE0, (uint8_t) 0x00)\
	DV(SPI_MODE1, (uint8_t) 0x04)\
	DV(SPI_MODE2, (uint8_t) 0x08)\
	DV(SPI_MODE3, (uint8_t) 0x0C)

enum spi_data_mode
{
	FOREACH_SPI_DATA_MODE(GENERATE_ENUM_VAL)
};


//bitmasks
#define SPI_MODE_MASK 0x0C  // CPOL = bit 3, CPHA = bit 2 on SPCR
#define SPI_CLOCK_MASK 0x03  // SPR1 = bit 1, SPR0 = bit 0 on SPCR
#define SPI_2XCLOCK_MASK 0x01  // SPI2X = bit 0 on SPSR

typedef int8_t cs_id;

struct spi_chip_select_pins
{
	cs_id csid;                               //identical number
	enum ports e_port;                      //port
	uint8_t bit_mask;                       //bit mask of the CS
	struct spi_chip_select_pins * next;     //pointer to the next record
};

struct spi_dev
{
    struct dev_descr_node * ddn;            //node instance
    enum spi_port e_dev;
	enum spi_mode e_mode;                   //mode
	enum spi_clock e_clock;                 //clock source
	enum spi_bit_order e_bit_order;         //bit order
	enum spi_data_mode e_data_mode;         //spi mode (see datasheet)
	struct spi_chip_select_pins * cs_list;  //pointer to the chain list
	struct kproc_mutex * mtx_spi;

	struct spi_chip_select_pins * cs_locked; //in use
};

struct spi_dev_envelope
{
    enum spi_port e_dev;
	enum spi_mode e_mode;
	enum spi_clock e_clock;
	enum spi_bit_order e_bit_order;
	enum spi_data_mode e_data_mode;
};

struct spi_setup
{
    enum spi_port e_dev;
	enum spi_mode e_mode;
	enum spi_clock e_clock;
	enum spi_bit_order e_bit_order;
	enum spi_data_mode e_data_mode;
};

/*
 *   Standart module functions
 */
int8_t spi_hal_message(struct dev_descr_node * ddn,
                          enum dd_event e_event,
                          void * data0,
                          size_t * dsize0);

int8_t spi_kstream_init(struct dev_descr_node * ddn,
                           struct kstream * ks);

int8_t spi_stream_init(struct dev_descr_node * ddn,
                             FILE * file);


cs_id spi_add_slave(struct spi_dev * s_spi, enum ports e_port, uint8_t bit_mask, uint8_t flags);
int8_t spi_remove_slave(struct spi_dev * s_spi, cs_id csid);

uint8_t spi_run_session(struct spi_dev * s_spi, cs_id csid);
int8_t spi_finish_session(struct spi_dev * s_spi, cs_id csid);
uint8_t spi_transfer(struct spi_dev * s_spi, uint8_t _data);
void driver_spi_attachInterrupt(struct spi_dev * s_spi, void (*ISRHANDLER)(), uint8_t delayed);
void driver_spi_detachInterrupt(struct spi_dev * s_spi); // Default


#endif /* SPI_H_ */
