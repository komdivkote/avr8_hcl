/*-
 *  ATTENTION!: the initial version of the DHCP logic was taken from:
 *		link:http://easyelectronics.ru/files/WE/redsh/eth_232.zip
 *		link2:http://we.easyelectronics.ru/electro-and-pc/podklyuchenie-mikrokontrollera-k-lokalnoy-seti-udp-server.html
 *		License: WTFPL
 *
 *	--------APPLICIABLE ONLY FOR DSC code, not DHCP logic/routnies------
 *	Copyright (c) 2014-2016	Alexander Morozov. NiXD ORG
 *	All rights reserved.
 *
 * LICENSE: This source file is subject to the New BSD license that is
 * available through the world-wide-web at the following URI:
 * http://www.opensource.org/licenses/bsd-license.php or in the root folder
 * of the project in file LICENSE.TXT.
 */


#include "../../hal/build_defs.h"
#if (WITH_DHCP == YES)

#include <avr/io.h>
#include <avr/pgmspace.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stdbool.h>
#include <inttypes.h>
#include <string.h>
#include "../../hal/hal.h"
#include "libnetwork.h"

#include "../../os/kernel/kernel_process.h"
#include "../../os/kernel/kernel_memory.h"
#include "../../os/kernel/kernel_mutex.h"

#define dhcp_add_option(ptr, optcode, type, value) \
	((dhcp_option_t*)ptr)->code = optcode; \
	((dhcp_option_t*)ptr)->len = sizeof(type); \
	*(type*)(((dhcp_option_t*)ptr)->data) = value; \
	ptr += sizeof(dhcp_option_t) + sizeof(type); \
	if(sizeof(type)&1) *(ptr++) = 0;

struct dhcp_record * dhcp_recs[DHCP_SLOTS] =  {[ 0 ... DHCP_SLOTS-1 ] = NULL};
static k_pid pid = K_NO_PID;
static struct knetlisten * sock = NULL;
volatile struct kproc_mutex * volatile dhcp_mux = NULL;

static int8_t dhclient_process(void * arg0);
static int8_t dhcp_onrcv(struct knetdevice * kndev, struct knetstate * state, uint8_t *frame, uint16_t len);
static int8_t dhcp_req_ren(uint8_t id);
static struct dhcp_record * dhcp_get_instance(t_netdev_id nid);


//blocking operatio
static int8_t
dhclient_process(void * arg0)
{
    int8_t status = 0;

    while (1)
    {
        kernel_mutex_lock(dhcp_mux);

        for (uint8_t i = 0; i < DHCP_SLOTS; i++)
        {
            status = 0;

            if (dhcp_recs[i] == NULL)
            {
                continue;
            }

            if (dhcp_recs[i]->dhcp_renew_time < kernel_get_seconds())
            {
                switch (dhcp_recs[i]->dhcp_status)
                {
                    case DHCP_ASSIGNED:
                    case DHCP_INIT:
                        status = dhcp_req_ren(i);
                        //fprintf_P(stdout, PSTR("DHCP: dhcp req status=%d\r\n"), status);
                    break;

                    case DHCP_WAITING_ACK:
                        if (dhcp_recs[i]->dhcp_renew_time < kernel_get_seconds())
                        {
                            //retry
                            status = dhcp_req_ren(i);
                        }
                    break;

                    case DHCP_WAITING_OFFER:
                        status = dhcp_req_ren(i);
                    break;
                }
            }
        }

        kernel_mutex_unlock(dhcp_mux);

        //the answer is handled by NIF_HANDLER in ONRCV callback here (see code in dhcp_onrcv)

        kernel_process_usleep(1000000, 0);
        //if (usleep == -1) __delay_ms(50);
    }


    return 0;

}

int8_t
dhclient(struct knetdevice * kndev)
{
    if (pid == K_NO_PID)
    {
        pid = kernel_create_process(PSTR("dhclient"), KPROC_TYPE_SYSTEM, /*400*//*250*/230, dhclient_process, NULL);
    }

    if (dhcp_mux == NULL)
    {
        dhcp_mux = kernel_mutex_init();
    }

	if (pid != K_NO_PID)
	{
	    if (sock == NULL)
        {
            struct knetlisten_handlers hndls;
            hndls.ONRCV = dhcp_onrcv;
            hndls.ONOPEN = NULL; //ONLY TCP
            hndls.CLOSED = NULL; //ONLY TCP
            sock = hal_net_listen_socket(KNET_UDP, 0, DHCP_CLIENT_PORT, &hndls);
            if (sock == NULL)
            {
                //ERROR CREATING SOCKET
            }
            #if DEBUG_MESSAGES == YES
            if (lst)
            {

                printf_P(PSTR("DHCP: listening *:68 \r\n"));
            }
            #endif // DEBUG_MESSAGES
        }

        if (sock != NULL)
        {
            int8_t sel = -1;
            kernel_mutex_lock(dhcp_mux);
            for (uint8_t i=0; i < DHCP_SLOTS; i++)
            {
                if (dhcp_recs[i] == NULL)
                {
                    sel = i;
                    break;
                }
            }
            kernel_mutex_unlock(dhcp_mux);

            if (sel == -1)
            {
                //no space
                return 1;
            }

            struct dhcp_record * s_dhcp = (struct dhcp_record *) kernel_malloc(sizeof(struct dhcp_record));
            ASSERT_NULL_ER(s_dhcp, SYSTEM_MALLOC_FAILED, 1);

            s_dhcp->dhcp_status = DHCP_INIT;
            s_dhcp->kndev = kndev;
            s_dhcp->dhcp_renew_time = 0;
            //add to list
            kernel_mutex_lock(dhcp_mux);
            dhcp_recs[sel] = s_dhcp;
            kernel_mutex_unlock(dhcp_mux);

            return 0;
        }

	}

	return 1;
}

//handling from process NIF_HANDLER
static int8_t
dhcp_onrcv(struct knetdevice * kndev, struct knetstate * state, uint8_t *frame, uint16_t len)
{
	/*ip_packet_t *ip = (void*)(frame->data);
	udp_packet_t *udp = (void*)(ip->data);*/
	MUTEX_BLOCK(dhcp_mux, MUTEX_ACTION_MANUAL)
	{
        struct dhcp_record * dhrec = dhcp_get_instance(kndev->net_dev_id);
        if (dhrec == NULL)
        {
            setSysError(SYSTEM_ITEM_NOT_FOUND);
            hal_net_onrcv_release_rx(kndev);
            return 1;
        }

        dhcp_message_t *dhcp = (void*)frame;
        dhcp_message_t *dhcp_ans = hal_net_grab_tx_buffer(kndev, KNET_UDP);
        dhcp_option_t *option;
        uint8_t *op, optlen;
        uint32_t offered_net_mask = 0, offered_gateway = 0;
        uint32_t lease_time = 0, renew_time = 0, renew_server = 0;
        uint8_t type = 0;
        uint32_t temp;

        #if DEBUG_MESSAGES == YES
            printf_P(PSTR("DPCH: onreceive handler len= %"PRIu16">%"PRIu16" op=%"PRIu8"->%"PRIu8" tr_id= %"PRIu32"->%"PRIu32" co= %"PRIu32"->%"PRIu32"\r\n"),
                    len, sizeof(struct dhcp_message),
                    dhcp->operation, DHCP_OP_REPLY,
                    dhcp->transaction_id, dhrec->dhcp_transaction_id,
                    dhcp->magic_cookie, DHCP_MAGIC_COOKIE);
        #endif // DEBUG_MESSAGES


        // Check if DHCP messages directed to us
        if( (len >= sizeof(struct dhcp_message)) &&
            (dhcp->operation == DHCP_OP_REPLY) &&
            (dhcp->transaction_id == dhrec->dhcp_transaction_id) &&
            (dhcp->magic_cookie == DHCP_MAGIC_COOKIE)
          )
        {
            #if DEBUG_MESSAGES == YES
                printf_P(PSTR("UDPCH: DHCP_OP_REPLY: \r\n"));
            #endif // DEBUG_MESSAGES

            //copy pkf to output buffer
            memcpy(dhcp_ans, dhcp, len);

            len -= sizeof(dhcp_message_t);


            // parse DHCP message
            op = dhcp_ans->options;
            while(len >= sizeof(dhcp_option_t))
            {
                option = (void*)op;
                if(option->code == DHCP_CODE_PAD)
                {
                    op++;
                    len--;
                }
                else if(option->code == DHCP_CODE_END)
                {
                    break;
                }
                else
                {
                    switch(option->code)
                    {
                    case DHCP_CODE_MESSAGETYPE:
                        type = *(option->data);
                        break;
                    case DHCP_CODE_SUBNETMASK:
                        offered_net_mask = *((uint32_t*)(option->data));
                        break;
                    case DHCP_CODE_GATEWAY:
                        offered_gateway = *((uint32_t*)(option->data));
                        break;
                    case DHCP_CODE_DHCPSERVER:
                        renew_server = *((uint32_t*)(option->data));
                        break;
                    case DHCP_CODE_LEASETIME:
                        temp = *(uint32_t*)(option->data);
                        lease_time = ntohl(temp);
                        /*if(lease_time > 4294966)
                            lease_time = 4294966;*/
                        break;
                    /*case DHCP_CODE_RENEWTIME:
                        temp = *(uint32_t*)(option->data);
                        renew_time = ntohl(temp);
                        if(renew_time > 21600)
                            renew_time = 21600;
                        break;*/
                    }

                    optlen = sizeof(dhcp_option_t) + option->len;
                    op += optlen;
                    len -= optlen;
                }
            }

            if(!renew_server)
            {
                renew_server = state->raddr;
            }

            switch(type)
            {
            // DHCP offer?
            case DHCP_MESSAGE_OFFER:
                if ((dhrec->dhcp_status == DHCP_WAITING_OFFER) &&
                    (dhcp->offered_addr != 0)
                   )
                {
                    #if DEBUG_MESSAGES == YES
                    printf_P(PSTR("UDPCH: DHCP_MESSAGE_OFFER: \r\n"));
                    #endif // DEBUG_MESSAGES

                    dhrec->dhcp_status = DHCP_WAITING_ACK;

                    #if DEBUG_MESSAGES == YES
                    printf_P(PSTR("UDPCH:--> DHCP_WAITING_ACK: \r\n"));
                    #endif // DEBUG_MESSAGES

                    op = dhcp_ans->options;
                    dhcp_add_option(op, DHCP_CODE_MESSAGETYPE, uint8_t, DHCP_MESSAGE_REQUEST);
                    dhcp_add_option(op, DHCP_CODE_REQUESTEDADDR, uint32_t, dhcp_ans->offered_addr);
                    dhcp_add_option(op, DHCP_CODE_DHCPSERVER, uint32_t, renew_server);
                    *(op++) = DHCP_CODE_END;

                    dhcp_ans->operation = DHCP_OP_REQUEST;
                    dhcp_ans->offered_addr = 0;
                    dhcp_ans->server_addr = 0;
                    dhcp_ans->flags = DHCP_FLAG_BROADCAST;

                    /*kernel_net_send_udp(kndev,
                                        DHCP_CLIENT_PORT,
                                        (inet_addr(255,255,255,255)),
                                        DHCP_SERVER_PORT,
                                        (void*)(kndev->GET_OUTPUT_BUF(kndev->driver_nic_stuct, KNET_ROOT)),
                                    (uint8_t*)op - (uint8_t*)dhcp_ans);*/
                     hal_send_dgram(kndev,
                                   inet_addr(255,255,255,255),
                                   DHCP_SERVER_PORT,
                                   DHCP_CLIENT_PORT,
                                   KNET_UDP,
                                   kndev->tx_buf,
                                   (uint8_t*)op - (uint8_t*)dhcp_ans);
                }
                break;

            // DHCP ack?
            case DHCP_MESSAGE_ACK:
                if( (dhrec->dhcp_status == DHCP_WAITING_ACK) && (lease_time) )
                {
                /*	if(!renew_time)
                        renew_time = lease_time/2;*/
                    #if DEBUG_MESSAGES == YES
                    printf_P(PSTR("UDPCH: DHCP_MESSAGE_ACK: \r\n"));
                    #endif // DEBUG_MESSAGES

                    dhrec->dhcp_status = DHCP_ASSIGNED;
                    dhrec->dhcp_server = renew_server;
                    dhrec->dhcp_renew_time = lease_time + kernel_get_seconds();
                    //kernel_update_task_time_id(dhrec->poll_task_id, dhrec->dhcp_renew_time);
                    //dhrec->dhcp_retry_time = TASK_SEC_TO_MS(lease_time);

                    // network up
                    kndev->nic_set.ipv4 = dhcp->offered_addr;
                    kndev->nic_set.ipv4_mask = offered_net_mask;
                    kndev->nic_set.ipv4_gateway = offered_gateway;

                    #if DEBUG_MESSAGES == YES
                    printf_P(PSTR("UDPCH: New IP=%x mask=%x gateway=%x: \r\n"), kndev->nic_set.ipv4, kndev->nic_set.ipv4_mask, kndev->nic_set.ipv4_gateway);
                    #endif // DEBUG_MESSAGES
                }
                break;

                default:
                 #if DEBUG_MESSAGES == YES
                    printf_P(PSTR("DHCP: Received unknown pkt type: %"PRIu8"\r\n"), type);
                 #endif // DEBUG_MESSAGES
                    setSysError(SYSTEM_DHCP_UNKNOWN_PKT_TYPE);
                break;
            }
        }
        else
        {
            setSysError(SYSTEM_DHCP_NOT_FOR_US);
        }

        //release TX buffer
        hal_net_relese_tx_buffer(kndev);
        //release RX buffer
        hal_net_onrcv_release_rx(kndev);
    }
	return 0;
}

static int8_t
dhcp_req_ren(uint8_t id)
{
/*	eth_frame_t *frame = (void*)kndev->driver_output_buf;
	ip_packet_t *ip = (void*)(frame->data);
	udp_packet_t *udp = (void*)(ip->data);*/

	struct dhcp_record * drec = dhcp_recs[id];
	//printf_P(PSTR("trying to lock tx\r\n"));
	dhcp_message_t *dhcp = hal_net_grab_tx_buffer(drec->kndev, KNET_UDP);
	//printf_P(PSTR("trying to locked tx\r\n"));
	uint8_t *op;

	#if DEBUG_MESSAGES == YES
	fprintf_P(stdout, PSTR("DHCP: debug 1\r\n"));
	#endif // DEBUG_MESSAGES


	if ((drec->dhcp_status == DHCP_WAITING_OFFER) ||
		(drec->dhcp_status == DHCP_INIT)
	   )
	{
		drec->dhcp_status = DHCP_WAITING_OFFER;
		//drec->dhcp_retry_time = sec() + 15;
		drec->dhcp_renew_time = kernel_get_seconds()+5;//TASK_SEC_TO_MS(sec()+5); //25
		drec->dhcp_transaction_id = (uint32_t)kernel_get_milis() | (((uint32_t)kernel_get_milis()) << 16);

		#if DEBUG_MESSAGES == YES
		fprintf_P(stdout, PSTR("DHCP: %lu\r\n"), drec->dhcp_transaction_id);
		#endif // DEBUG_MESSAGES

		drec->kndev->nic_set.ipv4 = 0;
		drec->kndev->nic_set.ipv4_gateway = 0;
		drec->kndev->nic_set.ipv4_mask = 0;

		memset(dhcp, 0, sizeof(dhcp_message_t));
		dhcp->operation = DHCP_OP_REQUEST;
		dhcp->hw_addr_type = DHCP_HW_ADDR_TYPE_ETH;
		dhcp->hw_addr_len = 6;
		dhcp->transaction_id = drec->dhcp_transaction_id;
		dhcp->flags = DHCP_FLAG_BROADCAST;
		memcpy(dhcp->hw_addr, drec->kndev->mac, 6);
		dhcp->magic_cookie = DHCP_MAGIC_COOKIE;

		op = dhcp->options;
		dhcp_add_option(op, DHCP_CODE_MESSAGETYPE, uint8_t, DHCP_MESSAGE_DISCOVER);
		*(op++) = DHCP_CODE_END;

		//udp_send((struct enc28_record *)kndev->driver_nic_stuct, frame, (uint8_t*)op - (uint8_t*)dhcp);
		//kernel_net_send(kndev, klid, frame, (uint8_t*)op - (uint8_t*)dhcp);

		#if DEBUG_MESSAGES == YES
		fprintf_P(stdout, PSTR("DHCP: sending\r\n"));
		#endif // DEBUG_MESSAGES

        int8_t ret = hal_send_dgram(drec->kndev,
                                   inet_addr(255,255,255,255),
                                   DHCP_SERVER_PORT,
                                   DHCP_CLIENT_PORT,
                                   KNET_UDP,
                                   drec->kndev->tx_buf,
                                   (uint8_t*)op - (uint8_t*)dhcp);

        if (ret != 0)
        {
            setSysError(SYSTEM_NET_SEND_UDP_FAILED);
        }
        //fprintf_P(stdout, PSTR("DHCP SENT!\r\n"));
		#if DEBUG_MESSAGES == YES
		//fprintf_P(stdout, PSTR("DHCP: sent\r\n"));
		#endif // DEBUG_MESSAGES

        hal_net_relese_tx_buffer(drec->kndev);

		return 0;
	}

	// time to renew lease
	if (drec->dhcp_status == DHCP_ASSIGNED)
	{
		drec->dhcp_transaction_id = (uint32_t)kernel_get_milis() | (((uint32_t)kernel_get_milis()) << 16);

		memset(dhcp, 0, sizeof(dhcp_message_t));
		dhcp->operation = DHCP_OP_REQUEST;
		dhcp->hw_addr_type = DHCP_HW_ADDR_TYPE_ETH;
		dhcp->hw_addr_len = 6;
		dhcp->transaction_id = drec->dhcp_transaction_id;
		dhcp->client_addr = drec->kndev->nic_set.ipv4;
		memcpy(dhcp->hw_addr, drec->kndev->mac, 6);
		dhcp->magic_cookie = DHCP_MAGIC_COOKIE;

		op = dhcp->options;
		dhcp_add_option(op, DHCP_CODE_MESSAGETYPE, uint8_t, DHCP_MESSAGE_REQUEST);
		dhcp_add_option(op, DHCP_CODE_REQUESTEDADDR, uint32_t, drec->kndev->nic_set.ipv4);
		dhcp_add_option(op, DHCP_CODE_DHCPSERVER, uint32_t, drec->dhcp_server);
		*(op++) = DHCP_CODE_END;

        drec->dhcp_renew_time = kernel_get_seconds()+5;

		//if(udp_send(enc28, frame, (uint8_t*)op - (uint8_t*)dhcp) == 0)
		//if (kernel_net_send(kndev, klid, frame, (uint8_t*)op - (uint8_t*)dhcp) == 0)
		if( hal_send_dgram(drec->kndev,
                       drec->dhcp_server,
                       DHCP_SERVER_PORT,
                       DHCP_CLIENT_PORT,
                       KNET_UDP,
                       drec->kndev->tx_buf,
                       (uint8_t*)op - (uint8_t*)dhcp) != 0)
        {

            hal_net_relese_tx_buffer(drec->kndev);
            setSysError(SYSTEM_NET_SEND_UDP_FAILED);
			return 1;
        }

		drec->dhcp_status = DHCP_WAITING_ACK;
		hal_net_relese_tx_buffer(drec->kndev);

		return 0;
	}

	setSysError(SYSTEM_DHCP_GENERAL_FAIL);
	hal_net_relese_tx_buffer(drec->kndev);

	return 0;

}

int8_t
dhcp_wait_for_assignment(struct knetdevice * kndev)
{
	ASSERT_NULL_E(kndev, SYSTEM_NULL_ARGUMENT, error);

	while (kndev->nic_set.ipv4 == 0); //wait

	return 0;

error:
	return 1;
}


enum dhcp_status_code *
dhcp_get_status(struct knetdevice * kndev)
{
    return &(dhcp_get_instance(kndev->net_dev_id)->dhcp_status);
}

t_dev_id
dhcp_get_did(uint8_t slot)
{
    return (dhcp_recs[slot] == NULL) ? K_NO_DD : dhcp_recs[slot]->kndev->ddn->dev_id;
}

static struct dhcp_record *
dhcp_get_instance(t_netdev_id nid)
{
    for (uint8_t i = 0; i < DHCP_SLOTS; i++)
    {
        if (nid == dhcp_recs[i]->kndev->net_dev_id)
        {
            return dhcp_recs[i];
        }
    }

    return NULL;
}

#endif
