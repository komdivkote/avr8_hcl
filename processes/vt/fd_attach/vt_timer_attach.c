/*-
 *	Copyright (c) 2014-2016	Alexander Morozov. NiXD ORG
 *	All rights reserved.
 *
 * LICENSE: This source file is subject to the New BSD license that is
 * available through the world-wide-web at the following URI:
 * http://www.opensource.org/licenses/bsd-license.php or in the root folder
 * of the project in file LICENSE.TXT
 */

#include "../../../hal/build_defs.h"
#include <avr/io.h>
#include <avr/pgmspace.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include "../../../hal/hal.h"
#include "../../../hal/drivers/timer/timer.h"
#include "../../../hal/handlers.h"
#include "../virtual_terminal.h"
#include "../../../lib/conio/conio.h"
#include "../../../lib/conio/kbd.h"
#include "../../../lib/conio/ui.h"
#include "../vt_text.h"
#include "vt_attach.h"

static int8_t __ui_query(FILE * file, struct timer_setup * timerset);
static int8_t __ui_vals_change(FILE * file, uint8_t bw, uint16_t * ocra, uint16_t * ocrb, uint16_t * ocrc);
static int8_t __ui_query_info(FILE * file, uint8_t * opt);

FOREACH_PWM_OPERA(GENERATE_VARS_VAL);
const struct menudialog_entry vt_timer_pwm_opera_captions_struct[] PROGMEM = {
	FOREACH_PWM_OPERA(GENERATE_VAR_STRUCT)
	{
		P_NULL, NULL
	}
};

FOREACH_PWM_OPERB(GENERATE_VARS_VAL);
const struct menudialog_entry vt_timer_pwm_operb_captions_struct[] PROGMEM = {
	FOREACH_PWM_OPERB(GENERATE_VAR_STRUCT)
	{
		P_NULL, NULL
	}
};

FOREACH_WAVEFORM_MODE(GENERATE_VARS_VAL);
const struct menudialog_entry vt_timer_waveform_mode_captions_struct[] PROGMEM = {
	FOREACH_WAVEFORM_MODE(GENERATE_VAR_STRUCT)
	{
		P_NULL, NULL
	}
};

FOREACH_TIMER_CLOCK(GENERATE_VARS_VAL);
const struct menudialog_entry vt_timer_clock_captions_struct[] PROGMEM = {
	FOREACH_TIMER_CLOCK(GENERATE_VAR_STRUCT)
	{
		P_NULL, NULL
	}
};

FOREACH_TIMER_HW(GENERATE_VARS_VAL);
const struct menudialog_entry vt_timers_ids_captions_struct[] PROGMEM = {
	FOREACH_TIMER_HW(GENERATE_VAR_STRUCT)
	{
		P_NULL, NULL
	}
};

const struct menudialog_entry vt_options_captions_struct[] PROGMEM = {
	{
	    value_capt, VT_INFO_OPTS_CHANGEVAL
	},
	{
		P_NULL, NULL
	}
};

static int8_t
__ui_query(FILE * file, struct timer_setup * timerset)
{
	int ret = 0;
	int res = 0;

	ret = runmenu0_s(file, 10, 10, 0, 10, vt_timers_ids_captions_struct, &res);
	//try to create fd for the device
	if (ret == UI_NOTHING)
	{
		return F_FAIL;
	}
	timerset->e_timer = (uint8_t) res;

    if (timer_is_system(timerset->e_timer) == 1)
    {
        //system timer
        runerrord1(file, 10, 10, 30, error_capt, system_timer_busy_capt, timerset->e_timer);
        return F_FAIL;
    }

    const struct timer_hwaddr * tim_hw_ptr = &s_timer_addr[(uint8_t) timerset->e_timer];
    uint8_t bw = pgm_read_word(&(tim_hw_ptr->timer_bit_width));

	ret = runmenu0_s(file, 12, 10, 0, 10, vt_timer_pwm_opera_captions_struct, &res);
	//try to create fd for the device
	if (ret == UI_NOTHING)
	{
		return F_FAIL;
	}
	timerset->e_operA = (uint8_t) res;

	ret = runmenu0_s(file, 16, 10, 0, 10, vt_timer_pwm_operb_captions_struct, &res);
	//try to create fd for the device
	if (ret == UI_NOTHING)
	{
		return F_FAIL;
	}
	timerset->e_operB = (uint8_t) res;

	ret = runmenu0_s(file, 21, 10, 0, 10, vt_timer_waveform_mode_captions_struct, &res);
	//try to create fd for the device
	if (ret == UI_NOTHING)
	{
		return F_FAIL;
	}

	timerset->e_wave_mode = (uint8_t) res;

    ret = runmenu0_s(file, 21, 10, 0, 10, vt_timer_clock_captions_struct, &res);
	//try to create fd for the device
	if (ret == UI_NOTHING)
	{
		return F_FAIL;
	}

	timerset->e_clock = (uint8_t) res;

    uint8_t data[8];
    memset(data, 0, 8);
	ret = draw_input_dialog(file, 10, 10, (bw == TIMER_BIT_WIDTH_8) ? ocra8bit : ocra16bit, data, 8);
	if (ret < 1)
    {
        return F_FAIL;
    }

    timerset->ocra = (uint16_t) atoi ( (const char *) data );

    memset(data, 0, 8);
	ret = draw_input_dialog(file, 10, 10, (bw == TIMER_BIT_WIDTH_8) ? ocrb8bit : ocrb16bit, data, 8);
	if (ret < 1)
    {
        return F_FAIL;
    }

    timerset->ocrb = (uint16_t) atoi ( (const char *) data );

    if (bw == TIMER_BIT_WIDTH_16)
    {
        memset(data, 0, 8);
        ret = draw_input_dialog(file, 10, 10, ocrc16bit, data, 8);
        if (ret < 1)
        {
            return F_FAIL;
        }

        timerset->ocrc = (uint16_t) atoi ( (const char *) data );

        memset(data, 0, 8);
        ret = draw_input_dialog(file, 10, 10, icr16bit, data, 8);
        if (ret < 1)
        {
            return F_FAIL;
        }

        timerset->icr = (uint16_t) atoi ( (const char *) data );
    }
    else
    {
        timerset->ocrc = 0;
        timerset->icr = 0;
    }
	return F_OK;
}

static int8_t
__ui_vals_change(FILE * file, uint8_t bw, uint16_t * ocra, uint16_t * ocrb, uint16_t * ocrc)
{
    int ret = 0;
    uint8_t data[8];
    memset(data, 0, 6);
	ret = draw_input_dialog(file, 10, 10, (bw == TIMER_BIT_WIDTH_8) ? ocra8bit : ocra16bit, data, 8);
	if (ret < 1)
    {
        return F_FAIL;
    }

    *ocra = (uint16_t) atoi ( (const char *) data );

    memset(data, 0, 8);
	ret = draw_input_dialog(file, 10, 10, (bw == TIMER_BIT_WIDTH_8) ? ocrb8bit : ocrb16bit, data, 8);
	if (ret < 1)
    {
        return F_FAIL;
    }

    *ocrb = (uint16_t) atoi ( (const char *) data );

    if (bw == TIMER_BIT_WIDTH_16)
    {
        memset(data, 0, 8);
        ret = draw_input_dialog(file, 10, 10, ocrc16bit, data, 8);
        if (ret < 1)
        {
            return F_FAIL;
        }

        *ocrc = (uint16_t) atoi ( (const char *) data );

        /*memset(data, 0, 8);
        ret = draw_input_dialog(10, 10, icr16bit, data, 8);
        if (ret < 1)
        {
            return F_FAIL;
        }

        *icr = (uint16_t) atoi ( (const char *) data );*/
    }
    else
    {
        *ocrc = 0;
    }

	return F_OK;
}

static int8_t
__ui_query_info(FILE * file, uint8_t * opt)
{
    int ret = 0;
	int res = 0;
	ret = runmenu0_s(file, 10, 10, 0, 10, vt_options_captions_struct, &res);
	//try to create fd for the device
	if (ret == UI_NOTHING)
	{
		return F_FAIL;
	}
	*opt = (uint8_t) res;

	return F_OK;
}

int8_t vt_attach_timer(FILE * file, struct timer_setup * timerset)
{
	int8_t res = 0;
	res = __ui_query(file, timerset);
	ASSERT_DATA(res != F_OK, return F_FAIL);

	return F_OK;
}

int8_t vt_modify_timer(FILE * file, struct timer_setup * timerset)
{

	/*int8_t res = 0;
	enum adc_channel_selector _e_adc_ch_sel;
	enum adc_ref e_ref_src;
	enum adc_presc _e_adc_clock;
	uint8_t flags;

	res = __ui_query_modify(&_e_adc_ch_sel, &e_ref_src, &_e_adc_clock, &flags);
	ASSERT_DATA(res != F_OK, return F_FAIL);

	res = adc_enable_channel(_e_adc_ch_sel, e_ref_src, _e_adc_clock, flags, NULL);

	ASSERT_DATA(res == -1, return F_FAIL);*/

	return F_OK;
}

int8_t vt_info_timer(FILE * file, struct dev_descr_node * dd)
{
    uint8_t opt = 0;
    int8_t res = 0;

    res = __ui_query_info(file, &opt);
	ASSERT_DATA(res != F_OK, return F_FAIL);

    switch (opt)
    {
        case VT_INFO_OPTS_CHANGEVAL:
        {
            uint16_t ocra;
            uint16_t ocrb;
            uint16_t ocrc;

            struct timer_rec * tr = timer_get_by_did(dd->dev_id);
            if (tr == NULL)
            {
                runerrord1(file, 10, 10, 30, error_capt, PSTR("did %d timer rec not found!"), dd->dev_id);
                return F_FAIL;
            }
            res = __ui_vals_change(file, tr->counter_bit_width, &ocra, &ocrb, &ocrc);
            ASSERT_DATA(res != F_OK, return F_FAIL);

            res = timer_update_ocrs(dd->instance, ocra, ocrb, ocrc);
            ASSERT_DATA(res == 1, return F_FAIL);
        }
        break;
    }



	return F_OK;
}
