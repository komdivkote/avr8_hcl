/*-
 *	Copyright (c) 2014-2016	Alexander Morozov. www.nixd.org
 *	All rights reserved.
 *
 * LICENSE: This source file is subject to the New BSD license that is
 * available through the world-wide-web at the following URI:
 * http://www.opensource.org/licenses/bsd-license.php or in the root folder
 * of the project in file LICENSE.TXT.
 */

#include "../../../hal/build_defs.h"
#include <avr/io.h>
#include <avr/pgmspace.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include "../../../hal/hal.h"
#include "../../../hal/drivers/input_scanner/inpscan.h"
#include "../../../hal/handlers.h"
#include "../virtual_terminal.h"
#include "../../../lib/conio/conio.h"
#include "../../../lib/conio/kbd.h"
#include "../../../lib/conio/ui.h"
#include "../vt_text.h"
#include "vt_attach.h"


static int8_t __ui_query_info(FILE * file, uint8_t * opt);

const struct menudialog_entry vt_inpscan_options_captions_struct[] PROGMEM = {
	{
	    add_slave, VT_INFO_OPTS_ADD
	},
	{
		P_NULL, NULL
	}
};

static int8_t
__ui_query_info(FILE * file, uint8_t * opt)
{
    int ret = 0;
	int res = 0;
	ret = runmenu0_s(file, 10, 10, 0, 10, vt_inpscan_options_captions_struct, &res);
	//try to create fd for the device
	if (ret == UI_NOTHING)
	{
		return F_FAIL;
	}
	*opt = (uint8_t) res;

	return F_OK;
}

int8_t vt_attach_inpscan(FILE * file, struct dim_instance_config * dis)
{
    char data[16];
    char ip[4] = {0,0,0,0};
    memset(data, 0, 16);
    int16_t ret = draw_input_dialog(file, 10, 10, netdev_pan_netid, data, 4);
    if (ret < 1)
    {
        runerrord0(file, 3,3, error_capt, length_assert);
        return F_FAIL;
    }

    dis->netdev = (uint8_t) atoi ( (const char *) data );

    struct knetdevice * knd = hal_get_knetdev(dis->netdev);
    if (dis->netdev == NULL)
    {
        runerrord0(file, 3,3, error_capt, netdev_pan_notfound);
        return F_FAIL;
    }

    memset(data, 0, 16);
    ret = draw_input_dialog(file, 10, 10, inp_ip_addr, data, 16);
    if (ret < 1)
    {
        runerrord0(file, 3,3, error_capt, length_assert);
        return F_FAIL;
    }

    __convert_ip(data, ip);
    dis->ip_to = *(uint32_t*)ip;

    memset(data, 0, 16);
    ret = draw_input_dialog(file, 10, 10, label_netstat_lis_lport, data, 7);
    if (ret < 1)
    {
        runerrord0(file, 3,3, error_capt, length_assert);
        return F_FAIL;
    }

    dis->port_to = (uint16_t) atoi ( (const char *) data );

    return F_OK;
}

int8_t vt_modify_inpscan(FILE * file, struct dev_descr_node * dd)
{
    return F_FAIL;
}

int8_t vt_info_inpscan(FILE * file, struct dev_descr_node * dd)
{
    uint8_t opt = 0;
    int8_t res = 0;
    int8_t ret = 0;

    res = __ui_query_info(file, &opt);
	ASSERT_DATA(res != F_OK, return F_FAIL);

    switch (opt)
    {
        case VT_INFO_OPTS_ADD:
        {
            char data[4];
            ret = draw_input_dialog(file, 10, 10, io_sel_id_mon, data, 4);
            if (ret < 1)
            {
                runerrord0(file, 3,3, error_capt, length_assert);
                return F_FAIL;
            }

            t_dev_id iodid = (t_dev_id) atoi ( (const char *) data );

            if (inpscan_listen(dd, iodid) != 0)
            {
                return F_FAIL;
            }
        }
        break;
    }
    return F_OK;
}
