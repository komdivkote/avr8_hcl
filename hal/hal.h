#ifndef HAL_H_INCLUDED
#define HAL_H_INCLUDED

#define KERNEL_VERSION 20160830

#include "./build_defs.h"
#include "./htypes.h"
#include "./extlib/e_enum.h"
#include "./shared.h"
#include "./device/dev.h"
#include "./error.h"
#include "lib/buffer/buffer.h"

#define MAGIC_DWORD 0x34AACC11
#define EEPROM_MAX_SECTION 20

/* Base events which must be handled by modules */
enum dd_event
{
    DD_EVENT_ATTACH     = (uint8_t) 0,
	DD_EVENT_DETACH     = (uint8_t) 1,
	DD_EVENT_UPDATE     = (uint8_t) 2,
	DD_DUMP             = (uint8_t) 3,
	DD_RESTORE          = (uint8_t) 4,
	DD_QUERY            = (uint8_t) 5,
	DD_ISRSIGNAL        = (uint8_t) 6,
	DD_RESET            = (uint8_t) 7,
	DD_DROP             = (uint8_t) 8,
	DD_FLUSH            = (uint8_t) 9
};

#include "handlers.h"

struct knetstate;
struct knetdevice;

//used for eeprom storage
struct dev_descr_node_envilope
{
	enum dev_descr_code e_dev_descr;
	enum dev_desc_handlers e_dh_code;
	t_dev_id dev_id;
};

/*A record of the node. */
struct dev_descr_node
{
	t_dev_id dev_id;                            //device unique number
	enum dev_descr_code e_dev_descr;            //device driver
	enum dev_desc_handlers e_dh_code;           //device handler i.e (NULL_DEVICE is driver)
    void * instance;                            //pointer to the dev instance
    struct dd_handlers * prgm_handlers;         //in PROGMEM
};

struct dd_list
{
	struct dev_descr_node * node;

	struct dd_list *next;
};

struct hw_port_bind_rec
{
	t_dev_id idev;           //owner
	enum ports nport;     //port
	uint8_t pins;           //i.e 0b00001111 <- last 4 pins busy fy fd
};

struct hw_bind_list
{
	struct hw_port_bind_rec * node;
	struct hw_bind_list * next;
};

/*-------NETWORKING--------*/
/*
 * The structures below is an attempt to standatize the
 *  control and data exchange between MCU and network boards.
 *  As different boards uses various implementation of network stacks,
 *  and data control it was decided create an software layer which will
 *  allow to use standart methods to work with boards.
 */
LIST_STRUCT(knetdevice_list, knetdevice);
LIST_STRUCT(knetstate_list, knetstate);
LIST_STRUCT(knetlisten_list, knetlisten);

#define FOREACH_KNETPROTO(DV)\
    DV(KNET_ROOT, (uint8_t) 0)\
    DV(KNET_ETH, (uint8_t) 1)\
    DV(KNET_IP, (uint8_t) 2)\
    DV(KNET_ICMP, (uint8_t) 3)\
    DV(KNET_TCP, (uint8_t) 4)\
    DV(KNET_UDP, (uint8_t) 5)

enum knet_proto
{
    FOREACH_KNETPROTO(GENERATE_ENUM_VAL)
};

#define FOREACH_KNETCONECTION(DV)\
    DV(KNETCON_NEW, (uint8_t) 0)\
    DV(KNETCON_REM, (uint8_t) 1)

enum knet_newconn_opts
{
    FOREACH_KNETCONECTION(GENERATE_ENUM_VAL)
};

struct kern_nic_setup
{
    t_net_addr ipv4;                  //IP address
    t_net_addr ipv4_mask;             //MASK
    t_net_addr ipv4_gateway;          //Gateway IP
};

struct knetstate
{
    t_state_id kstate_id;           //socket unique id (if -1 then single state)
    enum knet_proto e_knet_proto;   //protocol
    t_net_addr laddr;                 //local address
    t_net_addr raddr;                 //remote address
    t_net_port lport;                 //local port
    t_net_port rport;                 //remote port

    void * sock_opts;               //i.e tcp_state (depends on proto used)

};

/*server*/
struct knetlisten_handlers
{
    //on receive
    int8_t (*ONRCV)(struct knetdevice * kndev,
                    struct knetstate * state,
                    uint8_t *frame,
                    uint16_t len);

    //for tcp only, for rest is pointing to DUMMY functions
    //the connection was established
    int8_t (*ONOPEN)(struct knetdevice * kndev,
                     const struct knetstate * knetstate);
    //the connection was closed
    int8_t (*CLOSED)(struct knetdevice * kndev,
                     const struct knetstate * knetstate);
};

struct knetdev_handlers
{
    //send signal to driver that socket was opened
    int8_t (*MOD_CONNECTION)(enum knet_newconn_opts kconnopts,
                             struct knetdevice * k_net_dev,
                             struct knetstate * k_net_state
                            );

    //pointer to device polling function
    //if poll returns -1 error, 0 nothing or >0 data received
    int8_t (*POLL_DEVICE)(struct knetdevice * k_net_dev);


    //push data to transmitter
    int32_t (*TRANSMIT)(struct knetdevice * k_net_dev,
                       void * tx_buf_ptr,
                       uint16_t len);
};

struct knetlisten
{
    t_listen_id k_listen;               //id of the service
    enum knet_proto e_knet_proto;       //protocol
    t_net_addr laddr;                   //listen address
    t_net_port lport;                   //listen port
    struct knetlisten_handlers hndls;
};
//                     T->TCP - state
//POLL -> RCV -> PARSE -->UDP - temp state
//                     L->ARP/ICMP -> answer
#include "lib/networking/hal_net_stack.h"
struct knetdevice
{
    t_netdev_id net_dev_id;                     //network device alias
    struct dev_descr_node * ddn;                //network device
    struct kern_nic_setup nic_set;              //network interface setup
    void * driver_nic_stuct;                    //pointer to the driver record for NIC to accelerate polling
    uint8_t * mac;                              //pointer to mac address
    struct arp_cache_entry arp[ARP_CACHE_SIZE]; //ARP cache
    uint8_t rx_buf[NETWORK_MAX_FRAMELEN];       //RX buffer space
    struct buffer * b_rx_buf;                   //RX buffer access helper
    struct knetstate * rx_state;        //RX state
    uint8_t tx_buf[NETWORK_MAX_FRAMELEN];       //TX buffer
    struct buffer * b_tx_buf;                   //TX buffer access space
    struct kproc_mutex * m_tx;                  //TX buffer mutex
    struct kproc_mutex * m_rx;                  //RX buffer mutex

#if (NETWORK_COUNT_TRAFFIC == YES)
    uint32_t in_bytes;  // [4]
    uint32_t out_bytes; // [4] = 8 bytes RAM
#endif // NETWORK_COUNT_TRAFFIC

    struct knetdev_handlers hndls;
};

enum hal_net_rcv
{
    HAL_NRCV_NOBLOCK = (uint8_t) 1,
    HAL_NRCV_BLOCK = (uint8_t) 2
};

//giant locks public
extern volatile struct kproc_mutex * mtx_giant_io; //giant lock of the IO read/write


uint8_t ret_mcusr();
void hal_run_debug_serial();

/* Entry point */
void hal_init();

void hal_postinit();

/*
 * kernel_get_max_devs
 * returns the highest fid which is also indicates the max amount of fd
 */
t_dev_count hal_get_max_devs();

/*
 * hal_dev_node_create - this function creates a new record in device nodes table and
 *  calls the ATTACH_EVENT for specified device driver or device handler
 * e_dev_code (enum dev_descr_code) the device code number
 * e_dev_hndl (enum dev_desc_handlers) the handler for the device
 * data0    (void *) pointer to the data which is used by driver/handler for node init
 * data0_len (size_t) length of the data0 agrument data which is referenced
 * returns a valid pointer to the record or NULL
 */
struct dev_descr_node * hal_dev_node_create(t_dev_id tid,
                                            enum dev_descr_code e_dev_code,
                                            enum dev_desc_handlers e_dev_hndl,
                                            void * data0,
                                            size_t data0_len);

/*
 * kernel_fd_update
 * sends FD_UPDATE signal to the device
 */
t_dev_id hal_dev_node_update(struct dev_descr_node * node, void * data0, size_t * data0_len);

/*
 * kernel_fd_query
 * sends FD_QUERY to the device
 */
enum hal_query_opcode
{
 HQO_INFO_TO_STDOUT = (uint8_t) 1,
 HQO_INFO_SPECIAL   = (uint8_t) 2
};

struct hal_query_info
{
    enum hal_query_opcode e_hqo;
    void * data0;
    size_t * data0_size;
    void * data1;
    size_t * data1_size;
};
t_dev_id hal_dev_node_query(struct dev_descr_node * node, struct hal_query_info * query);

/*enum hal_flush_flag
{
    HAL_FLUSH_OUTPUT    = (uint8_t) 1,
    HAL_FLUSH_INPUT     = (uint8_t) 2,
    HAL_FLUSH_BOTH      = (uint8_t) 3,
    HAL_FLUSH_OUT_NUM   = (uint8_t) 4,
    HAL_FLUSH_IN_NUM    = (uint8_t) 5
};

struct hal_flush
{
    enum hal_flush_flag e_hflush;
    uint8_t buf_id;
};*/

t_dev_id hal_dev_node_flush(struct dev_descr_node * node);//, struct hal_flush * hf);

/*
 * kernel_fd_destroy
 * removes the device from device table
 */
t_dev_id hal_dev_node_destroy(struct dev_descr_node * node);

/*
 * kernel_fd_get
 * returnes pointer to instance of the requested fd
 */
struct dev_descr_node * hal_node_get(t_dev_id dev_id);

/*
 * kernel_fd_mem_cpy
 * this function will copy data from record table the record with
 * offset to the valid fd_dev_node pointer
 * return 1 if error and 0 if ok
 */
int8_t hal_dd_mem_cpy(t_dev_id offs, struct dev_descr_node * fdl);

/*
 * kernel_fd_get_total
 * returns amount of records in the fd table
 *  this function is going through the list and counting
 */
t_dev_count hal_fd_get_total();

/*
 * hardware binds
 * controlling the hardware
 */

 /*
  * kernel_hw_bind_pin
  * this function is trying to set (port_addr and pins_mask) is bind by fd.
  *     if requested port and masks are free then record is created or modified
  *     and returned 0. If error happened returned 1 then error is recorded to
  *     the journal.
  */
uint8_t hal_hw_bind_pin(t_dev_id did, enum ports nport, uint8_t pins);

/*
 * kernel_hw_unbind_pin
 * this function removes bind from set arguments (port_addr and pins).
 *  if fd does not own provided (port_addr and pins) then nothing will
 *  happen and returned 1.
 */
uint8_t hal_hw_unbind_pin(t_dev_id did, enum ports nport, uint8_t pins);

/*
 * kernel_hw_unbind_fd_all_pin
 * all allocated port pins will be deallocated for specified fd
 */
uint8_t hal_hw_unbind_dd_all_pin(t_dev_id did);

/*
 * kernel_hw_check_available
 * checks the port pins for availability
 * return 0 if free and 1 if busy -1 if list empty
 */
int8_t hal_hw_check_available(enum ports nport, uint8_t pins);

/*
 * kernel_hw_pin_owner
 * this functions returns the type of fd which allocated privided as arguments
 *  port_id and pin_mask
 */
t_dev_id hal_hw_pin_owner(enum ports nport, uint8_t pins);

/*
 * kernel_hw_count_recs
 * returns the amount of records in the table
 */
uint8_t hal_hw_count_recs();

/*
 * kernel_hw_mem_cpy
 * this function copies hw_port_bind_rec record specified by offset
 *  from first record.
 */
int8_t hal_hw_mem_cpy(uint8_t offs, struct hw_port_bind_rec * hwn);


void hal_write_reg8(uint16_t addr, uint8_t data);
void hal_write_reg16(uint16_t addr, uint16_t data);
uint8_t hal_read_reg8(uint16_t addr);
uint16_t hal_read_reg16(uint16_t addr);
#define _HAL_RREG8(__ADDR__) hal_read_reg8(__ADDR__)
#define _HAL_RREG16(__ADDR__) hal_read_reg16(__ADDR__)
#define _HAL_WREG8(__ADDR__, __DATA__) hal_write_reg8(__ADDR__, __DATA__)
#define _HAL_WREG16(__ADDR__, __DATA__) hal_write_reg16(__ADDR__, __DATA__)
uint16_t hal_hw_get_addr_ddr(enum ports port);
uint16_t hal_hw_get_addr_pin(enum ports port);
uint16_t hal_hw_get_addr_port(enum ports port);



void hal_clean_eeprom();
int8_t hal_has_configuration();
int8_t hal_load_config();
int8_t hal_dump_config(FILE * file,
                       void (*CALLBACK(FILE *, t_dev_count)),
                       void (*CALLBACK_ERROR(FILE *, t_dev_count)));
t_dev_count hal_read_eeprom_recs_am();
int8_t hal_read_eeprom_offset(t_dev_count offset, struct dev_descr_node_envilope * envlp);
uint16_t hal_eeprom_read_magic();



/*************FILE***********************/
enum hal_file_perm
{
    FOPEN_R = (uint8_t) 1,
    FOPEN_W = (uint8_t) 2,
    FOPEN_RW = (uint8_t) 3
};

struct hal_stream_envelope
{
    t_stream_id sid;                    //stream id
    struct dev_descr_node * dest;       //destination
    FILE * node;                        //pointer to the record
};

FILE * hal_dopen(const struct dev_descr_node * ddest, enum hal_file_perm e_perm, struct knetstate * state);
int8_t hal_dclose(FILE * file);
int8_t hal_dupdate(FILE * file, struct dev_descr_node * ddest);
int8_t hal_cpy_stream_mem(t_stream_id offset, struct hal_stream_envelope * env);

int8_t hal_attach_kstream(FILE * file, struct dev_descr_node * node);

//file.c
void fileflush(FILE * file);
struct dev_descr_node * filedest(FILE * file);

/*************networking*****************/
int8_t hal_init_network_stack();

struct knetdevice *
hal_net_add_dd(struct dev_descr_node * dd,
               void * driver_nic_set,
               uint8_t * mac_addr,
               struct knetdev_handlers * hndlr);
#define hal_add_nic hal_net_add_fd

struct knetdevice * hal_get_knetdev(t_netdev_id knid);
#define hal_get_nic hal_get_knetdev
struct knetdevice * hal_get_knetev_by_did(t_dev_id did);

int8_t hal_net_remove_dd(struct knetdevice * net_dev);

struct knetdevice_list * hal_net_mem_knetdev();
struct knetlisten_list * hal_net_mem_knetlisten();
struct knetstate_list * hal_net_mem_knetstate();

/*#define hal_remove_nic hal_net_remove_fd
t_listen_id hal_open_server_socket(t_net_addr listen_ip,
                                   t_net_port listen_port,
                                   enum knet_proto listen_proto,
                                   struct knetlisten_handlers * hndlr);
#define knet_slisten hal_open_server_socket    //alias*/

/*t_listen_id hal_open_client_socket(t_net_addr remote_ip, t_net_port remote_port, enum knet_proto e_proto);
#define hal_sconnect hal_open_client_socket   //alias*/

//t_state_id hal_get_state_by_destip(t_net_addr remote_ip);
const struct knetlisten * hal_get_listen(t_net_addr local_ip, t_net_port loc_net_port, enum knet_proto e_proto);
const struct knetlisten * hal_get_listen_by_state(struct knetstate * state);
//const struct knetlisten * hal_get_listen_for_udp(t_net_addr local_ip, t_net_port local_port);
//const struct knetsocket * kernel_get_socket_source(uint32_t local_ip, uint16_t loc_net_port, enum knet_proto e_proto);
struct knetlisten *
hal_net_listen_socket(enum knet_proto e_knet_proto, t_net_addr laddr, t_net_port lport, struct knetlisten_handlers * hndls);

struct knetstate * hal_request_new_state();
int8_t hal_net_remove_state(struct knetstate * state);
const struct knetstate * hal_get_state(t_net_addr raddr, t_net_port rport, t_net_port lport);
int8_t hal_net_closeport(t_listen_id sock_id);
int8_t hal_send_dgram(struct knetdevice * k_net_dev,
                      uint32_t destIP,
                      uint16_t destPORT,
                      uint16_t sourcePORT,
                      enum knet_proto e_proto,
                      uint8_t * buff,
                      uint16_t len);
int8_t hal_net_receive(struct knetdevice * knetd,
                        struct knetlisten * knetl,
                        uint8_t ** data_out,
                        uint16_t * data_out_len,
                        enum hal_net_rcv e_net_rcv);
int8_t hal_net_send(struct knetdevice * knetd, struct knetstate * state, uint8_t * buffer, uint16_t len);
uint8_t * hal_net_grab_tx_buffer(struct knetdevice * knetd, enum knet_proto proto);
int8_t hal_net_relese_tx_buffer(struct knetdevice * knetd);
uint8_t * hal_net_buffer_payload(uint8_t * buff, enum knet_proto proto);

int8_t hal_net_release_rx(struct knetdevice * knetd);
int8_t hal_net_onrcv_release_rx(struct knetdevice * knetd);

#endif // HAL_H_INCLUDED
