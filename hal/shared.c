/*-
 *	Copyright (c) 2014-2016	Alexander Morozov. www.nixd.org
 *	All rights reserved.
 *
 * LICENSE: This source file is subject to the New BSD license that is
 * available through the world-wide-web at the following URI:
 * http://www.opensource.org/licenses/bsd-license.php or in the root folder
 * of the project in file LICENSE.TXT.
 */

#include "build_defs.h"
#include <avr/io.h>
#include <avr/pgmspace.h>
#include "shared.h"

const char P_NULL[] PROGMEM = "\0";
