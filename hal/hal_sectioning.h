/*-
 *	Copyright (c) 2014-2016	Alexander Morozov. www.nixd.org
 *	All rights reserved.
 *
 * LICENSE: This source file is subject to the New BSD license that is
 * available through the world-wide-web at the following URI:
 * http://www.opensource.org/licenses/bsd-license.php or in the root folder
 * of the project in file LICENSE.TXT.
 */


#ifndef HAL_SECTIONING_H_
#define HAL_SECTIONING_H_

#define STR(x) #x
#define HAL_SECTION	.text.hal
#define ATTRIBUTE_HAL_SECTION __attribute__ ((section (STR(HAL_SECTION))))
#define ASM_HAL_SECTION .section HAL_SECTION, "ax", @progbits


#endif /* KERNEL_SECTIONING_H_ */
