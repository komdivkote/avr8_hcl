/*-
 *	Copyright (c) 2014-2016	Alexander Morozov. NiXD ORG
 *	All rights reserved.
 *
 * LICENSE: This source file is subject to the New BSD license that is
 * available through the world-wide-web at the following URI:
 * http://www.opensource.org/licenses/bsd-license.php or in the root folder
 * of the project in file LICENSE.TXT
 */

#include "../../../hal/build_defs.h"
#include <avr/io.h>
#include <avr/pgmspace.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include "../../../hal/hal.h"
#include "../../../hal/drivers/io/io.h"
#include "../../../hal/handlers.h"
#include "../virtual_terminal.h"
#include "../../../lib/conio/conio.h"
#include "../../../lib/conio/kbd.h"
#include "../../../lib/conio/ui.h"
#include "../vt_text.h"
#include "vt_attach.h"

static int8_t __ui_query(FILE * file, struct io_setup * ios);
static int8_t __ui_query_info(FILE * file, uint8_t * opt);
static int8_t __ui_query_input(FILE * file, uint8_t * val);



FOREACH_IO_PORT_DIR(GENERATE_VARS_VAL);
const struct menudialog_entry vt_io_ports_dir_captions_struct[] PROGMEM = {
	FOREACH_IO_PORT_DIR(GENERATE_VAR_STRUCT)
	{
		P_NULL, NULL
	}
};

const struct menudialog_entry vt_io_options_captions_struct[] PROGMEM = {
	{
	    value_capt, VT_INFO_OPTS_CHANGEVAL
	},
	{
		P_NULL, NULL
	}
};

static int8_t
__ui_query(FILE * file, struct io_setup * ios)
{
	int ret = 0;
	int res = 0;

	ret = runmenu0_s(file, 10, 10, 0, 10, vt_io_ports_captions_struct, &res);
	//try to create fd for the device
	if (ret == UI_NOTHING)
	{
		return F_FAIL;
	}
	ios->e_port = (uint8_t) res;

	ret = runmenu0_s(file, 12, 10, 0, 10, vt_io_ports_dir_captions_struct, &res);
	//try to create fd for the device
	if (ret == UI_NOTHING)
	{
		return F_FAIL;
	}
	ios->e_io_dir = (uint8_t) res;

    uint8_t data[5];
    memset(data, 0, 5);
	ret = draw_input_dialog(file, 10, 10, io_mask_input_capt, data, 5);
	if (ret < 1)
    {
        return F_FAIL;
    }

    ios->bit_mask = (uint16_t) atoi ( (const char *) data );

	return F_OK;
}

static int8_t
__ui_query_input(FILE * file, uint8_t * val)
{
	int ret = 0;
    uint8_t data[5];
    memset(data, 0, 5);
	ret = draw_input_dialog(file, 10, 10, io_mask_input_capt, data, 5);
	if (ret < 1)
    {
        return F_FAIL;
    }

    *val = (uint16_t) atoi ( (const char *) data );

	return F_OK;
}

static int8_t
__ui_query_info(FILE * file, uint8_t * opt)
{
    int ret = 0;
	int res = 0;
	ret = runmenu0_s(file, 10, 10, 0, 10, vt_io_options_captions_struct, &res);
	//try to create fd for the device
	if (ret == UI_NOTHING)
	{
		return F_FAIL;
	}
	*opt = (uint8_t) res;

	return F_OK;
}

int8_t vt_attach_io(FILE * file, struct io_setup * ios)
{
	int8_t res = 0;

	res = __ui_query(file, ios);
	ASSERT_DATA(res != F_OK, return F_FAIL);

	return F_OK;
}

int8_t vt_modify_io(FILE * file, struct dev_descr_node * dd)
{

	/*int8_t res = 0;
	enum adc_channel_selector _e_adc_ch_sel;
	enum adc_ref e_ref_src;
	enum adc_presc _e_adc_clock;
	uint8_t flags;

	res = __ui_query_modify(&_e_adc_ch_sel, &e_ref_src, &_e_adc_clock, &flags);
	ASSERT_DATA(res != F_OK, return F_FAIL);

	res = adc_enable_channel(_e_adc_ch_sel, e_ref_src, _e_adc_clock, flags, NULL);

	ASSERT_DATA(res == -1, return F_FAIL);*/

	return F_OK;
}

int8_t vt_info_io(FILE * file, struct dev_descr_node * dd)
{
    uint8_t opt = 0;
    int8_t res = 0;

    res = __ui_query_info(file, &opt);
	ASSERT_DATA(res != F_OK, return F_FAIL);

    switch (opt)
    {

        case VT_INFO_OPTS_CHANGEVAL:
        {
            //we can use dd->instance but we need to make sure that record presents
            struct io_null_dev * iod = hal_node_get(dd->dev_id)->instance;
            if (iod == NULL)
            {
                runerrord1(file, 10, 10, 30, error_capt, PSTR("did %d io rec not found!"), dd->dev_id);
                return F_FAIL;
            }
            /*if (iod->e_io_dir == IO_PORT_DIR_OUT)
            {*/
                //show dialog with input
                uint8_t mask = 0;
                res = __ui_query_input(file, &mask);
                ASSERT_DATA(res != F_OK, return F_FAIL);

                res = io_write8_direct(iod, io_mask_prepare(iod, mask));
                ASSERT_DATA(res == 1, return F_FAIL);
            //}
            //ToDo: complete this part

        }
        break;
    }



	return F_OK;
}
