#ifndef ONEWIRE_H_
#define ONEWIRE_H_

/*******************************************/
/* Hardware connection                     */
/*******************************************/

/* Define OW_ONE_BUS if only one 1-Wire-Bus is used
   in the application -> shorter code.
   If not defined make sure to call ow_set_bus() before using
   a bus. Runtime bus-select increases code size by around 300
   bytes so use OW_ONE_BUS if possible */
// #define OW_ONE_BUS


#if ( F_CPU < 1843200 )
#warning | Experimental multi-bus-mode is not tested for
#warning | frequencies below 1,84MHz. Use OW_ONE_WIRE or
#warning | faster clock-source (i.e. internal 2MHz R/C-Osc.).
#endif

#define OW_CONF_DELAYOFFSET 4 //0 <- modified
//#define OW_CONF_CYCLESPERACCESS 13
//#define OW_CONF_DELAYOFFSET ( (uint16_t)( ((OW_CONF_CYCLESPERACCESS) * 1000000L) / F_CPU ) )


// Recovery time (T_Rec) minimum 1usec - increase for long lines
// 5 usecs is a value give in some Maxim AppNotes
// 30u secs seem to be reliable for longer lines
//#define OW_RECOVERY_TIME        5  /* usec */
//#define OW_RECOVERY_TIME      300 /* usec */
//#define OW_RECOVERY_TIME         10 /* usec */
#define OW_RECOVERY_TIME         20 /* usec */ //<--changed

// Use AVR's internal pull-up resistor instead of external 4,7k resistor.
// Based on information from Sascha Schade. Experimental but worked in tests
// with one DS18B20 and one DS18S20 on a rather short bus (60cm), where both
// sensores have been parasite-powered.
#define OW_USE_INTERNAL_PULLUP     0  /* 0=external, 1=internal */

/*******************************************/


struct ow_bus_rec
{
	uint16_t ow_ddr;
	uint16_t ow_port;
	uint16_t ow_in;
	uint8_t ow_pin;
};

#define OW_MATCH_ROM    0x55
#define OW_SKIP_ROM     0xCC
#define OW_SEARCH_ROM   0xF0

#define OW_SEARCH_FIRST 0xFF        // start new search
#define OW_PRESENCE_ERR 0xFF
#define OW_DATA_ERR     0xFE
#define OW_LAST_DEVICE  0x00        // last device found

extern uint8_t ow_reset(struct ow_bus_rec * owbr);

extern uint8_t ow_bit_io(struct ow_bus_rec * owbr, uint8_t b ); //COOP+CS
extern uint8_t ow_byte_wr(struct ow_bus_rec * owbr, uint8_t b ); // PROT->ow_bit_io->ow_bit_io_intern
extern uint8_t ow_byte_rd( struct ow_bus_rec * owbr ); //PROT -> ow_byte_wr

extern uint8_t ow_rom_search(struct ow_bus_rec * owbr, uint8_t diff, uint8_t *id ); //PROT->ow_bit_io & other

extern void ow_command(struct ow_bus_rec * owbr, uint8_t command, uint8_t *id );    //PROT ->ow_command_intern
extern void ow_command_with_parasite_enable(struct ow_bus_rec * owbr, uint8_t command, uint8_t *id ); //PROT ->ow_command_intern

extern void ow_parasite_enable( struct ow_bus_rec * owbr );
extern void ow_parasite_disable( struct ow_bus_rec * owbr );
extern uint8_t ow_input_pin_state( struct ow_bus_rec * owbr );


//extern void ow_set_bus(uint16_t pin, uint16_t port, uint16_t ddr, uint8_t pin_mask);


#endif
