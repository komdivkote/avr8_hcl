/*-
 *	Copyright (c) 2014-2015	Alexander Morozov. NiXD ORG
 *	All rights reserved.
 *
 * LICENSE: This source file is subject to the New BSD license that is
 * available through the world-wide-web at the following URI:
 * http://www.opensource.org/licenses/bsd-license.php or in the root folder
 * of the project in file LICENSE.TXT.
 */
/*
 *  build_defs.h
 *  DSC project parameters.
 *  Details: this file contains macroses which is used to control
 *  settings of the project and code to include in the firmware.
 */


#ifndef BUILD_DEFS_H_
#define BUILD_DEFS_H_

#include "../os/kernel/kernel_preproc_defs.h"

#define YES 1
#define NO 0

#define ACCEPT_TERMS_NEW_BSD_LICENSE YES
#define ACCEPT_TERMS_GNU_GPL_V2_LICENSE YES

#if (ACCEPT_TERMS_NEW_BSD_LICENSE == NO)
    #error Please read LICENSE.TXT, then go to build_defs.h and accept new_bsd_license.
#endif

#if (ACCEPT_TERMS_GNU_GPL_V2_LICENSE == NO)
    #warning The project is compiled without drivers/modules which were shared under GPL v2 license!
#endif

#define FIRMWARE_VER 1
#define FIRMWARE_STABLE NO

#define DEBUG_MESSAGES NO

#if (FIRMWARE_STABLE == NO)
    #warning You are using not stable version of the DSC!
#endif

#define F_CPU 16000000UL
#define F_OSC F_CPU
#define HEAP_ENDS_AT 7000//5600

#define EEPROM_MAGIC 0x3468
#define EEPROM_DEV_MEM_ALLOC 25

//------------NETWORK HAL---------------

#define NETWORKING_HIGH_MEM NO

#define NET_MAC_ADDR_SIZE       6
#define NETWORK_COUNT_TRAFFIC   YES
#define NETWORK_MAX_FRAMELEN	1518

#define WITH_ICMP               YES
#define WITH_DHCP               YES
#define WITH_UDP                YES
#define WITH_TCP                YES
#if (WITH_TCP == YES)
#   define WITH_TCP_REXMIT      NO //not available
#endif
#define WITH_ROUTING            NO //not yet implemented
#define WITH_IGMP               NO //not yet implemented

#define MAX_NIC_DEVS            7
#define ARP_CACHE_SIZE          5   //for each Net board
#define IP_PACKET_TTL           64
#define TCP_MAX_CONNECTIONS		4
#define TCP_WINDOW_SIZE			65535
#define TCP_SYN_MSS				512
#if WITH_TCP_REXMIT == YES
#   error TCP (WITH_TCP_REXMIT) rexmit is not available.
#   define TCP_REXMIT_TIMEOUT	1000
#   define TCP_REXMIT_LIMIT		5
#else
#	define TCP_CONN_TIMEOUT		2500
#endif

#define TABITA_PORT 20000

//--------------------------------------

#define KERNEL_USE_FAST_DEVCOUNT NO

#define HAL_HW_USE_RESET_PIN NO
#if HAL_HW_USE_RESET_PIN == YES
	#define HAL_HW_RESET_TRIGGER_DDR
	#define HAL_HW_RESET_TRIGGER_PORT
	#define HAL_HW_RESET_TRIGGER_PIN
#endif
#define HAL_HW_USE_WATCHDOG_RESET NO
#define HAL_HW_USE_SOFTWARE_RESET YES

//the reset pin can be used for instanc to clean eeprom or force to run in static mode
#define HAL_HW_USE_RESET_FOR_MODE_SWITCHING NO

#define BUILD_WITH_U2ETH YES

#define BUILD_WITH_SERIAL YES
    #define SERAIL_STDIO_RW_DEVICE_NUMBER 0         //the DID of the device which is used for communication
    #define SERIAL_BUILD_WITH_TEXT_QUERY YES
    #define SERIAL_BUILD_WITH_BYTES_COUNTER YES
    #define BUILD_WITH_TOP YES
    #define BUILD_WITH_SRAM_VIEWER YES

#define BUILD_WITH_IO YES
    #define IO_BUILD_WITH_TEXT_QUERY YES

#define BUILD_WITH_PWM YES
    #define PWM_BUILD_WITH_TEXT_QUERY YES
    #define TIMER_USED_BY_SYSTEM 0

#define BUILD_WITH_ADC YES
    #define ADC_BUILD_WITH_TEXT_QUERY YES
//-----------------------------------------

#define BUILD_WITH_DS18X20 YES
    #define OW_ROMCODE_SIZE 8
    #define DS18_MAXSENSORS_ON_BUS 10
    #define DS18X20_MAX_RESOLUTION 1
    #define DS18X20_BUILD_WITH_TEXT_QUERY YES


//SPI
#define BUILD_WITH_SPI YES
    #define SPI_ASSERT_MY_FS_IN_TRASMIT_FUNC    NO
    #define SPI_BUILD_WITH_TEXT_QUERY           YES

//SPI<->ENC28
#define BUILD_WITH_ENC28 YES
    #define ENC28_BUILD_WITH_TEXT_QUERY YES

#define BUILD_WITH_ANON_PANEL YES
    #define ANON_PANEL_BUILD_WITH_TEXT_QUERY YES

#define BUILD_WITH_INPUT_SCANNER YES
    #define INPUT_SCANNER_BUILD_WITH_TEXT_QUERY YES

#define __AVR_LIBC_DEPRECATED_ENABLE__

#endif /* BUILD_DEFS_H_ */
