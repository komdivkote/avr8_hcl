#include "../../build_defs.h"
#include <avr/io.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include "../../hal.h"
#include "./hal_net_stack.h"
#include "./hal_net_stack_func.h"

#if WITH_ICMP == YES

//ToDo complete this section
// process ICMP packet
void
icmp_filter(struct knetdevice * knd,
			eth_frame_t *frame,
			uint16_t len)
{
	ip_packet_t *packet = (void*)frame->data;
	icmp_echo_packet_t *icmp = (void*)packet->data;

	if(len >= sizeof(icmp_echo_packet_t) )
	{
		if(icmp->type == ICMP_TYPE_ECHO_RQ)
		{
			icmp->type = ICMP_TYPE_ECHO_RPLY;
			icmp->cksum += 8; // update cksum
			ip_reply(knd, frame, len);
		}
	}

	return;
}

#endif
