#ifndef DEV_INPSCAN_H_INCLUDED
#define DEV_INPSCAN_H_INCLUDED

#define FOREACH_INPSCAN_CMD(DV)\
    DV(INPSCAN_CMD_LIST_SCANS, (uint8_t) 0)\
	DV(INPSCAN_CMD_MESSAGE, (uint8_t) 1)\
	DV(INPSCAN_CMD_OPER_NOT_SUP, (uint8_t) 0xFF)


enum inpscan_cmds
{
	FOREACH_INPSCAN_CMD(GENERATE_ENUM_VAL)
};

struct proto_inpscan_list_scans
{
    t_dev_id io_did;
    uint8_t scanmask;
    uint8_t readmask;
    uint8_t next[];
};

struct proto_inpscan_message
{
    t_dev_id io_did;
    uint8_t changedmask;
};

#endif // DEV_INPSCAN_H_INCLUDED
