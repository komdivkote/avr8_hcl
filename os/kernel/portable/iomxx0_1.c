/*-
 *	Copyright (c) 2014-2016	Alexander Morozov. www.nixd.org
 *	All rights reserved.
 *
 * LICENSE: This source file is subject to the New BSD license that is
 * available through the world-wide-web at the following URI:
 * http://www.opensource.org/licenses/bsd-license.php or in the root folder
 * of the project in file LICENSE.TXT.
 */

#include <avr/io.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include "../kernel_process_private.h"
#include "kiomxx0_1.h"
#include "shared.h"



ISR(INT0_vect, ISR_NAKED)
{
    DEFAULT_ISR_CODE_SECTION(INT0_vect_num)
}

ISR(INT1_vect, ISR_NAKED)
{
	DEFAULT_ISR_CODE_SECTION(INT1_vect_num)
}

ISR(INT2_vect, ISR_NAKED)
{
	DEFAULT_ISR_CODE_SECTION(INT2_vect_num)
}

ISR(INT3_vect, ISR_NAKED)
{
	DEFAULT_ISR_CODE_SECTION(INT3_vect_num)
}

ISR(INT4_vect, ISR_NAKED)
{
	DEFAULT_ISR_CODE_SECTION(INT4_vect_num)
}

ISR(INT5_vect, ISR_NAKED)
{
	DEFAULT_ISR_CODE_SECTION(INT5_vect_num)
}

ISR(INT6_vect, ISR_NAKED)
{
	DEFAULT_ISR_CODE_SECTION(INT6_vect_num)
}

ISR(INT7_vect, ISR_NAKED)
{
	DEFAULT_ISR_CODE_SECTION(INT7_vect_num)
}

ISR(PCINT0_vect, ISR_NAKED)
{
	DEFAULT_ISR_CODE_SECTION(PCINT0_vect_num)
}

ISR(PCINT1_vect, ISR_NAKED)
{
	DEFAULT_ISR_CODE_SECTION(PCINT1_vect_num)
}

#if defined(__ATmegaxx0__)
ISR(PCINT2_vect, ISR_NAKED)
{
	DEFAULT_ISR_CODE_SECTION(INT0_vect_num)
}
#endif /* __ATmegaxx0__ */

/*ISR(WDT_vect, ISR_NAKED)
{

}*/

ISR(TIMER2_COMPA_vect, ISR_NAKED)
{
	DEFAULT_ISR_CODE_SECTION(TIMER2_COMPA_vect_num)
}

ISR(TIMER2_COMPB_vect, ISR_NAKED)
{
	//save context
	DEFAULT_ISR_CODE_SECTION(TIMER2_COMPB_vect_num)
}

ISR(TIMER2_OVF_vect, ISR_NAKED)
{
	DEFAULT_ISR_CODE_SECTION(TIMER2_OVF_vect_num)
}

#if (KERNEL_MEASURE_PERF_ISR == NO)

ISR(TIMER1_CAPT_vect, ISR_NAKED)
{
	DEFAULT_ISR_CODE_SECTION(TIMER1_CAPT_vect_num)
}

ISR(TIMER1_COMPA_vect, ISR_NAKED)
{
	DEFAULT_ISR_CODE_SECTION(TIMER1_COMPA_vect_num)
}

ISR(TIMER1_COMPB_vect, ISR_NAKED)
{
	DEFAULT_ISR_CODE_SECTION(TIMER1_COMPB_vect_num)
}

//---

ISR(TIMER1_COMPC_vect, ISR_NAKED)
{
	DEFAULT_ISR_CODE_SECTION(TIMER1_COMPC_vect_num)
}

ISR(TIMER1_OVF_vect, ISR_NAKED)
{
	DEFAULT_ISR_CODE_SECTION(TIMER1_OVF_vect_num)
}
#endif

ISR(SPI_STC_vect, ISR_NAKED)
{
	DEFAULT_ISR_CODE_SECTION(SPI_STC_vect_num)
}

ISR(USART0_RX_vect, ISR_NAKED)
{
	DEFAULT_ISR_CODE_SECTION(USART0_RX_vect_num)
}

ISR(USART0_UDRE_vect, ISR_NAKED)
{
	DEFAULT_ISR_CODE_SECTION(USART0_UDRE_vect_num)
}

ISR(USART0_TX_vect, ISR_NAKED)
{
	DEFAULT_ISR_CODE_SECTION(USART0_TX_vect_num)
}

//---

ISR(ANALOG_COMP_vect, ISR_NAKED)
{
	DEFAULT_ISR_CODE_SECTION(ANALOG_COMP_vect_num)
}

ISR(ADC_vect, ISR_NAKED)
{
	DEFAULT_ISR_CODE_SECTION(ADC_vect_num)
}

ISR(EE_READY_vect, ISR_NAKED)
{
	DEFAULT_ISR_CODE_SECTION(EE_READY_vect_num)
}

ISR(TIMER3_CAPT_vect, ISR_NAKED)
{
	DEFAULT_ISR_CODE_SECTION(TIMER3_CAPT_vect_num)
}

ISR(TIMER3_COMPA_vect, ISR_NAKED)
{
	DEFAULT_ISR_CODE_SECTION(TIMER3_COMPA_vect_num)
}

ISR(TIMER3_COMPB_vect, ISR_NAKED)
{
	DEFAULT_ISR_CODE_SECTION(TIMER3_COMPB_vect_num)
}

//---

ISR(TIMER3_COMPC_vect, ISR_NAKED)
{
	DEFAULT_ISR_CODE_SECTION(TIMER3_COMPC_vect_num)
}

ISR(TIMER3_OVF_vect, ISR_NAKED)
{
	DEFAULT_ISR_CODE_SECTION(TIMER3_OVF_vect_num)
}

ISR(USART1_RX_vect, ISR_NAKED)
{
	DEFAULT_ISR_CODE_SECTION(USART1_RX_vect_num)
}

ISR(USART1_UDRE_vect, ISR_NAKED)
{
	DEFAULT_ISR_CODE_SECTION(USART1_UDRE_vect_num)
}

ISR(USART1_TX_vect, ISR_NAKED)
{
	DEFAULT_ISR_CODE_SECTION(USART1_TX_vect_num)
}

ISR(TWI_vect, ISR_NAKED)
{
	DEFAULT_ISR_CODE_SECTION(TWI_vect_num)
}

//---

ISR(SPM_READY_vect, ISR_NAKED)
{
	DEFAULT_ISR_CODE_SECTION(SPM_READY_vect_num)
}

#if defined(__ATmegaxx0__)
ISR(TIMER4_CAPT_vect, ISR_NAKED)
{
	DEFAULT_ISR_CODE_SECTION(INT0_vect_num)
}

#endif /* __ATmegaxx0__ */

ISR(TIMER4_COMPA_vect, ISR_NAKED)
{
	DEFAULT_ISR_CODE_SECTION(TIMER4_COMPA_vect_num)
}

ISR(TIMER4_COMPB_vect, ISR_NAKED)
{
	DEFAULT_ISR_CODE_SECTION(TIMER4_COMPB_vect_num)
}

ISR(TIMER4_COMPC_vect, ISR_NAKED)
{
	DEFAULT_ISR_CODE_SECTION(TIMER4_COMPC_vect_num)
}

ISR(TIMER4_OVF_vect, ISR_NAKED)
{
	DEFAULT_ISR_CODE_SECTION(TIMER4_OVF_vect_num)
}

//---
#if defined(__ATmegaxx0__)
ISR(TIMER5_CAPT_vect, ISR_NAKED)
{
	DEFAULT_ISR_CODE_SECTION(TIMER5_CAPT_vect_num)
}

#endif /* __ATmegaxx0__ */

ISR(TIMER5_COMPA_vect, ISR_NAKED)
{
	DEFAULT_ISR_CODE_SECTION(TIMER5_COMPA_vect_num)
}

ISR(TIMER5_COMPB_vect, ISR_NAKED)
{
	DEFAULT_ISR_CODE_SECTION(TIMER5_COMPB_vect_num)
}

ISR(TIMER5_COMPC_vect, ISR_NAKED)
{
	DEFAULT_ISR_CODE_SECTION(TIMER5_COMPC_vect_num)
}

ISR(TIMER5_OVF_vect, ISR_NAKED)
{
	DEFAULT_ISR_CODE_SECTION(TIMER5_OVF_vect_num)
}

#if defined(__ATmegaxx1__)


#else

ISR(USART2_RX_vect, ISR_NAKED)
{
	DEFAULT_ISR_CODE_SECTION(USART2_RX_vect_num)
}

//---

ISR(USART2_UDRE_vect, ISR_NAKED)
{
	DEFAULT_ISR_CODE_SECTION(USART2_UDRE_vect_num)
}

ISR(USART2_TX_vect, ISR_NAKED)
{
	DEFAULT_ISR_CODE_SECTION(USART2_TX_vect_num)
}

ISR(USART3_RX_vect, ISR_NAKED)
{
	DEFAULT_ISR_CODE_SECTION(USART3_RX_vect_num)
}

ISR(USART3_UDRE_vect, ISR_NAKED)
{
	DEFAULT_ISR_CODE_SECTION(USART3_UDRE_vect_num)
}

ISR(USART3_TX_vect, ISR_NAKED)
{
	DEFAULT_ISR_CODE_SECTION(USART3_TX_vect_num)
}

# define _VECTORS_SIZE 228

#endif /* __ATmegaxx1__ */




