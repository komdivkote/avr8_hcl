/*-
 *	Copyright (c) 2014-2015	Alexander Morozov. NiXD ORG
 *	All rights reserved.
 *
 * LICENSE: This source file is subject to the New BSD license that is
 * available through the world-wide-web at the following URI:
 * http://www.opensource.org/licenses/bsd-license.php or in the root folder
 * of the project in file LICENSE.TXT.
 */

#include "../../../hal/build_defs.h"
#include <avr/io.h>
#include <avr/pgmspace.h>
#include <stdlib.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include "../../../hal/hal.h"
#include "../../../hal/drivers/spi/spi.h"
#include "../../../hal/handlers.h"
#include "../virtual_terminal.h"
#include "../../../lib/conio/conio.h"
#include "../../../lib/conio/kbd.h"
#include "../../../lib/conio/ui.h"
#include "../vt_text.h"
#include "vt_attach.h"

FOREACH_SPI_DEV(GENERATE_VARS_VAL);
const struct menudialog_entry vt_spi_dev_struct[] PROGMEM =
{
	FOREACH_SPI_DEV(GENERATE_VAR_STRUCT)
	{
		P_NULL, NULL
	}
};

FOREACH_SPI_BIT_ORDER(GENERATE_VARS_VAL);
const struct menudialog_entry vt_spi_bit_ord_captions_struct[] PROGMEM = {
	FOREACH_SPI_BIT_ORDER(GENERATE_VAR_STRUCT)
	{
		P_NULL, NULL
	}
};

FOREACH_SPI_MODE(GENERATE_VARS_VAL);
const struct menudialog_entry vt_spi_mode_struct[] PROGMEM = {
	FOREACH_SPI_MODE(GENERATE_VAR_STRUCT)
	{
		P_NULL, NULL
	}
};

FOREACH_SPI_CLOCK(GENERATE_VARS_VAL);
const struct menudialog_entry vt_spi_clock_struct[] PROGMEM = {
	FOREACH_SPI_CLOCK(GENERATE_VAR_STRUCT)
	{
		P_NULL, NULL
	}
};

FOREACH_SPI_DATA_MODE(GENERATE_VARS_VAL);
const struct menudialog_entry vt_spi_data_mode_struct[] PROGMEM = {
	FOREACH_SPI_DATA_MODE(GENERATE_VAR_STRUCT)
	{
		P_NULL, NULL
	}
};

/*const struct menudialog_entry vt_spi_options_captions_struct[] PROGMEM = {
	{
	    add_slave, VT_INFO_OPTS_ADD
	},
	{
        remove_slave, VT_INFO_OPTS_REMOVE
	},
	{
		P_NULL, NULL
	}
};*/

/*const char * const vt_spi_data_mode[] PROGMEM = {
	FOREACH_SPI_DATA_MODE(GENERATE_VAR_NAMES_VAL)
	P_NULL
};*/

static int8_t __ui_query(FILE * file, struct spi_setup * spiset);
//static int8_t __ui_query_info(FILE * file, uint8_t * opt);

/*static int8_t
__ui_query_info(FILE * file, uint8_t * opt)
{
    int ret = 0;
	int res = 0;
	ret = runmenu0_s(file, 10, 10, 0, 10, vt_spi_options_captions_struct, &res);
	//try to create fd for the device
	if (ret == UI_NOTHING)
	{
		return F_FAIL;
	}
	*opt = (uint8_t) res;

	return F_OK;
}*/

static int8_t __ui_query(FILE * file, struct spi_setup * spiset)
{
	int ret = 0;
	int res = 0;

    ret = runmenu0_s(file, 10, 10, 0, 10, vt_spi_dev_struct, &res);
	//try to create fd for the device
	if (ret == UI_NOTHING)
	{
		return F_FAIL;
	}
    spiset->e_dev = (uint8_t) res;

	ret = runmenu0_s(file, 10, 10, 0, 10, vt_spi_bit_ord_captions_struct, &res);
	//try to create fd for the device
	if (ret == UI_NOTHING)
	{
		return F_FAIL;
	}
	spiset->e_bit_order = (uint8_t) res;

	ret = runmenu0_s(file, 12, 10, 0, 10, vt_spi_mode_struct, &res);
	//try to create fd for the device
	if (ret == UI_NOTHING)
	{
		return F_FAIL;
	}
	spiset->e_mode = (uint8_t) res;

	ret = runmenu0_s(file, 16, 10, 0, 10, vt_spi_clock_struct, &res);
	//try to create fd for the device
	if (ret == UI_NOTHING)
	{
		return F_FAIL;
	}
	spiset->e_clock = (uint8_t) res;

	ret = runmenu0_s(file, 21, 10, 0, 10, vt_spi_data_mode_struct, &res);
	//try to create fd for the device
	if (ret == UI_NOTHING)
	{
		return F_FAIL;
	}
	spiset->e_data_mode = (uint8_t) res; // TODO fix conversion

	return F_OK;
}

int8_t vt_attach_spi(FILE * file, struct spi_setup * spiset)
{
	int8_t res = 0;

	res = __ui_query(file, spiset);
	ASSERT_DATA(res != F_OK, return F_FAIL);


	return F_OK;
}

int8_t vt_modify_spi(FILE * file, struct dev_descr_node * dd)
{

	/*int8_t res = 0;
	struct spi_setup spiset;
	uint8_t sp_size = 1;

	res = __ui_query(&spiset);
	ASSERT_DATA(res != F_OK, return F_FAIL);

	res = kernel_fd_update(fd->dev_fd, &sp_env, &sp_size);

	ASSERT_DATA(res == 1, return F_FAIL);
*/
	return F_OK;
}

int8_t vt_info_spi(FILE * file, struct dev_descr_node * dd)
{
	//print information summery
    /*uint8_t opt = 0;
    int8_t res = 0;

    res = __ui_query_info(file, &opt);
	ASSERT_DATA(res != F_OK, return F_FAIL);

    switch (opt)
    {

        case VT_INFO_OPTS_ADD:
        {


        }
        break;

        case VT_INFO_OPTS_REMOVE:


        break;
    }*/

	return F_OK;
}
